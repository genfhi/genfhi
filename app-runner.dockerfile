FROM clojure:tools-deps as clojure
RUN apt-get update
RUN apt-get --assume-yes install curl
ENV NODE_VERSION=16.7.0
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
SHELL ["/bin/bash", "-c"]


COPY . /gen_fhi
WORKDIR /gen_fhi/gen_fhi
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN ["clj", "-T:build", "app-uber"]

FROM openjdk:22-slim-bullseye
EXPOSE 8080
COPY --from=clojure /gen_fhi/gen_fhi/target/app-backend-standalone.jar ./app-backend-standalone.jar
CMD java -jar app-backend-standalone.jar app-backend