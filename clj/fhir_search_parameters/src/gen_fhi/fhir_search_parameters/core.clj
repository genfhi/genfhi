(ns gen-fhi.fhir-search-parameters.core
  (:gen-class)
  (:require [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [clojure.pprint :as pprint]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.java.io :as io]
            [clojure.data.json :as json]

            [gen-fhi.fhirpath.core :as fp]))

(defn- process-expression [resource-type expression]
  (let [resource-starts-with (str (name resource-type) ".")]
    (->> (string/split expression #"\|")
         (map string/trim)
         (filter
          (fn [expr-chunk]
            (and
             (not (string/includes? expr-chunk " as "))
             (not (string/includes? expr-chunk " is "))
             (not (string/includes? expr-chunk "resolve()"))
             (string/starts-with?
              expr-chunk
              resource-starts-with))))
         (map (fn [exp-chunk] (clojure.string/replace exp-chunk resource-starts-with "$this.")))
         (string/join " | "))))


(defn- ->search-parameter-map [search-parameter-bundle]
  (let [search-parameters (fp/evaluate "$this.entry.resource" search-parameter-bundle)]
    (reduce
     (fn [param-hash search-parameter]
       (if-let [expression (get search-parameter :expression)]
        (reduce
         (fn [param-hash base]
           (let [processed-expression (process-expression base expression)]
             (if (empty? processed-expression)
               (do
                 ;;(println base ":Failed:" expression)
                param-hash)
               (assoc-in param-hash [(keyword base) (get search-parameter :code)]
                         processed-expression))))
         param-hash
         (get search-parameter :base []))
        param-hash))
     {}
     search-parameters)))


(defn generate-search [namespace input-file output-file]
  (let [search-parameter-bundle (json/read-str (slurp (io/file input-file)) :key-fn keyword)
        search-map              (->search-parameter-map search-parameter-bundle)]
    (spit
     (io/file output-file)
     (str "(ns " namespace "\n  \"Generated vai gen-fhi.fhir-search-parameters.core.\")\n\n"
          "(def search-parameters \n"
          (with-out-str (pprint/pprint search-map))
          "\n)"))))

(def cli-options
  [["-i" "--input-file INPUT" "Input file"]
   ["-n" "--name-space NAMESPACE" "Namespace"]
   ["-o" "--output-file OUTPUT" "Output file"]])

(defn -main [& args]
  (let [parsed-args (parse-opts args cli-options)]
    (match (:arguments parsed-args)
           ["generate-search"] (generate-search
                                (get-in parsed-args [:options :name-space])
                                (get-in parsed-args [:options :input-file])
                                (get-in parsed-args [:options :output-file])))))

