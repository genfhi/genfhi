(ns gen-fhi.fhir-server.core
  (:require [clojure.string :as string]
            [clojure.spec.alpha :as spec]
            [clojure.core.match :refer [match]]
            [clojure.spec.test.alpha :as stest]
            [taoensso.timbre :as log]
            [clojure.core.async :as a]

            [async-errors.core :as ae]
            [gen-fhi.operations.client-injector :as client-injector]
            [gen-fhi.fhir-specs.generate :refer [*terminology-provider*]]
            [gen-fhi.fhir-terminology.protocols :as terminology]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.fhir-client.protocols :as client]
            [gen-fhi.interceptors.core :as interceptors]))

;; Enabling assertion failures.

(defn dynamic-spec-import
  []
  "Import Dynamically to avoid AOT comp failures ..."
  (use 'gen-fhi.generated.r4.specs))

(spec/check-asserts true)

(def response-request-types #{:fhir/create
                              :fhir/update
                              :fhir/patch
                              :fhir/delete

                              :fhir/capabilities
                              :fhir/search
                              :fhir/read
                              :fhir/vread
                              :fhir/history

                              :fhir/invoke
                              :fhir/batch
                              :fhir/transaction})

(spec/def :fhir/request-type response-request-types)
(spec/def :fhir/level #{:fhir/system :fhir/type :fhir/instance})
(spec/def :fhir/data any?)

(spec/def :fhir-parameter/modifier  #{:text :missing})
(spec/def :fhir-parameter/value any?)
(spec/def :fhir-parameter/parameter (spec/keys :req-un [:fhir-parameter/value] :opt-un [:fhir-parameter/modifier]))
(spec/def :fhir/parameters (spec/map-of keyword? :fhir-parameter/parameter))
(spec/def :fhir/workspace string?)
(spec/def :fhir/author    string?)

(spec/def :fhir/type string?)
(spec/def :fhir/id string?)
(spec/def :fhir/op keyword?)
(spec/def :fhir/vid string?)
(spec/def :fhir/if-match string?)

(spec/def :fhir/request
  (spec/keys :req    [:fhir/request-type :fhir/level :fhir/workspace]
             :req-un [:fhir/author]
             :opt    [:fhir/if-match
                      :fhir/parameters
                      :fhir/data
                      :fhir/type
                      :fhir/id
                      :fhir/parameters
                      :fhir/op]))

(spec/def :fhir/response-type response-request-types)
(spec/def :fhir/bundle map?)
(spec/def :fhir/response
  (spec/keys :req [:fhir/response-type :fhir/level]
             :opt [:fhir/bundle]))

(defn- bind-terminology [terminology-provider func]
  (fn [& args]
    (binding [*terminology-provider*
              (fn [valueset strength code]
                (if (and (some? valueset) (= strength "required"))
                  (terminology/validate
                   terminology-provider
                   {:url valueset :code code})
                  (string? code)))]
      (apply func args))))

(defn- r-system [& {:as fhir-req}]
  (assoc fhir-req :fhir/level :fhir/system))
(defn- r-type [& {:as fhir-req}]
  (assoc fhir-req :fhir/level :fhir/type))
(defn- r-instance [& {:as fhir-req}]
  (assoc fhir-req :fhir/level :fhir/instance))

;; System level
;; -----------------------------------------------------------------------------------------------
;; search-system
;; history-system   GET    [base]/_history{?[parameters]&_format=[mime-type]}
;; capabilities     GET    [base]/metadata{?mode=[mode]} {&_format=[mime-type]}

;; Type
;; -----------------------------------------------------------------------------------------------
;; create           POST   [base]/[type] {?_format=[mime-type]}
;; search           GET    [base]/[type]{?[parameters]{&_format=[mime-type]}}
;;                  POST   [base]/[type]/_search{?[parameters]{&_format=[mime-type]}}
;; history-type     GET    [base]/[type]/_history{?[parameters]&_format=[mime-type]}

;; Instance
;; -----------------------------------------------------------------------------------------------
;; read             GET    [base]/[type]/[id] {?_format=[mime-type]}
;; update           PUT    [base]/[type]/[id] {?_format=[mime-type]}
;; delete           DELETE [base]/[type]/[id]
;; patch            PATCH  [base]/[type]/[id] {?_format=[mime-type]}
;; history-instance GET    [base]/[type]/[id]/_history{?[parameters]&_format=[mime-type]}
;; vread            GET    [base]/[type]/[id]/_history/[vid] {?_format=[mime-type]}

(defn- is-operation? [op-str]
  (string/starts-with? op-str "$"))

(defn- ring-params->fhir-params
  "Given ring params produce fhir-params which holds info on modifiers."
  [params]
  (into
   {}
   (map
    (fn [[ring-param-name param-value]]
      (let [[param-name modifier] (clojure.string/split (name ring-param-name) #":")]
        [(keyword param-name)
         (let [param-val {:value    param-value}]
           (if (some? modifier)
             (assoc param-val :modifier (keyword modifier))
             param-val))]))
    params)))

(def ^:private if-match-regex #"W/\"(.*)\"")

(defn- assoc-constraints [ring-req fhir-req]
  (if-let [if-match (get-in ring-req [:headers "if-match"])]
    (assoc fhir-req :fhir/if-match (second (re-find if-match-regex if-match)))
    fhir-req))

(defn- ring-req->fhir-req
  "Parses a given request into the correct fhir type request."
  ([req]
   (ring-req->fhir-req req (fn [_req fhir-req]
                             fhir-req)))
  ([req post-process]
   {:pre (spec/valid? req map?)
    :post [(spec/valid? :fhir/request %)]}
   (let [fhir-req
         (->
          (match [(:request-method req) (string/split (get-in req [:path-params :fhir-location] "") #"\/")]
              ;; System
            [:get    [""]]                                (r-system   :fhir/request-type :fhir/search                                        :fhir/parameters (ring-params->fhir-params
                                                                                                                                                               (:params req)))
              ;; [TODO]  Support various bundle types
            [:post   [""]]                                (r-system   :fhir/request-type :fhir/transaction                                   :fhir/data       (:body-params req))
            [:get    ["_history"]]                        (r-system   :fhir/request-type :fhir/history)
            [:get    ["metadata"]]                        (r-system   :fhir/request-type :fhir/capabilities)
            [:post   [(op :guard is-operation?)]]         (r-system   :fhir/request-type :fhir/invoke                                        :fhir/op  (keyword (subs op 1)) :fhir/data (:body-params req))
              ;; Type
            [:get    [type]]                              (r-type     :fhir/request-type :fhir/search           :fhir/type type              :fhir/parameters (ring-params->fhir-params
                                                                                                                                                               (:params req)))
              ;; -- Search supports additional format via post
              ;; [:post   [type "_search"]]                    (r-type     :fhir/request-type :fhir/search           :fhir/type type)
            [:post   [type]]                              (r-type     :fhir/request-type :fhir/create           :fhir/type type              :fhir/data (:body-params req))
            [:get    [type "_history"]]                   (r-type     :fhir/request-type :fhir/history          :fhir/type type)
            [:post   [type (op :guard is-operation?)]]    (r-type     :fhir/request-type :fhir/invoke           :fhir/type type              :fhir/op  (keyword (subs op 1)) :fhir/data (:body-params req))
              ;; Instance
            [:get    [type id]]                           (r-instance :fhir/request-type :fhir/read             :fhir/type type :fhir/id id)
            [:put    [type id]]                           (r-instance :fhir/request-type :fhir/update           :fhir/type type :fhir/id id  :fhir/data (:body-params req))
            [:delete [type id]]                           (r-instance :fhir/request-type :fhir/delete           :fhir/type type :fhir/id id)
            [:patch  [type id]]                           (r-instance :fhir/request-type :fhir/patch            :fhir/type type :fhir/id id  :fhir/data (:body-params req))
            [:get    [type id "_history"]]                (r-instance :fhir/request-type :fhir/history          :fhir/type type :fhir/id id)
            [:get    [type id "_history" vid]]            (r-instance :fhir/request-type :fhir/vread            :fhir/type type :fhir/id id  :fhir/vid vid)
            [:post   [type id (op :guard is-operation?)]] (r-instance :fhir/request-type :fhir/invoke           :fhir/type type :fhir/id id  :fhir/op  (keyword (subs op 1)) :fhir/data (:body-params req))
            :else                                         (oo/throw-outcome
                                                           (oo/outcome-error
                                                            "invalid"
                                                            (str
                                                             "Query '"
                                                             (-> req :path-params :fhir-location)
                                                             "' is invalid"))))

          (assoc :author (-> req :claims :sub))
          ((partial assoc-constraints req))
          ((partial post-process req)))]
     fhir-req)))

(defn- resources->bundle [bundle-type coll-resources]
  {:resourceType "Bundle"
   :type         bundle-type
   :entry        (map (fn [resource] {:resource resource}) coll-resources)})

(defn- fhir-res->ring-response [fhir-response]
  (match fhir-response
    {:fhir/response-type :fhir/capabilities
     :fhir/response      response}       (if response {:status 200 :body response} {:status 400})
    {:fhir/response-type :fhir/transaction
     :fhir/response bundle}              {:status 200 :body bundle}
    {:fhir/response-type :fhir/vread
     :fhir/response      response}       (if response {:status 200 :body response} {:status 400})
    {:fhir/response-type :fhir/history
     :fhir/response       response}      {:status 200 :body (resources->bundle "history" response)}
    {:fhir/response-type :fhir/search
     :fhir/response       response}      {:status 200 :body (resources->bundle "searchset" response)}
    {:fhir/level         :fhir/instance
     :fhir/response-type :fhir/read
     :fhir/response      response}       (if response {:status 200 :body response} {:status 404})
    {:fhir/level         :fhir/type
     :fhir/response-type :fhir/create
     :fhir/response      response}       (if response {:status 201 :body response} {:status 400})
    {:fhir/level         :fhir/instance
     :fhir/response-type :fhir/update
     :fhir/response      response}       (if response {:status 200 :body response} {:status 400})
    {:fhir/response-type :fhir/patch
     :fhir/response      response}       (if response {:status 200 :body response} {:status 400})
    {:fhir/response-type :fhir/invoke
     :fhir/response      response}       (if response {:status 200 :body response} {:status 400})
    {:fhir/response-type :fhir/delete
     :fhir/response
     {:success success}}                 (if success {:status 204} {:status 400})
    {:fhir/response-type :fhir/invoke
     :fhir/response
     {:success success}}                 (if success {:status 204} {:status 400})

    :else
    (oo/throw-outcome
     (oo/outcome-error
      "not-supported"
      (str
       "Processing fhir response "
       (:fhir/response fhir-response)
       " not supported")))))

(spec/fdef fhir-req->fhir-res
  :args (spec/cat :ctx any? :req :fhir/request)
  :ret :fhir/response)

(defn- fhir-req->fhir-res [{:keys [fhir-db op-handler capabilities custom-resource-handlers] :as ctx}  fhir-req]
  (if-let [resource-handler (get-in custom-resource-handlers [(keyword (get fhir-req :fhir/type)) (get fhir-req :fhir/request-type)])]
    (resource-handler ctx fhir-req)
    (match fhir-req
      {:fhir/request-type
       :fhir/capabilities}                (r-instance
                                           :fhir/response-type :fhir/capabilities
                                           :fhir/response capabilities)

      {:fhir/request-type :fhir/invoke}   (op-handler ctx fhir-req)

      {:fhir/request-type
       :fhir/transaction
       :fhir/data bundle}                 (r-system
                                           :fhir/response-type :fhir/transaction
                                           :fhir/response
                                           (client/transaction fhir-db bundle))

      {:fhir/request-type :fhir/vread
       :fhir/type          resource-type
       :fhir/id            id
       :fhir/vid           vid}           (r-instance
                                           :fhir/response-type :fhir/vread
                                           :fhir/response
                                           (client/vread fhir-db resource-type id vid))

      {:fhir/request-type :fhir/history
       :fhir/type         resource-type
       :fhir/id           id}             (r-instance
                                           :fhir/response-type :fhir/history
                                           :fhir/response
                                           (client/history fhir-db resource-type id))

      {:fhir/request-type :fhir/read
       :fhir/type         resource-type
       :fhir/id           id}             (r-instance
                                           :fhir/response-type :fhir/read
                                           :fhir/response
                                           (client/read fhir-db resource-type id))
      {:fhir/level        :fhir/type
       :fhir/request-type :fhir/search
       :fhir/parameters   parameters
       :fhir/type         resource-type}  (r-type
                                           :fhir/response-type :fhir/search
                                           :fhir/response
                                           (client/search fhir-db resource-type parameters))
      {:fhir/level        :fhir/system
       :fhir/request-type :fhir/search
       :fhir/parameters   parameters}     (r-system
                                           :fhir/response-type :fhir/search
                                           :fhir/response
                                           (client/search fhir-db parameters))
      {:fhir/request-type :fhir/create
       :fhir/type         _resource-type
       :fhir/data         resource}       (r-type
                                           :fhir/response-type :fhir/create
                                           :fhir/response
                                           (client/create fhir-db resource))
      {:fhir/request-type :fhir/update
       :fhir/type         resource-type
       :fhir/id           id
       :fhir/data         resource}       (r-instance
                                           :fhir/response-type :fhir/update
                                           :fhir/response
                                           (client/update fhir-db resource-type id resource))

      {:fhir/request-type :fhir/delete
       :fhir/type         resource-type
       :fhir/id           id}             (r-instance
                                           :fhir/response-type :fhir/delete
                                           :fhir/response
                                           {:success
                                            (client/delete fhir-db resource-type id)})

      {:fhir/request-type :fhir/patch
       :fhir/type         resource-type
       :fhir/id           id
       :fhir/if-match     version-constraint
       :fhir/data         patch}          (r-instance
                                           :fhir/response-type :fhir/patch
                                           :fhir/response
                                           (client/patch fhir-db resource-type id patch {:fhir/if-match version-constraint}))

      :else
      (oo/throw-outcome
       (oo/outcome-error
        "not-supported"
        (str "Request " (:fhir/request-type fhir-req) " not supported."))))))

(spec/def :json-patch/op #{"add" "remove" "replace" "copy" "move" "test"})
(spec/def :json-patch/patch (spec/keys :req-un [:json-patch/op]))
(spec/def :json-patch/patches (spec/coll-of :json-patch/patch))

(defn- validate-fhir-req [fhir-req]
  (match fhir-req
         ;; Don't require validation of data.
    {:fhir/request-type :fhir/capabilities}      (do)
    {:fhir/request-type :fhir/delete}            (do)
    {:fhir/request-type :fhir/history}           (do)
    {:fhir/request-type :fhir/read}              (do)
    {:fhir/request-type :fhir/vread}             (do)
    {:fhir/request-type :fhir/search}            (do)

    {:fhir/request-type :fhir/invoke
     :fhir/data         parameters}              (oo/outcome-validation :fhir-types/Parameters parameters)

    {:fhir/request-type :fhir/patch
     :fhir/data         data}                    (oo/outcome-validation :json-patch/patches data)

    {:fhir/request-type :fhir/create
     :fhir/type resource-type
     :fhir/data data}                            (oo/outcome-validation (keyword "fhir-types" resource-type) data)

    {:fhir/request-type :fhir/transaction
     :fhir/data         data}                    (oo/outcome-validation :fhir-types/Bundle data)

    {:fhir/request-type :fhir/update
     :fhir/data         data
     :fhir/type         resource-type}           (oo/outcome-validation (keyword "fhir-types" resource-type) data)

    :else
    (oo/throw-outcome
     (oo/outcome-error
      "not-found"
      (str "Could not validate request " (:fhir/request-type fhir-req)))))

  fhir-req)

(defn- is-system-interaction? [capability-statement interaction]
  (let [interaction (if (keyword? interaction) (name interaction) interaction)]
    (first
     (fp/evaluate
      "$this.rest.where(mode='server').interaction.where(code=%interaction).exists()"
      capability-statement
      {:options/variables
       {:interaction interaction}}))))

(defn- is-resource-interaction? [capability-statement resource-type interaction]
  (let [interaction (if (keyword? interaction) (name interaction) interaction)]
    (first
     (fp/evaluate
      "$this.rest.where(mode='server').resource.where(type=%resourceType).interaction.where(code=%interaction).exists()"
      capability-statement
      {:options/variables
       {:resourceType resource-type
        :interaction  interaction}}))))

(defn- is-in-capability-statement?
  "Enforces CapabilityStatement to see if request is allowed."
  [capability-statement fhir-request]
  (match fhir-request
    ;; Always allow capabilities to be queried.
    {:fhir/request-type
     :fhir/capabilities}             true
    {:fhir/level :fhir/system
     :fhir/request-type interaction} (if (= :fhir/invoke interaction)
                                       true
                                       (is-system-interaction?
                                        capability-statement
                                        interaction))

    {:fhir/type resource-type
     :fhir/request-type interaction}  (if (= :fhir/invoke interaction)
                                        true
                                        (is-resource-interaction?
                                         capability-statement
                                         resource-type
                                         interaction))))

(spec/def :create/custom-resource-handlers
  (spec/map-of keyword? (spec/map-of response-request-types fn?)))

(spec/def :create/arg (spec/keys
                       :opt-un [:create/custom-resource-handlers]))

(spec/fdef create
  :args (spec/cat :arg :create/arg))

(defn- ->workspace-client
  "Creates a workspace client for ops dependent on ops"
  [workspace author meta-data {:keys [terminology-provider] :as ctx}]
  (let [interceptor
        (interceptors/->interceptor
         [{:exit  (interceptors/alt-value
                   (fn [res]
                     (ae/go-try res)))}
          {:error (fn [e]
                    (log/error e))
           :enter (interceptors/alt-value
                   (fn [fhir-req]
                     (merge
                      meta-data
                      (assoc
                       fhir-req
                       :fhir/workspace workspace
                       :author author))))}
          {:enter (interceptors/alt-value
                   (bind-terminology terminology-provider validate-fhir-req))}
          {:exit (interceptors/alt-value
                  (bind-terminology
                   terminology-provider
                   (fn [req]
                     (fhir-req->fhir-res
                      (assoc
                       ctx
                       :fhir/client
                       (->workspace-client
                        workspace
                        author
                        meta-data
                        ctx))
                      req))))}])]
    (reify client/Client
      (request     [this request] (interceptor request))
      (metadata    [this]
        (interceptor (r-system :fhir/request-type :fhir/capabilities)))

      (invoke      [this op parameters]
        (client-injector/op-processing interceptor
                                       (r-system
                                        :fhir/request-type :fhir/invoke
                                        :fhir/op           op
                                        :fhir/data         parameters)))
      (invoke      [this op type parameters]
        (client-injector/op-processing interceptor
                                       (r-type
                                        :fhir/request-type :fhir/invoke
                                        :fhir/op           op
                                        :fhir/type         type
                                        :fhir/data         parameters)))
      (invoke      [this op type id parameters]
        (client-injector/op-processing interceptor
                                       (r-instance
                                        :fhir/request-type :fhir/invoke
                                        :fhir/op           op
                                        :fhir/type         type
                                        :fhir/id           id
                                        :fhir/data         parameters)))
      (search [this parameters]
        (interceptor (r-system
                      :fhir/request-type :fhir/search
                      :fhir/parameters   parameters)))

      (search [this resource-type parameters]
        (interceptor (r-type
                      :fhir/request-type :fhir/search
                      :fhir/type         resource-type
                      :fhir/parameters   parameters)))

      (create [this resource]
        (interceptor (r-type
                      :fhir/request-type :fhir/create
                      :fhir/type         (:resourceType resource)
                      :fhir/data         resource)))

      (batch [this bundle]
        (throw (ex-message "Not Implemented")))

      (transaction [this bundle]
        (throw (ex-message "Not Implemented")))

      (history [this]
        (throw (ex-message "Not Implemented")))

      (history [this type]
        (throw (ex-message "Not Implemented")))

      (history [this resource-type id]
        (throw (ex-message "Not Implemented")))

      (read [this resource-type id]
        (interceptor (r-instance
                      :fhir/request-type :fhir/read
                      :fhir/type resource-type
                      :fhir/id id)))

      (update [this type id resource]
        (interceptor {:fhir/request-type :fhir/update
                      :fhir/level        :fhir/instance
                      :fhir/type          type
                      :fhir/id            id
                      :fhir/data         resource}))

      (delete [this type id]
        (interceptor {:fhir/request-type :fhir/delete
                      :fhir/level        :fhir/instance
                      :fhir/type         type
                      :fhir/id           id}))

      (patch [this type id patches]
        (throw (ex-message "Not Implemented")))

      (vread       [this resource-type id vid]
        (throw (ex-message "Not Implemented"))))))

;; Defines the context for instantiated services.
;; Such as Encryption and the database access.
(defrecord CTX [custom-resource-handlers
                fhir-db
                op-handler
                capabilities
                encryption-provider
                terminology-provider])

(defn- log-fhir-request [fhir-request]
  ;; only print subset of the keys
  (log/info "REQUEST:"
            (select-keys
             fhir-request
             [:fhir/level :fhir/type :fhir/request-type :fhir/op])))

(defn ->fhir-interceptor [{:keys [capabilities
                                  post-process-fhir-req
                                  fhir-req->ctx]}]

  (interceptors/->interceptor
   [{:exit (fn [res]
             (ae/go-try res))}

    {:enter (interceptors/alt-value
             (fn [req]
               (if post-process-fhir-req
                 (ring-req->fhir-req req post-process-fhir-req)
                 (ring-req->fhir-req req))))

     :exit (interceptors/alt-value
            fhir-res->ring-response)}

    {:enter (interceptors/alt-value
             (fn [fhir-request]
               (log-fhir-request fhir-request)
               (if (is-in-capability-statement? capabilities fhir-request)
                 fhir-request
                 (oo/throw-outcome
                  (oo/outcome-error
                   "not-supported"
                   (str
                    (name (:fhir/request-type fhir-request))
                    " not supported in capabilites."))))))}

    {:enter (interceptors/alt-value
             (fn [fhir-request]
               [(fhir-req->ctx fhir-request) fhir-request]))}

    {:enter (interceptors/alt-value
             (fn [[{:keys [terminology-provider] :as ctx} fhir-request]]
               (let [validator (bind-terminology
                                terminology-provider
                                (validate-fhir-req fhir-request))]
                 (validator fhir-request))
               [ctx fhir-request]))}

    {:exit (interceptors/alt-value
            (fn [[{:keys [terminology-provider] :as ctx} fhir-request]]
              (let [->response (bind-terminology
                                terminology-provider
                                (fn [fhir-req]
                                  (fhir-req->fhir-res
                                   (assoc ctx :fhir/client (->workspace-client
                                                            (get fhir-req :fhir/workspace)
                                                            (get fhir-req :author)
                                                            (select-keys
                                                             fhir-req
                                                             (filter
                                                              #(= "meta" (namespace %))
                                                              (keys fhir-req)))
                                                            ctx))
                                   fhir-req)))]
                (->response fhir-request))))}]))

(defn- error->operation-outcome [exception]
  (let [outcome (ex-data exception)]
    (log/error "exception:returned" exception)
    {:status (match (get-in outcome [:issue 0 :code])
               "conflict" 409
               :else 400)
     :body (if (= "OperationOutcome" (:resourceType outcome))
             outcome
             (oo/outcome-error
              "exception"
              "An error occured processing request."))}))

(defn create [params]
  (log/info "[INITIATED] FHIR Server")
  (let [fhir-interceptor (->fhir-interceptor params)]
    (fn
      ([ring-req]
       (try
         (ae/<?? (fhir-interceptor ring-req))
         (catch Exception ex
           (error->operation-outcome ex))))
      ([ring-req respond _raise]
       (try
         (let [return (fhir-interceptor ring-req)]
           (a/take!
            return
            (fn [response]
              (if (instance? Throwable response)
                (respond (error->operation-outcome response))
                (respond response)))))
         (catch Exception ex
           (respond (error->operation-outcome ex))))))))

(stest/instrument `create)
