(ns gen-fhi.fhir-terminology.core
  (:require [clojure.java.io :as io]
            [gen-fhi.fhir-terminology.providers.memory :refer [file-paths->memory-terminology-provider]]))

(defn  hl7-fhir-terminology []
  (file-paths->memory-terminology-provider (io/resource "fhir_hl7_terminology")))
