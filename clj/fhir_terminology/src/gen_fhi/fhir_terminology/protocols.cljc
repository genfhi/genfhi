(ns gen-fhi.fhir-terminology.protocols)


(defprotocol FHIRTerminologyProvider
  (validate [this parameter])
  (expand        [this parameter]))
