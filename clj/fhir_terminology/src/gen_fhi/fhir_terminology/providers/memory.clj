(ns gen-fhi.fhir-terminology.providers.memory
  (:require
   [clojure.string :as string]
   [gen-fhi.fhir-client.protocols :as client]

   [gen-fhi.fhir-terminology.utilities :as utilities]
   [gen-fhi.fhir-terminology.protocols :refer [FHIRTerminologyProvider expand]]))

(defn- codesystem-concepts->valueset-contains [concepts]
  (map
   (fn [concept]
     ;; [TODO] did not define :abstract + :inactive fields.
     (cond-> {:code (:code concept)} ;; Required
       (some? (:extension   concept)) (assoc :extension   (:extension   concept))
       (some? (:url         concept)) (assoc :url         (:url         concept))
       (some? (:version     concept)) (assoc :version     (:version     concept))
       (some? (:display     concept)) (assoc :display     (:display     concept))
       (some? (:designation concept)) (assoc :designation (:designation concept))
       (some? (:concept    concept))  (assoc :contains    (codesystem-concepts->valueset-contains
                                                           (:concept    concept)))
       ;; Uncertain if should assume abstract if there are nested concepts?
       (some? (:concept    concept))  (assoc :abstract true)))
       ;;:inactive    ???
   concepts))

(defn- codesystem->valueset-expansion
  "Transform concepts to entries in valueset expansion"
  [codesystem]
  (codesystem-concepts->valueset-contains (get codesystem :concept [])))

(defn codesystem-uri->codesystem [source codesystem-uri]
  (first (client/search
          source
          :CodeSystem
          {:url codesystem-uri
           :_count 1})))

(defn- inline-codeset->valueset-expansion [inline-codeset]
  (map
   (fn [concept]
     (cond-> {:system (:system inline-codeset)
              :code (:code concept)}
       (some? (:display concept))     (assoc :display (:display concept))
       (some? (:designation concept)) (assoc :designation (:designation concept))))
   (get inline-codeset :concept [])))

(defn- codes-inline? [include]
  (not-empty (get include :concept)))

(defn- valueset-expansion [term-provider valueset]
  (mapcat
   (fn [include]
     (let [codes (if (codes-inline? include)
                   (inline-codeset->valueset-expansion include)
                   (if-let [valuesets (:valueSet include)]
                     (mapcat
                      #(get-in (expand term-provider {:url %})
                               [:expansion :contains])
                      valuesets)
                     (codesystem->valueset-expansion
                      (codesystem-uri->codesystem
                       (:source term-provider)
                       (get include :system)))))]
       codes))

   (get-in valueset [:compose :include] [])))

(defrecord MemoryTerminologyProvider [source]
  FHIRTerminologyProvider
  (expand        [this params]
    (let [valueset-canonical               (params :url)
          ;; TODO need to validate the version as well.
          [valueset-uri _valueset-version] (string/split valueset-canonical #"\|")
          valueset                         (first (client/search
                                                   (:source this)
                                                   :ValueSet
                                                   {:url    valueset-uri
                                                    :_count 1}))]

      (when valueset
        (-> valueset
            (assoc-in [:expansion :timestamp] (.format (java.text.SimpleDateFormat. "yyyy-MM-dd") (java.util.Date.)))
            (assoc-in [:expansion :contains]  (valueset-expansion this valueset))))))

  (validate [this {:keys [code url]}]
    (let [code-exists? (utilities/expand-to-set this url)]
      (some? (code-exists? code)))))

