(ns gen-fhi.fhir-terminology.utilities
  (:require [gen-fhi.fhir-terminology.protocols :refer [expand]]))

(defn- flatten-codes [expansion-entry]
  (let [expansion-entry-contains (mapcat flatten-codes (get expansion-entry :contains []))]
    (conj  expansion-entry-contains (:code expansion-entry))))

(defn valueset->set-codes
  "Given a terminology provider fully expand valueset-uri and flatten codes into a set."
  [expanded-vs]
  (into
   #{}
   (comp
    (mapcat flatten-codes))
   (get-in expanded-vs [:expansion :contains] [])))

(defn expand-to-set
  "Given a terminology provider fully expand valueset-uri and flatten codes into a set."
  [terminology-provider valueset-url]
  (let [expanded-vs (expand terminology-provider {:url valueset-url})]
    (valueset->set-codes expanded-vs)))
