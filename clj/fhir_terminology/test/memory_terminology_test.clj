(ns memory-terminology-test
  (:require  [clojure.test :as t]
             [gen-fhi.fhir-terminology.protocols :refer [validate]]
             [gen-fhi.fhir-terminology.utilities :refer [expand-to-set]]
             [gen-fhi.resource-providers.filesystem.core :refer [file-paths->memory-terminology-provider]]
             [gen-fhi.fhir-terminology.providers.memory :refer [->MemoryTerminologyProvider]]))

(defn- ->term-provider []
  (->MemoryTerminologyProvider
                      (file-paths->memory-terminology-provider
                       "resources/fhir_hl7_terminology")))
  

(t/deftest terminology.memory.expansion
  (let [mem-provider (->term-provider)]
    (t/is
     (= (expand-to-set mem-provider "http://hl7.org/fhir/ValueSet/publication-status")
        #{"draft" "unknown" "active" "retired"}))))

(t/deftest terminology.memory.validate
  (let [mem-provider (->term-provider)]
    (t/is
     (= (validate mem-provider {:url "http://hl7.org/fhir/ValueSet/publication-status" :code "draft"})
        true))))
