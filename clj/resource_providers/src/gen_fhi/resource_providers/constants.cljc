(ns gen-fhi.resource-providers.constants)

(def file-system  "file-system")
(def postgres     "postgres")
(def multi-source "mult-source")
(def s3 "s3")
