(ns gen-fhi.resource-providers.filesystem.core
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [taoensso.timbre :as timbre]

            [gen-fhi.resource-providers.util :refer [param-value->param-value-map]]
            [gen-fhi.fhir-client.protocols :refer [Client] :as fhir-client]))
            

(defn- resource-type->resources [resource-map resource-type]
  (vals (get resource-map resource-type)))

(defn- ->all-resources [resource-map]
  (mapcat vals (vals resource-map)))

(defn- string-search [parameter value]
  (try
    (if-let [param-value (get parameter :value)]
      (match (get parameter :modifier)
        :text (string/includes?    value param-value)
        nil   (string/starts-with? value param-value))
      false)
    (catch Exception _e
      (timbre/error "Failed to perform search string search" parameter value))))

(def ^:private modifier-keys [:_count])

(defn- process-parameter [params]
  {:modifiers (select-keys params modifier-keys)
   :filters   (apply dissoc params modifier-keys)})

(defn- search-fn [result [param-key param-value]]
  (let [param-value (param-value->param-value-map param-value)]
    (match param-key
      :name   (filter #(string-search param-value (get % :name "")) result)
      :url    (filter #(string-search param-value (get % :url  "")) result)
      :else   (throw (ex-message (str "Parameter not supported: '" (name key) "'"))))))

(defn- apply-modifiers [result modifiers]
  (reduce
   (fn [result [modifier value]]
     (match modifier
       :_count (take value result)
       :else   (throw (ex-message (str "Parameter not supported: '" (name key) "'")))))
   result
   modifiers))

;; Resource map resource-type -> id -> resource
(defrecord MemoryClientProvider [resource-map]
  Client
  (request    [this request]
    (throw (ex-message "Not Implemented")))
  (metadata    [this]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op parameters]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op type parameters]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op type id parameters]
    (throw (ex-message "Not Implemented")))

  (search [{:keys [resource-map]} parameters]
    (let [parameters (process-parameter parameters)]
      (->
       (reduce search-fn
               (->all-resources resource-map)
               (:filters parameters))
       (apply-modifiers (:modifiers parameters)))))

  (search [{:keys [resource-map]} resource-type parameters]
    (let [parameters (process-parameter parameters)]
      (-> (reduce search-fn
                  (resource-type->resources resource-map (keyword resource-type))
                  (:filters parameters))
          (apply-modifiers (:modifiers parameters)))))

  (create [this resource]
    (throw (ex-message "Not Implemented")))

  (batch [this bundle]
    (throw (ex-message "Not Implemented")))

  (transaction [this bundle]
    (throw (ex-message "Not Implemented")))

  (history [this]
    (throw (ex-message "Not Implemented")))

  (history [this type]
    (throw (ex-message "Not Implemented")))

  (history [this resource-type id]
    (throw (ex-message "Not Implemented")))

  (read [{:keys [resource-map]} resource-type id]
    (get-in resource-map [(keyword resource-type) id]))

  (update [this type id resource]
    (throw (ex-message "Not Implemented")))

  (update [this type id resource constraints]
    (throw (ex-message "Not Implemented")))

  (delete [this type id]
    (throw (ex-message "Not Implemented")))

  (patch [this type id patches]
    (throw (ex-message "Not Implemented")))

  (patch [this type id patches constraints]
    (throw (ex-message "Not Implemented")))

  (vread [this resource-type id vid]
    (throw (ex-message "Not Implemented"))))

(defn- bundle->coll-resources [bundle]
  (sequence
   (comp
    (map    (fn [entry] (:resource entry)))
    ;; Not going to deal with nested Bundles
    (filter (fn [resource] (not= "Bundle" (:resourceType resource)))))
   (get bundle :entry [])))

(defn- resource->coll-resources [resource]
  (let [resource-type (:resourceType resource)]
    (cond
      (= "Bundle" resource-type)              (bundle->coll-resources resource)
      :else                                   [resource])))

(defn- resource-path->coll-resources [resource-path]
  (if (string/ends-with? resource-path ".json")
    (resource->coll-resources
     (json/read-str
      (slurp (io/resource resource-path))
      :key-fn keyword))
    []))

(defn- file-path->resources
  "Given a file directory return a sequence of resources."
  [file-path]
  (sequence
   (comp
    (filter (fn [file] (not (.isDirectory file))))
    (filter (fn [file] (string/ends-with? (str file) ".json")))
    (map    slurp)
    (map    (fn [file-contents] (json/read-str file-contents :key-fn keyword)))
    (mapcat (fn [resource] (resource->coll-resources resource))))
   (file-seq
    (io/file file-path))))

(defn- resources->resource-map [resources]
  (reduce
   (fn [map resource]
     (let [resource (if (nil? (:id resource))
                      (assoc resource :id (str (java.util.UUID/randomUUID)))
                      resource)]
       (assoc-in
        map
        [(keyword (:resourceType resource))
         (:id resource)]
        resource)))
   {}
   resources))

(defn file-paths->memory-terminology-provider [& file-paths]
  (let [file-paths (if (coll? file-paths) file-paths [file-paths])
        resources  (mapcat file-path->resources file-paths)
        resource-map (resources->resource-map resources)]
    (MemoryClientProvider. resource-map)))

(defn resource-paths->memory-client-provider [resource-paths]
  (let [resource-paths (if (coll? resource-paths) resource-paths [resource-paths])
        resources      (mapcat resource-path->coll-resources resource-paths)
        resource-map   (resources->resource-map resources)]
    (MemoryClientProvider. resource-map)))

(defn bundle->memory-client-provider [bundle]
  (let [resources      (bundle->coll-resources bundle)
        resource-map   (resources->resource-map resources)]
    (MemoryClientProvider. resource-map)))
