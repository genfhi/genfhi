(ns gen-fhi.resource-providers.multi-source.core
  (:require [clojure.spec.alpha :as s]
            [gen-fhi.fhir-client.protocols :refer [Client] :as fhir-client]))

(defrecord MultiSourceProvider [sources]
  Client
  (request    [this request]
    (throw (ex-message "Not Implemented")))
  (metadata    [this]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op parameters]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op type parameters]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op type id parameters]
    (throw (ex-message "Not Implemented")))

  (search [{:keys [sources]} parameters]
    (loop [sources-to-run sources
           results []]
      ;; If we have count parameter and our search meets criteria
      ;; shortcircuit and skip the search on other sources.
      ;; This improves speed namely on terminology queries during validation
      (if (and (get parameters :_count) (>= (count results)
                                            (get parameters :_count)))
        (take (get parameters :_count) results)
        (if (empty? sources-to-run)
          results
          (recur
           (rest sources-to-run)
           (concat
            results
            (fhir-client/search
             (:source (first sources-to-run))
             (dissoc parameters :_count))))))))

  (search [{:keys [sources]} resource-type parameters]
    (loop [sources-to-run sources
           results []]
      ;; If we have count parameter and our search meets criteria
      ;; shortcircuit and skip the search on other sources.
      ;; This improves speed namely on terminology queries during validation
      (if (and (get parameters :_count) (>= (count results)
                                            (get parameters :_count)))
        (take (get parameters :_count) results)
        (if (empty? sources-to-run)
          results
          (recur
           (rest sources-to-run)
           (concat
            results
            (fhir-client/search
             (:source (first sources-to-run))
             resource-type
             (dissoc parameters :_count))))))))

  (create [this resource]
    (throw (ex-message "Not Implemented")))

  (batch [this bundle]
    (throw (ex-message "Not Implemented")))

  (transaction [this bundle]
    (throw (ex-message "Not Implemented")))

  (history [this]
    (throw (ex-message "Not Implemented")))

  (history [this type]
    (throw (ex-message "Not Implemented")))

  (history [this resource-type id]
    (throw (ex-message "Not Implemented")))

  (read [{:keys [resource-map]} resource-type id]
    (get-in resource-map [resource-type id]))

  (update [this type id resource]
    (throw (ex-message "Not Implemented")))

  (update [this type id resource constraints]
    (throw (ex-message "Not Implemented")))

  (delete [this type id]
    (throw (ex-message "Not Implemented")))

  (patch [this type id patches]
    (throw (ex-message "Not Implemented")))

  (patch [this type id patches constraints]
    (throw (ex-message "Not Implemented")))

  (vread [this resource-type id vid]
    (throw (ex-message "Not Implemented"))))

(s/def ::id keyword?)
(s/def ::source any?)
(s/def ::source-element
  (s/keys :req-un [::id ::source]))
(s/def ::sources (s/coll-of ::source-element))
(s/fdef ->MultiSource
  :args (s/cat :s ::sources))

(defn multi-source->source
  "Given mutli-source returns a specific source by id."
  [multi-source id]
  (first
   (filter
    (fn [source] (= id (get source :id)))
    (get multi-source :source []))))
