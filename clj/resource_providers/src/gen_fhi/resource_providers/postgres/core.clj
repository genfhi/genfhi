(ns gen-fhi.resource-providers.postgres.core
  (:require [next.jdbc :as jdbc]
            [next.jdbc.transaction :refer [*nested-tx*]]
            [hugsql.core :as hugsql]
            [hugsql.adapter.next-jdbc :as next-adapter]
            [clj-json-patch.core :as patch]
            [clojure.core.match :refer [match]]
            [clojure.string :as string]
            [honey.sql :as hsql]
            [honey.sql.helpers :refer [where]]
            [loom.graph :as graph]
            [loom.alg :as alg]

            [gen-fhi.resource-providers.transactions :as bundle-transaction]
            [gen-fhi.resource-providers.util :refer [param-value->param-value-map]]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.resource-providers.postgres.extensions]
            [gen-fhi.resource-providers.ws :refer [WSDataHandler get-patches-after]]
            [gen-fhi.fhir-client.protocols :refer [Client] :as fdb]
            [gen-fhi.fhirpath.core :as fp]))

(hugsql/set-adapter! (next-adapter/hugsql-adapter-next-jdbc))
(hsql/register-op! :->>)
(hsql/register-op! (keyword "@@"))
(hugsql/def-db-fns "gen_fhi/resource_providers/postgres/sql/resources.sql")
(hugsql/def-db-fns "gen_fhi/resource_providers/postgres/sql/channel.sql")
(hugsql/def-sqlvec-fns "gen_fhi/resource_providers/postgres/sql/resources.sql")
(hugsql/def-sqlvec-fns "gen_fhi/resource_providers/postgres/sql/channel.sql")

(defn- string-search-clause
  "If modifier for text then see if it contains the text as opposed to starts with"
  [modifier value]
  (match modifier
    :text (str "%" value "%")
    nil  (str value "%")))

(defn- param->resource-path
  "Converts param to a json path access on postgres."
  [param]
  [:->> :resource (keyword (str "'" (name param) "'"))])

(defn- parameters->clauses [parameters]
  (when (not-empty parameters)
    (into
     [:and]
     (map
      (fn [[param-key param-info]]
        (let [param-info (param-value->param-value-map param-info)
              modifier (get param-info :modifier)]
          (into
           [:or]
           (map
            (fn [param-value]
              (match [param-key modifier]
                [param-key :missing] (if (Boolean/valueOf param-value)
                                       [:= (param->resource-path param-key) nil]
                                       [:<> (param->resource-path param-key) nil])
                [(:or :code :url :name :title) _] [:ilike (param->resource-path param-key) (string-search-clause modifier param-value)]
                ;; Pull Client Applications that use a select endpoint
                [:endpoint _]   [(keyword "@@")
                                 :resource
                                 [:cast
                                  (str "$.action[*].endpoint.reference == \"" (str "Endpoint/" param-value) "\"")
                                  :jsonpath]]
                [:_id _]        [:=    :id                                        param-value]
                [:_type _]      [:=    :resource_type                             param-value]))
            (string/split (str (get param-info :value)) #",")))))
      parameters))))

(defrecord PGFHIRDatabase [capabilities workspace author source]
  WSDataHandler
  (get-patches-after [{:keys [source] :as this}]
    (jdbc/with-transaction [tx source]
      (let [res (get-latest-version-id tx {:workspace workspace})]
        (get-patches-after this (- (get res :version_id 1) 1)))))

  (get-patches-after [{:keys [source]} version-id]
    (let [version-id (if (string? version-id) (Integer/parseInt version-id) version-id)]
      (get-patches-and-prev-version source {:workspace  workspace
                                            :version-id version-id
                                            :limit      10})))

  Client
  (request    [this request]
    (throw (ex-message "Not Implemented")))
  (metadata    [this]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op parameters]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op type parameters]
    (throw (ex-message "Not Implemented")))
  (invoke      [this op type id parameters]
    (throw (ex-message "Not Implemented")))

  (search [{:keys [source]} parameters]
    (let [search-types (fp/evaluate "$this.rest.resource.where($this.interaction.where(code='search').exists()).type" capabilities)
          res (search-system source {:workspace workspace
                                     :resource_types search-types
                                     :cols ["resource"]

                                     :where (hsql/format
                                             (where
                                              [:and
                                               (parameters->clauses parameters)
                                               [:= :deleted false]]))})]
      (map :resource res)))

  (search [{:keys [source]} resource-type parameters]
    (let [res (search-type source {:workspace workspace
                                   :resource_type (name resource-type)
                                   :cols ["resource"]
                                   :where (hsql/format
                                           (where
                                            [:and
                                             (parameters->clauses parameters)
                                             [:= :deleted false]]))})]
      (map :resource res)))

  (create [{:keys [source]} resource]
    (:resource
     (create-resource
      source
      {:workspace workspace
       :author author
       ;; Remove id if it's there on create this will be generated
       :resource (dissoc resource :id)})))

  (batch [this bundle]
    (throw (ex-message "Not Implemented")))

  (transaction [{:keys [source] :as this} bundle]
    (assert
     (= (:type bundle) "transaction")
     (str "Can't process non transactions of type '" (:type bundle) "'"))
    ;; Because can have nested transactions in operations called too.
    ;; Going to absorb them into the outermost transaction here.
    (binding [*nested-tx* :ignore]
      (jdbc/with-transaction [tx source]
        (let [dependency-graph  (bundle-transaction/->dependency-graph bundle)
              indices-topsorted (alg/topsort dependency-graph)
              ;; Use the transaction in additional requests
              this              (assoc this :source tx)]
          (assert (some? indices-topsorted) "transaction is circular")
          (let [result (reduce
                        (fn [results idx]
                          (let [dependencies (graph/predecessors dependency-graph idx)
                                request      (update
                                              (nth (get bundle :entry) idx)
                                              :resource
                                              (fn [resource]
                                                (bundle-transaction/update-dependent-references
                                                 resource
                                                 dependencies
                                                 results)))]
                            (assoc
                             results
                             idx
                             {:fullUrl  (get request :fullUrl)
                              :resource
                              (match (get-in request [:request :method])
                                "POST"   (fdb/create
                                          this
                                          (get request :resource))
                                "PUT"    (fdb/update
                                          this
                                          (get-in request [:resource :resourceType])
                                          (get-in request [:resource :id])
                                          (get request :resource))
                                :else (throw (ex-message
                                              (str "Unsupported request type in transaction '"
                                                   (get-in request [:request :type])
                                                   "'"))))})))
                        {}
                        indices-topsorted)]
            {:resourceType "Bundle"
             :type         "transaction-response"
             :entry
             (map
              (fn [info]
                {:response {:status "200"
                            :location (str
                                       (get-in info [:resource :resourceType])
                                       "/"
                                       (get-in info [:resource :id]))}
                 :resource (get info :resource)})
              (vals result))})))))

  (history [this]
    (throw (ex-message "Not Implemented")))

  (history [this type]
    (throw (ex-message "Not Implemented")))

  (history [{:keys [source]} resource-type id]
    (map
     :resource
     (get-resources-history
      source
      {:workspace workspace
       :resource_type resource-type
       :id id})))

  (read [{:keys [source]} resource-type id]
    (let [db-response (get-latest-resource
                       source
                       {:workspace     workspace
                        :id            id
                        :resource_type resource-type})]
      (:resource db-response)))

  (update [this type id resource]
    (fdb/update this type id resource {}))

  (update [this type id resource constraints]
    (fdb/patch this type id [{:op "replace" :path "" :value resource}] constraints))

  (delete [{:keys [source] :as this} type id]
    (jdbc/with-transaction [tx source {:isolation :serializable}]
      (let [resource (fdb/read (assoc this :source tx) type id)]
        (delete-resource
         tx
         {:workspace       workspace
          :author          author
          :resource        resource
          :prev_version_id (Integer/parseInt (-> resource :meta :versionId))
          :patches         []})
        true)))

  (patch [this type id patches]
    (fdb/patch this type id patches {}))

  (patch [{:keys [source] :as this} type id patches constraints]
    (try
      (jdbc/with-transaction [tx source {:isolation :serializable}]
        (let [resource         (fdb/read (assoc this :source tx) type id)]
          (if (and
               (some? (get constraints :fhir/if-match))
               (not= (get constraints :fhir/if-match)
                     (get-in resource [:meta :versionId])))
            (throw (ex-info
                    "Version mismatch"
                    {:found    (get-in resource [:meta :versionId])
                     :expected (get constraints :fhir/if-match)}))
            (let [updated-resource (patch/patch resource patches true)]
              (oo/outcome-validation (keyword "fhir-types" type) updated-resource)
              (assert (= (:id updated-resource) id) (str "Ids mismatch on update '" id "'!='" (:id updated-resource) "'"))
              (:resource
               (update-resource
                tx
                {:workspace       workspace
                 :author          author
                 :resource        updated-resource
                 :prev_version_id (Integer/parseInt (-> resource :meta :versionId))
                 :patches         patches}))))))
      (catch org.postgresql.util.PSQLException exception
        (let [sql-state (.getSQLState exception)]
          (match sql-state
            ;; Transaction rollback codes defined in
            ;;[https://www.postgresql.org/docs/current/errcodes-appendix.html]
            (:or "40000" "40002" "40001") (oo/throw-outcome
                                           (oo/outcome-error
                                            "conflict"
                                            "conflict occured during patch"))
            :else                         (oo/throw-outcome
                                           (oo/outcome-error
                                            "unknown"
                                            "An Error occured during update")))))
      (catch Exception exception
        (println exception)
        (let [message (ex-message exception)
              data    (ex-data exception)]
          (match message
            "Version mismatch" (oo/throw-outcome
                                (oo/outcome-error
                                 "conflict"
                                 (str "Mismatched version ids '" (get data :expected) "' != '" (get data :found) "'")))
            :else              (throw exception))))))

  (vread [{:keys [source]} resource-type id vid]
    (try
      (let [version-id (Integer/parseInt vid)]
        (:resource
         (get-versioned-resource
          source
          {:workspace workspace
           :resource_type resource-type
           :id id
           :version_id version-id})))
      (catch Exception ex
        nil))))


