-- :name get-patches-and-prev-version :? :*
SELECT id, resource_type, patches, version_id, prev_version_id, resource -> 'meta' as meta from resources
WHERE version_id > :version-id
AND   workspace  = :workspace
AND   prev_version_id IS NOT NULL
ORDER BY version_id ASC
LIMIT :sql:limit;

-- :name get-latest-version-id  :? :1
-- :doc  Get the latest version id
SELECT version_id from resources
WHERE workspace = :workspace
ORDER BY version_id DESC
LIMIT 1;
