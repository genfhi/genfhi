-- Postgres resource sql statements 

-- :snip cond-snip
-- {:conj "and" :cond ["id" "=" 1]}
:sql:conj :i:cond.0 :sql:cond.1 :v:cond.2

-- :snip where-snip
( :snip*:cond )


-- :name search-system :? :*
-- :doc Retrieve all resources for given workspace.
SELECT :i*:cols FROM (
 SELECT DISTINCT ON (id) * FROM resources
 WHERE workspace = :workspace AND resource_type in (:v*:resource_types)
 ORDER BY id, version_id DESC
) as t
:snip:where
ORDER BY t.created_at DESC;

-- :name search-type :? :*
-- :doc Retrieve latest resources for given workspace and type.
SELECT :i*:cols FROM (
  SELECT DISTINCT ON (id) *  FROM resources
  WHERE resource_type = :resource_type
  AND   workspace     = :workspace
  ORDER BY id, version_id DESC
) as t
:snip:where
ORDER BY t.created_at DESC;

-- :name create-resource :<! :1
-- :doc Create a new resource.
INSERT INTO resources (workspace, author, resource)
VALUES (:workspace, :author, :resource) returning resource;

-- :name update-resource :<! :1
-- :doc Update a resource (creates a new resource in table with patches that caused the update).
INSERT INTO resources (workspace, author, resource, patches, prev_version_id)
VALUES (:workspace, :author, :resource, :patches, :prev_version_id) returning resource;

-- :name get-latest-resource :? :1
-- :doc Retrieve latest resource by workspace and id.
SELECT * FROM (
 SELECT resource, deleted FROM resources
 WHERE id            = :id
 AND   resource_type = :resource_type
 AND   workspace     = :workspace
 ORDER BY version_id DESC
 LIMIT 1) as t
WHERE t.deleted = false;

-- :name get-versioned-resource :? :1
-- :doc Retrieve resource at version.
SELECT resource FROM resources
WHERE id            = :id
AND   resource_type = :resource_type
AND   workspace     = :workspace
AND   version_id    = :version_id;





-- :name get-resources-history :? :*
-- :doc Retrieve latest resources for given workspace and type.
SELECT resource FROM resources
WHERE id            = :id
AND   resource_type = :resource_type
AND   workspace     = :workspace
ORDER BY version_id DESC;


-- :name delete-resource :<! :!
-- :doc Sets resource as deleted (does not actually remove from db).
INSERT INTO resources (workspace, author, resource, patches, prev_version_id, deleted)
VALUES (:workspace, :author, :resource, :patches, :prev_version_id, true) returning resource;
