(ns gen-fhi.resource-providers.transactions
  (:require [gen-fhi.fhirpath.core :as fp]
            [loom.graph :as graph]
            [loom.alg :as alg]))

(defn- find-url-idx [transaction url]
  (let [result (->> (get transaction :entry)
                    (map-indexed (fn [i v] [i v]))
                    (filter (fn [[_i request]] (= url (get request :fullUrl))))
                    (first))]
    (when (some? result)
      (first result))))

(defn ->dependency-graph [transaction]
  (let [dependencies (->> (map-indexed (fn [i v] [i v]) (:entry transaction))
                          (reduce
                           (fn [acc [index request]]
                             (let [dep-local-idxes (->>
                                                    (fp/evaluate
                                                     "$this.descendants().reference"
                                                     (get request :resource))
                                                    (map #(find-url-idx transaction %))
                                                    (filter some?))]
                               (concat
                                acc
                                ;; Node iteslf
                                [index]
                                ;; Any dependencies
                                (map
                                 (fn [dep-idx]
                                   [dep-idx index])
                                 dep-local-idxes))))
                           []))]

    (apply graph/digraph dependencies)))

(defn update-dependent-references [resource dependencies results]
  (reduce
   (fn [resource dep-info]
     (let [dep-url             (get dep-info :fullUrl)
           dep                 (get dep-info :resource)
           dep-reference       (str (get dep :resourceType) "/" (get dep :id))
           locations-to-update (fp/locations
                                "$this.descendants().where(reference=%url)"
                                resource
                                {:options/variables {:url dep-url}})]
       (reduce
        (fn [resource location]
          (assoc-in
           resource
           (concat location [:reference])
           dep-reference))
        resource
        locations-to-update)))
   resource
   (map #(get results %) dependencies)))

