(ns gen-fhi.resource-providers.util)

(defn param-value->param-value-map
  "Check if map for param value if not place raw value in map"
  [param-value]
  (if (map? param-value)
    param-value
    {:value param-value}))
