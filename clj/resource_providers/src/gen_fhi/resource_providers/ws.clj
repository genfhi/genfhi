(ns gen-fhi.resource-providers.ws)

(defprotocol WSDataHandler
  (get-patches-after
    [this]
    [this version-id]))
