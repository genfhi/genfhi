(ns error-test
  (:require  [clojure.test :as t]
             [async-errors.core :as ae]))



(t/deftest test-success-and-failure
  (t/is
   5
   (ae/<?? (ae/go-try 5)))
  
  (t/is
   (thrown?
    Exception
    (ae/<??
         (ae/go-try
          (throw (ex-info "test" {})))))))

(t/deftest test-nested-failures
  (let [safe (ae/go-try 5)
        c    (ae/go-try (throw (ex-info "Test" {})))]
    (t/is
     5
     (ae/<??
      (ae/go-try
       (let [res (ae/<? safe)]
         res))))
    
    (t/is
     (thrown?
      Exception
      (ae/<??
       (ae/go-try
        (let [res (ae/<? c)]
         5)))))))
