(ns gen-fhi.fhir-client.protocols
  (:refer-clojure :exclude [read update]))

(defprotocol Client
  (request     [this request])

  (metadata    [this])

  (invoke
    [this op parameters]
    [this op type parameters]
    [this op type id parameters])

  (search
    [this parameters]
    [this type parameters])

  (create      [this resource])

  (batch       [this bundle])

  (transaction [this bundle])

  (history
    [this]
    [this type]
    [this type id])
  (read        [this type id])

  (update
    [this type id resource]
    [this type id resource constraints])

  (delete      [this type id])

  (patch
    [this type id patches]
    [this type id patches constraints])

  (vread       [this type id vid]))
