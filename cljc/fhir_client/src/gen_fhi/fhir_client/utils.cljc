(ns gen-fhi.fhir-client.utils
  "Client utilities used on server and client for remote http requests"
  #?(:clj
     (:import [java.nio.charset StandardCharsets]
              [java.net URLEncoder]))
  (:require [clojure.string :as string]
            [clojure.core.match :refer [match]]
            #?(:clj [clojure.data.json :as json])))

(defn- ->encode [url-piece]
  #?(:clj  (URLEncoder/encode url-piece (.toString StandardCharsets/UTF_8))
     :cljs (js/encodeURI url-piece)))

(defn- parameters-string
  "Convers parameter map into query string"
  [parameter-map]
  (if (not-empty parameter-map)
    (let [param-string
          (reduce
           (fn [param-string [key val]]
             (let [val (if (or (seq? val) (list? val) (vector? val))
                         (string/join "," val)
                         (str val))]
               (str param-string (->encode (name key)) "=" (->encode (name val)) "&")))
           "?"
           parameter-map)]
      (subs param-string 0 (- (count param-string) 1)))
    ""))

(defn- url-root [api-root fhir-req]
  ;; Remove trailing /
  (let [api-root (if (string/ends-with? api-root "/")
                   (subs api-root 0 (- (count api-root) 1))
                   api-root)]

    (match fhir-req
      {:fhir/level :fhir/system}  (str api-root)
      {:fhir/level :fhir/type
       :fhir/type  resource-type} (str api-root "/" resource-type)
      {:fhir/level :fhir/instance
       :fhir/type  resource-type
       :fhir/id    id}            (str api-root "/" resource-type "/" id))))

(defn- body-parser [body]
  #?(:cljs (.stringify js/JSON (clj->js body))
     :clj  (json/write-str body)))

(defn- error [message]
  #?(:clj  (ex-message message)
     :cljs (js/Error message)))

(defn oo->fhir-res
  ([operation-outcome]
   (oo->fhir-res operation-outcome nil))
  ([operation-outcome status]
   {:fhir/response-type :fhir/error
    :http/status        (or status 400)
    :fhir/issue         (get operation-outcome :issue)}))

(defn- fhir-req->http-headers [fhir-req]
  (let [meta-keys  (select-keys
                    fhir-req
                    (filter #(and (= "meta" (namespace %))
                                   ;; Remove nils 
                                  (some? (get fhir-req %)))
                            (keys fhir-req)))
        headers (reduce
                 (fn [headers [key value]]
                   (assoc-in
                    headers
                    [(str "genfhi-meta-" (name key))]
                    value))
                 {}
                 meta-keys)
        ;; [See https://build.fhir.org/http.html#versioning]
        ;; version id is weak tag that needs to be enclosed in quotes.
        headers (if-let [version-match (get fhir-req :fhir/if-match)]
                  (assoc headers "If-Match" (str "W/\"" version-match "\""))
                  headers)]
    headers))

(defn fhir-req->http-req
  "Produces a http request for consumption by http lib."
  [api-root fhir-req]
  (let [url (url-root api-root fhir-req)
        http-req (match fhir-req
                   {:fhir/request-type :fhir/transaction
                    :fhir/data         data}           {:method :post
                                                        :url url
                                                        :body (body-parser data)}
                   {:fhir/request-type
                    :fhir/capabilities}                {:method :get
                                                        :url (str url "/metadata")}
                   {:fhir/request-type :fhir/invoke
                    :fhir/data         parameters
                    :fhir/op           op}             {:method :post
                                                        :url (str url "/$" (name op))
                                                        :body (body-parser parameters)}
                   {:fhir/request-type :fhir/patch
                    :fhir/data patches}                {:method :patch
                                                        :url url
                                                        :body (body-parser patches)}
                   {:fhir/request-type :fhir/search}   {:method :get
                                                        :url (str url (parameters-string (get fhir-req :fhir/parameters {})))}
                   {:fhir/request-type :fhir/update
                    :fhir/data         data}           {:method :put
                                                        :url url
                                                        :body  (body-parser data)}
                   {:fhir/request-type :fhir/delete}   {:method :delete
                                                        :url url}
                   {:fhir/request-type :fhir/create
                    :fhir/data         data}           {:method :post
                                                        :url url
                                                        :body (body-parser data)}
                   {:fhir/request-type :fhir/read}     {:method :get
                                                        :url url}

                   :else (throw (error (str "Unhandled fhir request '" (:fhir/request-type fhir-req) "'"))))]

    (update
     http-req
     :headers
     (fn [headers]
       (merge
        headers
        (fhir-req->http-headers
         fhir-req))))))

(defn http-res->fhir-res [fhir-req http-res]
  (if (<= 400 (:status http-res))
    (oo->fhir-res (get http-res :body) (:status http-res))
    (match (:fhir/request-type fhir-req)
      :fhir/history {:fhir/level         (:fhir/level fhir-req)
                     :fhir/response-type (:fhir/request-type fhir-req)
                     :fhir/response      (map :resource (get-in http-res [:body :entry]))}
      :fhir/search  {:fhir/level         (:fhir/level fhir-req)
                     :fhir/response-type (:fhir/request-type fhir-req)
                     :fhir/response      (map :resource (get-in http-res [:body :entry]))
                     :fhir/bundle        (get http-res :body)}
      :fhir/invoke  {:fhir/op            (:fhir/op fhir-req)
                     :fhir/level         (:fhir/level fhir-req)
                     :fhir/response-type (:fhir/request-type fhir-req)
                     :fhir/response      (:body http-res)}
      :else         {:fhir/level         (:fhir/level fhir-req)
                     :fhir/response-type (:fhir/request-type fhir-req)
                     :fhir/response      (:body http-res)})))
