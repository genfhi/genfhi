(ns cli.core
  (:gen-class)
  (:require [clojure.core.match :refer [match]]
            [clojure.tools.cli :refer [parse-opts]]
            [gen-fhi.fhir-specs.macros :refer [write-specs-to-file write-type-sets-to-file]]))

(def cli-options
  [["-i" "--input-file INPUT" "Input file"]
   ["-n" "--name-space NAMESPACE" "Namespace"]
   ["-o" "--output-file OUTPUT" "Output file"]])

(defn -main [& args]
  (let [parsed-args (parse-opts args cli-options)]
    (match (:arguments parsed-args)
      ["generate-sets"]  (eval `(write-type-sets-to-file ~(-> parsed-args :options :name-space) ~(-> parsed-args :options :input-file) ~(-> parsed-args :options :output-file)))
      ["generate-specs"] (eval `(write-specs-to-file ~(-> parsed-args :options :name-space) ~(-> parsed-args :options :input-file) ~(-> parsed-args :options :output-file))))))
