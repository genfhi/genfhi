#!/bin/zsh
clj -M:cli generate-specs -n gen-fhi.generated.r4.specs    -i ../definitions/resources/structure_definitions -o generated/gen_fhi/generated/r4/specs.cljc
clj -M:cli generate-sets -n gen-fhi.generated.r4.type-sets -i ../definitions/resources/structure_definitions -o generated/gen_fhi/generated/r4/type_sets.cljc
