(ns gen-fhi.fhir-specs.generate
  (:require [clojure.core.match :refer [match]]
            [clojure.spec.alpha :as spec]
            [clojure.string :as string]
            [gen-fhi.fhir-specs.traversal :as traversal]
            [gen-fhi.fhirpath.core :as fp]))

;; Used for bound code validations.
(def ^:dynamic *terminology-provider*)

(def ^:private resource-type  "resource")
(def ^:private complex-type   "complex-type")
(def ^:private primitive-type "primitive-type")

;; FHIRPath primitives used for .value .id and extension.url values on SDs (real primitives non fhir based ones).
(def fhir-system-type->predicate
  {"http://hl7.org/fhirpath/System.Boolean"  'boolean?
   "http://hl7.org/fhirpath/System.String"   'string?
   "http://hl7.org/fhirpath/System.Date"     'string?
   "http://hl7.org/fhirpath/System.DateTime" 'string?
   "http://hl7.org/fhirpath/System.Decimal"  'number?
   "http://hl7.org/fhirpath/System.Integer"  'integer?
   "http://hl7.org/fhirpath/System.Time"     'string?})

(def ^:private fhir-type-ns "fhir-types")
(defn- generate-spec-type-code [type]
  (keyword fhir-type-ns type))

(defn- typechoice? [ele]
  (> (count (:type ele)) 1))

(defn- remove-typechoice-x [field]
  (clojure.string/replace (name field) #"\[x\]" ""))

(defn- generate-typechoice-field-name [field type-code]
  (let [field-name      (remove-typechoice-x field)
        camel-case-type (str (Character/toUpperCase (first type-code)) (subs type-code 1))
        field           (str field-name camel-case-type)]
    field))

(defn- path->path-coll [path]
  (rest (string/split path #"\.")))

(defn- is-required? [ele]
  (and (not (typechoice? ele)) (> (:min ele) 0)))

(defn- ele->field-name [ele]
  (last (path->path-coll (:path ele))))

(defn- is-coll? [ele]
  (match (:max ele)
    "*" true
    :else (< 1 (Integer/parseInt (:max ele)))))

(defn- root? [ele]
  (= 0 (count (path->path-coll (:path ele)))))

(defn- should-have-resource-type?
  "Only have resourcetype when resource-type and not abstract."
  [sd ele]
  (let [res
        (and (root? ele)
             (= resource-type (:kind sd))
             (not (:abstract sd)))]
    res))

(defn- generate-spec-keyword [sd ele type]
  (let [root-field     (ele->field-name ele)
        field          (if (typechoice? ele)
                         (generate-typechoice-field-name root-field type)
                         root-field)]
    (if (nil? field)
      (generate-spec-type-code (:id sd))
      (keyword (str (:id sd) "$" (remove-typechoice-x (:id ele)))
               field))))

(defn- generate-single-ele-spec-keyword [spec-keyword]
  (keyword (namespace spec-keyword) (str (name spec-keyword) "__element")))

(defn- sd->resource-type-specname [sd]
  (keyword (:id sd) "resourceType"))

(defn- ->fhir-map-spec
  "Generates a fhir map spec. Adds validation to ensure no extra keys allowed."
  [sd ele children]
  (let [all-children-spec-names    (map #(keyword (name (:spec-keyword %))) children)
        has-resource-type?         (should-have-resource-type? sd ele)
        allowed-fields             (if has-resource-type?
                                     (conj all-children-spec-names :resourceType)
                                     all-children-spec-names)
        children-required-fields   (vec (map :spec-keyword (filter #(:required %) children)))
        required-fields            (if has-resource-type?
                                     (conj children-required-fields
                                           (sd->resource-type-specname sd))
                                     children-required-fields)

        opt-fields                 (vec (map :spec-keyword (filter #(not (:required %)) children)))
        value-symbol               (symbol "value")]
    `(spec/and
      (spec/keys
       :req-un
       ~required-fields
       :opt-un
       ~opt-fields)
      ;; When abstract don't check that it's only the fields in the SD.
      ;; Make an exception for element (so I can varify on _primitive fields
      ~(if (and (not= "Element" (:id sd)) (:abstract sd))
         `(spec/spec (constantly true))
         `(spec/spec (with-meta
                       (fn [~value-symbol]
                         (clojure.set/subset?
                          (set (keys ~value-symbol))
                          ~(set allowed-fields)))
                       {:human (str "Only keys allowed are: '" ~(string/join ", " (map name allowed-fields)) "'")}))))))

(defn- wrap-as-coll? [ele spec-form]
  ;; Note some roots have max: * but I want to treat these as singular values.
  (let [has-many? (and (not (root? ele)) (is-coll? ele))]
    (if has-many?
      `(spec/coll-of
        ~spec-form)
      spec-form)))

(def ^:private regex-extension-url "http://hl7.org/fhir/StructureDefinition/regex")
(defn- wrap-ele-regex
  "Checks for a regex on primitive and wraps it."
  [type spec-form]
  (let [regex-extension (->> (get type :extension [])
                             (filter #(= (:url %) regex-extension-url)))
        value-symbol    (symbol "value")]
    (if (empty? regex-extension)
      spec-form
      `(spec/and
        ~spec-form
        (with-meta
          (fn [~value-symbol] (re-matches (re-pattern ~(:valueString (first regex-extension))) (str ~value-symbol)))
          {:human (str "Value must match regex pattern: '" ~(:valueString (first regex-extension)) "'")})))))

(def is-shortened-code #"(?:[a-zA-Z]*\s\|\s)+(?:[a-zA-Z]*)")

(defn- expand-valueset [ele]
  (let [code-sym (symbol "code")]
    `(spec/spec
      (fn [~code-sym]
        (if (not (instance? clojure.lang.Var$Unbound *terminology-provider*))
          (*terminology-provider* ~(get-in ele [:binding :valueSet])
                                  ~(get-in ele [:binding :strength])
                                  ~code-sym)
          ~(if (and (some? (:short ele))
                    (some? (re-matches is-shortened-code (:short ele))))
             (let [split-codes (into
                                #{}
                                (comp
                                 (map string/trim))
                                (clojure.string/split (:short ele) #"\|"))]
               `(spec/spec ~split-codes))
             'string?))))))

(defn- ele->leaf-spec [ele type]
  (if (= "code" (:code type))
    (expand-valueset ele)
    (wrap-ele-regex type
                    `(spec/and ~(if (fhir-system-type->predicate (:code type))
                                  (fhir-system-type->predicate (:code type))
                                  (generate-spec-type-code (:code type)))))))

;; Custom genfhi unique id check and valueset checks.
;; que-2 to validate unique linkIds
(def supported-constraint-invariants #{"genfhi-unique-id" "csd-1" "que-2"})

(defn- ->constraint-invariants
  "Checks for constraints and creates specs to validate them."
  [ele]
  (let [ctx-sym     (symbol "ctx")
        constraints (filter
                     (fn [constraint] (some? (supported-constraint-invariants (:key constraint))))
                     (get
                      ele
                      :constraint))]
    `(spec/spec
      (spec/and
       ~@(map
          (fn [constraint]
            `(spec/spec
              (with-meta
                (fn [~ctx-sym]
                  (let [~(symbol "return") (first (fp/evaluate ~(:expression constraint) ~ctx-sym))]
                    ~(symbol "return")))
                {:human ~(:human constraint)})))

          constraints)))))

(defn- ele->content-reference-spec
  "Processing for local references IE Bundle.entry.link which references Bundle.link"
  [sd ele]
  (let [spec-keyword           (generate-spec-keyword sd ele nil)
        spec-ele-keyword       (generate-single-ele-spec-keyword spec-keyword)

        content-reference-id   (string/replace (:contentReference ele) #"#" "")
        content-referenced-ele (first (filter
                                       #(= (:id %) content-reference-id)
                                       (get-in sd [:snapshot :element])))
        reference-ele-keyword  (generate-single-ele-spec-keyword (generate-spec-keyword sd content-referenced-ele nil))]
    [{:required     (is-required? ele)
      :spec-keyword spec-keyword
      :spec         `(do
                       (spec/def ~spec-ele-keyword
                         (spec/and
                          ~reference-ele-keyword))
                       (spec/def ~spec-keyword
                         ~(wrap-as-coll? ele reference-ele-keyword)))}]))

(defn- generate-primitive-element-spec-name [sd ele type]
  ;; Reuse the generated spec-name but alter the name to have _ infront.
  (let [primitive-spec-name (generate-spec-keyword sd ele type)]
    (keyword (namespace primitive-spec-name)
             (str "_" (name primitive-spec-name)))))

(defn- primitive-specs
  "Creates the specs for the primitive itself and the element field _fieldname."
  [sd ele type]
  (let [primitive-spec-keyword             (generate-spec-keyword sd ele (:code type))
        primitive-spec-ele-keyword         (generate-single-ele-spec-keyword primitive-spec-keyword)
        primitive-element-spec-keyword     (generate-primitive-element-spec-name sd ele (:code type))
        primitive-element-spec-ele-keyword (generate-single-ele-spec-keyword primitive-element-spec-keyword)]

    ;; Elements must exist on _fields for json serialization
    [{:required     false
      :spec-keyword primitive-element-spec-keyword
      :spec         `(do
                       (spec/def ~primitive-element-spec-ele-keyword
                         (spec/and :fhir-types/Element))
                       (spec/def ~primitive-element-spec-keyword
                         ~(wrap-as-coll?
                           ele
                           primitive-element-spec-ele-keyword)))}

     ;; Actual primitive value
     {:required     (is-required? ele)
      :spec-keyword primitive-spec-keyword
      :spec         `(do
                       (spec/def ~primitive-spec-ele-keyword
                         (spec/and
                          ~(->constraint-invariants ele)
                          ~(ele->leaf-spec ele type)))

                       (spec/def ~primitive-spec-keyword
                         ~(wrap-as-coll?
                           ele
                           primitive-spec-ele-keyword)))}]))

(defn- map-spec [sd ele type children]
  (let [spec-keyword     (generate-spec-keyword sd ele (:code type))
        spec-ele-keyword (generate-single-ele-spec-keyword spec-keyword)]
    [{:required     (is-required? ele)
      :spec-keyword spec-keyword
      :spec         `(do
                       (spec/def ~spec-ele-keyword
                         (spec/and
                          ~(->constraint-invariants ele)
                          ~(->fhir-map-spec sd ele children)))
                       (spec/def ~spec-keyword
                         ~(wrap-as-coll?
                           ele
                           spec-ele-keyword)))}]))

(defn- ->generate-spec-checker [sd ele children]
  (if (:contentReference ele)
    (ele->content-reference-spec sd ele)
    (vec
     (mapcat
      (fn [type]
        (let [spec-keyword     (generate-spec-keyword sd ele (:code type))
              spec-ele-keyword (generate-single-ele-spec-keyword spec-keyword)]
          (if (empty? children)
            (primitive-specs sd ele type)
            (map-spec sd ele type children))))
      (get ele :type [{:code (:type sd)}])))))

(defn- complex+resource-sd->spec [sd]
  (traversal/traversal-bottom-up
   sd
   (fn [ele children]
     (let [accumulated-specs (mapcat :acc-specs children)
           children          (mapcat :child children)
           ele-spec          (->generate-spec-checker sd ele children)]
       {:child ele-spec
        :acc-specs
        (concat
         accumulated-specs
         (if (should-have-resource-type? sd ele)
           [{:required true
             :spec `(spec/def
                      ~(sd->resource-type-specname sd) (fn [~(symbol "resource")] (= ~(symbol "resource") ~(:type sd))))}]
           [])
         ele-spec)}))))

(defn- primitive-sd->spec [sd]
  (let [value-element    (->> (get-in sd [:snapshot :element] [])
                              (filter #(clojure.string/includes? (:path %) "value"))
                              first)
        value-type      (get-in value-element [:type 0])]
    {:acc-specs [{:spec `(spec/def ~(generate-spec-type-code (:id sd))
                           ~(ele->leaf-spec value-element value-type))}]}))

(defn sd->specs
  "Traverse over snapshot elements registering them as specs
  "
  [sd]
  (match (:kind sd)
    "resource"       (complex+resource-sd->spec sd)
    "complex-type"   (complex+resource-sd->spec sd)
    "primitive-type" (primitive-sd->spec        sd)))
