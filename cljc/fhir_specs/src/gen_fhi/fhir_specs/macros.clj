(ns gen-fhi.fhir-specs.macros
  (:require [clojure.data.json :as json]
            [gen-fhi.fhir-specs.generate :refer [sd->specs]]))

(defn- process-bundle [bundle]
  (sequence
   (comp
    (map :resource)
    (filter #(= "StructureDefinition" (:resourceType %))))
   (get bundle :entry [])))

(defn- resource->sds [resource]
  (cond
    (= "Bundle"              (:resourceType resource)) (process-bundle resource)
    (= "StructureDefinition" (:resourceType resource)) [resource]))

(defn- file-path->sds [file-path]
  (sequence
   (comp
    (filter (fn [file] (not (.isDirectory file))))
    (filter (fn [file] (clojure.string/ends-with? (str file) ".json")))
    (map    slurp)
    (map    (fn [file-contents] (json/read-str file-contents :key-fn keyword)))
    (mapcat (fn [resource] (resource->sds resource)))
    (filter (fn [sd] (or (not= "logical" (:kind sd))))))
   (file-seq (clojure.java.io/file file-path))))

(defn sds->filter-by-type [sds type]
  (sequence
   (comp
    (filter #(= (:kind %) type)))
   sds))

(defmacro file-path->specs
  "Used primarily as a means to generate data specs (both complex and primitive types)."
  [file-path]
  (let [sds (file-path->sds file-path)]
    `(do
       ~@(mapcat
          (fn [sd]
            (map :spec (:acc-specs (sd->specs sd))))
          sds)
       nil)))

(defmacro file-path->type-sets [file-path]
  (let [sds                  (file-path->sds file-path)
        resource-set-symbol  (symbol "resource-types")
        complex-set-symbol   (symbol "complex-types")
        primitive-set-symbol (symbol "primitive-types")
        resource-sds         (sds->filter-by-type sds "resource")
        complex-sds          (sds->filter-by-type sds "complex-type")
        primitive-sds        (sds->filter-by-type sds "primitive-type")]
    `(do (def ~resource-set-symbol   ~(set (map :name resource-sds)))
         (def ~complex-set-symbol    ~(set (map :name complex-sds)))
         (def ~primitive-set-symbol  ~(set (map :name primitive-sds))))))

(defmacro write-type-sets-to-file [namespace input-file output-file]
  `(spit
    ~output-file
    (with-out-str
      (clojure.pprint/pprint '(~(symbol "ns") ~(symbol namespace)
                                              (:require [clojure.set])))
      (clojure.pprint/pprint (macroexpand '(file-path->type-sets ~input-file))))))

(defmacro write-specs-to-file [namespace input-file output-file]
  `(spit
    ~output-file
    (with-out-str
      (clojure.pprint/pprint '(ns ~(symbol namespace)
                                (:require [clojure.set]
                                          [gen-fhi.fhir-specs.generate])))
      (clojure.pprint/pprint (macroexpand '(file-path->specs ~input-file))))))
