(ns gen-fhi.fhir-specs.traversal
  (:require [clojure.string :as string]))

(defn- ele-index->child-indices
  "Returns the indices of child elements"
  ([elements] (ele-index->child-indices elements 0))
  ([elements index]
   (let [parent                 (nth elements index)
         parent-path            (parent :path)
         parent-path-escaped    (string/replace parent-path #"\." "\\\\.")
         child-regex            (re-pattern (str parent-path-escaped "\\." "[^\\.]+"))
         [_ remaining-elements] (split-at (+ 1 index) elements)]
     (sequence
      (comp
       (keep-indexed
        (fn [child-index child]
          (if (re-matches child-regex (child :path))
            ;; Add where split so proper index on elements
            (+ 1 index child-index)
            nil))))
      remaining-elements))))

(defn- traversal-sd-elements [elements index visitor-fn]
  (let [child-indices          (ele-index->child-indices elements index)
        child-traversal-values (doall (map #(traversal-sd-elements elements % visitor-fn) child-indices))]
    (visitor-fn
     (nth elements index)
     child-traversal-values)))


(defn traversal-bottom-up
  "Traverse an sd bottom up. Uses visitor-fn to perform side effects."
  [sd visitor-fn]
  (let [elements (get-in sd [:snapshot :element] [])]
    (traversal-sd-elements elements 0 visitor-fn)))


