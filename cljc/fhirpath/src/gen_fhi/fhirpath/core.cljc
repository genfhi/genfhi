(ns gen-fhi.fhirpath.core
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [clojure.core.match :refer [match]]

            [gen-fhi.fhirpath.loc :refer [->loc] :as loc]
            [gen-fhi.fhirpath.parser :refer [parse]]
            [gen-fhi.fhirpath.node :refer [->fp-node FPNode child loc value child-keys]]))

(declare eval-expression)
(declare -evaluate)

(defn- operator-2 [operator-fn]
  (fn [ast ctx opts]
    (let [[_ lh-ast _ rh-ast] ast
          lh                 (eval-expression lh-ast ctx opts)
          rh                 (eval-expression rh-ast ctx opts)]
      (operator-fn lh rh ctx opts))))

(defn- singular-value [vals]
  (assert (< (count vals) 2) (str "Must be a singular value found: '" vals "'"))
  (if (empty? vals)
    nil
    (value (first vals))))

(def ^:private operators
  {"=" (fn [lh rh _ctx _opts]
         (list (->fp-node (= (map value lh) (map value rh)))))

   "!=" (fn [lh rh _ctx _opts]
          (list (->fp-node (not= (map value lh) (map value rh)))))

   ">" (fn [lh rh _ctx _opts]
         (list (->fp-node (> (singular-value lh) (singular-value rh)))))

   ">=" (fn [lh rh _ctx _opts]
          (list (->fp-node (>= (singular-value lh) (singular-value rh)))))

   "<" (fn [lh rh _ctx _opts]
         (list (->fp-node (< (singular-value lh) (singular-value rh)))))

   "<=" (fn [lh rh _ctx _opts]
          (list (->fp-node (<= (singular-value lh) (singular-value rh)))))

   "and" (fn [lh rh _ctx _opts]
           (list (->fp-node (and (singular-value lh) (singular-value rh)))))

   "or" (fn [lh rh _ctx _opts]
          (list (->fp-node (or (singular-value lh) (singular-value rh)))))

   "|" (fn [lh rh _ctx _opts]
         (concat lh rh))

   "+" (fn [lh rh _ctx _opts]
         (let [lh-val (singular-value lh)
               rh-val (singular-value rh)]
           (cond
             (and (number? lh-val) (number? rh-val)) (list (->fp-node (+ lh-val rh-val)))
             (or (string? lh-val) (string? rh-val))  (list (->fp-node (str lh-val rh-val))))))

   "*" (fn [lh rh _ctx _opts]
         (list (->fp-node (* (singular-value lh) (singular-value rh)))))

   "/" (fn [lh rh _ctx _opts]
         (list (->fp-node (/ (singular-value lh) (singular-value rh)))))

   "&" (fn [lh rh _ctx _opts]
         (list (->fp-node (str (singular-value lh) (singular-value rh)))))

   "-" (fn [lh rh _ctx _opts]
         (list (->fp-node (- (singular-value lh) (singular-value rh)))))})

(defn- error [message]
  #?(:clj (Exception. message)
     :cljs (js/Error. message)))

(defn- -repeat [expression ctx opts]
  (let [new-ctx (eval-expression expression ctx opts)]
    (reduce
     (fn [acc val]
       (concat
        acc
        (-repeat expression [val] opts)))
     new-ctx
     new-ctx)))

(defn- add-meta-singular [evaluation]
  (with-meta evaluation (assoc
                         (meta evaluation) :singular? true)))

(defn- fp-opts->node-opts [fp-opts]
  (cond-> {:loc (get fp-opts :fp-node/loc (->loc))}
    (some? (:fp-node/get-value fp-opts))
    (assoc :get-value (:fp-node/get-value fp-opts))))

(defn- resolve-external-constant [ctx identifier opts]
  ;; Two locations user passed in variables and the environment variables.
  ;; By default environment variable will take precedence.
  (let [variable-resolver (if (contains? (get opts :options/environment-variables) identifier)
                            (get opts :options/environment-variables)
                            (get opts :options/variables))
        loc               (let [nxt (->loc [] {:id identifier})]
                            (if-let [node (first ctx)]
                              (loc/next-loc (loc node) nxt)
                              nxt))
        extern            (if (fn? variable-resolver)
                            (variable-resolver loc identifier)
                            (get variable-resolver identifier '()))
        extern-list       (if (sequential? extern)
                            extern
                            (list extern))]
    (map
     #(->fp-node
       %
       (assoc (fp-opts->node-opts opts)
              :loc loc))
     extern-list)))

(defn- eval-param [param-ast ctx opts]
  (if (= 1 (count param-ast))
    '()
    (eval-expression (second param-ast) ctx opts)))

(def ^:private functions
  {:combine
   ^{:doc "Merge collections into one"}
   (fn [[_ _ [_ & params]] ctx opts]
     (let [evaluation (eval-param (first params) (resolve-external-constant ctx :context opts) opts)]
       (concat
        ctx
        evaluation)))
   :descendants
   ^{:doc "Return a collection with all descendent nodes"}
   (fn [_args ctx _opts]
     (->> (-evaluate "$this.repeat(children())" ctx)))

   :children
   ^{:doc "Get all immediate children."}
   (fn [_args ctx _opts]
     (mapcat
      (fn [val]
        (mapcat
         (fn [k] (child val k))
         (child-keys val)))
      ctx))

   :repeat
   ^{:doc "Repeat projection until returns empty [projection [expression]]"}
   (fn [[_ _ [_ & params]] ctx opts]
     (-repeat (first params) ctx opts))
   :where
   ^{:doc "Filter for true [filter [expression]]."}
   (fn [[_ _ [_ & params]] ctx opts]
     (filter
      (fn [v]
        (let [evaluation (eval-param (first params) (list v) opts)]
          (assert (= 1 (count evaluation)) "Where expression did not return a single boolean value.")
          (= true (singular-value evaluation))))
      ctx))
   ;; SEE [https://build.fhir.org/ig/HL7/FHIRPath/#iifcriterion-expression-true-result-collection-otherwise-result-collection-collection]
   :iif
   ^{:doc "The iif function in FHIRPath is an immediate if, also known as a conditional operator (such as C’s ? : operator)."}
   (fn [[_ _ [_ & params]] ctx opts]
     (let [conditional-result (eval-param (first params) ctx opts)]
       (if (and (not-empty conditional-result) (true? (value (first conditional-result))))
         (eval-param (second params) ctx opts)
         (if (= (count params) 3)
           (eval-param (nth params 2) ctx opts)
           '()))))

   :aggregate
   ^{:doc "Aggregate function see See [https://build.fhir.org/ig/HL7/FHIRPath/#aggregates]."}
   (fn [[_ _ [_ & params]] ctx opts]
     (cond
       (= 1 (count params)) (reduce
                             (fn [acc v]
                               (eval-param
                                (first params)
                                (with-meta [v] {:total acc})
                                opts))
                             ;; Scenario where ctx is empty just return empty value.
                             (if (empty? ctx)
                               []
                               [(first ctx)])
                             (rest ctx))

       (= 2 (count params)) (reduce
                             (fn [acc v]
                               (eval-param
                                (first params)
                                (with-meta [v] {:total acc})
                                opts))
                             (eval-param (second params) ctx opts)
                             ctx)

       :else (throw (ex-info "Aggregate only allows 2 parameters." {}))))
   :sum
   ^{:doc "equivalent to aggregate($this + $total)"}
   (fn [_ ctx opts]
     (->> (-evaluate "aggregate($this + $total)" ctx opts)))
   :answers
   ^{:doc "Get all answers for a QR. See [https://build.fhir.org/ig/HL7/sdc/expressions.html]"}
   (fn [_ ctx opts]
     (->> (-evaluate "descendants().where(answer.value.exists()).answer" ctx opts)))
   :ordinal
   ^{:doc "Returns ordinal value see [http://hl7.org/fhir/R4/extension-ordinalvalue.html]"}
   (fn [_ ctx opts]
     (->> (-evaluate
           "$this.extension.where(url='http://hl7.org/fhir/StructureDefinition/ordinalValue').value"
           ctx
           opts)))
   :count
   ^{:doc "Count of values returned."}
   (fn [[_ _ [_ & params]] ctx _opts]
     (assert (= 0 (count params)))
     (list (->fp-node (count ctx))))
   :first
   ^{:doc "Returns the first value."}
   (fn [[_ _ [_ & params]] ctx _opts]
     (assert (= 0 (count params)))
     (add-meta-singular
      (if-let [f-val (first ctx)]
        (list f-val)
        (list))))
   :last
   ^{:doc "Returns the last value."}
   (fn [[_ _ [_ & params]] ctx _opts]
     (assert (= 0 (count params)))
     (add-meta-singular
      (if-let [l-val (last ctx)]
        (list l-val)
        (list))))
   :take
   ^{:doc "Returns collection containing first (n) items. [nth [expression]]"}
   (fn [[_ _ [_ & params]] ctx opts]
     (let [number-node (first (eval-param (first params) ctx opts))]
       (assert (some? number-node) "Evaluation of take parameter returned nil.")
       (assert (number? (value number-node)) (str "Param for number must be an integer: " (value number-node)))
       (take (value number-node) ctx)))
   :empty
   ^{:doc "Check if return is empty."}
   (fn [[_ _ [_ & params]] ctx _opts]
     (assert (= 0 (count params)))
     (list (->fp-node (= 0 (count ctx)))))
   :distinct
   ^{:doc "Check all values returned are unqiue."}
   (fn [[_ _ [_ & params]] ctx _opts]
     (assert (= 0 (count params)))
     (->> ctx
          (map #(value %))
          (set)
          (map ->fp-node)
          (into '())))
   :startsWith
   ^{:doc "Returns true when the input string starts with the given prefix"}
   (fn [[_ _ [_ & params]] ctx opts]
     (let [prefix      (first (eval-param (first params) ctx opts))]
       (map
        (fn [node]
          (->fp-node
           (string/starts-with? (value node)
                                (value prefix))))
        ctx)))

   :replace
   ^{:doc "Replace string with pattern  [pattern-expression [expression], replace-with [expression]]"}
   (fn [[_ _ [_ & params]] ctx opts]
     (let [pattern      (first (eval-param (first params) ctx opts))
           replace-with (first (eval-param (second params) ctx opts))]

       (assert (string? (value pattern)))
       (assert (string? (value replace-with)))

       (map
        (fn [node]
          (->fp-node
           (string/replace (value node) (value pattern) (value replace-with))))
        ctx)))
   :isDistinct
   ^{:doc "Check all values returned are unqiue."}
   (fn [args ctx _opts]
     (let [distinct-ctx ((functions :distinct) args ctx _opts)]
       (list
        (->fp-node
         (= (count distinct-ctx) (count ctx))))))
   :exists
   ^{:doc "Check if any value is returned."}
   (fn [[_ _ [_ & params]] ctx _opts]
     (assert (= 0 (count params)))
     (list (->fp-node (< 0 (count ctx)))))})

(defn- literal->fp-nodes [[_ [_ literal-value]]]
  (add-meta-singular
   (list (->fp-node literal-value))))

(defn- eval-operation [[_ _ op-type :as op-node] ctx opts]
  (loop [rh              (eval-expression (second op-node) ctx opts)
         operations-left (subvec op-node 2)]
    (if (empty? operations-left)
      rh
      (let [operation (first operations-left)
            lh        (eval-expression (second operations-left) ctx opts)]
        (recur ((operators operation) rh lh ctx opts)
               (subvec operations-left 2))))))

(defn- eval-indexed [[_node-name expression] ctx opts]
  (add-meta-singular
   (let [evaluation (eval-expression expression ctx opts)]
     (assert (= 1 (count evaluation)))
     (let [index (value (first evaluation))]
       (assert (number? index))
       (if (> (count ctx) index)
         (list (nth ctx index))
         (list))))))

(defn- eval-function [[_ [_ fn-identifier] :as fn-ast] ctx opts]
  (let [extern (when (seq ctx) (child (first ctx) fn-identifier))]
    (if (and (seq extern)
             (fn? (value (first extern))))
      ;; Used with factory function
      (let [[_ _ [_ & params]] fn-ast
            extern-fn          (value (first extern))]
        (apply
         extern-fn
         (map
          #(map value (eval-param % ctx opts))
          params)))
      (let [function (functions fn-identifier)]
        (function fn-ast ctx opts)))))

(defn- eval-identifier [[_ identifier-key] ctx _opts]
  (mapcat
   (fn [v] (child v identifier-key))
   ctx))

(defn- eval-invocation [[_ [invocation-type :as node]] ctx opts]
  (match invocation-type
    :IDENTIFIER (eval-identifier node ctx opts)
    :THIS       ctx
    :TOTAL      (get (meta ctx) :total)
    :INDEX      (throw (error "$index not implemented"))
    :FUNCTION   (eval-function node ctx opts)))

(defn- variable-node->identifier [[_ [_ identifier]]]
  identifier)

(defn- eval-term [[_ & children] ctx opts]
  (reduce
   (fn [ctx [node-type :as node]]
     (match node-type ;; Should also support keywords
       :LITERAL           (literal->fp-nodes node)
       :EXTERNAL_CONSTANT (resolve-external-constant ctx (variable-node->identifier node) opts)
       :EXPRESSION        (eval-expression node ctx opts)
       :INVOCATION        (eval-invocation node ctx opts)))
   ctx
   children))

(defn- eval-expression [[_ & children] ctx opts]
  (reduce
   (fn [ctx [node-type :as node]]
     (match node-type
       :OPERATION  (eval-operation node ctx opts)
       :INDEXED    (eval-indexed node ctx opts)
       :EXPRESSION (eval-expression node ctx opts)
       :TERM       (eval-term node ctx opts)
       :INVOCATION (eval-invocation node ctx opts)
       :else       (throw (ex-info "eval-expression-failure" {:data node}))))
   ctx
   children))

(s/def :options/variables (s/or :fn-resolver fn? :map-resolver (s/map-of keyword? any?)))
(s/def :fp-node/get-value fn?)
(s/def :options/allow-singular? boolean?)
(s/def :options/options   (s/keys :opt [:options/variables
                                        :options/allow-singular?
                                        :fp-node/get-value
                                        :fp-node/loc]))

(s/fdef evaluate
  :args (s/cat :fhirpath string? :ctx any? :options :options/options)
  :ret any?)

(def cached-asts (atom {}))

(defn- ->environment-variables [ctx]
  {:ucum    "http://unitsofmeasure.org"
   :context ctx
   :factory {:Coding (fn [system code display version]
                       (list
                        (->fp-node
                         (cond-> {}
                           (some? (first system))  (assoc :system  (first system))
                           (some? (first code))    (assoc :code    (first code))
                           (some? (first display)) (assoc :display (first display))
                           (some? (first version)) (assoc :version (first version))))))}})

(defn- ->assoc-environment-variables [opts ctx]
  (assoc opts :options/environment-variables
         (->environment-variables ctx)))

(defn- -evaluate
  "Evaluates a given FP expression."
  ([fhirpath] (-evaluate fhirpath {} {}))
  ([fhirpath ctx] (-evaluate fhirpath ctx {}))
  ([fhirpath raw-ctx opts]
   (let [raw-ctx        (->> (if (sequential? raw-ctx)
                               raw-ctx
                               (list raw-ctx)))
         ctx           (map (fn [v] (if (satisfies? FPNode v)
                                      v
                                      (->fp-node
                                       v
                                       (fp-opts->node-opts opts))))

                            raw-ctx)
         cached-ast (get @cached-asts fhirpath)
         ast        (if (some? cached-ast)
                      cached-ast
                      (let [ast (parse fhirpath)]
                        (swap! cached-asts assoc fhirpath ast)
                        ast))
         opts       (->assoc-environment-variables opts raw-ctx)
         result     (eval-expression ast ctx opts)]

     result)))

(defn- convert-to-return [evaluation allow-singular?]
  ;; Check if singular return allowed and value is a singular
  ;; Which is based of meta of the collection.
  ;; Currently this will happen on literals and indexing.
  (if (and allow-singular?
           (:singular? (meta evaluation)))
    (when-let [node  (first evaluation)]
      (value node))
    (sequence
     (comp
      (map (fn [v] (value v)))
      (filter (fn [v] (some? v))))
     evaluation)))

(defn evaluate
  "Public Func for fp evaluations does return value conversion"
  ([fhirpath]     (evaluate fhirpath {} {}))
  ([fhirpath ctx] (evaluate fhirpath ctx {}))
  ([fhirpath raw-ctx opts]
   (let [evaluation (-evaluate fhirpath raw-ctx opts)]
     (convert-to-return
      evaluation
      (:options/allow-singular? opts)))))

(defn locations [& args]
  (sequence
   (comp
    (map (fn [v] (loc/fields (loc v))))
    (filter (fn [v] (some? v))))
   (apply -evaluate args)))

(defn ->supported-functions []
  (reduce
   (fn [fn-docs fn-name]
     (assoc fn-docs fn-name (:doc (meta (functions fn-name)))))
   {}
   (keys functions)))

(defn ->variables
  "Returns set of variables the expression depends on."
  [fhirpath]
  (let [variables! (atom #{})]
    (evaluate fhirpath {} {:options/variables
                           (fn [_ctx id]
                             (swap!
                              variables!
                              (fn [s] (conj s id)))
                             {})})
    @variables!))

(defn ->environment-variables-ids []
  (keys (->environment-variables nil)))

(defn path-vec->fp
  "Given a vector path convert to an fp expression to the loc."
  [path]
  (reduce
   (fn [fp v]
     (if (number? v)
       (str fp "[" v "]")
       (str fp "." (name v))))
   "$this"
   path))
