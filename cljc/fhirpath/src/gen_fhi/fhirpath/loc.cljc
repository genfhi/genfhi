(ns gen-fhi.fhirpath.loc
  (:require [clojure.string :as string]
            [gen-fhi.fhirpath.utils :refer [coll-fp-primitive? fp-primitive?]]))

(defprotocol Location
  (fields [this])
  (descend
    [this field value]))

(defprotocol MultiLocations
  (circular? [this])
  (next-loc  [this loc]))

;; Because loc can also be nil just extending type here.
(extend-type
 nil
  Location
  (fields [_] nil)
  (descend [_this field value]
    nil))

(defn loc->str
  ([loc]
   (loc->str 0 loc))
  ([idx loc]
   (if (contains? loc :locs)
     (string/join "->" (map
                        #(loc->str (+ 1 idx) %)
                        (:locs loc)))
     (str
      (name (or (get-in loc [:meta :id]) :this))
      (if (empty? (:fields loc)) "" ".")
      (string/join "." (map name (:fields loc)))))))

(defrecord MetaLocation [fields meta]
  Location
  (fields  [this]
    (:fields this))
  (descend [this field value]
    (when (:fields this)
      (->MetaLocation
       (cond
         ;; If the field is .value return the loc-vec
         ;; primitive .value means the value not anything within the element. 
         (and
          (fp-primitive? value)
          (= :value field))     (:fields this)
         (coll-fp-primitive?
          value)                (let [index (last (:fields this))
                                      parent-field (last (pop (:fields this)))
                                      loc-vec (pop (pop (:fields this)))]
                                  (conj loc-vec
                                        (keyword (str "_" (name parent-field)))
                                        index
                                        field))
         (fp-primitive? value)  (if (empty? (:fields this))
                                  ;; Means trying to get a field on a literal value.
                                  ;; Won't have any loc associated to it that can use.
                                  []
                                  (let [parent-field (last (:fields this))
                                        loc-vec (pop (:fields this))]
                                    (conj loc-vec (keyword (str "_" (name parent-field)))
                                          field)))
         :else                  (conj (:fields this) field))
       (:meta this)))))

(defrecord MultiMetaLocation [locs]
  MultiLocations
  (circular? [this]
    (not (apply distinct? (pop (:locs this)))))
  (next-loc [this loc]
    (->MultiMetaLocation
     (conj (:locs this) loc)))
  Location
  (fields [this]
    (fields (last (:locs this))))
  (descend [this field v]
    (let [last-loc  (last (:locs this))
          locs      (pop (:locs this))]
      (->MultiMetaLocation
       (conj
        locs
        (descend
         last-loc
         field
         v))))))

(defn ->multi-loc
  ([] (->multi-loc []))
  ([locs]
   (->MultiMetaLocation locs)))

(defn ->loc
  ([]         (->loc [] nil))
  ([loc]      (->loc loc []))
  ([loc meta] (->multi-loc
               [(->MetaLocation loc meta)])))

