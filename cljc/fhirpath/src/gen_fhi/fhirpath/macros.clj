(ns gen-fhi.fhirpath.macros
  (:require [instaparse.core :as insta]
            [clojure.java.io :as io]))

(defmacro parser-generate [name path]
  (let [grammar (slurp (io/resource path))]
    `(insta/defparser ~name ~grammar)))
