(ns gen-fhi.fhirpath.node
  (:require [gen-fhi.fhirpath.loc :refer [descend]]
            [gen-fhi.fhirpath.utils :refer [raw-primitive? fp-primitive?]]))

(declare ->fp-node)

(defprotocol FPNode
  (loc [this])
  (value [this])
  (child-keys  [this])
  (child [this field]))

(defn- gen-primitive-extension-fieldname
  "Generates primitive extension field name which per spec for json is _fieldname"
  [field-name]
  (let [ext-field-name (str "_" (name field-name))]
    (if (keyword? field-name)
      (keyword ext-field-name)
      ext-field-name)))

(defn- primitive->fp-primitive
  "Creates a primitive node which includes the extension as well as value.
  Places metadata on node stating it's primitve."
  ([v] (primitive->fp-primitive v {}))
  ([v element] (primitive->fp-primitive v element false))
  ([v element is-coll?]
   (let [element (or element {})]
     (assert (map?  element) "Element must be a map")
     ^{:primitive? true
       :collection? is-coll?}
     {:value      v
      :element    element})))

(defn- coll->coll-nill-to-size [col size]
  (concat col (map (constantly nil) (range size))))

(defn- prim-coll->fp-coll
  "Primitive arrays contain extensions in :_fieldname [ext-val] need to combine them."
  [coll extensions {:keys [loc] :as node-opts}]
  (let [coll-diff-size     (- (count coll) (count extensions))
        abs-coll-diff-size (max coll-diff-size (- coll-diff-size))
        coll               (if (neg-int? coll-diff-size)
                             (coll->coll-nill-to-size coll abs-coll-diff-size)
                             coll)
        coll-extensions    (if (pos-int? coll-diff-size)
                             (coll->coll-nill-to-size extensions abs-coll-diff-size)
                             extensions)
        zipped-val+exts (map vector coll coll-extensions)]
    (map-indexed
     (fn [i [v ext]] (->fp-node (primitive->fp-primitive v ext true)
                                (assoc node-opts :loc (descend loc i ext))))
     zipped-val+exts)))

(defn- coll->fp-coll
  "If collection need to check the extension field if primitive and map it with value."
  [node value field {:keys [loc] :as node-opts}]
  (let [ext-coll (get node (gen-primitive-extension-fieldname field))]
    (if (or (raw-primitive? (first value))
            (some? ext-coll))
      (prim-coll->fp-coll value ext-coll node-opts)
      (map-indexed
       (fn [i v]
         (->fp-node v
                    (assoc
                     node-opts
                     :loc (descend loc i v))))
       value))))

(defn- check-typechoice-or-namespaced [node field]
  (let [typechoice-regex (re-pattern (str (name field) "[A-Z].*"))]
    (or
     (->> (keys node)
          (filter #(or (= (name %) (name field))
                       (some? (re-matches typechoice-regex (name %)))))
          (sort #(< (count (name %)) (count (name %2))))
          (first))
     field)))

(defn- compute-field-name
  "If field with given exists use that else check for a typechoice"
  [node field]
  (if (some? (get node field))
    field
    (check-typechoice-or-namespaced node field)))

(defn- get-ctx
  "If Primitive descend into extension element for fields if not value."
  [value field]
  (if (and (not= field :value) (fp-primitive? value))
    (get value :element)
    value))

(defn ->fp-node
  "Implements FP Protocol and childs object to next node wrapping in FPProtocol."
  ([raw-node] (->fp-node raw-node nil))
  ([raw-node {:keys [loc get-value]
              :or   {get-value (fn [_loc ctx field] (get ctx field))}
              :as   node-opts}]
   (if (satisfies? FPNode raw-node)
     raw-node
     ;; Checks in case passing a literal to wrap it as FPNode
     (let [node (if (raw-primitive? raw-node)
                  (primitive->fp-primitive raw-node)
                  raw-node)]
       (reify
         FPNode
         (loc [_] loc)
         (value [_] (if (fp-primitive? node)
                      (:value node)
                      node))
         (child-keys [_]
           (if (fp-primitive? node)
             '()
             (keys raw-node)))

         (child [_ -field]
           (let [;; Returns context if primitive and field not value it's the element section
                ;; Else returns the raw value back.
                 ctx      (get-ctx node -field)
                 field    (compute-field-name ctx -field)
                 next-loc (descend loc field node)
                 value    (get-value next-loc ctx field)
                 result (cond
                          (raw-primitive? value) [(->fp-node
                                                   (primitive->fp-primitive
                                                    value
                                                    (get ctx (gen-primitive-extension-fieldname field) {}))
                                                   (assoc node-opts :loc next-loc))]
                          (sequential? value)    (coll->fp-coll
                                                  ctx
                                                  value
                                                  field
                                                  (assoc node-opts :loc next-loc))
                         ;; handle case of nil but extensions existing IE nill primitive but
                         ;; primitive extension fields
                          (nil? value)           (if-let [prim-extension (get ctx (compute-field-name ctx (keyword (str "_" (name -field)))))]
                                                   [(->fp-node
                                                     (primitive->fp-primitive
                                                      value
                                                      prim-extension)
                                                     (assoc node-opts :loc next-loc))]
                                                   [])
                          :else                  [(->fp-node
                                                   value
                                                   (assoc node-opts :loc next-loc))])]
             result)))))))
