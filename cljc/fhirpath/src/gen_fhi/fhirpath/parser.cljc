(ns gen-fhi.fhirpath.parser
  #?(:clj (:refer-clojure :exclude [read-string]))
  #?(:cljs (:require-macros [gen-fhi.fhirpath.macros :refer [parser-generate]]))
  (:require
   #?(:cljs  [cljs.reader :refer [read-string]]
      :clj   [clojure.edn :refer [read-string]])
   #?(:clj [gen-fhi.fhirpath.macros :refer [parser-generate]])
   [instaparse.failure :as insta-f]
   [instaparse.core :as insta]))

(parser-generate -parser "gen_fhi/fhirpath/grammar.insta")

(defn- cleanup-operation [& operation-node]
  (if (= (count operation-node) 1)
    (first operation-node)
    [:EXPRESSION (apply conj [:OPERATION] operation-node)]))

(defn parse [fhirpath]
  (let [ast (instaparse.core/transform
             {:NON_OP (fn [& args] (apply conj [:EXPRESSION] args))
              :EQUALITY_OPERATION cleanup-operation
              :ADDITIVE_OPERATION cleanup-operation
              :MULTIPlICATIVE_OPERATION cleanup-operation
              :UNION_OPERATION cleanup-operation
              :MEM_OPERATION cleanup-operation
              :BOOL_OPERATION cleanup-operation
              :IMPLIES_OPERATION cleanup-operation
              :IDENTIFIER (fn [& characters] [:IDENTIFIER (keyword (apply str characters))])
              :STRING     (fn [& characters] [:STRING (apply str characters)])
              :BOOLEAN    (fn [& characters] [:BOOLEAN (= "true" (apply str characters))])
              :NUMBER     (fn [& characters] [:NUMBER
                                              (read-string (apply str characters))])}
             (-parser fhirpath))]
    (if (insta/failure? ast)
      (throw (ex-info
              (with-out-str (insta-f/pprint-failure ast))
              {:error ast}))
      ast)))

(defn find-node [parse-tree node]
  (if (= parse-tree node)
    [parse-tree]
    (if (vector? parse-tree)
      (apply
       concat
       (map
        #(find-node % node)
        parse-tree))
      [])))

(defn- find-variable-locs [expression variable-id]
  (let [parse-with-line-info (insta/add-line-and-column-info-to-metadata
                              expression
                              (parse expression))]
    (map
     meta
     (find-node parse-with-line-info [:EXTERNAL_CONSTANT
                                      [:IDENTIFIER (keyword variable-id)]]))))

(defn replace-variable [expression old-id new-id]
  (reduce
   (fn [expression loc]
     (str (subs expression 0 (:instaparse.gll/start-index loc))
          (str "%" new-id)
          (subs expression
                (:instaparse.gll/end-index loc)
                (count expression))))
   expression
   ;; Perform a sort to preserve indices as they mutate the expression
   (->>
    (find-variable-locs expression (keyword old-id))
    (sort
     (fn [a b]
       (> (:instaparse.gll/start-index a)
          (:instaparse.gll/start-index b)))))))

