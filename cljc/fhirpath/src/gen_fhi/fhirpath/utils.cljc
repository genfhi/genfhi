(ns gen-fhi.fhirpath.utils)

(defn fp-primitive?
  "Determines if fp primitive by meta data on node."
  [v]
  (:primitive? (meta v)))

(defn coll-fp-primitive?
  "Determines if fp primitive by meta data on node."
  [v]
  (and (:collection? (meta v))
       (fp-primitive? v)))

(defn raw-primitive?
  "Detects Raw primitive that has not yet been converted into an ->fp-node obj."
  [v]
  (and (some? v)
       (not (coll? v))))
