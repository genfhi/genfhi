(ns fhirpath.execute-test
  (:require  [clojure.test :as t]
             [gen-fhi.fhirpath.core :as fp]
             [clojure.java.io :as io]
             [clojure.data.json :as json]))

(t/deftest fhirpath.eval
  (t/is (= (fp/evaluate
            "$this.field1.field2"
            {:field1 {:field2 5}})
           '(5))))

(t/deftest fhirpath.primitive
  (t/is (= (fp/evaluate
            "field1.field2"
            {:field1 {:field2 5}})
           '(5))))

(t/deftest fhirpath.extension.primitive.ext
  (t/is (=
         (fp/evaluate "(test.extension.url)" [{:test  "asdf"
                                               :_test {:extension
                                                       [{:url "test.com"}]}}])
         '("test.com"))))

(t/deftest fhirpath.extension.primitive.array
  (t/is (=
         (fp/evaluate "(test.extension.url)" [{:test ["asdf" "asdf" "asdf"]
                                               :_test [{:extension [{:url "url1"}]}
                                                       {:extension [{:url "url2"}]}
                                                       {:extension [{:url "url3"}]}]}])
         '("url1" "url2" "url3"))))

(t/deftest fhirpath.allow.nilable.primitives
  (t/is (=
         (fp/evaluate "(test.extension.url)" [{:test ["asdf" nil "asdf"]
                                               :_test [{:extension [{:url "url1"}]}
                                                       {:extension [{:url "url2"}]}
                                                       {:extension [{:url "url3"}]}]}])
         '("url1" "url2" "url3"))))

(t/deftest fhirpath.nils
  (t/is (=
         '()
         (fp/evaluate "(test.extension.url)" [{:test ["asdf" nil "asdf"]}]))))

(t/deftest fhirpath.equal-operator
  (t/is (=
         '(true)
         (fp/evaluate "$this.test=2" {:test 2}))))

(t/deftest fhirpath.where
  (t/is (=
         '(2)
         (fp/evaluate "$this.test.where($this=2)" {:test [2 3 4]}))))

(t/deftest fhirpath.typechoice
  (t/is (=
         '(5 "test")
         (fp/evaluate "$this.test.value" {:test [{:valueInteger 5} {:valueString "test"}]}))))

(t/deftest fhirpath.extension-larger
  (t/is (=
         '("ext-val")
         (fp/evaluate "$this.value.extension.value"
                      {:_value [nil {:extension [{:value "ext-val"}]}]
                       :value ["test"]}))))

(t/deftest fhirpath.value-smaller
  (t/is (=
         '("test")
         (fp/evaluate "$this.value" {:_value [nil {:value "ext-val"}] :value ["test"]}))))

(t/deftest fhirpath.externals
  (t/is (=
         '({:val 5})
         (gen-fhi.fhirpath.core/evaluate
          "$this.vals.where($this.val=%test)"
          {:vals [{:val 5} {:val 6} {:val 7}]}
          {:options/variables {:test 5}}))))

(t/deftest fhirpath.externals.exists
  (t/is (=
         '(true)
         (gen-fhi.fhirpath.core/evaluate
          "$this.vals.where($this.val=%test).exists()"
          {:vals [{:val 5} {:val 6} {:val 7}]}
          {:options/variables {:test 7}})))
  (t/is (=
         '(false)
         (gen-fhi.fhirpath.core/evaluate
          "$this.vals.where($this.val=%test).exists()"
          {:vals [{:val 5} {:val 6} {:val 7}]}
          {:options/variables {:test 8}}))))

(t/deftest fhirpath.addition
  (t/is (=
         '(11)
         (fp/evaluate "%var1 + %var2" nil {:options/variables {:var1 5 :var2 6}}))))

(t/deftest fhirpath.boolean-literal
  (t/is (=
         '(true)
         (fp/evaluate "true" nil)))
  (t/is (=
         '(false)
         (fp/evaluate "false" nil))))

(t/deftest fhirpath.not=
  (t/is (=
         '(false)
         (fp/evaluate "true != true" nil)))
  (t/is (=
         '(true)
         (fp/evaluate "true != false" nil))))

(t/deftest fhirpath.|
  (t/is (=
         '("test" "test2" "test3" 5)
         (fp/evaluate "'test' | 'test2' | 'test3' | 5" nil))))

(t/deftest fhirpath.>
  (t/is (=
         '(true)
         (fp/evaluate "5 > 4" nil)))
  (t/is (=
         '(false)
         (fp/evaluate "4 > 5" nil))))

(t/deftest fhirpath.>=
  (t/is (=
         '(true)
         (fp/evaluate "5 >= 5" nil)))
  (t/is (=
         '(true)
         (fp/evaluate "5 >= 4" nil)))
  (t/is (=
         '(false)
         (fp/evaluate "4 >= 5" nil))))

(t/deftest fhirpath.<
  (t/is (=
         '(false)
         (fp/evaluate "5 < 5" nil)))
  (t/is (=
         '(false)
         (fp/evaluate "5 < 4" nil)))
  (t/is (=
         '(true)
         (fp/evaluate "4 < 5" nil))))

(t/deftest fhirpath.<=
  (t/is (=
         '(true)
         (fp/evaluate "5 <= 5" nil)))
  (t/is (=
         '(false)
         (fp/evaluate "5 <= 4" nil)))
  (t/is (=
         '(true)
         (fp/evaluate "4 <= 5" nil))))

(t/deftest fhirpath.and
  (t/is (=
         '(false)
         (fp/evaluate "true and false" nil)))
  (t/is (=
         '(true)
         (fp/evaluate "true and true" nil))))

(t/deftest fhirpath.or
  (t/is (=
         '(true)
         (fp/evaluate "true or false" nil)))
  (t/is (=
         '(false)
         (fp/evaluate "false or false" nil))))

(t/deftest fhirpath.or.2
  (t/is (=
         '(20)
         (fp/evaluate "20 or 10" nil)))
  (t/is (=
         '(15)
         (fp/evaluate "%test.value or 10" nil {:options/variables {:test {:value 15}}}))))

(t/deftest fhirpath.|.2
  (t/is (=
         '(true false "test")
         (fp/evaluate "true | false | 'test'" nil))))

(t/deftest fhirpath.*
  (t/is (=
         '(25)
         (fp/evaluate "5 * 5" nil)))
  (t/is (=
         '(5)
         (fp/evaluate "25 / 5" nil))))

(t/deftest fhirpath.&
  (t/is (=
         '("25 test")
         (fp/evaluate "'25' & ' test'" nil))))

(t/deftest ^:concatenate fhirpath.same-var-addition
  (t/is (=
         '("given given")
         (fp/evaluate "%querylist.state.selected.name.text + ' ' + %querylist.state.selected.name.text" nil {:options/variables {:querylist {:state {:selected {:name {:text "given"}}}}}}))))

(t/deftest fhirpath.count
  (t/is (=
         '(2)
         (fp/evaluate "$this.name.given.count()"
                      {:name {:given ["name1" "name2"]}}))))

(t/deftest fhirpath.count.non-existant
  (t/is (=
         '(0)
         (fp/evaluate "$this.name.given.count()" {}))))

(t/deftest fhirpath.test.null
  (t/is (=
         '("string")
         (fp/evaluate "%test.value + 'string'" {}))))

(t/deftest fhirpath.test.first
  (t/is (=
         '(1)
         (fp/evaluate "$this.first()" [1 2 3]))))

(t/deftest fhirpath.test.first.nil
  (t/is (=
         '()
         (fp/evaluate "$this.first()" nil))))

(t/deftest fhirpath.test.first.empty
  (t/is (=
         '()
         (fp/evaluate "$this.first()" []))))

(t/deftest fhirpath.test.last
  (t/is (=
         '(3)
         (fp/evaluate "$this.last()" [1 2 3]))))

(t/deftest fhirpath.test.last.empty
  (t/is (=
         '()
         (fp/evaluate "$this.last()" []))))

(t/deftest fhirpath.test.last.nil
  (t/is (=
         '()
         (fp/evaluate "$this.last()" nil))))

(t/deftest fhirpath.test.take
  (t/is (=
         '(1 2 3)
         (fp/evaluate "$this.take(5)" [1 2 3]))))

(t/deftest fhirpath.test.take.less
  (t/is (=
         '(1 2)
         (fp/evaluate "$this.take(2)" [1 2 3]))))

(t/deftest fhirpath.test.take.nil
  (t/is (=
         '()
         (fp/evaluate "$this.take(5)" nil))))

(t/deftest fhirpath.passing-in-vector
  (t/is
   (= ["test"]
      (fp/evaluate "$this.name.given"
                   [{:name [{:given ["test"]}]}]))))

(t/deftest fhirpath.empty
  (t/is
   (= '(true)
      (fp/evaluate "$this.name.empty()"
                   [{}])))
  (t/is
   (= '(true)
      (fp/evaluate "$this.name.empty()"
                   [{:name []}])))
  (t/is
   (= '(false)
      (fp/evaluate "$this.name.empty()"
                   [{:name [{:given ["test"]}]}]))))

(t/deftest fhirpath.distinct
  (t/is
   (= '(5)
      (fp/evaluate "$this.distinct().count()"
                   [1 2 3 4 5])))
  (t/is
   (= '(5)
      (fp/evaluate "$this.distinct().count()"
                   [{:test 5} {:hello 5} 3 4 5])))
  (t/is
   (= '(4)
      (fp/evaluate "$this.distinct().count()"
                   [{:test 5} {:test 5} 3 4 5])))
  (t/is
   (= '(5)
      (fp/evaluate "$this.distinct().count()"
                   [1 1 2 3 4 5]))))

(t/deftest fhirpath.isDistinct
  (t/is
   (= '(true)
      (fp/evaluate "$this.isDistinct()"
                   [1 2 3 4 5])))
  (t/is
   (= '(true)
      (fp/evaluate "$this.isDistinct()"
                   [{:test 5} {:hello 5} 3 4 5])))
  (t/is
   (= '(false)
      (fp/evaluate "$this.isDistinct()"
                   [{:test 5} {:test 5} 3 4 5])))
  (t/is
   (= '(false)
      (fp/evaluate "$this.isDistinct()"
                   [1 1 2 3 4 5]))))

(t/deftest fhirpath.indexing
  (t/is
   (= '(1)
      (fp/evaluate "$this[0]"
                   [1 2 3 4 5])))
  (t/is
   (= '({:hello "world"})
      (fp/evaluate "$this.test[2]"
                   {:test [{} {} {:hello "world"}]}))))

(t/deftest fhirpath.replace
  (t/is
   (= '("hello world")
      (fp/evaluate "$this[0].replace('-',' ')"
                   ["hello-world"]))))

(t/deftest fhirpath.invalid-index
  (t/is
   (= '()
      (fp/evaluate "$this[4]"
                   ["hello-world"]))))

(t/deftest fhirpath.repeat
  (t/is
   (= '({:item {:item "test"}} {:item "test"} "test")
      (fp/evaluate
       "$this.repeat(item)"
       {:item
        [{:item {:item "test"}}]}))))

(t/deftest fhirpath.children
  (t/is
   (= '({:v 1}
        {:v 2}
        {:hello "world"}
        {:given ["bob"]})

      (fp/evaluate
       "$this.children()"
       {:val [{:v 1} {:v 2}]
        :test {:hello "world"}
        :name {:given ["bob"]}}))))

(t/deftest fhirpath.descendants
  (t/is
   (= '({:v 1}
        {:v 2}
        {:hello "world"}
        {:given ["bob"]}
        1
        2
        "world"
        "bob")
      (fp/evaluate
       "$this.descendants()"
       {:val [{:v 1} {:v 2}]
        :test {:hello "world"}
        :name {:given ["bob"]}}))))

(t/deftest fhirpath.context.environment-variable
  (t/is
   (= '({:val [{:v 1} {:v 2}]
         :test {:hello "world"}
         :name {:given ["bob"]}})
      (fp/evaluate
       "%context"
       {:val [{:v 1} {:v 2}]
        :test {:hello "world"}
        :name {:given ["bob"]}}))))

(t/deftest fhirpath.combine
  (t/is
   (= '({:val [{:v 1} {:v 2}]
         :test {:hello "world"}
         :name {:given ["bob"]}}
        {:v 1}
        {:v 2})
      (fp/evaluate
       "$this.combine($this.val)"
       {:val [{:v 1} {:v 2}]
        :test {:hello "world"}
        :name {:given ["bob"]}}))))

(t/deftest fhirpath.array.extension
  (t/is
   (= '("url1" "url2")
      (fp/evaluate
       "$this.stringArray.extension.url"
       {:_stringArray [{:extension [{:url "url1"}]}
                       nil
                       {:extension [{:url "url2"}]}]
        :stringArray ["test" nil "test3"]}))))

(t/deftest fhirpath.indexing.op-check
  (t/is
   (=
    '("test")
    (fp/evaluate
     "$this.a.b.stringArray[0]"
     {:a
      {:b {:_stringArray [{:extension [{:url "https://genfhi.com/Extension/encrypt-value" :valueString "test"}]}]
           :stringArray ["test" nil "test3"]}}}))))

(t/deftest fhirpath.startsWith
  (t/is
   (=
    '(true)
    (fp/evaluate
     "$this.a.b.startsWith('Hello')"
     {:a
      {:b "Hello world"}})))
  (t/is
   (=
    '(false)
    (fp/evaluate
     "$this.a.b.startsWith('Bye')"
     {:a
      {:b "Hello world"}})))
  (t/is
   (=
    '(true)
    (fp/evaluate
     "$this.a.b.startsWith('Hello world')"
     {:a
      {:b "Hello world"}}))))

(t/deftest fhirpath.startsWith.predicate
  (t/is
   (=
    '({:url "asdf"} {:url "Hele"})
    (fp/evaluate
     "$this.extension.where($this.url.startsWith('Hello')!=true)"
     {:extension
      [{:url "asdf"}
       {:url "Hello world"}
       {:url "Hele"}]}))))

(t/deftest delim-identifier
  (t/is
   (=
    '("hello-world")
    (fp/evaluate
     "$this.`escaped-123-string%%%`.value"
     {:escaped-123-string%%% {:value "hello-world"}}))))

(t/deftest fhirpath.singular
  (t/is (= 5
           (gen-fhi.fhirpath.core/evaluate
            "5"
            {:vals [{:val 5} {:val 6} {:val 7}]}
            {:options/allow-singular? true})))
  (t/is (= '({:val 5} {:val 6} {:val 7})
           (gen-fhi.fhirpath.core/evaluate
            "$this.vals"
            {:vals [{:val 5} {:val 6} {:val 7}]}
            {:options/allow-singular? true})))
  (t/is (= {:val 6}
           (gen-fhi.fhirpath.core/evaluate
            "$this.vals[1]"
            {:vals [{:val 5} {:val 6} {:val 7}]}
            {:options/allow-singular? true})))
  (t/is (= {:val 7}
           (gen-fhi.fhirpath.core/evaluate
            "$this.vals.last()"
            {:vals [{:val 5} {:val 6} {:val 7}]}
            {:options/allow-singular? true})))
  (t/is (= nil
           (gen-fhi.fhirpath.core/evaluate
            "$this.vals.last()"
            {:vals []}
            {:options/allow-singular? true})))
  (t/is (= {:val 5}
           (gen-fhi.fhirpath.core/evaluate
            "$this.vals.first()"
            {:vals [{:val 5} {:val 6} {:val 7}]}
            {:options/allow-singular? true}))))

(t/deftest nil-primitivew
  (t/is
   (=
    '("v")
    (gen-fhi.fhirpath.core/evaluate
     "$this.value.extension.value"
     {:_valueString {:extension [{:url "test" :valueString "v"}]}}))))

(t/deftest iif-support
  (t/is
   (=
    '()
    (gen-fhi.fhirpath.core/evaluate
     "iif($this.value.extension.value='z', 'was-true')"
     {:_valueString {:extension [{:url "test" :valueString "v"}]}})))
  (t/is
   (=
    '("was-false")
    (gen-fhi.fhirpath.core/evaluate
     "iif($this.value.extension.value='z', 'was-true', 'was-false')"
     {:_valueString {:extension [{:url "test" :valueString "v"}]}})))
  (t/is
   (=
    '("was-true")
    (gen-fhi.fhirpath.core/evaluate
     "iif($this.value.extension.value='v', 'was-true', 'was-false')"
     {:_valueString {:extension [{:url "test" :valueString "v"}]}}))))

(t/deftest loc-issue
  (t/is
   (=
    ["json"]
    (fp/evaluate
     "$this.extension.where(url='https://genfhi.com/Extension/post-processing').value"
     {:language "application/x-fhir-query",
      :expression "{{%fetchschedules.response}}",
      :extension [{:url  "https://genfhi.com/Extension/post-processing"
                   :valueCode "json"}]}))))

(t/deftest order-operations
  (t/is (=
         '(true)
         (gen-fhi.fhirpath.core/evaluate
          "($this.val - 15 * 2 + 10 = 5) and (%test=5)"
          {:val 25}
          {:options/variables {:test 5}})))
  (t/is (=
         '(false)
         (gen-fhi.fhirpath.core/evaluate
          "($this.val - 15 * 2 + 10 = 5) and (%test=4)"
          {:val 25}
          {:options/variables {:test 5}})))
  (t/is (=
         '(true)
         (gen-fhi.fhirpath.core/evaluate
          "$this.val.nested - %test * 12 + 10 + 55 = 10"
          {:val {:nested 5}}
          {:options/variables {:test 5}})))
  (t/is (=
         '(true)
         (gen-fhi.fhirpath.core/evaluate
          "%test + 4 * 10 = 45"
          nil
          {:options/variables {:test 5}}))))

(t/deftest answers
  (t/is
   (= '({:system "http://cancer.questionnaire.org/system/code/yesno", :code "1", :display "Yes"}
        {:system "http://cancer.questionnaire.org/system/code/yesno", :code "1"}
        {:system "http://cancer.questionnaire.org/system/code/yesno", :code "1"}
        {:system "http://cancer.questionnaire.org/system/code/yesno", :code "0"})
      (gen-fhi.fhirpath.core/evaluate
       "$this.answers().value"
       (json/read-str
        (slurp (io/resource "data/qr-example.json"))
        :key-fn keyword)))))

(t/deftest ordinal-values
  (t/is
   (= '(0 1 2 3)
      (gen-fhi.fhirpath.core/evaluate
       "$this.compose.include.concept.ordinal()"
       (json/read-str
        (slurp (io/resource "data/valueset-example-ordinal.json"))
        :key-fn keyword)))))

(t/deftest aggregations
  (t/is
   (= '(10)
      (gen-fhi.fhirpath.core/evaluate
       "$this.aggregate($this + $total, 0)"
       '(0 1 2 3 4))))
  (t/is
   (= '(2)
      (gen-fhi.fhirpath.core/evaluate
       "$this.aggregate($this + $total, 2)"
       '()))))

(t/deftest aggregations-no-second
  (t/is
   (= '(10)
      (gen-fhi.fhirpath.core/evaluate
       "$this.aggregate($this + $total)"
       '(0 1 2 3 4))))
  (t/is
   (= '()
      (gen-fhi.fhirpath.core/evaluate
       "$this.aggregate($this + $total)"
       '()))))

(t/deftest aggregations-chaining
  (t/is
   (= '(35)
      (gen-fhi.fhirpath.core/evaluate
       "$this.aggregate($this + $total) * 4 - 5"
       '(0 1 2 3 4)))))

(t/deftest factory-test
  (t/is
   (= '({:system "system" :code "code" :display "display" :version "v5"})
      (gen-fhi.fhirpath.core/evaluate
       "%factory.Coding('system','code', 'display', 'v5')")))
  (t/is
   (= '({:system "system" :display "display" :version "v5"})
      (gen-fhi.fhirpath.core/evaluate
       "%factory.Coding('system', {} , 'display', 'v5')"))))

(t/deftest typechoice
  (t/is
   (= {:resourceType "Questionnaire"})
   (gen-fhi.fhirpath.core/evaluate
    "$this.questionnaire"
    {:property/questionnaire {:resourceType "Questionnaire"}
     :property/questionnaireResponse {:resourceType "QuestionnaireResponse"}})))


(t/deftest sum
  (t/is
   (= '(10)
      (gen-fhi.fhirpath.core/evaluate
       "sum()"
       '(0 1 2 3 4)))))

(t/deftest sum-chaining
  (t/is
   (= '(6)
      (gen-fhi.fhirpath.core/evaluate
       "$this.compose.include.concept.ordinal().sum()"
       (json/read-str
        (slurp (io/resource "data/valueset-example-ordinal.json"))
        :key-fn keyword)))))
