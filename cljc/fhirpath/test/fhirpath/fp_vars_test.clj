(ns fhirpath.fp-vars-test
  (:require  [clojure.test :as t]
             [gen-fhi.fhirpath.core :as fp]))

(t/deftest fhirpath.eval
  (t/is (= (fp/->variables
            "%test + %test2")
           #{:test :test2})))
