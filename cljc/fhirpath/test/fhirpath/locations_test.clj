(ns fhirpath.locations-test
  (:require  [clojure.test :as t]
             [gen-fhi.fhirpath.core :as fp]
             [gen-fhi.fhirpath.loc :as l]))

(defn test-equality-loc [expression v]
  (t/is
   (= (fp/evaluate expression v)
      (map #(get-in v %)
           (fp/locations  expression v)))))

(t/deftest fhirpath.array.extension+val
  (t/is
   (= '([:stringArray 2])
      (fp/locations
       "$this.stringArray.where($this='test3')"
       {:_stringArray [{:url "url1"} nil {:url "url2"}]
        :stringArray ["test" nil "test3"]})))
  (t/is
   (= '([:_stringArray 2 :extension 0 :url]
        [:_stringArray 2 :extension 1 :url])
      (fp/locations
       "$this.stringArray.where($this='test3').extension.url"
       {:_stringArray [nil nil {:extension [{:url "url1"} {:url "url2"}]}]
        :stringArray ["test" nil "test3"]}))))

(t/deftest fhirpath.array.extension+val-test-equality
  (let [v {:_stringArray [nil nil {:extension [{:url "url1"} {:url "url2"}]}]
           :stringArray ["test" nil "test3"]}
        expression "$this.stringArray.where($this='test3')"]
    (test-equality-loc expression v)))

(t/deftest fhirpath.descendants
  (let [v {:val [{:v 1} {:v 2}]
           :test {:hello "world"}
           :name {:given ["bob"]}}
        expression "$this.descendants()"]
    (test-equality-loc expression v)))

(t/deftest fhirpath.descendants.filter.extension
  (let [v {:a
           {:b {:_stringArray [{:extension [{:url "please-encrypt-me"}]}
                               nil
                               {:extension [{:url "enc" :valueString "123"}]}]
                :stringArray ["test" nil "test3"]}}}
        expression "$this.descendants().where($this.extension.url='please-encrypt-me')"]
    (test-equality-loc expression v)
    (t/is
     (= '([:a :b :_stringArray 0]
          [:a :b :stringArray 0])
        (fp/locations expression v)))))

(def encryption-url "https://genfhi.com/Extension/encrypt-value")

(t/deftest fhirpath.encrypt.test
  (let [v {:a
           {:b {:_stringArray [{:extension [{:url "https://genfhi.com/Extension/encrypt-value"}]}]
                :stringArray ["test" nil "test3"]}}}
        expression "$this.descendants().where($this.extension.url='https://genfhi.com/Extension/encrypt-value').extension"]
    (test-equality-loc expression v)
    (t/is
     ;; Occurs twice because of descendants
     (= '([:a :b :_stringArray 0 :extension 0] [:a :b :_stringArray 0 :extension 0])
        (fp/locations expression v)))))

(t/deftest fhirpath.primitive.value
  (let [v {:a
           {:b {:_stringArray [{:extension [{:url "https://genfhi.com/Extension/encrypt-value"
                                             :valueString ""}]}]
                :stringArray ["test" nil "test3"]}}}
        expression "$this.descendants().where($this.extension.url='https://genfhi.com/Extension/encrypt-value').value"]
    (test-equality-loc expression v)
    (t/is
     ;; Occurs twice because of descendants
     (= '([:a :b :stringArray 0])
        (fp/locations expression v)))))

(t/deftest fhirpath.primitive.value
  (let [v {:array [{:url "test"} {:url "asdf"} {:url "test"}]}]
    (t/is
     (= '([:array 2])
        (fp/locations "$this.array.where(url='test')[1]" v)))))

(t/deftest fhirpath.multiloc.circular?
  (t/is
   (= true
      (-> (l/->multi-loc)
          (l/next-loc (l/->loc))
          (l/descend "test" nil)
          (l/next-loc (l/->loc))
          (l/descend "test" nil)
          (l/next-loc (l/->loc))
          (l/circular?))))
  (t/is
   (= false
      (-> (l/->multi-loc)
          (l/next-loc (l/->loc))
          (l/descend "test" nil)
          (l/next-loc (l/->loc))
          (l/descend "asdf" nil)
          (l/next-loc (l/->loc))
          (l/circular?)))))

