(ns fhirpath.parser-test
  (:require [clojure.test :as t]
            [gen-fhi.fhirpath.parser :refer [parse]]))

(t/deftest parser-in-avoidance
  (t/is
   (=
    [:EXPRESSION
     [:EXPRESSION
      [:EXPRESSION
       [:EXPRESSION
        [:TERM [:INVOCATION [:THIS]]]]
       [:INVOCATION [:IDENTIFIER :extension]]]
      [:INVOCATION [:FUNCTION [:IDENTIFIER :where]
                    [:PARAM_LIST
                     [:PARAM
                      [:EXPRESSION
                       [:EXPRESSION
                        [:OPERATION [:EXPRESSION [:TERM [:INVOCATION [:IDENTIFIER :url]]]]
                         "="
                         [:EXPRESSION [:TERM [:EXTERNAL_CONSTANT [:IDENTIFIER :codestringurl]]]]]]]]]]]]]
    (parse "$this.extension.where(url = %codestringurl)"))))

(t/deftest parse-test-escaped-string
  (t/is
   (=
    [:EXPRESSION [:EXPRESSION
                  [:EXPRESSION [:EXPRESSION [:TERM [:INVOCATION [:THIS]]]]
                   [:INVOCATION [:IDENTIFIER :extension]]]
                  [:INVOCATION [:FUNCTION [:IDENTIFIER :where]
                                [:PARAM_LIST
                                 [:PARAM
                                  [:EXPRESSION
                                   [:EXPRESSION
                                    [:OPERATION [:EXPRESSION [:TERM [:INVOCATION [:IDENTIFIER :url]]]]
                                     "="
                                     [:EXPRESSION [:TERM [:EXTERNAL_CONSTANT [:IDENTIFIER :codestringurl]]]]]]]]]]]]]
    (parse "$this.`extension`.where(url = %codestringurl)"))))

(t/deftest parser-concatenation
  (t/is
   (=
    [:EXPRESSION
     [:EXPRESSION
      [:OPERATION
       [:EXPRESSION [:EXPRESSION [:EXPRESSION [:EXPRESSION [:EXPRESSION [:TERM [:EXTERNAL_CONSTANT [:IDENTIFIER :querylist]]]] [:INVOCATION [:IDENTIFIER :state]]] [:INVOCATION [:IDENTIFIER :selected]]] [:INVOCATION [:IDENTIFIER :name]]] [:INVOCATION [:IDENTIFIER :text]]]
       "+"
       [:EXPRESSION [:TERM [:LITERAL [:STRING " "]]]]
       "+"
       [:EXPRESSION
        [:EXPRESSION [:EXPRESSION [:EXPRESSION [:EXPRESSION [:TERM [:EXTERNAL_CONSTANT [:IDENTIFIER :querylist]]]] [:INVOCATION [:IDENTIFIER :state]]] [:INVOCATION [:IDENTIFIER :selected]]]
         [:INVOCATION [:IDENTIFIER :name]]]
        [:INVOCATION [:IDENTIFIER :text]]]]]]
    (parse "%querylist.state.selected.name.text + ' ' + %querylist.state.selected.name.text"))))

(t/deftest deliminated-identifier-variable
  (t/is
   (=
    [:EXPRESSION
     [:EXPRESSION
      [:EXPRESSION
       [:TERM
        [:EXTERNAL_CONSTANT
         [:IDENTIFIER :test-123]]]]
      [:INVOCATION [:IDENTIFIER :field]]]]
    (parse "%`test-123`.field"))))

(t/deftest order-operations
  (t/is
   (=
    [:EXPRESSION [:EXPRESSION [:OPERATION [:EXPRESSION [:TERM [:LITERAL [:NUMBER 5]]]]
                               "+"
                               [:EXPRESSION
                                [:OPERATION [:EXPRESSION [:TERM [:LITERAL [:NUMBER 2]]]]
                                 "*"
                                 [:EXPRESSION [:TERM [:LITERAL [:NUMBER 10]]]]]]
                               "-"
                               [:EXPRESSION [:TERM [:LITERAL [:NUMBER 2]]]]]]]
    (parse "5 + 2 * 10 - 2"))))

(t/deftest order-ops-with-extern
  (t/is
   (=
    [:EXPRESSION [:EXPRESSION [:OPERATION
                               [:EXPRESSION
                                [:OPERATION
                                 [:EXPRESSION [:TERM [:EXTERNAL_CONSTANT [:IDENTIFIER :test]]]]
                                 "+"
                                 [:EXPRESSION
                                  [:OPERATION [:EXPRESSION [:TERM [:LITERAL [:NUMBER 4]]]]
                                   "*"
                                   [:EXPRESSION [:TERM [:LITERAL [:NUMBER 10]]]]]]]]
                               "="
                               [:EXPRESSION [:TERM [:LITERAL [:NUMBER 45]]]]]]]
    (parse "%test + 4 * 10 = 45"))))

(t/deftest order-ops-bool
  (t/is
   (=
    [:EXPRESSION [:EXPRESSION
                  [:OPERATION
                   [:EXPRESSION
                    [:OPERATION [:EXPRESSION [:EXPRESSION [:TERM [:INVOCATION [:THIS]]]] [:INVOCATION [:IDENTIFIER :val]]] "-" [:EXPRESSION [:OPERATION [:EXPRESSION [:TERM [:LITERAL [:NUMBER 15]]]] "*" [:EXPRESSION [:TERM [:LITERAL [:NUMBER 2]]]]]] "+"
                     [:EXPRESSION [:TERM [:LITERAL [:NUMBER 10]]]]]]
                   "="
                   [:EXPRESSION
                    [:OPERATION
                     [:EXPRESSION [:TERM [:LITERAL [:NUMBER 5]]]] "and"
                     [:EXPRESSION
                      [:TERM [:LITERAL [:BOOLEAN true]]]]]]]]]
    (parse "$this.val - 15 * 2 + 10 = 5 and true"))))

