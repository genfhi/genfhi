(ns gen-fhi.interceptors.core
  #?(:cljs
     (:require-macros [async-errors.core :refer [go-try <?]]))
  (:require
   #?(:clj [async-errors.core :refer [go-try <?]])
   #?(:cljs [cljs.core.async.impl.channels :refer [ManyToManyChannel]])
   [clojure.spec.alpha :as s])
  (:import #?(:clj  [clojure.core.async.impl.channels ManyToManyChannel])))

;; Notes
;; ----------------------------------------------------------------------------------------------------------------
;; - DataModel
;; -- Interceptor data {:enter fn :exit fn :error fn}

;; - Execution framework
;; -- For each interceptor encountered push it onto a dequeu stack. (note this only occurs during processing).
;; -- During deque start by placing dequeu stack onto req reversed and execute in same fashion as entry processing.

;; - Additional Features
;; -- Also needs to support asynchronicity
;; -- Need support for error dequeuing.
;; ----------------------------------------------------------------------------------------------------------------

(defrecord InterceptorError [ctx ex error-stack])

(s/def :interceptors/dir   #{:enter :exit :error})

(s/def :interceptors/enter fn?)

(s/def :interceptors/exit  fn?)

(s/def :interceptors/error fn?)

(s/def :interceptors/interceptor (s/keys :opt-un [:interceptors/enter :interceptors/exit]))

(s/def :interceptors/interceptors (s/coll-of :interceptors/interceptor))

(defn is-async?
  "Determine whether channel value for processing."
  [value]
  (instance? ManyToManyChannel value))

(defn process-sub
  "Handles case of ctx being async or value returned being async.
  In either case returns async/channel ctx. Otherwise returns map with associated value."
  [ctx key value-fn]
  (if (is-async? ctx)
    (go-try
     (let [ctx   (<? ctx)
           res   (value-fn (get ctx key))
           value (if (is-async? res) (<? res) res)]
       (assoc ctx key value)))
    (let [res (value-fn (get ctx key))]
      (if (is-async? res)
        (go-try
         (let [value (<? res)]
           (assoc ctx key value)))
        (assoc ctx key res)))))

(defn alt-value
  "Helper function to modify just ctx value. Will merge result with unchanged interceptor chain.
  parameter: value"
  [value-fn]
  (fn [ctx]
    (process-sub ctx :interceptors/value value-fn)))

(defn alt-interceptors
  "Helper function to modify just interceptors.
  parameter: interceptors"
  [interceptor-fn]
  (fn [ctx]
    (process-sub ctx :interceptors/interceptors interceptor-fn)))

(defn alt-exception
  [exception-fn]
  (fn [ctx]
    (process-sub ctx :interceptors/exception exception-fn)))

(defn- execute-interceptor
  "Executes the first interceptor on the ctx returning the response and interceptor that processed the next ctx"
  [ctx dir]
  (let [ctx-interceptors (:interceptors/interceptors ctx)
        interceptor      (first ctx-interceptors)
        interceptor-fn   (interceptor dir)
        ctx-without-int  (assoc ctx :interceptors/interceptors (rest ctx-interceptors))]
    (if interceptor-fn
      (interceptor-fn ctx-without-int)
      ctx-without-int)))

(def InterceptorErrorMessage "InterceptorException")

(defn- move-to-error [{:keys [ctx dir enqueu-stack dequeu-stack exception]}]
  [(assoc ctx :interceptors/exception exception)
   (cond
     (= dir :exit)  enqueu-stack
     (= dir :enter) @dequeu-stack)
   true])

(defn- execute-queue
  "Executes the queue of interceptors on the ctx (Note this queue can change from the interceptor being processed).
  Returns ctx+queue-to-process and whether is error."
  ([ctx dir] (execute-queue ctx dir (:interceptors/interceptors ctx) '()))
  ([ctx dir enqueu-stack dequeu-stack]
   ;; Holding on atom for errors
   (let [dequeu-stack (atom dequeu-stack)]
     (if (empty? (:interceptors/interceptors ctx))
       [ctx @dequeu-stack false]
       (do
         (swap! dequeu-stack conj (first (:interceptors/interceptors ctx)))
         (let [next-ctx (execute-interceptor ctx dir)]
           (if (is-async? next-ctx)
             (go-try
              (let [next-ctx (<? next-ctx)
                    ret (execute-queue next-ctx dir (:interceptors/interceptors next-ctx) @dequeu-stack)]
                (if (is-async? ret)
                  (let [return (<? ret)]
                    return)
                  ret)))
             (execute-queue next-ctx dir (:interceptors/interceptors next-ctx) @dequeu-stack))))))))

(defn- ctx+dequeu->ctx-val [ctx+dequeu]
  (if (is-async? ctx+dequeu)
    (go-try
     (let [res (<? ctx+dequeu)]
       (:interceptors/value (first res))))
    (let [[ctx _]  ctx+dequeu]
      (:interceptors/value ctx))))

(defn ->interceptor
  "Instantiates a function that takes collection of interceptors and executes+initiates them against a ctx."
  [initial-interceptors]
  {:pre [(s/valid? :interceptors/interceptors initial-interceptors)]}
  (fn [value]
    (let [enter-res  (execute-queue
                      {:interceptors/value value :interceptors/interceptors initial-interceptors}
                      :enter)

          exit-res   (if (is-async? enter-res)
                       (go-try
                        (let [[ctx dequeue-stack err?] (<? enter-res)]
                          (execute-queue
                           (assoc ctx :interceptors/interceptors dequeue-stack)
                           (if err? :error :exit))))
                       (let [[ctx dequeue-stack err?] enter-res]
                         (execute-queue
                          (assoc ctx :interceptors/interceptors dequeue-stack)
                          (if err? :error :exit))))

          final-res  (if (is-async? exit-res)
                       (go-try
                        (let [[ctx dequeue-stack err? :as res] (<? exit-res)]
                          (if (not err?)
                            res
                            (execute-queue
                             (assoc ctx :interceptors/interceptors dequeue-stack)
                             :error))))
                       (let [[ctx dequeue-stack err? :as res] exit-res]
                         (if (not err?)
                           res
                           (execute-queue
                            (assoc ctx :interceptors/interceptors dequeue-stack)
                            :error))))]

      (ctx+dequeu->ctx-val final-res))))
