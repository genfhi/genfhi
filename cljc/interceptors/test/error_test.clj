(ns error-test
  (:require  [clojure.test :as t]
             [async-errors.core :as ae]
             [gen-fhi.interceptors.core :as it]))

(t/deftest interceptor.async.error-test
  (let [interceptor (it/->interceptor
                     [{:exit (it/alt-value
                              (fn [v]
                                (ae/go-try
                                 (throw (Exception. "induced failures"))
                                 (update v :value dec))))}

                      {:enter (it/alt-value
                               (fn [v]
                                 (ae/go-try
                                  (update v :value inc))))
                       :exit (it/alt-value
                              (fn [v]
                                (update v :value dec)))}])]
    (t/is
     (thrown?
      Exception
      (ae/<?? (interceptor {}))))))
