(ns interceptor-test
  (:require  #?(:clj [clojure.test :as t]
                :cljs [cljs.test :as t :include-macros true])
             #?(:clj  [clojure.core.async :as a]
                :cljs [cljs.core.async :as a])
             [gen-fhi.interceptors.core :as it]
             [async-errors.core :as ae]))

(t/deftest interceptor.sync-test
  (let [interceptor (it/->interceptor
                     [{:id    :id-1
                       :enter (it/alt-value
                               (fn [v] (assoc v :value 1)))
                       :exit (it/alt-value
                              (fn [v] (update v :value dec)))}
                      {:id    :id-2
                       :enter (it/alt-value
                               (fn [v] (update v :value inc)))
                       :exit (it/alt-value
                              (fn [v] (update v :value dec)))}
                      {:id    :id-3
                       :enter (it/alt-value
                               (fn [v] (update v :value inc)))
                       :exit  (it/alt-value
                               (fn [v] (update v :value #(* 2 %))))}])]
    (t/is (=
           {:value 4}
           (interceptor {})))))

(t/deftest interceptor.sync-test.error-on-exit
  (let [interceptor (it/->interceptor
                     [{:enter (it/alt-value
                               (fn [v] (assoc v :value 1)))

                       :exit (it/alt-value
                              (fn [v] (update v :value dec)))}

                      {:enter (it/alt-value
                               (fn [v] (update v :value inc)))

                       :exit (it/alt-value
                              (fn [v]
                                (throw (ex-info "induced failure" {}))
                                (update v :value dec)))}

                      {:enter (it/alt-value
                               (fn [v] (update v :value inc)))

                       :exit  (it/alt-value
                               (fn [v]
                                 (update v :value #(* 2 %))))}])]
    (t/is (thrown? Exception (interceptor {})))))

(t/deftest interceptor.sync-test.error-on-enter
  (let [interceptor (it/->interceptor
                     [{:enter (it/alt-value
                               (fn [v] (assoc v :value 1)))

                       :exit (it/alt-value
                              (fn [v] (update v :value dec)))}

                      {:enter (it/alt-value
                               (fn [v]
                                 (update v :value inc)))

                       :exit (it/alt-value
                              (fn [v]
                                (update v :value dec)))}

                      {:enter (it/alt-value
                               (fn [v]
                                 (throw (ex-info "induced failure" {}))
                                 (update v :value inc)))

                       :exit  (it/alt-value
                               (fn [v]
                                 (update v :value #(* 2 %))))}])]
    (t/is (thrown? Exception (interceptor {})))))

(t/deftest interceptor.async
  (let [interceptor (it/->interceptor
                     [{:enter (it/alt-value
                               (fn [v] (a/to-chan [{:value 1}])))

                       :exit (it/alt-value
                              (fn [v] (update v :value dec)))}

                      {:enter (it/alt-value
                               (fn [v]
                                 (update v :value inc)))

                       :exit (it/alt-value
                              (fn [v]
                                (update v :value dec)))}])]
    (t/is {:value 0}
          (ae/<?? (interceptor {})))))


(t/deftest interceptor.async.error
  (let [interceptor (it/->interceptor
                     [{:enter (it/alt-value
                               (fn [v] (a/to-chan [{:value 1}])))
                       :exit (it/alt-value
                              (fn [v] (update v :value dec)))}
                      {:enter (it/alt-value
                               (fn [v]
                                 (ae/go-try
                                  (throw (Exception. "induced failures"))
                                  (update v :value inc))))
                       :exit (it/alt-value
                              (fn [v]
                                (update v :value dec)))}])]
    (t/is (thrown? Exception (ae/<?? (interceptor {}))))))
