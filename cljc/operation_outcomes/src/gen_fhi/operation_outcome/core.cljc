(ns gen-fhi.operation-outcome.core
  (:require [clojure.spec.alpha :as spec]))

(defn throw-outcome [oo]
  (throw (ex-info "OperationOutcome" oo)))

(defn outcome [issues]
  {:resourceType "OperationOutcome"
   :issue issues})

(defn issue
  ([severity code diagnostics]
   (issue severity code diagnostics nil))
  ([severity code diagnostics expression]
   (cond-> {:severity   severity
            :code       code
            :diagnostics diagnostics}
     (some? expression) (assoc :expression expression))))

(defn issue-error
  ([type diagnostics]
   (issue "error" type diagnostics))
  ([type diagnostics expression]
   (issue "error" type diagnostics expression)))

(defn issue-fatal
  ([type diagnostics]
   (issue "fatal" type diagnostics))
  ([type diagnostics expression]
   (issue "fatal" type diagnostics expression)))

(defn issue-warning
  ([type diagnostics]
   (issue "warning" type diagnostics))
  ([type diagnostics expression]
   (issue "warning" type diagnostics expression)))

(defn issue-information
  ([type diagnostics]
   (issue "information" type diagnostics))
  ([type diagnostics expression]
   (issue "information" type diagnostics expression)))

(defn outcome-error
  ([type diagnostics]
   (outcome-error type diagnostics nil))
  ([type diagnostics expression]
   (outcome [(issue-error type diagnostics expression)])))

(defn outcome-warning
  ([type diagnostics]
   (outcome-warning type diagnostics nil))
  ([type diagnostics expression]
   (outcome [(issue-warning type diagnostics expression)])))

(defn outcome-fatal
  ([type diagnostics]
   (outcome-fatal type diagnostics nil))
  ([type diagnostics expression]
   (outcome [(issue-fatal type diagnostics expression)])))

(defn outcome-information
  ([type diagnostics]
   (outcome-information type diagnostics nil))
  ([type diagnostics expression]
   (outcome [(issue-information type diagnostics expression)])))

;; Because cannot use :human meta on (spec/keys) search for regex pred which looks for a required key.
(def missing-item-regex #"\(clojure.core/fn\s*\[\%\]\s*\(clojure.core/contains\?\s*\%\s*\:([a-zA-Z].*)\s*\)\s*\)\s*")

(defn outcome-validation [spec-keyword data]
  (try
    (spec/assert spec-keyword data)
    (catch Exception ex
      (let [{:keys [:clojure.spec.alpha/problems] :as spec-data} (ex-data ex)]
        (let [{:keys [in pred] :as problem} (first problems)
              ;; Eval the form of the pred to get any metadata off of it (can be wrapped in with-meta)
              ;; If not there use pprint of string as pred info.
              pred-string                   (or (:human (meta (eval pred)))
                                                (let [pred-string (with-out-str (clojure.pprint/pprint pred))]
                                                  (if-let [match (re-matches missing-item-regex pred-string)]
                                                    (str "Missing required key: '" (second match) "'")
                                                    pred-string)))]
          (throw-outcome
           (outcome-error
            "invalid"
            (str "Invalid at: '"
                 (str "/" (clojure.string/join "/" (map (fn [v] (if (keyword? v) (name v) v)) in)))
                 "' \n '" pred-string "'"))))))))
