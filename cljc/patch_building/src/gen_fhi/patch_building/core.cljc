(ns gen-fhi.patch-building.core
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [clj-json-patch.core :as patch]
            [gen-fhi.patch-building.path :as p]
            [gen-fhi.patch-building.utils :as utils]
            [taoensso.timbre :as timbre]))

(s/def :json-patch/op #{"add" "remove" "replace" "copy" "move" "test"})
(s/def :json-patch/path string?)
(s/def :json-patch/from string?)
(s/def :json-patch/value any?)

(defmulti json-patch :op)
(defmethod json-patch "add" [_]
  (s/keys :req-un [:json-patch/op :json-patch/path :json-patch/value]))
(defmethod json-patch "remove" [_]
  (s/keys :req-un [:json-patch/op :json-patch/path]))
(defmethod json-patch "replace" [_]
  (s/keys :req-un [:json-patch/op :json-patch/path :json-patch/value]))
(defmethod json-patch "copy" [_]
  (s/keys :req-un [:json-patch/op :json-patch/path :json-patch/from]))
(defmethod json-patch "move" [_]
  (s/keys :req-un [:json-patch/op :json-patch/path :json-patch/from]))
(defmethod json-patch "test" [_]
  (s/keys :req-un [:json-patch/op :json-patch/path :json-patch/value]))

(s/def :json-patch/patch (s/multi-spec json-patch :op))
(s/def :json-patch/patches (s/coll-of :json-patch/patch))

(defn- string-field->resource-field [str-field]
  (if (utils/is-number? str-field)
    (utils/parse-int str-field)
    (keyword (p/unescape-field str-field))))

(defn- value-exists-at-json-pointer? [resource json-pointer]
  (let [fields (rest (map string-field->resource-field (string/split json-pointer #"/")))]
    (loop [ctx   resource
           idx   0]
      (if (= idx (count fields))
        (some? ctx)
        (if-let [new-ctx (get ctx (nth fields idx))]
          (recur new-ctx (inc idx))
          false)))))

(defn- ->next-value
  "will return either :array or :map for next auto-created type depending on next field."
  [fields]
  (cond
    ;; Map means it's a search for a value if can't find value than utilize the map as the default during building.
    (map? (first fields))              (first fields)
    ;; If number means next sequence array
    (utils/is-number? (second fields)) []
    ;; Map means filter is being applied so should be array.
    (map? (second fields))             []
    (= :- (second fields))            []
    :else                              {}))

(defn- create-patches-non-existant-fields
  "For deeply nested pointers build out the nested pieces."
  [resource path]
  (let [{:keys [resource-type id]} (p/path-meta path)]
    (loop [fields   (p/fields path)
           cur-path (p/->path resource-type id)
           patches  []]
      (if (empty? fields)
        patches
        (let [cur-path    (p/descend cur-path (first fields))
              cur-pointer (p/to-jsonpointer cur-path resource)]
          (if (value-exists-at-json-pointer? resource cur-pointer)
            (recur
             (rest fields)
             cur-path
             patches)
            (let [next-value (->next-value fields)]
              (recur
               (rest fields)
               cur-path
               (conj patches {:op "add" :path cur-pointer :value next-value})))))))))

(s/def ::type #{:add :remove :replace :move})
(s/def ::path #(satisfies? p/Path %))
(s/def ::from #(satisfies? p/Path %))
(s/def ::value any?)

(s/def ::mutation (s/keys :req-un [::type ::path]
                          :opt-un [::value ::from]))

(s/fdef build-patches
  :args (s/cat :object map? :mutation ::mutation)
  :ret (s/coll-of :json-patch/patches))

(defn- get-value
  "get value using jsonpath"
  [resource json-pointer]
  (let [fields (into [] (rest (map (fn [f] (if (utils/is-number? f)
                                             (utils/parse-int f)
                                             (keyword f)))
                                   (string/split json-pointer #"/"))))]
    (loop [value resource
           fields fields]
      (if (empty? fields)
        value
        (if (nil? value)
          nil
          (recur
           (cond
             (and (or (vector? value) (seq? value))
                  (utils/is-number? (first fields))
                  (> (count value) (first fields)))
             (nth value (first fields))
             (map? value)                            (get value (first fields))
             :else                                   nil)
           (rest fields)))))))

(defn- shared-parent
  "return the shared vec between p1 and p2"
  [p p2]
  (loop [parent []
         fields (map (fn [x y] [x y]) p p2)]
    (if (and (seq fields)
             (= (first (first fields)) (second (first fields))))
      (recur
       (conj parent (first (first fields)))
       (rest fields))
      parent)))

(defn- movement-patches [path-from path-to resource]
  [{:op   "remove"
    :path (p/to-jsonpointer path-from resource)}
   {:op    "add"
    :path  (p/to-jsonpointer   path-to resource)
    :value (get-value resource path-from)}])

(defn- generate-move-patches [mutation resource]
  (assert (= :move (:type mutation)) "Generate move called with non move mutation")
  (let [from-fields        (into [] (p/fields (:from mutation)))
        to-fields          (into [] (p/fields (:path mutation)))
        parent             (shared-parent to-fields from-fields)
        unique-from-fields (subvec from-fields (count parent) (count from-fields))
        unique-to-fields   (subvec to-fields   (count parent) (count to-fields))]

    ;; If shared parent and first diff is index means altering array and custom logic needed.
    (if (and (not-empty parent)
             (int? (first unique-from-fields))
             (int? (first unique-to-fields)))

      ;; IF moving from deeply nested to more shallow indices will be maintained on shallow
      (if (and (<= (count from-fields) (count to-fields))
               (< (first unique-from-fields) (first unique-to-fields)))
        (movement-patches (:from mutation) (apply p/extend-path (p/path->root (:path mutation))
                                                  (concat
                                                   parent
                                                   (assoc unique-to-fields
                                                          0
                                                          (- (get unique-to-fields 0) 1))))
                          resource)
        (movement-patches (:from mutation) (:path mutation) resource))

      (movement-patches (:from mutation) (:path mutation) resource))))

(defn- generate-removes [resource json-pointer]
  (let [fields  (rest (map string-field->resource-field (string/split json-pointer #"/")))
        fields  (into [] fields)
        patches [{:op "remove" :path json-pointer}]]
    (loop [patches     patches
           fields      (pop fields)
           resource    (patch/patch resource patches  true)]
      (if (and (not-empty fields) (empty? (get-in resource fields)))
        (let [patch {:op "remove"
                     :path (str "/" (string/join "/" (map #(if (keyword? %) (name %) %) fields)))}]
          (recur
           (conj patches patch)
           (pop fields)
           (patch/patch resource [patch]  true)))

        patches))))

(defn build-patches
  "Given a mutation object build out the json patches neccessary to satisfy it.
  This includes building out non-existant fields along the path."
  [resource mutation]
  (let [path         (:path mutation)
        json-pointer (p/to-jsonpointer path resource)]
    (cond
      (= :remove (:type mutation))

      (if (some? (get-value resource json-pointer))
        (generate-removes resource json-pointer)
        (do
          (timbre/error (str "attempted to remove non existing item at '" (:path mutation) "'"))
          []))

      ;; For add and replace using same logic at the end to just replace auto created value.
      (= :replace (:type mutation))
      ;; If replacing to a nil or empty string than that means removal
      (if (or (nil? (:value mutation)) (= "" (:value mutation)))
        (build-patches resource {:type :remove
                                 :path (:path mutation)})
        (conj (create-patches-non-existant-fields resource path)
              {:op "replace" :path json-pointer :value (:value mutation)}))

      (= :add     (:type mutation))
      (let [patches (create-patches-non-existant-fields resource path)]
        (if (and (= "add" (:op (last patches)))
                 (= json-pointer (:path (last patches))))
          ;; If last is adding value remove here as collection will only add once.
          (into [] (concat
                    (pop patches)
                    [{:op "add" :path json-pointer :value (:value mutation)}]))
          (conj patches
                {:op "add" :path json-pointer :value (:value mutation)})))

      (= :move    (:type mutation))
      (let [patches (create-patches-non-existant-fields resource (:path mutation))]
        (if (and (= "add" (:op (last patches)))
                 (= json-pointer (:path (last patches))))
          ;; If last is adding value remove here as collection will only add once.
          (into [] (concat (pop patches)
                           (generate-move-patches mutation resource)))
          (concat
           patches
           (generate-move-patches mutation resource))))

      :else (throw (ex-info (str "Unkown type " (:type mutation)) {})))))
