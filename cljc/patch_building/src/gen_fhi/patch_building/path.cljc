(ns gen-fhi.patch-building.path
  "Extends JSON Pointer RFC6901 with creation syntax."
  (:require [clojure.string :as string]
            [gen-fhi.patch-building.utils :as utils]
            #?(:cljs [cljs.reader :refer [read-string]]
               :clj  [clojure.core :refer [read-string]])))

(defprotocol Path
  (path-meta      [this])
  (fields         [this])
  (to-expression  [this])
  (to-jsonpointer [this resource])
  (descend        [this field])
  (ascend         [this]))

(defn escape-field
  "See [https://datatracker.ietf.org/doc/html/rfc6901#section-3] for reference."
  [field]
  (-> (str (if (keyword? field) (name field) field))
      (string/replace #"~" "~0")
      (string/replace #"/" "~1")))

(defn unescape-field [field]
  ;; Because ordering matters on replacing ~ manually do it here
  (-> (str field)
      (string/replace #"\~1" "/")
      (string/replace #"\~0" "~")))

(defn- ->root-indice [path]
  (or (string/index-of path "/") (count path)))

(defn- search-map->search-string [search-field]
  (str
   "#["
   (string/join
    ","
    (map
     (fn [field-name]
       (str (escape-field (name field-name))
            "="
            (escape-field (pr-str (get search-field field-name)))))
     (keys search-field)))
   "]"))

(defn- search-field? [field]
  (and (string/starts-with? field "#[")
       (string/ends-with? field "]")))

(defn- search-string->search-map [search-string]
  ;; First field is #[ and last is ]
  (let [search-content (subs search-string 2 (- (count search-string) 1))
        search-parameters (reduce
                           (fn [acc field-and-value]
                             (let [[field-name value] (string/split field-and-value #"=")]
                               (assoc
                                acc
                                (keyword (unescape-field field-name))
                                (read-string (unescape-field value)))))
                           {}
                           (string/split search-content #","))]
    search-parameters))

;; Used in value.cljs to resolve searches
(defn field->resolved-field [field value]
  (cond
    (map? field) (or
                  (first
                   (first
                    (filter
                     (fn [[_index value]]
                       (reduce
                        (fn [t search-key]
                          (and t (= (str (get field search-key))
                                    (str (get value search-key)))))
                        true
                        (keys field)))
                     (map-indexed vector value))))

                  ;; If non existant return ending indice.
                  (count value))
    :else         field))

(defn- path->field-vec [path]
  (loop [fields  '()
         path    path]
    (if-let [{:keys [parent field]} (ascend path)]
      (recur (conj fields field)
             parent)
      fields)))

(defn- map-field->where-clause [field-map]
  (str
   "where("
   (string/join
    " and "
    (map
     (fn [[key val]]
       (str
        (name key)
        "="
        (if (string? val)
          (str "'" val "'")
          val)))
     field-map))
   ")"))

(defn- field->expression-field [field]
  (cond
    (map? field)     (str "." (map-field->where-clause field))
    (keyword? field) (str "." (name field))
    (number? field)  (str "[" field "]")
    :else            (str "." field)))

(defn- path->expression [path]
  (let [{:keys [resource-type ]} (path-meta path)]
   (reduce
    (fn [expression field]
      (str expression (field->expression-field field)))
    (if resource-type
      resource-type
      "$this")
    (fields path))))

(extend-type
 #?(:cljs string
    :clj  java.lang.String)
  Path
  (path-meta  [path]
    (let [meta-data          (subs path 0 (->root-indice path))
          [resource-type id] (string/split meta-data #"\|")]
      {:resource-type resource-type
       :id            id}))

  (fields [path]
    ;; Altering from keyword to map
    (path->field-vec path))

  (to-expression [path]
    (path->expression path))
  
  (to-jsonpointer [path value]
    (let [fields (path->field-vec path)]
      (loop [json-pointer ""
             fields       fields
             value        value]
        (if (empty? fields)
          json-pointer
          (let [json-path-field (field->resolved-field (first fields) value)]
            (recur
             (str json-pointer "/"
                  (if (= :- json-path-field)
                    (count value)
                    (if (keyword? json-path-field)
                      (name json-path-field)
                      json-path-field)))
             (rest fields)
             (if (sequential? value)
               (when (integer? json-path-field)
                 (when (< json-path-field (count value))
                   (nth value json-path-field)))
               (get value json-path-field))))))))

  (descend [path field]
    (str
     path
     "/"
     (cond
       (map? field) (search-map->search-string field)
       :else        (escape-field field))))

  (ascend [path]
    (when-let [slash-loc (string/last-index-of path "/")]
      (let [parent-path (subs path 0 slash-loc)
            field       (->
                         (subs path (+ 1 slash-loc))
                         unescape-field)]

        {:parent parent-path
         :field  (if (search-field? field)
                   (search-string->search-map field)
                   (if (utils/is-number? field)
                     (utils/parse-int field)
                     (keyword field)))}))))

(defn- resource-type+id->path [resource-type id]
  (str (name resource-type) "|" id))

(defn extend-path [path & fields]
  (reduce
   (fn [path field] (descend path field))
   path
   fields))

(defn ->path
  "Create a path."
  [resource-type id & fields]
  (assert (some? resource-type) "Must have resource-type for path creation.")
  (assert (some? id) "Must have id for path creation.")
  (let [path (resource-type+id->path resource-type id)]
    (reduce
     (fn [path field] (descend path field))
     path
     fields)))

(defn path->root [path]
  (let [{:keys [resource-type id]} (path-meta path)]
    (->path resource-type id)))

(defn reference->path [reference]
  (let [ref-pieces (clojure.string/split (get reference :reference) #"/")]
    (when (= 2 (count ref-pieces))
      (->path (first ref-pieces) (second ref-pieces)))))
