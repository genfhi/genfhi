(ns gen-fhi.patch-building.utils)

(defn parse-int
  "Attempts to parse string to integer if it fails returns nil."
  [v]
  #?(:clj  (try
             (if (number? v)
               v
               (Integer/parseInt v))
             (catch Exception _ex
               nil))
     :cljs (if (number? v)
             v
             (let [result (js/parseInt v)]
               (if (js/isNaN result)
                 nil
                 result)))))

(defn is-number? [field]
  (or
   (number? field)
   (some? (parse-int field))))
