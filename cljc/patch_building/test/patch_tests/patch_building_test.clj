(ns patch-tests.patch-building-test
  (:require [clojure.test :as t]
            [clj-json-patch.core :as patch]

            [gen-fhi.patch-building.path :as path]
            [gen-fhi.patch-building.core :as pb]))

(def resource {:resourceType "Patient" :name [{:first "Bob" :family ["Elm"]}]})

(t/deftest patch-building-existing-parents
  (let [mutation {:type   :replace
                  :path  (path/->path "Patient" "123" "name" 0 "first")
                  :value "John"}]
    (t/is (= [{:op "replace", :path "/name/0/first", :value "John"}]
             (pb/build-patches resource mutation)))))

(t/deftest patch-building-nonexisting-parents-replace
  (let [mutation {:type   :replace
                  :path  (path/->path "Patient" "123" "name-fake" 0 "first")
                  :value "John"}]
    (t/is (= [{:op "add", :path "/name-fake", :value []}
              {:op "add", :path "/name-fake/0", :value {}}
              {:op "add", :path "/name-fake/0/first", :value {}}
              {:op "replace", :path "/name-fake/0/first", :value "John"}]
             (pb/build-patches resource mutation)))

    (t/is (= {:name-fake [{:first "John"}]}
             (patch/patch
              {}
              (pb/build-patches resource mutation)
              true)))))

(t/deftest patch-building-nonexisting-parents-add
  (let [mutation {:type   :add
                  :path  (path/->path "Patient" "123" "name-fake" 0 "first")
                  :value "John"}]
    (t/is (= [{:op "add", :path "/name-fake", :value []}
              {:op "add", :path "/name-fake/0", :value {}}
              {:op "add", :path "/name-fake/0/first", :value "John"}]
             (pb/build-patches resource mutation)))

    (t/is (= {:name-fake [{:first "John"}]}
             (patch/patch
              {}
              (pb/build-patches resource mutation)
              true)))))

(t/deftest test-remove
  (t/is
   (=
    []
    (pb/build-patches
     resource
     {:type   :remove
      :path  (path/->path "Patient" "123" 0 "z")})))
  (t/is
   (=
    []
    (pb/build-patches
     resource
     {:type   :remove
      :path  (path/->path "Patient" "123" "name" "test")})))
  (t/is
   (=
    []
    (pb/build-patches
     resource
     {:type   :remove
      :path  (path/->path "Patient" "123" "what" 0 "test")})))
  (t/is
   (=
    [{:op "remove" :path "/name/0/first"}]
    (pb/build-patches
     resource
     {:type   :remove
      :path  (path/->path "Patient" "123" "name" 0 "first")}))))

(t/deftest patch-building-meta-filter
  (let [resource {:question [{:id 4} {:id 6} {:id 7}]}
        mutation {:type   :replace
                  :path  (path/->path "Patient" "123" "question" {:id 5} "value")
                  :value "new-value"}]

    (t/is (= [{:op "add", :path "/question/3", :value {:id 5}}
              {:op "add", :path "/question/3/value", :value {}}
              {:op "replace", :path "/question/3/value", :value "new-value"}]
             (pb/build-patches resource mutation)))

    (t/is (= {:question [{:id 4} {:id 6} {:id 7} {:id 5 :value "new-value"}]}
             (patch/patch
              resource
              (pb/build-patches resource mutation)
              true)))))

(t/deftest patch-building-meta-filter-exists
  (let [resource {:question [{:id 4} {:id 5 :value "existing-value"} {:id 7}]}
        mutation {:type   :replace
                  :path  (path/->path "Patient" "123" "question" {:id 5} "value")
                  :value "new-value"}]

    (t/is (= [{:op "replace", :path "/question/1/value", :value "new-value"}]
             (pb/build-patches resource mutation)))

    (t/is (= {:question [{:id 4} {:id 5 :value "new-value"} {:id 7}]}
             (patch/patch
              resource
              (pb/build-patches resource mutation)
              true)))))

(t/deftest patch-building-meta-non-existant-array
  (let [resource {}
        mutation {:type   :replace
                  :path  (path/->path "Patient" "123" "question" {:id 5} "value")
                  :value "new-value"}]

    (t/is (= [{:op "add"      :path "/question" :value []}
              {:op "add"      :path "/question/0" :value {:id 5}}
              {:op "add"      :path "/question/0/value" :value {}}
              {:op "replace", :path "/question/0/value", :value "new-value"}]
             (pb/build-patches resource mutation)))

    (t/is (= {:question [{:id 5 :value "new-value"}]}
             (patch/patch
              resource
              (pb/build-patches resource mutation)
              true)))))

(t/deftest patch-building-add-end-of-array
  (let [resource {}
        mutation {:type   :add
                  :path  (path/->path "QuestionnaireResponse" "123" "item" {:linkId "5"} "item" "-")
                  :value {:linkId "5.1"}}]

    (t/is (= [{:op "add"      :path "/item" :value []}
              {:op "add"      :path "/item/0" :value {:linkId "5"}}
              {:op "add"      :path "/item/0/item" :value []}
              {:op "add",     :path "/item/0/item/0", :value {:linkId "5.1"}}]
             (pb/build-patches resource mutation)))

    (t/is (= {:item [{:linkId "5" :item [{:linkId "5.1"}]}]}
             (patch/patch
              resource
              (pb/build-patches resource mutation)
              true)))))

(t/deftest patch-building-add-end-of-existing-array
  (let [resource {:item [{:linkId "1"} {:linkId "2" :item [{:linkId "2.1"} {:linkId "2.2"}]} {:linkId "3"}]}
        mutation {:type   :add
                  :path  (path/->path "QuestionnaireResponse" "123" "item" {:linkId "2"} "item" "-")
                  :value {:linkId "2.3"}}]

    (t/is (= [{:op "add",     :path "/item/1/item/2", :value {:linkId "2.3"}}]
             (pb/build-patches resource mutation)))

    (t/is (= {:item [{:linkId "1"}
                     {:linkId "2" :item [{:linkId "2.1"} {:linkId "2.2"} {:linkId "2.3"}]}
                     {:linkId "3"}]}
             (patch/patch
              resource
              (pb/build-patches resource mutation)
              true)))))

(t/deftest test-remove-nested
  (t/is
   (=
    [{:op "remove" :path "/name/0/first"}
     {:op "remove" :path "/name/0"}
     {:op "remove" :path "/name"}]
    (pb/build-patches
     {:name [{:first "z"}]}
     {:type   :remove
      :path  (path/->path "Patient" "123" "name" 0 "first")})))
  (t/is
   (=
    {}
    (patch/patch
     {:name [{:first "z"}]}
     (pb/build-patches
      {:name [{:first "z"}]}
      {:type   :remove
       :path  (path/->path "Patient" "123" "name" 0 "first")})
     true))))

(t/deftest test-non-existant
  (t/is
   (=
    []
    (pb/build-patches
     {:name [{:first "z"}]}
     {:type   :remove
      :path  (path/->path "Patient" "123" "fake")}))))

(t/deftest test-questionnaire
  (t/is
   (=
    [{:op "remove", :path "/item/0/item/0/answer/0/valueString"}
     {:op "remove", :path "/item/0/item/0/answer/0"}
     {:op "remove", :path "/item/0/item/0/answer"}]
    (pb/build-patches
     {:item [{:linkId "2" :item [{:linkId "2.3" :answer [{:valueString "a"}]}]}]}
     {:type :replace
      :path (path/->path "QuestionnaireResponse" "123" :item 0 :item 0 :answer 0 :valueString)
      :value ""})))
  (t/is
   (=
    [{:op "remove", :path "/item/0/item/0/answer/0/valueString"}
     {:op "remove", :path "/item/0/item/0/answer/0"}
     {:op "remove", :path "/item/0/item/0/answer"}]
    (pb/build-patches
     {:item [{:linkId "2" :item [{:linkId "2.3" :answer [{:valueString "a"}]}]}]}
     {:type :remove
      :path (path/->path "QuestionnaireResponse" "123" :item 0 :item 0 :answer 0 :valueString)}))))






