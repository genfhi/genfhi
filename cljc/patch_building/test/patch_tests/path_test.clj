(ns patch-tests.path-test
  (:require [gen-fhi.patch-building.path :as path :refer [ascend]]
            [clojure.test :as t]))

(t/deftest path-descend
  (let [p (path/->path "Patient" "123" "field1" "field/2" "field~3")]
    (t/is (= (:field (ascend p))
             (keyword "field~3")))
    (t/is (= (:field (ascend (:parent (ascend p))))
             (keyword "field/2")))
    (t/is (= (:field (ascend (:parent (ascend (:parent (ascend p))))))
             :field1))))

(t/deftest path-extension
  (let [p (path/->path "Patient" "123")
        p (path/extend-path p "field1" "field/2" "field~3")]
    (t/is (= (:field (ascend p))
             (keyword "field~3")))
    (t/is (= (:field (ascend (:parent (ascend p))))
             (keyword "field/2")))
    (t/is (= (:field (ascend (:parent (ascend (:parent (ascend p))))))
             :field1))))

(t/deftest to-jsonpointer
  (t/is (= "/field1/field2/field3"
           (path/to-jsonpointer
            (path/->path "Patient" "123" "field1" "field2" "field3")
            {}))))

(t/deftest reference->path-test
  (t/is (= "Patient|123"
           (path/reference->path {:reference "Patient/123"}))))

(t/deftest path-meta
  (let [p (path/->path "Patient" "123")]
    (t/is
     (= "Patient|123/#[field1=\"123\",field2=\"456\"]"
        (path/descend p {:field1 "123"
                         :field2 "456"})))
    (t/is
     (=
      (:field (path/ascend (path/descend p {:field1 "123"
                                            :field2 "456"})))
      {:field1 "123"
       :field2 "456"}))))

(t/deftest path-meta-escaping
  (let [p (path/->path "Patient" "123")]
    (t/is
     (= "Patient|123/#[field1=\"~1123\",field2=\"~0456\"]"
        (path/descend p {:field1 "/123"
                         :field2 "~456"})))
    (t/is
     (= (:field
         (path/ascend
          (path/descend p {:field1 "/123"
                           :field2 "~456"})))
        {:field1 "/123"
         :field2 "~456"}))))

(t/deftest to-jsonpointer-with-meta
  (t/is (= "/vec/3/name"
           (path/to-jsonpointer
            (path/->path "Patient" "123" "vec" {:id "20"} "name")
            {:vec [{:id "6"} {:id "5"} {:id "4"}]})))

  (t/is (= "/vec/0"
           (path/to-jsonpointer
            (path/->path "Patient" "123" "vec" {:id "6"})
            {:vec [{:id "6"} {:id "5"} {:id "4"}]})))

  (t/is (= "/vec/2"
           (path/to-jsonpointer
            (path/->path "Patient" "123" "vec" {:id "4"})
            {:vec [{:id "6"} {:id "5"} {:id "4"}]})))

  (t/is (= "/vec/1"
           (path/to-jsonpointer
            (path/->path "Patient" "123" "vec" {:id "5"})
            {:vec [{:id "6"} {:id "5"} {:id "4"}]}))))

(t/deftest ascend-with-num
  (let [{:keys [parent field]} (path/ascend "ClientApplication|0727bd5c-5995-43c1-a36d-0998e0a45c14/manifest/7")]
    (t/is (= parent "ClientApplication|0727bd5c-5995-43c1-a36d-0998e0a45c14/manifest"))
    (t/is (= field 7))))

(t/deftest to-expression-with-meta
  (t/is (= "Patient.vec.where(id='20').name"
           (path/to-expression
            (path/->path "Patient" "123" "vec" {:id "20"} "name"))))

  (t/is (= "Patient.vec.where(number=6)"
           (path/to-expression
            (path/->path "Patient" "123" "vec" {:number 6}))))

  (t/is (= "Patient.vec.where(id='4')"
           (path/to-expression
            (path/->path "Patient" "123" "vec" {:id "4"}))))

  (t/is (= "Patient.vec.where(id='5')"
           (path/to-expression
            (path/->path "Patient" "123" "vec" {:id "5"})))))
