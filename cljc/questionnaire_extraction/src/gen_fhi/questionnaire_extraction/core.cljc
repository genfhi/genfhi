(ns gen-fhi.questionnaire-extraction.core)

(defprotocol ExtractionResolver
  (resolve [query])
  (resolve [type id]))

;; (defn- item->map [acc item]
;;   (reduce
;;    (fn [acc child-item]
;;      (item->map))

;;    (assoc acc (:linkId item) item)
;;    (get item :item [])))

(defn- questionnaire->linkid-map
  "Converts a questionnaire into map of linkid -> item"
  [questionnaire]
  (loop [item-map  {}
         cur-items (get questionnaire :item [])]
    (let [[item-map next-items]
          (reduce
           (fn [[item-map next-items] item]
             [(assoc item-map (:linkId item) item)
              (concat next-items (get item :item))])
           [item-map []]
           cur-items)]
      (if (empty? next-items)
        item-map
        (recur
         item-map
         next-items)))))

(defn- q-item+qr-item->extraction [q-item qr-item {:keys [item-map resource-resolver] :as ops}]
  (let [children-extractions (mapcat
                              (fn [child-qr-item]
                                (q-item+qr-item->extraction
                                 (get item-map (:linkId qr-item))
                                 child-qr-item
                                 ops))
                              (if (= "group" (:type q-item))
                                (get qr-item :item [])
                                ;; Need to handle repeat answers with nested values.
                                (get-in qr-item [:answer 0 :item] [])))]
    (if (:definition q-item)
      [{:definition (:definition q-item)
        :nested children-extractions}]

      (if (some? children-extractions)
        children-extractions
        []))))

(defn definition-based [questionnaire questionnaire-response resolver]
  (let [transaction {:resourceType "Bundle"
                     :type         "transaction"}
        item-map (questionnaire->linkid-map questionnaire)]

    (map
     (fn [qr-item]
       (let [q-item (get item-map (:linkId qr-item))]
         (q-item+qr-item->extraction
          q-item
          qr-item
          {:item-map item-map
           :resource-resolver nil})))

     (get questionnaire-response :item []))))

