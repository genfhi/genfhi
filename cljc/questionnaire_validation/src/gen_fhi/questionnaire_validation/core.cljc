(ns gen-fhi.questionnaire-validation.core
  (:require [clojure.core.match :refer [match]]
            [clojure.string :as string]

            [gen-fhi.patch-building.path :as path]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.operation-outcome.core :as oo]))

(defn find-qr-items [q-item qr-items qr-path]
  (->> (map-indexed
        (fn [i v] [(path/extend-path qr-path :item i) v])
        qr-items)
       (filter
        (fn [[_loc qr-item]]
          (= (:linkId q-item)
             (:linkId qr-item))))
       (map
        (fn [[loc qr-item]]
          {:loc     loc
           :qr-item qr-item}))))

(defn- validate-required [q-item q-path qr-loc+items]
  (when (and (:required q-item)
             (empty? (mapcat #(get-in % [:qr-item :answer]) qr-loc+items)))
    [(oo/issue-error
      "validation"
      (str "Required item missing in QR '" (:linkId q-item) "'")
      [(path/to-expression q-path)])]))

(defn q-item-type->fhir-primitive [item-type]
  (match item-type
    "group"       ""

    "display"     ""

    "date"        "date"

    "dateTime"    "dateTime"

    "time"        "time"

    "boolean"     "boolean"

    "string"      "string"

    "text"        "string"

    "url"         "uri"

    "integer"     "integer"

    "decimal"     "decimal"

    "choice"      "Coding"

    "open-choice" "Coding"

    "attachment"  "attachment"

    "reference"   "reference"

    "quantity"    "quantity"))

(defn- validate-answers [q-item qr-loc+items]
  (let [fhir-type (q-item-type->fhir-primitive (:type q-item))
        fhir-spec (keyword "fhir-types" fhir-type)]
    (mapcat
     (fn [{:keys [loc qr-item]}]
       (apply
        concat
        (map-indexed
         (fn [i answer]
           (let [answer-loc    (path/extend-path loc :answer i)
                 value-keyword (keyword (str "value" (string/capitalize fhir-type)))]
             (try
               (oo/outcome-validation
                fhir-spec
                (get answer value-keyword))
               []
               (catch Exception _ex
                 [(oo/issue-error
                   "invalid"
                   (str "Answer did not conform to type: '" fhir-type  "'")
                   ;; Specify Type so system knows it's on QuestionnaireResponse
                   [(path/to-expression answer-loc)])]))))
         (get qr-item :answer))))
     qr-loc+items)))

;; How to do validation
;; Loop over the q-item checking if existing value is in current-position of qr-item
(defn- validate-item [q-item all-qr-items q-path qr-path]
  (let [nested-items    (get q-item :item)
        qr-loc+items    (find-qr-items q-item all-qr-items qr-path)
        required-issues (validate-required q-item q-path qr-loc+items)
        answer-issues   (validate-answers q-item qr-loc+items)
        issues          (concat
                         required-issues
                         answer-issues)]

    (concat
     issues
     (mapcat
      (fn [{:keys [qr-item loc]}]
        (let [return
              (apply
               concat
               (when (not-empty nested-items)
                 (if (= "group" (:type q-item))
                   (map-indexed
                    (fn [i item]
                      (validate-item
                       item
                       (get qr-item :item)
                       (path/extend-path q-path :item i)
                       (path/extend-path loc :item)))
                    nested-items)
                   ;; If non group nested answer :item
                   (map-indexed
                    (fn [q-index item]
                      (apply
                       concat
                       (map-indexed
                        (fn [answer-index answer]
                          (validate-item
                           item
                           (get answer :item)
                           (path/extend-path q-path :item q-index)
                           (path/extend-path :answer answer-index :item)))
                        (get qr-item :answer))))
                    nested-items))))]
          return))
      qr-loc+items))))

(defn validate [questionnaire questionnaire-response]
  (let [q-path (path/->path
                (get questionnaire :resourceType)
                (get questionnaire :id))
        issues (apply
                concat
                (map-indexed
                 (fn [i q-item]
                   (validate-item
                    q-item
                    (get questionnaire-response :item [])
                    (path/extend-path q-path :item i)
                    (path/->path
                     (get questionnaire-response :resourceType)
                     (get questionnaire-response :id))))
                 (get questionnaire :item [])))]
    (when (not-empty issues)
      (throw (oo/throw-outcome
              (oo/outcome
               issues))))))

