(ns validation-test
  (:require  [clojure.test :as t]
             [clojure.data.json :as json]
             [clojure.java.io :as io]
             
             [gen-fhi.generated.r4.specs]
             [gen-fhi.questionnaire-validation.core :as q-validation]))

(clojure.spec.alpha/check-asserts true)

(def phq-9-questionnaire
  (json/read-str
   (slurp (io/file "./test/data/phq-9-questionnaire.json"))
   :key-fn keyword))

(def phq-9-questionnaire-response
  (json/read-str
   (slurp (io/file "./test/data/phq-9-questionnaire-response.json"))
   :key-fn keyword))

(t/deftest validate-phq-9
  (q-validation/validate
   phq-9-questionnaire
   phq-9-questionnaire-response))
