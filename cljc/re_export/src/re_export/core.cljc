(ns re-export.core)

(defmacro re-export [sym]
  (let [ns   (symbol (namespace sym))
        name (symbol (name sym))]
    `(do
       (require '~ns)
       (def ~name ~sym))))
