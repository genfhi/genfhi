(ns gen-fhi.smart-client.core
  (:import [java.net URI]
           [javax.net.ssl
            SNIHostName SSLEngine SSLParameters])
  (:require
   [clojure.string :as string]
   [clojure.core.match :refer [match]]

   [org.httpkit.client :as http]
   ;; :cljs [cljs-http.client :as http])

   [clojure.data.json :as json]
   [clojure.core.async :as async]
   ;; :cljs [cljs.core.async :as async]

   [gen-fhi.smart-client.url :as url]))

;; [https://kumarshantanu.medium.com/using-server-name-indication-sni-with-http-kit-client-f7d92954e165]
;; Because SMART app launcher uses SNI needed to configure client to handle it.
(defn- sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setUseClientMode ssl-engine true)
    (.setSSLParameters ssl-engine ssl-params)))

(def ^:private sni-client (http/make-client
                           {:ssl-configurer sni-configure}))

(defn request [request-data]
  ;; #?(:cljs (http/request request-data)
  ;;    :clj
  (let [return (async/chan)]
    (http/request
     request-data
     (fn [res]
       (async/>!!
        return
        (update
         res
         :body
         (fn [body]
           (let [content-type (get-in res [:headers :content-type])]
             (cond
               (string/includes? content-type "application/json") (json/read-str body :key-fn keyword)
               :else body)))))))

    return))

(defn- ->smart-extensions [server-url]
  (let [smart-configuration-url (url/extend-path server-url ".well-known" "smart-configuration")]
    (request {:method :get
              :headers {"Accept" "application/json"}
              :url smart-configuration-url})))

(def smart-authorization-type #{:standalone :ehr-launch :backend-service})
(def smart-authentication-type #{:symmetric :asymmetric})

(defn authorize [{:keys [iss client-id client-secret] :as parameters}]
  (match [iss client-id client-secret]
    ;; Standalone Launch
    [iss client-id nil]           (throw (ex-info "Standalone Launch not supported" parameters))
    ;; EHR Launch
    [nil client-id nil]           (throw (ex-info "EHR Launch not supported" parameters))
    ;; Backend launch
    [iss client-id client-secret] (do (let [extensions (->smart-extensions iss)]
                                        extensions))
    :else (throw (ex-info
                  "Invalid parameters passed to authorize. Must include client-id"
                  parameters))))

;; (defprotocol SMARTAuthClient
;;   (authorize [this parameters]))
