(ns gen-fhi.smart-client.jwt
  (:import  (java.util Base64))
  (:require [buddy.core.keys :as keys]
            [buddy.core.nonce :as nonce]
            [buddy.sign.jwt :as jwt]))

;; SEE [http://www.hl7.org/fhir/smart-app-launch/client-confidential-asymmetric.html#request]
;; This is a fixed value for client_assertion_type parameter
(def client-assertion-type "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")

;; Must be :rs384 or :es384 see [http://www.hl7.org/fhir/smart-app-launch/client-confidential-asymmetric.html#advertising-server-support-for-this-profile]
;; Server may only support one method so will need to confirm what encrypiton method is supported on the server.
(def supported-algorithm-types #{:rs384 :es384})

(defn ->nonce-string []
  (.encodeToString (Base64/getUrlEncoder) (nonce/random-bytes 16)))

;; SEE [http://www.hl7.org/fhir/smart-app-launch/client-confidential-asymmetric.html#request]

;; HEADERS
;; ------------------------------------------------------------------------------------------------------------
;; alg	required	The JWA algorithm (e.g., RS384, ES384) used for signing the authentication JWT.
;; kid	required	The identifier of the key-pair used to sign this JWT. This identifier SHALL be unique within the client's JWK Set.
;; typ	required	Fixed value: JWT.
;; jku	optional	The TLS-protected URL to the JWK Set containing the public key(s) accessible without authentication or authorization. When present, this SHALL match the JWKS URL value that the client supplied to the FHIR authorization server at client registration time. When absent, the FHIR authorization server SHOULD fall back on the JWK Set URL or the JWK Set supplied at registration time. See Signature Verification for details.
;; ------------------------------------------------------------------------------------------------------------

;; Claims
;; ------------------------------------------------------------------------------------------------------------
;; iss	required	Issuer of the JWT -- the client's client_id, as determined during registration with the FHIR authorization server (note that this is the same as the value for the sub claim)
;; sub	required	The client's client_id, as determined during registration with the FHIR authorization server (note that this is the same as the value for the iss claim)
;; aud	required	The FHIR authorization server's "token URL" (the same URL to which this authentication JWT will be posted -- see below)
;; exp	required	Expiration time integer for this authentication JWT, expressed in seconds since the "Epoch" (1970-01-01T00:00:00Z UTC). This time SHALL be no more than five minutes in the future.
;; jti	required	A nonce string value that uniquely identifies this authentication JWT.
;; ------------------------------------------------------------------------------------------------------------

(defn- create-token [{:keys [headers claims]} private-key]
  (let [jti     (->nonce-string)
        headers (assoc headers
                       :typ "JWT"
                       :alg (keyword (get headers :alg)))]
    (assert (contains? supported-algorithm-types (keyword (get headers :alg))))
    (buddy.sign.jwt/sign
     (assoc claims :jti jti)
     private-key
     {:alg (get headers :alg) :header headers})))

(defn ->exp [minutes-till-exp]
  (-> (* 60000 minutes-till-exp)
      (+ (System/currentTimeMillis))))

(defn create-authorization-request
  [{:keys [token-url scope private-key client-id iss exp]}]
  (let [kid (get private-key :kid)
        private-key (cond (string? private-key) (keys/str->private-key private-key)
                          (map? private-key)    (keys/jwk->private-key private-key)
                          :else (throw (ex-info
                                        (str "Unknown key type:" (type private-key))
                                        {:type (type private-key)})))
        token (create-token
               {:headers  {:alg :rs384
                           :kid kid}
                :claims {;; Allow a user to override the iss? Note applauncher requires this to be the serviceurl
                         :iss (or iss client-id)
                         :sub client-id
                         :exp exp
                         :aud token-url
                         :jti "random-string"}}
               private-key)]
    {:url    token-url
     :method :post
     :headers {"Content-Type" "application/x-www-form-urlencoded"}
     :form-params {"client_assertion"      token
                   "scope"                 scope
                   "client_assertion_type" "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"
                   "grant_type"            "client_credentials"}}))


