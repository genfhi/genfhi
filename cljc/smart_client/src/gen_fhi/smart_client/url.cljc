(ns gen-fhi.smart-client.url
  #?(:clj (:import [java.net URI]))
  (:require [clojure.string :as string]))

(defprotocol URLBuilder
  (get-path [this])
  (extend-path [this & path-chunks])
  (set-query-parameters [this query-map])
  (get-query-parameters [this]))

(defn- -extend-path [path fields]
  (clojure.string/join
   "/"
   (concat (clojure.string/split path #"/")
           fields)))

(defn- query-string->query-map [query]
  (if (nil? query)
    {}
    (let [parameters (clojure.string/split query #"&")]
      (reduce
       (fn [m parameter]
         (let [[k v] (string/split parameter #"=")]
           (assoc m (keyword k) v)))
       {}
       parameters))))

(defn- query-map->query-string [query-map]
  (let [query-string (reduce
                      (fn [query-string [k v]]
                        (str query-string (name k) "=" v "&"))
                      ""
                      query-map)]

    ;; Remove the ending substring.
    (if (empty? query-string)
      query-string
      (str
       "?"
       (subs query-string 0 (- (count query-string) 1))))))

(defn- url->url-pieces [url]
  #?(:cljs (let [url   (URL. url)
                 host  (.-host url)
                 query (.-search url)
                 path  (.-pathname url)]
             {:host  host
              :query (if (and (string? query)
                              (clojure.string/starts-with? query "?"))
                       (subs query 1)
                       query)
              :path  path})
     :clj (let [url   (URI. url)
                path  (.getPath url)
                host  (.getHost url)
                query (.getQuery url)]
            {:path path
             :host host
             :query query})))

(defn- url->path-meta [url]
  (let [{:keys [path host query]} (url->url-pieces url)
        query-parameters (query-string->query-map query)]
    {:host host
     :path path
     :query query-parameters}))

(defn- path-meta->url
  {:malli/schema [:=> [:cat :string] :string]}
  [meta]
  (str
   ;; must be tls
   "https://"
   (:host meta)
   (:path meta)
   (query-map->query-string (:query meta))))

(extend-type
 #?(:cljs clojure.string
    :clj java.lang.String)
  URLBuilder
  (get-path [this]
    (:path (url->path-meta this)))
  (extend-path [this & fields]
    (let [meta (url->path-meta this)]
      (path-meta->url
       (update meta
               :path
               (fn [path]
                 (-extend-path path fields))))))
  (get-query-parameters [this]
    (:query (url->path-meta this)))
  (set-query-parameters [this query]
    (let [meta (url->path-meta this)]
      (path-meta->url
       (assoc meta :query query)))))




