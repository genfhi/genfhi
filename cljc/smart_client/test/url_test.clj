(ns url-test
  (:require [clojure.test :as t]
            [gen-fhi.smart-client.url :as url]
            [malli.core :as m]
            [malli.generator :as mg]))

(t/deftest query-parameter
  (t/is
   (= "https://test?new=parameter&q5=asdf2"
      (url/set-query-parameters
       "test?q1=asdf&q2=asdf2"
       {:new "parameter"
        :q5 "asdf2"})))
  (t/is
   (= (url/get-query-parameters "https://test?q1=asdf&q2=asdf2")
      {:q1 "asdf"
       :q2 "asdf2"})))

(t/deftest extending-path
  (t/is
   (= "https://test/nested/path?test=5"
      (url/extend-path "test?test=5" "nested" "path"))))

(t/deftest generative-testing
  (t/is
   (= nil
      (m/explain
       (m/schema
        [:=> [:cat :string] :string]
        {::m/function-checker mg/function-checker})
       url/get-path)))
  (t/is
   (= nil
      (m/explain
       (m/schema
        [:=> [:cat :string] :map]
        {::m/function-checker mg/function-checker})
       url/get-query-parameters)))
  (t/is
   (= nil
      (m/explain
       (m/schema
        [:=> [:cat :string [:map-of :keyword :string]] :string]
        {::m/function-checker mg/function-checker})
       url/set-query-parameters)))
  (t/is
   (= nil
      (m/explain
       (m/schema
        [:=> [:cat :string :string :string] [:string]]
        {::m/function-checker mg/function-checker})
       url/extend-path))))
