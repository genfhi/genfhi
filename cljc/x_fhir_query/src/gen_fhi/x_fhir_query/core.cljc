(ns gen-fhi.x-fhir-query.core
  (:require [clojure.set :as set]
            [clojure.string :as string]

            [gen-fhi.fhirpath.parser :as fp-parser]
            [gen-fhi.fhirpath.core :as fp]))

;; Specification [http://build.fhir.org/ig/HL7/sdc/expressions.html#x-fhir-query-enhancements]

(defn- parse-int
  "Attempts to parse string to integer if it fails returns nil."
  [str]
  #?(:clj  (try
             (Integer/parseInt str)
             (catch Exception ex
               nil))
     :cljs (let [result (js/parseInt str)]
             (if (js/isNaN result)
               nil
               result))))

(def fp-expresion-regex #"\{\{([^\}]*)\}\}")

(defn execute [x-fhir-query {:keys [eval->string fp-options]}]
  (let [eval->string (or eval->string (fn [fhirpath-eval] (string/join "," fhirpath-eval)))]
    (loop [x-fhir-query x-fhir-query]
      (let [[match expression] (re-find fp-expresion-regex (or x-fhir-query ""))]
        (if (some? match)
          (recur
           (string/replace
            x-fhir-query
            match
            (eval->string
             (fp/evaluate (string/trim expression) nil fp-options))))
          x-fhir-query)))))

(defn ->variables [x-fhir-query]
  (let [variables (atom #{})]
    (if (nil? x-fhir-query)
      {}
      (loop [x-fhir-query x-fhir-query]
        (let [[match expression] (re-find fp-expresion-regex x-fhir-query)]
          (if (some? match)
            (do
              (swap!
               variables
               (fn [variables]
                 (set/union
                  variables
                  (fp/->variables (string/trim expression)))))
              (recur
               (string/replace
                x-fhir-query
                match
                "")))
            @variables))))))

(defn replace-variable [x-fhir-query old-id new-id]
  (let [matches (re-seq fp-expresion-regex x-fhir-query)]
    (if (empty? matches)
      x-fhir-query
      (reduce
       (fn [x-fhir-query [full-match expression]]
         (clojure.string/replace
          x-fhir-query
          full-match
          (str "{{" (fp-parser/replace-variable expression old-id new-id) "}}")))
       x-fhir-query
       matches))))

(def placeholder-regex #"/(\d+)")
(defn placeholder [place] (str "/" place))
(defn escape [query] (string/replace query #"/" "~1"))
(defn unescape [query] (string/replace (or query "") #"~1" "/"))

(defn- x-fhir-query->placeholded-query+matches [x-fhir-query]
  (loop [x-fhir-query (escape x-fhir-query)
         matches      []]
    (let [[match _expression] (re-find fp-expresion-regex x-fhir-query)]
      (if (some? match)
        (recur
         (string/replace
          x-fhir-query
          match
          (placeholder (count matches)))
         (conj matches match))
        [x-fhir-query matches]))))

(defn- add-back-expressions [matches placeholder-query]
  (loop [x-fhir-query (or placeholder-query "")]
    (let [[match match-index] (re-find placeholder-regex x-fhir-query)]
      (if (some? match)
        (recur
         (string/replace
          x-fhir-query
          match
          (nth matches (parse-int match-index))))
        (unescape x-fhir-query)))))

(defn parse-query [x-fhir-query]
  (loop [[placeholder-query matches] (x-fhir-query->placeholded-query+matches x-fhir-query)]
    (let [[path queries] (string/split placeholder-query #"\?" 2)
          queries        (if queries (string/split queries  #"&") [])]
      {:path    (into
                 []
                 (map
                  (partial add-back-expressions matches)
                  (string/split path #"~1")))
       :queries (into
                 []
                 (map
                  (fn [query]
                    (let [[key val] (string/split query #"=" 2)]
                      {:key (add-back-expressions matches key)
                       :val (add-back-expressions matches val)}))
                  queries))})))

(defn parse-query->string [parsed-query]
  (let [query-string (reduce
                      (fn [acc q] (str acc (:key q) "=" (:val q) "&"))
                      "?"
                      (get parsed-query :queries []))]
    (str (string/join "/" (get parsed-query :path []))
         (subs query-string 0 (- (count query-string) 1)))))
