(ns x-fhir-query-test
  (:require  [clojure.test :as t]
             [gen-fhi.x-fhir-query.core :as x-fhir-query]))

(t/deftest variable.test
  (t/is
   (= "test asdf"
      (x-fhir-query/execute "test {{ %value }}" , {:fp-options {:options/variables {:value "asdf"}}}))))

(t/deftest variable.manyexpressions
  (t/is
   (= "test value2 10"
      (x-fhir-query/execute "test {{ %value + '2' }} {{5 + 5}}" , {:fp-options {:options/variables {:value "value"}}}))))

(t/deftest variable.singular
  (t/is
   (= "test variablestring2 5"
      (x-fhir-query/execute
       "test {{ (%value + '2')[0] }} {{5}}"
       {:eval->string  (fn [eval]
                         (with-out-str
                           (print eval)))

        :fp-options {:options/allow-singular? true
                     :options/variables {:value "variablestring"}}}))))


(t/deftest complex.or.statements
  (t/is
   (=
    "Patient?name=d&_count=20&_skip=40"
    
    (x-fhir-query/execute
     "Patient?name={{%search.value}}&_count=20&_skip={{%pl.offset or 20}}"
     {:fp-options
      {:options/variables
       {:search {:value "d"}
        :pl     {:offset 40}}}}))))
    
