(ns x-fhir-query-variables-test
  (:require  [clojure.test :as t]
             [gen-fhi.x-fhir-query.core :as x-fhir-query]))

(t/deftest variable.test
  (t/is
   (= #{:value :test :test2}
      (x-fhir-query/->variables
       "test {{ %value }} {{%test + %test2}}"))))
