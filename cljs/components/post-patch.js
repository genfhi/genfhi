const fs = require("fs");

const packages_to_patch = ["@codemirror/view", "@codemirror/state"];

const package_json_filenames = packages_to_patch.map(
  (package) => "node_modules/" + package + "/package.json"
);

// Do this patch because cljs not recognizing cjs files so patching to index.js
const package_jsons_content = package_json_filenames
  .map((p) => fs.readFileSync(p, { encoding: "utf8", flag: "r" }))
  .map((contents) => JSON.parse(contents))
  .map((package_json) => ({ ...package_json, main: "dist/index.js" }))
  .map((package_json) => JSON.stringify(package_json, null, 2));

package_json_filenames.forEach((file_name, idx) =>
  fs.writeFileSync(file_name, package_jsons_content[idx])
);

console.log("PATCHED:", JSON.stringify(packages_to_patch));
