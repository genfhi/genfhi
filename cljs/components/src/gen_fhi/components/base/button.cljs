(ns gen-fhi.components.base.button
  (:require [clojure.core.match :refer [match]]))

(defn ->button-class [type]
  (let [button-type-class
        (match (keyword type)
          :danger "bg-transparent hover:bg-transparent text-red-400 hover:text-red-500 border-red-400 hover:border-red-500 "
          :secondary "border-slate-400 hover:bg-transparent bg-transparent text-slate-600 hover:border-blue-400 hover:text-blue-600"
          :else "text-white border-0 bg-blue-500 hover:bg-blue-600")]
    (str
     button-type-class
     " "
     "align-middle rounded border inline-block text-center cursor-pointer py-2 px-4 font-semibold disabled:bg-gray-300"
     " ")))

(def button
  ^{:handlers        ["on-click"]

    :storybook-props {:title "Base/Button"
                      :argTypes {:type {:options ["primary" "secondary" "danger"] :control {:type "select"}}}}

    :properties      [{:name              "type"
                       :type              "code"
                       :editor-props      {:label "Type"
                                           :component-type :select
                                           :valueset {:expansion {:contains [{:code "primary"   :display "Primary"}
                                                                             {:code "secondary" :display "Secondary"}
                                                                             {:code "danger"    :display "Danger"}]}}
                                           :default-value "primary"}

                       :storybook-default "primary"}

                      {:name              "className"
                       :type              "string"
                       :hide?             true
                       :storybook-default ""}]}

  (fn [{:keys [className type] :as props} children]
    [:button (merge
              props
              {:className  (str
                            (->button-class type)
                            className)})
     [:div {:className "w-full h-full flex items-center justify-center"}
      (if (some? children) children " ")]]))
