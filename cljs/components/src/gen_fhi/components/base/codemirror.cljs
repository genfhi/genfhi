(ns gen-fhi.components.base.codemirror
  (:require [reagent.core :as r]
            [oops.core :as oops]

            ["@codemirror/state" :refer [Annotation EditorState StateEffect]]
            ["@codemirror/view" :refer [EditorView placeholder]]
            ["react" :refer [useEffect useRef]]))
            ;[gen-fhi.workspace.utilities.debounce :as debounce]))

(def SyncAnnotation (oops/ocall Annotation "define" "Synchronization"))

(defn- is-sync-update? [view-update]
  (let [transactions (oops/oget view-update "transactions")]
    (reduce
     (fn [is-user-triggered? transaction]
       (or is-user-triggered?
           (some? (oops/ocall transaction "annotation" SyncAnnotation)))) ;;(oops/oget Transaction "userEvent")))))
     false
     transactions)))

;; Note because sync can have a race condition as follows:
;; 1. New state gets computed in subs
;; 2. While sub outputing computation new change get's dispatched.
;; 3. Sub returns old value even though DB has been updated.
;; THIS is a work around that debounces check on sync during those fast typing.
(def ^{:private true} ->debounce-resync
  (fn []
    (fn [view value]
      (let [current-value (if (some? view) (oops/ocall view "state.doc.toString") "")
            current-selection (if (some? view) {:anchor (oops/oget view "state.selection.ranges.0.anchor")
                                                :head   (oops/oget view "state.selection.ranges.0.head")}
                                  {:anchor 0 :head 0})
            offset-from-end   (- (count current-value) (:head current-selection))]

        (when (and (some? view)
                   (not= value current-value))
          ;; (println
          ;;  "refreshing codemirror"
          ;;  "offset-from-end:" offset-from-end
          ;;  "count value"      (count value)
          ;;  "count cur-value:" (count current-value)
          ;;  "next-pos:" (- (count value) offset-from-end))
          ;; (println "value:'" value "' != " "current-value:'" current-value "'" current-selection)

          (oops/ocall view "dispatch"  (clj->js {:selection   {:anchor (max 0 (- (count value) offset-from-end))
                                                               :head   (max 0 (- (count value) offset-from-end))}
                                                 :annotations [(oops/ocall SyncAnnotation "of" "syncing")]
                                                 :changes     {:from 0
                                                               :to (.-length current-value)
                                                               :insert (or value "")}}))))
      (fn []))))

(defn- codemirror-inner []
  (let [debounce-resync (->debounce-resync)]
    (fn [{:keys [value auto-focus? theme view state on-change disabled? extensions placeholder-string height width max-height max-width]
          :or {auto-focus? false}}]
      (let [root            (useRef)
            default-theme   (oops/ocall  EditorView "theme" (clj->js
                                                             (merge
                                                              theme
                                                              {"&"
                                                               (merge
                                                                (get theme "&" {})
                                                                {:height height
                                                                 :width width
                                                                 :font-size "14px"
                                                                 :max-height max-height
                                                                 :max-width max-width
                                                                 :padding-bottom "6px"})})))
            update-listener (oops/ocall EditorView "updateListener.of"
                                        (fn [view-update]
                                          (when (oops/oget view-update "docChanged")
                                            (let [doc (oops/oget view-update "state.doc")
                                                  new-value (oops/ocall doc "toString")
                                                  is-sync-update? (is-sync-update? view-update)]
                                              (when (not is-sync-update?)
                                              ;; Trigger on-change 
                                                (when (some? on-change)
                                                  (on-change new-value view-update)))))))
            get-extensions  (cond-> (or extensions [])
                              (some? default-theme)      (conj default-theme)
                              (some? update-listener)    (conj update-listener)
                              (some? placeholder-string) (conj (placeholder placeholder-string))
                              disabled?                  (conj (oops/ocall EditorView "editable.of"  false)))]

        (useEffect
         (fn []
           (when (.-current root)
             (reset! state (oops/ocall EditorState "create"  #js{:doc (or value "")
                                                                 :extensions (clj->js get-extensions)}))
             (reset! view (EditorView. #js{:state @state
                                           :parent (.-current root)}))
             (when auto-focus?
               (.focus @view)))
           (fn []))
         #js[root])

        (useEffect
         (fn [] (debounce-resync @view value))
         #js[@view value])

        (useEffect
         (fn []
           (when-let [view @view]
             (oops/ocall view "dispatch"  #js{:effects (oops/ocall StateEffect "reconfigure.of" (clj->js get-extensions))}))
           (fn []))
         #js[extensions disabled? placeholder-string height width max-height max-width theme])

        [:div {:style {:height height
                       :width width
                       :max-height max-height
                       :max-width max-width}
               :ref   root}]))))

(def codemirror
  ^{:handlers ["on-change"]
    :storybook-props {:title "Base/CodeMirror"}
    :properties [{:name "disabled?"
                  :type "boolean"
                  :editor-props {:label "Read-only?"}
                  :storybook-default false}
                 {:name "placeholder-string"
                  :type "string"
                  :editor-props {:label "Placeholder"}
                  :storybook-default "placeholder text"}
                 {:name "value"
                  :type "string"
                  :editor-props {:label "Value"}
                  :storybook-default "value"}]}
  (fn [_props]
    (let [view (r/atom nil)
          state (r/atom nil)]
      (fn [props]
        [:f> codemirror-inner
         (assoc props
                ;; (update props :on-change (fn [on-change]
                ;;                            (when on-change
                ;;                              (debounce/debounce
                ;;                               on-change
                ;;                               400))))
                :view view
                :state state)]))))

