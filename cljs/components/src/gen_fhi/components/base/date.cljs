(ns gen-fhi.components.base.date
  (:require [oops.core :as oops]
            [clojure.core.match :refer [match]]

            ["dayjs" :as dayjs]
            ["rc-picker/lib/generate/dayjs" :as dayjs-generate-config]
            ["antd/es/date-picker/generatePicker" :as generate-date-picker]))

(def DatePicker  ((oops/oget generate-date-picker "default")
                  (oops/oget dayjs-generate-config "default")))

(def date
  ^{:handlers        ["on-change"]
    :storybook-props {:title "Base/Date"}
    :properties      [{:name "isDisabled"
                       :type "boolean"
                       :editor-props {:label "Read-only?"}
                       :storybook-default false}
                      {:name "value"
                       :type "string"
                       :editor-props {:label "Value"}
                       :storybook-default "1980-01-01"}]}

  (fn [{:keys [value type on-change isDisabled]}]
    (let [props {:style {:height "100%" :width "100%"}
                 :value (when-let [date value]
                          (match type
                            "time"     (dayjs date "HH:mm:ss")
                            "date"     (dayjs date "YYYY-MM-DD")
                            "datetime" (dayjs date "YYYY-MM-DDTHH:mm:ssZ")))
                 :disabled isDisabled
                 :on-change (fn [date]
                              (on-change
                               (if (nil? date)
                                 nil
                                 (match type
                                   "time"     (oops/ocall date "format" "HH:mm:ss")
                                   "date"     (oops/ocall date "format" "YYYY-MM-DD")
                                   "datetime" (oops/ocall date "format" "YYYY-MM-DDTHH:mm:ssZ")))))}]
      [:> DatePicker
       (match type
         "date"     props

         "time"     (assoc props :picker "time"
                           :mode js/undefined
                           :placeholder "Select time")

         "datetime" (assoc props :showTime true))])))
