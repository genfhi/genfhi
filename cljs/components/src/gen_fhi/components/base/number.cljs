(ns gen-fhi.components.base.number
  (:require [clojure.core.match :refer [match]]
            ["antd" :refer [InputNumber]]))

(def number
  ^{:storybook-props {:title "Base/Number"
                      :argTypes {:type {:options ["integer" "decimal"] :control {:type "select"}}}}

    :properties [{:name         "isDisabled"
                  :type         "boolean"
                  :editor-props {:label "Read-only?"}

                  :storybook-default false}

                 {:name         "type"
                  :hide?        true
                  :type         "string"
                  :storybook-default "integer"}
                 {:name         "value"
                   ;; Hiding because will be specified as decimal or integer by component
                  :hide?        true
                  :type         "number"
                  :storybook-default 0}]}

  (fn [{:keys [isDisabled type value on-change]}]
    [:> InputNumber
     {:disabled   isDisabled
      :value      value
      :style      {:width "100%" :height "100%"}
      :on-change  (fn [num]
                    (let [num (match type
                                "integer" (.round js/Math num)
                                "decimal" num)]
                      (on-change num)))}]))

