(ns stories.button-stories
  (:require [stories.helper :as helper]
            [reagent.core :as reagent]
            ["@storybook/addon-docs" :refer [Title
                                             Subtitle
                                             Description
                                             Primary]]
            [gen-fhi.components.base.button :refer [button]]))

(defn button-component-custom-docs []
  [:<>
   [:> Title]
   [:> Subtitle "This is a custom subtitle"]
   [:> Description {:type "INFO"} "Some details about the component"]
   [:> Primary]])

(def ^:export default
  (helper/->default button))

(defn ^:export Button [args]
  (let [params   (-> args helper/->params)
        on-click (:on-click params)]
    (reagent/as-element
     [button
      {:className (get params :className)
       :type     (keyword (get params :type))
       :on-click on-click}
      "Button"])))
