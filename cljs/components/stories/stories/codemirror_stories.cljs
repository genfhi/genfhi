(ns stories.codemirror-stories
  (:require [stories.helper :as helper]
            [reagent.core :as reagent]

            ["@codemirror/lang-javascript" :refer [javascript]]
            ["@codemirror/basic-setup" :refer [basicSetup]]

            [gen-fhi.components.base.codemirror :refer [codemirror]]))

(def ^:export default
  (helper/->default codemirror))

(def ^:export no-extensions {})

(defn ^:export basic-setup [args]
  (let [params   (-> args helper/->params)]
    (reagent/as-element
     [codemirror
      {:extensions         [basicSetup]
       :value              (get params :value)
       :disabled?          (get params :disabled?)
       :placeholder-string (get params :placeholder-string)}])))

(def ^:export json-language-setup
  (clj->js
   {:args   {:value "{\"field\": \"value\"}"}
    :render (fn [args]
              (let [params   (-> args helper/->params)]
                (reagent/as-element
                 [codemirror
                  {:extensions         [basicSetup (javascript #js{})]
                   :value              (get params :value)
                   :disabled?          (get params :disabled?)
                   :placeholder-string (get params :placeholder-string)}])))}))
