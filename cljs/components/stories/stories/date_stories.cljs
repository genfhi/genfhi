(ns stories.date-stories
  (:require [stories.helper :as helper]
            [reagent.core :as r]

            [gen-fhi.components.base.date :refer [date]]))

(defn- primary-date-inner [{:keys [value]}]
  (let [value                   (r/atom value)]
    (fn [args]
      [date (assoc args
                   :on-change (fn [next-value] (reset! value next-value))
                   :value     @value)])))

(def ^:export default
  (clj->js
   (helper/->reactified
    (assoc
     (js->clj (helper/->default date))
     :component primary-date-inner)
    [:component])))

(def ^:export Date
  (clj->js
   {:args {:isDisabled false
           :type "date"
           :value "1980-01-01"}}))

(def ^:export DateTime
  (clj->js
   {:args {:isDisabled false
           :type "datetime"
           :value "2021-04-13T16:05:29-05:00"}}))

(def ^:export Time
  (clj->js
   {:args {:isDisabled false
           :type "time"
           :value "09:09:00"}}))



