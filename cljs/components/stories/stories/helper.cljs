(ns stories.helper
  (:require [reagent.core :as reagent]))

(defn ->params [^js args]
  (js->clj args :keywordize-keys true))

(defn ->reactified [options path]
  (if (get-in options path)
    (update-in options path reagent/reactify-component)
    options))

(defn- metadata->props [metadata]
  (let [args (into
              {}
              (map
               (fn [property]
                 [(:name property) (:storybook-default property)])
               (:properties metadata)))]

    (merge
     {:args args}
     (:storybook-props metadata))))

(defn component->props [component]
  (let [metadata (meta component)
        return (merge
                (metadata->props metadata)
                {:component component})]
    return))

(defn ->default [component]
  (let [options (component->props component)]
    (-> options
        (->reactified [:component])
        (->reactified [:parameters :docs :page])
        clj->js)))
