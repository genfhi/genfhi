(ns stories.number-stories
  (:require [stories.helper :as helper]

            [gen-fhi.components.base.number :refer [number]]))



(def ^:export default
  (helper/->default number))

(def ^:export Integer
  (clj->js
   {:args {:type "integer"
           :value 42}}))

(def ^:export Decimal
  (clj->js
   {:args {:type "decimal"
           :value 42.5}}))

