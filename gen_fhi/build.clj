(ns build
  (:import  [org.eclipse.jgit.api Git])
  (:require [clojure.tools.build.api :as b]
            [clojure.java.io :as io]))

(defmacro ->git-sha [git-directory]
  (let [repo (.call (.setDirectory (Git/init) (io/file git-directory)))]
    (.getName (.next (.iterator (.call (.setMaxCount (.log repo) 1)))))))

(defonce version (->git-sha "../"))
(def class-dir "target/classes")

(def src-dirs ["clj" "cljc" "migrate" "resources" "cli"])

(defn- build-css
  ([]
   (build-css (str "./target/public/main." version ".css")))
  ([output-dir]
   (let [result (b/process {:command-args ["npx" "tailwindcss"
                                           "-i" "./css/custom.css"
                                           "-o"   output-dir
                                           "--minify"]})]
     (when (not= 0 (:exit result))
       (throw (Exception. (str "[TAILWIND FAILED]" (:err result))))))))

(defn- clean [dir]
  (if dir
    (b/delete {:path dir})
    (b/delete {:path "target"})))

(defn- build-cljs [profile]
  (let [result (b/process {:command-args ["clj" "-A:fig" "-m" "figwheel.main" "-bo" profile]
                           :err :capture})]
    (when (not= 0 (:exit result))
      (throw (Exception. (str "[FIGWHEEL FAILED]" (:err result)))))))

(defn- npm-installation []
  (let [result (b/process {:command-args ["npm" "i"]})]
    (when (not= 0 (:exit result))
      (throw (Exception. (str "[NPM INSTALL FAILED]" (:err result)))))))

(defn workspace-uber [_]
  (let [main     'gen-fhi.workspace.core
        lib      'gen-fhi/workspace
        basis    (b/create-basis)
        uber-lib (format "target/%s-standalone.jar" (name lib))]
    (clean nil)

    (npm-installation)
    (build-cljs "workspace-frontend-prod")
    (build-css)
    (b/copy-dir {:src-dirs ["target/public"]
                 :target-dir (str class-dir "/public")})

    (b/copy-dir {:src-dirs src-dirs
                 :target-dir class-dir})

    (b/compile-clj {:basis basis
                    :src-dirs src-dirs
                    :class-dir class-dir})

    (b/write-pom {:class-dir class-dir
                  :lib lib
                  :version "latest"
                  :basis basis
                  :src-dirs src-dirs})

    (b/uber {:class-dir class-dir
             :uber-file uber-lib
             :basis basis
             :main main})))

(defn app-uber [_]
  (let [main     'gen-fhi.app-backend.core
        lib      'gen-fhi/app-backend
        basis    (b/create-basis)
        uber-lib (format "target/%s-standalone.jar" (name lib))]
    (clean nil)

    (npm-installation)
    (build-cljs "app-frontend-prod")
    (build-css)

    (b/copy-dir {:src-dirs src-dirs
                 :target-dir class-dir})

    (b/copy-dir {:src-dirs ["target/public"]
                 :target-dir (str class-dir "/public")})

    (b/compile-clj {:basis basis
                    :src-dirs src-dirs
                    :class-dir class-dir})

    (b/write-pom {:class-dir class-dir
                  :lib lib
                  :version "latest"
                  :basis basis
                  :src-dirs src-dirs})

    (b/uber {:class-dir class-dir
             :uber-file uber-lib
             :basis basis
             :main main})))

(defn questionnaire-component [_]
  (b/copy-file {:src "resources/public/antd.min.inc.css"
                :target "js/questionnaire-component/css/antd.min.css"})
  (b/copy-file {:src "resources/public/antd.min.css.map"
                :target "js/questionnaire-component/css/antd.min.css.map"})
  
  (build-css "js/questionnaire-component/css/custom.css")

  (let [result (b/process {:command-args ["clj" "-A:shadow-cljs" "compile" "questionnaire"]
                           :err :capture})]

    (when (not= 0 (:exit result))
      (throw (Exception. (str "Questionnaire component failed to compile" (:err result)))))))

