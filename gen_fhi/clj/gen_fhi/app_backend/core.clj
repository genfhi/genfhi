(ns gen-fhi.app-backend.core
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.core.match :refer [match]]
            [integrant.core :as ig]
            [org.httpkit.server :as http-kit]
            [gen-fhi.app-backend.handler :as handler]
            [taoensso.timbre :as timbre]))

(defmethod ig/init-key :gen-fhi.app-backend.core/http-kit [_ {:keys [port]}]
  (let [return (http-kit/run-server (handler/handler) {:port port})]
    (timbre/info "[STARTED] app backend.")
    return))

(defmethod ig/halt-key! :gen-fhi.app-backend.core/http-kit [_ server]
  (timbre/info "[Halting] app backend.")
  (server))

(def cli-options [])

(defn -main [& args]
  (let [config (ig/read-string (slurp (io/resource "gen_fhi/services.edn")))
        parsed-args (parse-opts args cli-options)]
    (ig/load-namespaces config)
    (match (:arguments parsed-args)
      ["app-backend"] (ig/init config [:app-backend/server]))))
