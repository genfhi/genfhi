(ns gen-fhi.app-backend.handler
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [config.core :refer [load-env]]
            [muuntaja.middleware :as muuntaja]
            [muuntaja.core :as m]
            [hiccup.page :as hiccup]
            [reitit.ring :as reitit]
            [ring.middleware.gzip :as gzip]
            [ring.middleware.session]
            [ring.middleware.params]
            [ring.middleware.keyword-params]
            [ring.middleware.anti-forgery]
            [clojure.core.match :refer [match]]

            [gen-fhi.storage.aws-s3.core :as s3]
            [gen-fhi.encryption.providers.core :as encryption-providers]
            [gen-fhi.storage.protocols :as store]
            [gen-fhi.inline-ops.cloud-deploy :refer [->cloud-object-identifier]]
            [gen-fhi.resource-providers.constants :as provider-types]
            [gen-fhi.file.core :as gf]
            [gen-fhi.workspace.git :refer [->git-sha]]
            [gen-fhi.fhir-server.core :as fhir-server]
            [gen-fhi.inline-ops.core :as inline-op]
            [gen-fhi.fhir-terminology.providers.memory :refer [->MemoryTerminologyProvider]]
            [gen-fhi.resource-providers.multi-source.core :as multi-source]
            [gen-fhi.resource-providers.filesystem.core :as filesystem-provider]
            [clojure.data.json :as json]
            [gen-fhi.app-subdomain :as subdomain]
            [gen-fhi.middleware.core :as middleware]
            [gen-fhi.fhir-ops-executor.handler :as op-handler]))

(defonce version (if ((load-env) :dev)
                   "dev"
                   (->git-sha "../")))

(defn- load-frontend-environment
  "Public env vars sent to client "
  []
  (let [env (load-env)]
    (into
     {}
     (filter
      (fn [[key _]]
        ;; [TODO] Should consider remove client and instead use keys prefixed only with app-frontend?
        (or (= (namespace key) "client")
            (= (namespace key) "app-frontend")))
      env))))

(defn- load-backend-environment
  "Private env vars on server"
  []
  (let [env (load-env)]
    (into
     {}
     (filter
      (fn [[key _]]
        ;; [TODO] Should consider remove server and instead use keys prefixed only with app-backend?
        (or (= (namespace key) "server")
            (= (namespace key) "app-backend")))
      env))))

(def ^:private backend_env (load-backend-environment))
(def ^:private client_env (load-frontend-environment))

(defn- ->encryption-provider [env workspace]
  (let [{:keys [server/encryption-type
                server/encryption-local-secret-key

                server/aws-region
                server/aws-kms-access-key-id
                server/aws-kms-access-key-secret]} env]
    (when (some? encryption-type)
      (encryption-providers/->provider
       {:type   encryption-type
        :config (match encryption-type
                  "local"    (do (assert (some? encryption-local-secret-key)
                                         "Must have server__encryption_local_secret_key environment variable set.")
                                 {:secret-key encryption-local-secret-key})

                  "aws-kms"
                  {:workspace         workspace
                   :region            aws-region
                   :access-key-id     aws-kms-access-key-id
                   :access-key-secret aws-kms-access-key-secret})}))))

(defn- ->inline-ops []
  [(inline-op/->external-fhir-request)
   (inline-op/->valueset-expand)])

(defn- ->terminology-provider [fhir-db file-system-provider]
  (->MemoryTerminologyProvider
   (multi-source/->MultiSourceProvider
    [{:id :database
      :source fhir-db}
     {:id :filesystem
      :source file-system-provider}])))

(def capabilities
  {:resourceType "CapabilityStatement"
   :kind         "capability"
   :fhirVersion  "4.0.1"
   :status       "active"
   :date         (.format (java.text.SimpleDateFormat. "yyyy-MM-dd") (java.util.Date.))
   :format       ["application/json" "application/edn" "application/fhir+json"]
   :rest
   [{:mode "server"
     :interaction  [{:code "search"}]
     :resource
     [{:type "Questionnaire"
       :interaction [{:code "read"} {:code "search"}]}
      {:type "ClientApplication"
       :interaction [{:code "read"} {:code "search"}]}
      {:type "ImplementationGuide"
       :interaction []}
      {:type "OperationDefinition"
       :interaction []}
      {:type "Endpoint"
       :interaction [{:code "read"}]}]}]})

(defn- ring-req-fake-user-inject [ring-req]
  (-> ring-req
      (assoc-in [:claims :sub] "app-user")
      (assoc-in [:path-params :workspace]
                (if-let [workspace (get-in ring-req [:path-params :workspace])]
                  workspace
                  (str (java.util.UUID/randomUUID))))))

(defn- ->fhir-db [env workspace app-id _author]
  (let [{:keys [app-backend/app-resource-path
                app-backend/storage-type]}     env
        storage-type                           (or storage-type provider-types/file-system)]
    (cond
      (= provider-types/s3 storage-type)
      (let [s3-storage         (s3/->s3-storage
                                {:workspace         workspace
                                 :region            (get env :server/aws-region)
                                 :bucket-name       (get env :server/aws-s3-bucket-name)
                                 :access-key-id     (get env :server/aws-s3-access-key-id)
                                 :secret-access-key (get env :server/aws-s3-secret-access-key)})
            bundle             (json/read-str
                                (slurp (:Body (store/get-item s3-storage (->cloud-object-identifier app-id))))
                                :key-fn keyword)]

        (filesystem-provider/bundle->memory-client-provider bundle))
      (= provider-types/file-system storage-type)
      (filesystem-provider/file-paths->memory-terminology-provider
       app-resource-path))))

(defn- fhir-req->ctx [env]
  (let [inline-ops (->inline-ops)
        file-system-provider (filesystem-provider/resource-paths->memory-client-provider
                              (gf/path->resources-uris "../cljc/definitions/resources" "terminology"))]
    (fn [req]
       ;; Insantiate db with workspace and author params
      (let [workspace (get req :fhir/workspace)
            author    (get req :author)
            ;; Workspace can publish multiple apps.
            app-id    (get req :fhir/app-id)
            fhir-db   (->fhir-db env workspace app-id author)
            terminology-provider (->terminology-provider fhir-db file-system-provider)]

        (fhir-server/map->CTX
         {:capabilities             capabilities
          :fhir-db                  fhir-db
          :op-handler               (op-handler/->op-handler {:inline-ops inline-ops})
          :encryption-provider      (->encryption-provider env (get req :fhir/workspace))
          :terminology-provider     terminology-provider})))))

(defn- ->fhir-handler []
  (fhir-server/dynamic-spec-import)
  (s/assert :fhir-types/CapabilityStatement capabilities)
  (let [{:keys [app-backend/storage-type]} backend_env
        fhir-handler                       (fhir-server/create
                                            {:capabilities capabilities
                                             :fhir-req->ctx
                                             (fhir-req->ctx backend_env)
                                             :post-process-fhir-req
                                             (cond
                                               (= provider-types/file-system storage-type)
                                               (fn [_req fhir-req]
                                                 (-> fhir-req
                                                     ;; Spoofing this for now but longterm should likely be deriving this from files used.
                                                     (assoc :fhir/workspace "file-system")))
                                               ;; If S3 means published on cloud in which case workspace should be pulled off of the host subdomain.
                                               ;; Additionally in this case unlike filesystem multiple workspaces are using same executor.
                                               (= provider-types/s3 storage-type)
                                               (fn [req fhir-req]
                                                 (let [host       (get-in req [:headers "host"])
                                                       subdomain (first (clojure.string/split host #"\."))
                                                       {:keys [workspace app-id]} (subdomain/subdomain->workspace-identifier subdomain)]
                                                   (-> fhir-req
                                                       (assoc  :fhir/workspace (str workspace))
                                                       (assoc :fhir/app-id (str app-id))))))})]

    (fn
      ([ring-req respond raise]
       (fhir-handler (ring-req-fake-user-inject ring-req) respond raise))
      ([ring-req]
      ;; Quick hack to have a workspace injected (won't have it for app).
       (fhir-handler (ring-req-fake-user-inject ring-req))))))

(defn- client-response []
  {:status 200
   :body
   (hiccup/html5
    [:head
     [:meta {:charset "UTF-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:link {:rel "icon" :href "/assets/icon.svg" :type "image/svg"}]
     [:link {:rel "stylesheet" :href "/assets/antd.min.inc.css"}]
     [:link {:rel "stylesheet" :href (str "/assets/main." version ".css")}]]
    [:body
     [:div {:id "app" :style "height: 100%;"}]
     [:script
      (str "window.env = " (slurp (m/encode "application/json" client_env)) ";"
           "window.version = '" version "';")]
     [:script {:src (str "/assets/cljs-out/js/app-frontend/main." version ".js")
               :type "text/javascript"}]])})

(def ^:private client-handler
  (fn
    ([_req]
     (client-response))
    ([_req respond _raise]
     (respond (client-response)))))

(defn handler []
  (let [fhir-handler (->fhir-handler)]
    (reitit/ring-handler
     (reitit/router
      [["/api"                      {:middleware [muuntaja/wrap-format]}

        ["/v1"
         ["/fhir/*fhir-location"    {:get    fhir-handler
                                     :post   fhir-handler
                                     :patch  fhir-handler
                                     :delete fhir-handler
                                     :put    fhir-handler}]]]

       ["/assets/*"                 (reitit/create-resource-handler)]
      ;; Frontend SPA routing
       ["/"                         {:get client-handler}]
       ["/error"                    {:get client-handler}]
       ["/smart-login/*"            {:get client-handler}]
       ["/:resource-type"
        [""                         {:get client-handler}]
        ["/:id"                     {:get client-handler}]]]

      {:conflicts (constantly nil)})
     (reitit/redirect-trailing-slash-handler {:method :strip})
     {:middleware [middleware/->httpkit-async-middleware
                   gzip/wrap-gzip
                   ring.middleware.session/wrap-session
                   ring.middleware.params/wrap-params
                   ring.middleware.keyword-params/wrap-keyword-params]})))
