(ns gen-fhi.app-subdomain
  (:require [clojure.string :as string]
            [gen-fhi.shortened-uuid.core :as uuidshort])
  (:import [java.util UUID]))

(defn workspace+app-id->subdomain [workspace app-id]
  (let [ret (str
             (uuidshort/uuid->suuid (UUID/fromString workspace))
             "_"
             (uuidshort/uuid->suuid (UUID/fromString app-id)))]
    ret))

(defn subdomain->workspace-identifier [subdomain]
  (let [[workspace app-id] (string/split subdomain #"\_")]
    {:workspace (uuidshort/suuid->uuid workspace)
     :app-id    (uuidshort/suuid->uuid app-id)}))
