(ns gen-fhi.auth0
  (:import (com.auth0.client.auth AuthAPI)
           (com.auth0.client.mgmt ManagementAPI)))

(defn ->auth-api [domain client-id client-secret]
  (AuthAPI. domain client-id client-secret))

(defn ->token [domain client-id client-secret]
  (let [auth  (->auth-api domain client-id client-secret)
        token (.. auth
                  (requestToken (str "https://" domain "/api/v2/"))
                  (execute)
                  (getAccessToken))]
    token))

(defn ->management-api [domain managment-api-token]
  (ManagementAPI.  domain managment-api-token))
