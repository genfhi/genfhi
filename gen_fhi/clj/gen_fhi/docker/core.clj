(ns gen-fhi.docker.core
  (:import [com.github.dockerjava.core DefaultDockerClientConfig DockerClientImpl]
           [com.github.dockerjava.api DockerClient]
           [com.github.dockerjava.zerodep ZerodepDockerHttpClient$Builder]))

(defn ->docker-config [{:keys [host]}]
  (.. DefaultDockerClientConfig
      createDefaultConfigBuilder
      (withDockerHost host)
                          ;; (withRegistryUsername registryUser)
                          ;; (withRegistryPassword registryPass)
                          ;; (withRegistryEmail registryMail)
                          ;; (withRegistryUrl registryUrl)
      build))

(defn ->docker-http-client [config]
  (let [config (if (map? config) (->docker-config config) config)]
    (.. (ZerodepDockerHttpClient$Builder.)
        (dockerHost (.getDockerHost config))
        (build))))

(defn ->docker-client [config]
  (let [docker-config      (->docker-config config)
        docker-http-client (->docker-http-client docker-config)]
    (.. DockerClientImpl
        (getInstance docker-config docker-http-client))))
