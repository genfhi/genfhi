(ns gen-fhi.encryption.core
  (:require [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.encryption.protocols :as enc]))

(def encryption-extension "https://genfhi.com/Extension/encrypt-value")

(defn has-encryptions?
  "Returns nil if no encryption extensions are found on the resource."
  [resource]
  (seq
   (fp/locations
    "$this.descendants().where($this.extension.url=%extUrl).value"
    resource
    {:options/variables
     {:extUrl encryption-extension}})))

(defn encrypt-resource [encryption-provider resource]
  (let [value-locations
        (fp/locations
         "$this.descendants().where($this.extension.url=%extUrl).value"
         resource
         {:options/variables
          {:extUrl encryption-extension}})]
    (reduce
     (fn [resource value-loc]
       (let [expression-loc (str (fp/path-vec->fp value-loc) ".extension.where(url=%extUrl)")
             encryption-ext-loc (first (fp/locations
                                        expression-loc
                                        resource
                                        {:options/variables
                                         {:extUrl encryption-extension}}))
             enc-value (get-in resource (conj encryption-ext-loc :valueString))
             value     (get-in resource value-loc)]
         ;; If value and enc value are not equal perform an encryption on the value
         ;; And associate it to the resource and encryption extension.
         (if (not= value enc-value)
           (let [value-encrypted (enc/encrypt encryption-provider value)]
             (-> resource
                 (assoc-in encryption-ext-loc {:url encryption-extension
                                               :valueString value-encrypted})
                 (assoc-in value-loc value-encrypted)))
           resource)))
     resource
     value-locations)))

(defn decrypt-resource [encryption-provider resource]
  (let [value-locations
        (fp/locations
         "$this.descendants().where($this.extension.url=%extUrl).value"
         resource
         {:options/variables
          {:extUrl encryption-extension}})]
    (reduce
     (fn [resource value-loc]
       (let [value         (get-in resource value-loc)
             decrypt-value (enc/decrypt encryption-provider value)]
         (assoc-in resource value-loc decrypt-value)))
     resource
     value-locations)))
