(ns gen-fhi.encryption.protocols)

(defprotocol Encryption
  (encrypt [this value])
  (decrypt [this encrypted-value]))
