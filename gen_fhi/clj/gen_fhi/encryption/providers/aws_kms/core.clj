(ns gen-fhi.encryption.providers.aws-kms.core
  (:require [clojure.java.io :as io]
            [cognitect.aws.client.api :as aws]
            [cognitect.aws.credentials :as credentials]

            [gen-fhi.encryption.protocols :refer [Encryption]]))

(defn- encode-output-buffer [out]
  (.encodeToString (java.util.Base64/getEncoder) (.toByteArray out)))

(defn- encoded-string-bytes [encoded-string]
  (.decode (java.util.Base64/getDecoder) encoded-string))

(defrecord AWSKMS [key-id kms-client]
  Encryption
  (encrypt [{:keys [kms-client]} value]
    (let [out (java.io.ByteArrayOutputStream.)
          response (aws/invoke kms-client {:op :Encrypt :request
                                           {:KeyId key-id
                                            :Plaintext value}})]
      (when (:__type response)
        (throw (ex-info "Failed to encrypt" {:response response})))
      
      (io/copy (:CiphertextBlob response) out)
      (encode-output-buffer out)))

  (decrypt [{:keys [kms-client]} value]
    (let [byte-array (encoded-string-bytes value)
          response (aws/invoke kms-client {:op :Decrypt :request
                                           {:KeyId key-id
                                            :CiphertextBlob byte-array}})]
      (when (:__type response)
        (throw (ex-info "Failed to decrypt" {:response response})))

      (slurp (:Plaintext response)))))

(defn- create-key [kms-client alias]
  (let [createkey-response (aws/invoke kms-client {:op :CreateKey
                                                   :request
                                                   {:KeyUsage "ENCRYPT_DECRYPT"
                                                    :Tags [{:TagKey "origin", :TagValue "genfhi"}]
                                                    :Origin "AWS_KMS"
                                                    :Description "workspace-key"
                                                    :MultiRegion false}})]
    (aws/invoke kms-client {:op :CreateAlias
                            :request {:AliasName alias
                                      :TargetKeyId (get-in createkey-response [:KeyMetadata :KeyId])}})))

(defn ->provider
  ([info]
   (->provider info {:create-not-found? true}))
  ([{:keys [workspace region access-key-id access-key-secret]} {:keys [create-not-found?]}]
   (let [kms-client (aws/client {:api                  :kms
                                 :region               region
                                 :credentials-provider
                                 (credentials/basic-credentials-provider
                                  {:access-key-id     access-key-id
                                   :secret-access-key access-key-secret})})
         key-id (str "alias/" (str workspace))
         describe-key (aws/invoke kms-client {:op :DescribeKey
                                              :request {:KeyId key-id}})]
     (when (= "NotFoundException" (:__type describe-key))
       (if create-not-found?
         (create-key kms-client key-id)
         (throw (ex-info (str "Key not found for workspace [Not set to generate]")
                         {:workspace workspace
                          :create-not-found? create-not-found?}))))
     (AWSKMS. key-id kms-client))))

