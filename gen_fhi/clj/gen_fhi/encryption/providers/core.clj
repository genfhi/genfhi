(ns gen-fhi.encryption.providers.core
  (:require [gen-fhi.encryption.providers.aws-kms.core :as aws]
            [gen-fhi.encryption.providers.local.core :as local]))

(defmulti ->provider :type)
(defmethod ->provider "aws-kms" [{:keys [config]}]
  (aws/->provider config))
(defmethod ->provider "local" [{:keys [config]}]
  (if config
    (local/->provider config)
    (local/->provider)))
