(ns gen-fhi.encryption.providers.local.core
  (:import [java.util Base64]
           [java.security SecureRandom]
           [java.io ByteArrayOutputStream]
           [javax.crypto Cipher]
           [javax.crypto.spec IvParameterSpec]
           [javax.crypto.spec SecretKeySpec]
           [javax.crypto KeyGenerator])
  (:require [gen-fhi.encryption.protocols :refer [Encryption]]))

(defn secret-key->string [sec-key]
  (let [encoder (Base64/getEncoder)]
    (.encodeToString encoder (.getEncoded sec-key))))

(defn ->secret-key [byte-size]
  (let [key-generator (KeyGenerator/getInstance "AES")]
    (.init key-generator byte-size)
    (.generateKey key-generator)))

(defn string->secret-key [string-key]
  (let [secret-bytes (.decode (Base64/getDecoder) string-key)]
    (SecretKeySpec. secret-bytes "AES")))

(defn- ->iv
  "Generate an IV for encryption"
  []
  (let [iv-byte-array (byte-array 16)]
    (.nextBytes (SecureRandom.) iv-byte-array)
    (IvParameterSpec. iv-byte-array)))

(defn- bytes->iv "Convert bytes to iv"
  [bytes]
  (IvParameterSpec. bytes))

(defn- concat-byte-arrays [b1 b2]
  (let [output-stream (ByteArrayOutputStream.)]
    (.write output-stream b1)
    (.write output-stream b2)
    (.toByteArray output-stream)))

(defn- -encrypt [secret-key value]
  (let [cipher (Cipher/getInstance "AES/CBC/PKCS5Padding")
        iv     (->iv)]
    (.init cipher Cipher/ENCRYPT_MODE secret-key iv)
    (let [cipher-text (.doFinal cipher (.getBytes value))]
      (.encodeToString (Base64/getEncoder) (concat-byte-arrays (.getIV iv) cipher-text)))))

(defn- -decrypt [secret-key value]
  (let [cipher                  (Cipher/getInstance "AES/CBC/PKCS5Padding")
        [iv-bytes cipher-bytes] (split-at 16 (.decode (Base64/getDecoder) value))
        iv                      (bytes->iv (into-array Byte/TYPE iv-bytes))]
    (.init cipher Cipher/DECRYPT_MODE secret-key iv)
    (String. (.doFinal cipher (into-array Byte/TYPE cipher-bytes)))))

(defrecord LocalEncryption [secret-key]
  Encryption
  (encrypt [{:keys [secret-key]} value]
    (-encrypt secret-key value))
  (decrypt [{:keys [secret-key]} value]
    (-decrypt  secret-key value)))

(defn provider->key [provider]
  (secret-key->string
   (:secret-key provider)))

(defn ->provider
  ([]
   (let [sec-key (->secret-key 256)]
     (->provider {:secret-key (secret-key->string sec-key)})))
  ([{:keys [secret-key]}]
   (->LocalEncryption
    (string->secret-key secret-key))))
