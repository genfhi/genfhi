(ns gen-fhi.fhir-ops-executor.aws-lambda.core
  (:import [java.util.zip ZipEntry ZipOutputStream]
           [java.io ByteArrayOutputStream]

           [software.amazon.awssdk.regions Region]
           [software.amazon.awssdk.auth.credentials StaticCredentialsProvider]
           [software.amazon.awssdk.auth.credentials AwsBasicCredentials]
           [software.amazon.awssdk.core SdkBytes]
           [software.amazon.awssdk.services.lambda LambdaClient]
           [software.amazon.awssdk.services.lambda.model InvokeRequest])

  (:require [cognitect.aws.client.api :as aws]
            [clojure.java.io :as io]
            [clojure.data.json :as json]
            [cognitect.aws.credentials :as credentials]
            [gen-fhi.shortened-uuid.core :as suuid]

            [gen-fhi.operations.constants :as op-constant]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.fhir-ops-executor.protocols :refer [FHIROpExecutor]]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [clojure.core.async :as a]
            [taoensso.timbre :as timbre]))

(defn- ->credentials-provider [access-key-id secret-access-key]
  (credentials/basic-credentials-provider
   {:access-key-id     access-key-id
    :secret-access-key secret-access-key}))

(defn ->lambda-client [{:keys [region access-key-id secret-access-key]}]
  (aws/client {:api                  :lambda
               :region               region
               :credentials-provider (->credentials-provider access-key-id secret-access-key)}))

(defn ->tagging-client [{:keys [region access-key-id secret-access-key]}]
  (aws/client {:api                  :resourcegroupstaggingapi
               :region               region
               :credentials-provider (->credentials-provider access-key-id secret-access-key)}))

(defmacro ^:private with-zip-file [zip-sym & body]
  `(with-open [byte-stream# (ByteArrayOutputStream.)
               ~zip-sym    (ZipOutputStream. byte-stream#)]
     ~@body
     byte-stream#))

(defmacro ^:private add-to-zip
  [zip entry-name & body]
  `(let [^ZipOutputStream zip# ~zip]
     (.putNextEntry zip# (ZipEntry. ~entry-name))
     ~@body
     (flush)
     (.closeEntry zip#)))

(def ^:private aws-lambda-shim
  "
exports.handler = function (event, context, callback) {
  operation(
    event,
    (output) => {
      callback(null, output);
    },
    (error) => {
      callback(error);
    }
  );
};
")

(defn- operation-definition->code-to-execute
  "Returns code to be executed from an OperationDefinition.
  Uses custom extension."
  [operation-definition]
  (str
   "const operation = "
   (first
    (fp/evaluate
     "$this.extension.where(url=%codestringurl).valueExpression.expression"
     operation-definition
     {:options/variables
      {:codestringurl
       "https://genfhi.com/OperationDefinition/op-code"}}))
   ";"
   aws-lambda-shim))

(defn- create-code-zip [operation-definition]
  (with-zip-file zip-stream
    (with-open [code-stream (io/input-stream
                             (.getBytes
                              (operation-definition->code-to-execute
                               operation-definition)))]
      (add-to-zip zip-stream "index.js"
                  (io/copy code-stream zip-stream)))))

(defn- ->lambda-name [workspace operation-definition]
  (let [workspace-shortened (suuid/uuid->suuid
                             (java.util.UUID/fromString
                              workspace))
        op-id-shortened     (suuid/uuid->suuid
                             (java.util.UUID/fromString
                              (:id operation-definition)))]
    (str workspace-shortened "__"
         op-id-shortened "__"
         (get-in operation-definition [:meta :versionId]))))

(defn- lambda-res->clj-map [lambda-res]
  (clojure.data.json/read-str
   (slurp
    (:Payload
     lambda-res))
   :key-fn keyword))

(defn- wait-lambda-non-pending [lambda-client lambda-name]
  (let [invoke-get-lambda (fn []
                            (aws/invoke lambda-client {:op :GetFunction
                                                       :request {:FunctionName lambda-name}}))]
    (a/go-loop [lambda (invoke-get-lambda)]
      (if (= "Pending" (get-in lambda [:Configuration :State]))
        (do
          (a/<! (a/timeout 5000))
          (recur (invoke-get-lambda)))
        (get-in lambda [:Configuration :State])))))

(defn- operation->environment-variables
  "Assumes that operation definition has been decrypted."
  [operation-definition]
  (let [environment-variables (fp/evaluate
                               "$this.extension.where(url=%extUrl)"
                               operation-definition
                               {:options/variables {:extUrl op-constant/operation-environment-variable-url}})]
    (reduce
     (fn [aws-env environment-variable-ext]
       (let [name (first (fp/evaluate "$this.value" environment-variable-ext))
             value (first (fp/evaluate "$this.extension.where(url=%valueUrl).value"
                                       environment-variable-ext
                                       {:options/variables
                                        {:valueUrl  op-constant/operation-environment-variable-value-url}}))]
         (if (and name value)
           (assoc aws-env name value)
           aws-env)))
     {}
     environment-variables)))

(defn- ->create-lambda-fn! [lambda-client app-id workspace role operation-definition]
  (let [zip-stream  (create-code-zip operation-definition)
        lambda-name (->lambda-name workspace operation-definition)
        result      (aws/invoke
                     lambda-client
                     {:op :CreateFunction
                      :request
                      {:FunctionName lambda-name
                       :Timeout      60
                       :Runtime      "nodejs14.x"
                       :Role         role
                       :Layers       ["arn:aws:lambda:us-east-2:131386639821:layer:operation_definition_layer:5"]
                       :Handler      "index.handler"
                       :Environment  {:Variables (operation->environment-variables operation-definition)}
                       :Tags         {:appId      app-id
                                      :id         (:id operation-definition)
                                      :workspace  workspace
                                      :versionId (get-in
                                                  operation-definition
                                                  [:meta :versionId])}
                       :Code         {:ZipFile
                                      (.toByteArray zip-stream)}}})]
    (when (contains? result :cognitect.anomalies/category)
      (timbre/error result)
      (oo/throw-outcome (oo/outcome-fatal "exception" "failed to invoke operation.")))
    result))

(defn- ->lambda-arn
  "Attempts to get lambdas arn if exists else returns nil."
  [tagging-client app-id workspace operation-definition]
  (let [results (aws/invoke
                 tagging-client
                 {:op :GetResources
                  :request {:ResourceTypeFilters ["lambda"]
                            :TagFilters [{:Key "appId" :Values [app-id]}
                                         {:Key "workspace" :Values [workspace]}
                                         {:Key "id" :Values [(:id operation-definition)]}
                                         {:Key "versionId", :Values [(get-in operation-definition [:meta :versionId])]}]}})]
    (when (seq (get results :ResourceTagMappingList))
      (:ResourceARN (first (get results :ResourceTagMappingList))))))

(defn- fhir-request->lambda-request-data [request]
  (let [{data :fhir/data workspace :fhir/workspace type :fhir/type id :fhir/id} request
        parameters         (parameters/parameters->map data)
        data-to-lambda     (cond-> {:parameter parameters :workspace workspace}
                             (some? type) (assoc :type type)
                             (some? id)   (assoc :id id))]
    data-to-lambda))

(defn- execute-lambda!
  "[DOES NOT WORK] Because of inability to use ARNS must use execute-lambda-2!
  see [https://github.com/cognitect-labs/aws-api/issues/193]"
  [lambda-client lambda-name fhir-request]
  (let [data-to-lambda (fhir-request->lambda-request-data fhir-request)
        lambda-res
        (aws/invoke
         lambda-client
         {:op :Invoke
          :request
          {:FunctionName lambda-name
           :LogType      "Tail"
           :Payload      (-> data-to-lambda json/json-str .getBytes)}})]
    (when (some? (:FunctionError lambda-res))
      (oo/throw-outcome
       (oo/outcome-error
        "exception"
        "Lambda failed to execute.")))
    (lambda-res->clj-map lambda-res)))

(defn execute-lambda-2! [region access-key-id secret-access-key lambda-arn fhir-request]
  (try
    (let [data-to-lambda     (fhir-request->lambda-request-data fhir-request)
          basic-auth         (AwsBasicCredentials/create access-key-id secret-access-key)
          static-credentials (StaticCredentialsProvider/create basic-auth)
          lambda-client      (.. LambdaClient
                                 (builder)
                                 (credentialsProvider static-credentials)
                                 (region (Region/of region))
                                 (build))
          request            (.. (InvokeRequest/builder)
                                 (functionName lambda-arn)
                                 (payload (SdkBytes/fromByteArray (-> data-to-lambda json/json-str .getBytes)))
                                 (build))

          invoke-result      (.invoke lambda-client request)]
      (clojure.data.json/read-str
       (String.
        (.asByteArray (.payload invoke-result))
        java.nio.charset.StandardCharsets/UTF_8)
       :key-fn keyword))
    (catch Exception e
      (timbre/error e) -
      (oo/throw-outcome
       (oo/outcome-error
        "exception"
        "Lambda failed to execute.")))))

(defn ->op-executor [{:keys [app-id role region access-key-id secret-access-key]}]
  (let [tagging-client (->tagging-client {:region region
                                          :access-key-id access-key-id
                                          :secret-access-key secret-access-key})
        lambda-client (->lambda-client {:region            region
                                        :access-key-id     access-key-id
                                        :secret-access-key secret-access-key})]

    (reify FHIROpExecutor
      (execute [_ operation-definition request]
        (assert (= :fhir/invoke (:fhir/request-type request)))
        (let [{workspace :fhir/workspace} request
              lambda-arn (or (->lambda-arn tagging-client app-id workspace operation-definition)
                             ;; Create lambda func if can't find the arn
                             (let [created-fn (->create-lambda-fn! lambda-client app-id workspace role operation-definition)]
                               (a/<!! (wait-lambda-non-pending
                                       lambda-client
                                       (->lambda-name workspace operation-definition)))
                               (:FunctionArn created-fn)))]
          (execute-lambda-2!
           region
           access-key-id
           secret-access-key
           lambda-arn
           request))))))


