(ns gen-fhi.fhir-ops-executor.handler
  (:require [clojure.pprint :as pprint]

            [async-errors.core :as ae]
            [gen-fhi.operations.validation :as op-validation]
            [gen-fhi.inline-ops.protocols :as inline-op-proto]
            [gen-fhi.inline-ops.core :as inline-op]
            [gen-fhi.fhir-ops-executor.protocols :as operation-execution]
            [gen-fhi.fhir-client.protocols :as fhir-client]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.interceptors.core :as it]
            [gen-fhi.encryption.core :as enc]))

(defn- get-custom-operation [client fhir-request]
  (let [res (ae/<??
             (fhir-client/search
              client
              "OperationDefinition"
              {:code (name (:fhir/op fhir-request))}))]
    (first
     (get res :fhir/response []))))

(defn- request->operation-definition [client inline-op fhir-request]
  (if inline-op
    (inline-op-proto/get-definition inline-op)
    (get-custom-operation client fhir-request)))

(defn- execute-untrusted-operation! [client encryption-provider executioner fhir-request]
  (let [raw-operation (get-custom-operation client fhir-request)
        operation     (if encryption-provider
                        (enc/decrypt-resource encryption-provider raw-operation)
                        raw-operation)]
    (assert
     (and (some? executioner)
          (some? operation))
     (str "operation with name '" (name (:fhir/op fhir-request)) "' does not exist"))

    (operation-execution/execute
     executioner
     operation
     fhir-request)))

(defn ->op-handler [{:keys [inline-ops custom-executioner]}]
  (fn [{:keys [:fhir/client encryption-provider] :as ctx} fhir-request]
    (assert (= :fhir/invoke  (:fhir/request-type fhir-request)))
    (let [inline-op (inline-op/find-op inline-ops (:fhir/op fhir-request))
          operation-definition (request->operation-definition client inline-op fhir-request)
          _                    (assert (some? operation-definition) (str "Could not find operation: '" (:fhir/op fhir-request) "'"))
          input-parameters     (parameters/parameters->map (:fhir/data fhir-request))
          input-validator      (op-validation/op->param-validator operation-definition "in")
          output-validator     (op-validation/op->param-validator operation-definition "out")]
      (when-let [input-validation (input-validator input-parameters)]
        (oo/throw-outcome
         (oo/outcome-error
          "invalid"
          (with-out-str (pprint/pprint input-validation)))))
      (let [result (if inline-op
                        ;; Workspace execution (trusted ops).
                     (inline-op/execute inline-op ctx fhir-request)
                     (execute-untrusted-operation!
                      client
                      encryption-provider
                      custom-executioner
                      fhir-request))]
        (if (it/is-async? result)
          (ae/go-try
           (let [result (ae/<? result)]
             (when-let [output-validation (output-validator result)]
               (oo/throw-outcome
                (oo/outcome-error
                 "invalid"
                 (with-out-str (pprint/pprint output-validation)))))
             {:fhir/response-type :fhir/invoke
              :fhir/response
              (parameters/map->parameters
               operation-definition
               result
               "out")}))
          (do
            (when-let [output-validation (output-validator result)]
              (oo/throw-outcome
               (oo/outcome-error
                "invalid"
                (with-out-str (pprint/pprint output-validation)))))
            {:fhir/response-type :fhir/invoke
             :fhir/response
             (parameters/map->parameters
              operation-definition
              result
              "out")}))))))
