(ns gen-fhi.fhir-ops-executor.protocols)

(defprotocol FHIROpExecutor
  (execute [this operation request]))
