(ns gen-fhi.fhir-ops-executor.validation.core
  (:require [clojure.spec.alpha :as spec]
            [clojure.set :as set]
            [gen-fhi.operations.parameter-parsing.util :refer [required-parameters
                                                               filter-parameters
                                                               parameters+use->parameter-map]]
            [gen-fhi.fhirpath.core :as fp]))



(spec/def ::use #{"in" "out"})

(defn ->parameter-spec [parameter-def-map param]
  (let [param-def-type (get-in parameter-def-map [(:name param) :type])]
    (assert
     (some? param-def-type)
     (str "Could not find a type for parameter name '" (:name param) "'"))
    (keyword
     "fhir-types"
     param-def-type)))

(defn create-parameter-validator [op use]
  (let [required-parameter-names (required-parameters op use)
        parameters               (filter-parameters op use)]))

(defn validate-parameters [op parameters use]
  {:pre [(spec/assert :fhir-types/OperationDefinition op)
         (spec/assert :fhir-types/Parameters parameters)
         (spec/assert ::use use)]}
  (let [required-parameter-names (required-parameters op use)
        parameter-def-map        (parameters+use->parameter-map op use)]
    (assert (set/subset?
             required-parameter-names
             (into #{} (map :name (get parameters :parameter []))))
            "Missing required parameter")

    (for [param (get parameters :parameter [])]
      (let [parameter-spec-keyword (->parameter-spec parameter-def-map param)
            value                  (fp/evaluate "$this.value" param)]
        (spec/assert parameter-spec-keyword value)))
    true))
