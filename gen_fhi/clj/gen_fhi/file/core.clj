(ns gen-fhi.file.core
  (:require [clojure.string :as string]
            [clojure.java.io :as io]))

(defn- paths->resource-uris [file-dir resource-uris files]
  (loop [files files resource-uris resource-uris]
    (let [file-types (reduce
                      (fn [acc file] (if (.isDirectory file)
                                       (update acc :dir      (fn [dirs] (conj dirs file)))
                                       (update acc :resource (fn [resources] (conj resources file)))))
                      {:dir [] :resource []}
                      files)]
      (if (empty? files)
        resource-uris
        (recur
         (mapcat #(rest (file-seq %)) (get file-types :dir []))
         (concat
          resource-uris
          (map #(string/replace
                 (str %)
                 (re-pattern (str file-dir "/"))
                 "")
               (get file-types :resource []))))))))


(defn fn->resource-uris [file-dir path]
  (let [file          [(io/file file-dir path)]
        resource-uris (paths->resource-uris
                       (str (io/file file-dir))
                       []
                       file)]
   resource-uris))
  

(defmacro path->resources-uris
  "Removes file-dir from the resource uri."
  [file-dir path]
  (let [resource-uris (fn->resource-uris file-dir path)]
    `[~@resource-uris]))

