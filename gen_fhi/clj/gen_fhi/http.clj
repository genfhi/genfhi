(ns gen-fhi.http
  (:import [java.net URI]
           [javax.net.ssl
            SNIHostName SSLEngine SSLParameters])
  (:require [org.httpkit.client :as http]))

(def external-timeout 10000)

;; [https://kumarshantanu.medium.com/using-server-name-indication-sni-with-http-kit-client-f7d92954e165]
;; Because SMART app launcher uses SNI needed to configure client to handle it.
(defn- sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setUseClientMode ssl-engine true)
    (.setSSLParameters ssl-engine ssl-params)))

(def ^:private sni-client (http/make-client
                           {:ssl-configurer sni-configure}))

(defn request [request & opts]
  (apply
   http/request
   (assoc request
          :client sni-client
          :timeout external-timeout)
   opts))
