(ns gen-fhi.inline-ops.capability-completion
  (:require
   [clojure.data.json :as json]
   [clojure.core.match :refer [match]]
   [clojure.set :as sets]
   [clojure.string :as string]

   [async-errors.core :as ae]
   [gen-fhi.fhir-terminology.utilities :as term-utils]
   [gen-fhi.operations.core :as workspace-ops]
   [gen-fhi.fhirpath.core :as fp]
   [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
   [gen-fhi.operations.parameter-parsing.core :as parameters]
   [gen-fhi.fhir-client.protocols :as client]
   [gen-fhi.operation-outcome.core :as oo]))

(defn- get-caps-response [fhir-client endpoint]
  (ae/go-try
   (let [endpoint-resource? (= "Endpoint" (get endpoint :resourceType))
         cap-request (json/write-str
                      {:fhir/level        :fhir/system
                       :fhir/request-type :fhir/capabilities})
         response    (ae/<? (client/invoke
                             fhir-client
                             workspace-ops/external-fhir-request
                             (if endpoint-resource?
                               {:endpointResource endpoint
                                :request cap-request}
                               {:endpoint endpoint
                                :request cap-request})))]
     (try
       (let [fhir-response (-> (get-in response [:fhir/response :response])
                               (json/read-str :key-fn keyword))]
         (if (= (:response-type fhir-response) "error")
           (oo/throw-outcome
            (oo/outcome
             (:issue fhir-response)))
           (get fhir-response :response)))
       (catch Exception e
         (if (= (get (ex-data e) :resourceType) "OperationOutcome")
           (throw e)
           (oo/throw-outcome
            (oo/outcome-error
             "exception"
             "Failed to process CapabilityStatement from external response"))))))))

(defn ->all-resource-params [capabilities]
  (fp/evaluate "$this.rest.searchParam" capabilities))

(defn ->resource-params [capabilities resourceType]
  (fp/evaluate "$this.rest.resource.where(type=%resourceType).searchParam",
               capabilities
               {:options/variables {:resourceType resourceType}}))

(defn ->interactions [supported-interactions capabilities parameters]
  (let [is-system? (= "system" (get parameters :level))
        operations (if is-system?
                     (fp/evaluate "$this.rest.operation" capabilities)
                     (fp/evaluate "$this.rest.resource.where(type=%resourceType).operation"
                                  capabilities
                                  {:options/variables {:resourceType (:resourceType parameters)}}))]
    {:interactions
     (concat
      (when (seq operations)
        ["operation"])
      (sets/intersection
       supported-interactions
       (set
        (if is-system?
          (fp/evaluate "$this.rest.interaction.code" capabilities)
          (fp/evaluate "$this.rest.resource.where(type=%resourceType).interaction.code"
                       capabilities
                       {:options/variables {:resourceType (:resourceType parameters)}})))))}))

(defn ->operations [capabilities parameters]
  {:operations
   (match (get parameters :level)
     "system"     (fp/evaluate "$this.rest.operation" capabilities)
     "resource"   (fp/evaluate "$this.rest.resource.where(type=%resourceType).operation"
                               capabilities
                               {:options/variables {:resourceType (:resourceType parameters)}}))})

(defn ->resources [capabilities]
  (let [resource-types (fp/evaluate "$this.rest.resource.type" capabilities)]
    {:resourceTypes resource-types}))

(defn ->search-params [endpoint-capabilities parameters]
  (let [{:keys [level resourceType]} parameters]
    {:searchParameters
     (match level
       "system" (->all-resource-params endpoint-capabilities)
       "resource"   (let [all-resource-params (set (->all-resource-params endpoint-capabilities))
                          resource-params     (set (->resource-params endpoint-capabilities resourceType))]

                      (sort
                       (fn [a b]
                         (match [(string/starts-with? (get a :name "") "_")
                                 (string/starts-with? (get b :name "") "_")]
                           [false true]  -1
                           [true false]  1
                           [true true]   (compare (get a :name "") (get b :name ""))
                           [false false] (compare (get a :name "") (get b :name ""))))
                       (vec
                        (sets/union all-resource-params resource-params))))

       :else [])}))

(defn- supported-interactions-codeset [fhir-client]
  (ae/go-try
   (let [invocation-res (ae/<? (client/invoke
                                fhir-client
                                workspace-ops/expand
                                {:url "http://genfhi.com/restful-interaction"}))]
     (term-utils/valueset->set-codes (get-in invocation-res [:fhir/response :return])))))

(defn ->capability-completion []
  (->resource-operation
   "operation_definitions/genfhi/operation-capability-completion.json"
   (fn [_this ctx request]
     (ae/go-try
      (let [fhir-client             (get ctx :fhir/client)
            parameters              (parameters/parameters->map (:fhir/data request))
            {:keys [endpoint endpointResource type]} parameters
            endpoint-capabilities   (ae/<? (get-caps-response
                                            fhir-client
                                            (or endpoint endpointResource)))
            return (match type
                     "resources"           (->resources endpoint-capabilities)
                     "search-parameters"   (->search-params endpoint-capabilities parameters)
                     "interactions"        (let [supported-interactions (ae/<? (supported-interactions-codeset fhir-client))]
                                             (->interactions supported-interactions endpoint-capabilities parameters))
                     "operations"          (->operations endpoint-capabilities parameters)
                     :else {})]
        return)))))
