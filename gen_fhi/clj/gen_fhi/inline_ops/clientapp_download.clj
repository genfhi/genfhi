(ns gen-fhi.inline-ops.clientapp-download
  (:require
   [clojure.core.match :refer [match]]
   [clojure.string :as string]
   [clojure.core.async :as a]

   [gen-fhi.operation-outcome.core :as oo]
   [gen-fhi.fhirpath.core :as fp]
   [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
   [gen-fhi.operations.parameter-parsing.core :as parameters]
   [gen-fhi.fhir-client.protocols :as client]))

(defn- client-app->references [client-app]
  (concat
   (fp/evaluate "$this.manifest.property.where(name='questionnaireLocal').valueReference.reference" client-app)
   (fp/evaluate "$this.action.endpoint.reference | $this.action.location.reference.where($this!=%smartref)"
                client-app
                {:options/variables {:smartref "SMART"}})))

(defn client-app->resolved-refs [fhir-client client-app]
  ;; use set here because can have multiple references to same value.
  (let [client-valuesets    (get (a/<!! (client/search fhir-client "ValueSet" {}))
                                 :fhir/response
                                 [])
        client-codesystems  (get (a/<!! (client/search fhir-client "CodeSystem" {}))
                                 :fhir/response [])
        references          (set (client-app->references client-app))]
    (concat
     client-valuesets
     client-codesystems
     (map
      (fn [ref]
        (let [[resource-type id] (string/split ref #"/")]
          (:fhir/response
           (a/<!!
            (client/read
             fhir-client
             resource-type
             id)))))
      references))))

(def groupings {:frontend {:name "frontend"
                           :description "Frontend components of app."}
                :backend {:name "backend"
                          :description "Backend components of app."}})

(defn ->groupings []
  (vals groupings))

(defn resolved-resource->grouping-id [resource]
  (match (get resource :resourceType)
    "ClientApp" (get-in groupings [:frontend :name])
    :else       (get-in groupings [:backend  :name])))

(defn client-app->bundle [fhir-client client-app]
  (let [resolved-refs (->> (client-app->resolved-refs fhir-client client-app)
                           (filter some?))]
    {:resourceType "Bundle"
     :type         "collection"
     :entry
     (into
      []
      (concat
       (map
        (fn [resource] {:resource resource})
        (conj resolved-refs client-app))
       [{:resource
         {:id           (str (java.util.UUID/randomUUID))
          :resourceType "ImplementationGuide"
          :url          (str "https://genfhi.com/ImplementationGuide/"
                             (get client-app :id)
                             "/_history/"
                             (get-in client-app [:meta :versionId]))
          :version      (get-in client-app [:meta :versionId])
          :name         (str (get client-app :name) "-" (get-in client-app [:meta :versionId]))
          :title        (get client-app :name)
          :status       "active"
          :useContext   [{:code {:system "http://hl7.org/fhir/ValueSet/usage-context-type"
                                 :code "program"}
                          :valueCodeableConcept {:coding [{:system "https://genfhi.com/fhir/ValueSet/artifact-type"
                                                           :code "genfhi-app"}]}}]
          :experimental false
          :packageId    (str "genfhi-client-app-" (:id client-app) "-" (get-in client-app [:meta :versionId]))
          :publisher    "GenFHI"
          :fhirVersion  ["4.0.1"]
          :definition   {:grouping (->groupings)
                         :resource
                         (into
                          []
                          (conj
                           (map
                            (fn [resource]
                              {:groupingId (resolved-resource->grouping-id
                                            resource)
                               :reference
                               {:reference (str "#/" (:id resource))}})

                            resolved-refs)
                           {:groupingId (resolved-resource->grouping-id
                                         client-app)
                            :reference {:reference (str "#/" (:id client-app))}}))}}}]))}))

(defn ->clientapp-download []
  (->resource-operation
   "operation_definitions/genfhi/operation-clientapp-download.json"
   (fn [_this ctx request]
     (let [parameters           (parameters/parameters->map (:fhir/data request))
           fhir-client          (get ctx :fhir/client)
           client-app-reference (get parameters :client-app)
           [resource-type id]   (string/split (:reference client-app-reference) #"/")
           client-app           (:fhir/response
                                 (a/<!! (client/read
                                         fhir-client
                                         resource-type
                                         id)))
           bundle (client-app->bundle fhir-client client-app)]
       (oo/outcome-validation :fhir-types/Bundle bundle)
       {:return bundle}))))
