(ns gen-fhi.inline-ops.cloud-deploy
  (:require [clojure.data.json :as json]

            [gen-fhi.operations.core :as workspace-ops]
            [gen-fhi.storage.protocols :as store]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.fhir-client.protocols :as client]
            [clojure.core.async :as a]
            [clojure.string :as string]))

(defn ->application [fhir-client reference]
  (let [response (a/<!!
                  (client/invoke
                   fhir-client
                   workspace-ops/clientapp-download
                   {:client-app reference}))]
    (get-in response [:fhir/response :return])))

(defn ->link-to-app [fhir-client reference]
  (let [response (a/<!!
                  (client/invoke
                   fhir-client
                   :cloud-link
                   {:resourceType "Parameters"
                    :parameter
                    [{:name "client-app"
                      :valueReference reference}]}))

        fhir-response (:fhir/response response)
        parameters    (parameters/parameters->map fhir-response)]
    (:return parameters)))

(defn- save-image-storage [storage identifier app-impl]
  (let [data   (.getBytes (json/write-str app-impl))
        result (store/put-item storage identifier data)]
    result))

(defn ->cloud-objects-identifier []
  (str "cloud_deploy/"))

(defn ->cloud-object-identifier
  ([id]
   (->cloud-object-identifier id nil))
  ([id version-id]
   (let [version-id (or version-id "latest")]
     (str (->cloud-objects-identifier) id "/" version-id))))

(defn cloud-identifier->meta [cloud-identifier]
  (let [[_ _ id version-id] (string/split cloud-identifier #"/")]
    {:id id
     :version-id version-id}))

(defn- upload-cloud-storage [storage app-impl identifier]
  (save-image-storage
   storage
   identifier
   app-impl))

(defn ->cloud-deploy [->storage]
  (->resource-operation
   "operation_definitions/genfhi/operation-cloud-deploy.json"
   (fn [_this ctx request]
     (let [storage           (->storage (get request :fhir/workspace))
           fhir-client       (get ctx :fhir/client)
           parameters        (parameters/parameters->map (:fhir/data request))
           app-impl          (->application
                              fhir-client
                              (parameters :client-app))
           client-app        (first (fp/evaluate "$this.entry.resource.where(resourceType='ClientApplication')"
                                                 app-impl))

           identifier        (->cloud-object-identifier (:id client-app))]
       (upload-cloud-storage storage app-impl identifier)
       {:return (->link-to-app fhir-client (parameters :client-app))}))))


