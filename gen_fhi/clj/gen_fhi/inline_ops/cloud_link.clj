(ns gen-fhi.inline-ops.cloud-link
  (:require [clojure.string :as string]
            [gen-fhi.app-subdomain :as subdomain]
            [gen-fhi.storage.protocols :as store]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.inline-ops.cloud-deploy :refer [->cloud-object-identifier]]))

(defn public-app-host+workspace+app->cloud-link [public-app-host workspace client-app-id]
  (str "https://"
       (subdomain/workspace+app-id->subdomain workspace client-app-id)
       "." public-app-host "/ClientApplication/" client-app-id))

(defn ->cloud-link [public-app-host ->storage]
  (->resource-operation
   "operation_definitions/genfhi/operation-cloud-link.json"
   (fn [_this _ctx request]
     (let [workspace         (get request :fhir/workspace)
           storage           (->storage workspace)
           parameters        (parameters/parameters->map (:fhir/data request))
           client-app-id     (second (string/split
                                      (get-in parameters [:client-app  :reference]) #"/"))

           identifier        (->cloud-object-identifier client-app-id)
           content           (store/get-item storage identifier)]
       (if (some? (:Error content))
         {}
         {:return (public-app-host+workspace+app->cloud-link
                   public-app-host
                   workspace
                   client-app-id)})))))


