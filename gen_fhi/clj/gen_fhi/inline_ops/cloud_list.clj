(ns gen-fhi.inline-ops.cloud-list
  (:require [clojure.core.async :as a]
            [clojure.string :as string]

            [gen-fhi.storage.protocols :as store]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.inline-ops.cloud-deploy :refer [->cloud-objects-identifier cloud-identifier->meta]]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.fhir-client.protocols :as client]))

(defn ->cloud-list [->storage]
  (->resource-operation
   "operation_definitions/genfhi/operation-cloud-list-deployments.json"
   (fn [_this ctx request]
     (let [workspace         (get request :fhir/workspace)
           fhir-client       (get ctx :fhir/client)
           storage           (->storage workspace)
           cloud-identifiers (store/list-items storage (->cloud-objects-identifier))
           return            (map
                              (fn [identifier]
                                (let [{:keys [id version-id]} (cloud-identifier->meta identifier)]
                                  {:id id
                                   :versionId version-id}))
                              cloud-identifiers)
           applications      (a/<!! (client/search
                                     fhir-client
                                     {:_id (string/join  "," (map :id return))}))
           return  (into
                    []
                    (map
                     (fn [{:keys [id] :as return-info}]
                       (let [application (first
                                          (filter
                                           #(= id (:id %))
                                           (get applications :fhir/response)))]
                         (if (some? application)
                           (assoc return-info :resource application)
                           return-info)))
                     return))]
       (try
         {:return return}
         (catch Exception _ex
           (oo/throw-outcome
            (oo/outcome-error
             "invalid" "Failed to list cloud deployments "))))))))



