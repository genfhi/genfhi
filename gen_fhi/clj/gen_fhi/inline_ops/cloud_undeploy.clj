(ns gen-fhi.inline-ops.cloud-undeploy
  (:require [clojure.string :as string]

            [gen-fhi.storage.protocols :as store]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.inline-ops.cloud-deploy :refer [->cloud-object-identifier]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]))

(defn ->cloud-undeploy [->storage]
  (->resource-operation
   "operation_definitions/genfhi/operation-cloud-undeploy.json"
   (fn [_this _ctx request]
     (let [storage           (->storage (get request :fhir/workspace))
           parameters        (parameters/parameters->map (:fhir/data request))
           client-app-id     (second (string/split
                                      (get-in parameters [:client-app  :reference]) #"/"))

           identifier        (->cloud-object-identifier client-app-id)
           result            (store/delete-item storage identifier)]
       {:return (if (some? (:Error result))
                  "failure"
                  "success")}))))
