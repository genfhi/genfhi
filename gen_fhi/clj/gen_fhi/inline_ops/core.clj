(ns gen-fhi.inline-ops.core
  (:require [gen-fhi.inline-ops.protocols :refer [get-definition execute!]]

            [gen-fhi.inline-ops.clientapp-download :as clientapp-download]
            [gen-fhi.inline-ops.capability-completion :as capability-completion]
            [gen-fhi.inline-ops.external-fhir :as external-fhir-request]
            [gen-fhi.inline-ops.expand :as valueset-expand]
            [gen-fhi.inline-ops.docker-download :as docker-app-building-op]
            [gen-fhi.inline-ops.cloud-deploy :as cloud-deploy]
            [gen-fhi.inline-ops.cloud-link :as cloud-link]
            [gen-fhi.inline-ops.cloud-undeploy :as cloud-undeploy]
            [gen-fhi.inline-ops.resolve-canonical :as resolve-canonical]
            [gen-fhi.inline-ops.workspace-users :as workspace-users]
            [gen-fhi.inline-ops.invite-user :as invite-user]
            [gen-fhi.inline-ops.cloud-list :as cloud-list]
            [gen-fhi.inline-ops.validation :as validation]))

(def ->clientapp-download     clientapp-download/->clientapp-download)
(def ->capability-completion  capability-completion/->capability-completion)
(def ->external-fhir-request  external-fhir-request/->external-fhir-request)
(def ->valueset-expand        valueset-expand/->valueset-expand)
(def ->docker-app-building-op docker-app-building-op/->docker-app-building-op)
(def ->cloud-deploy           cloud-deploy/->cloud-deploy)
(def ->cloud-link             cloud-link/->cloud-link)
(def ->cloud-undeploy         cloud-undeploy/->cloud-undeploy)
(def ->canonical-resolve      resolve-canonical/->canonical-resolve)
(def ->workspace-users        workspace-users/->workspace-users)
(def ->invite-user            invite-user/->invite-user)
(def ->cloud-list             cloud-list/->cloud-list)
(def ->validate               validation/->validate)

(defn find-op [inline-ops code]
  (first
   (filter
    #(=
      (name (get (get-definition %) :code))
      (name code))
    inline-ops)))

(defn execute
  "Execute an inline op returning output as Parameter resource"
  [inline-op ctx parameters]
  (let [op-result (execute! inline-op ctx parameters)]
    op-result))
