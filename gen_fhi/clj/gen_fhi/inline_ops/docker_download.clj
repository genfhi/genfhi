(ns gen-fhi.inline-ops.docker-download
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]

            [gen-fhi.encryption.providers.core :as encryption-providers]
            [gen-fhi.encryption.providers.local.core :as local-encryption]
            [gen-fhi.workspace.version :refer [version]]
            [gen-fhi.storage.protocols :as store]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.fhir-client.protocols :as client]
            [gen-fhi.operations.core :as workspace-ops]
            [gen-fhi.encryption.core :as enc]
            [clojure.core.async :as a]))

(defn- ->docker-file-content [version]
  (let [app-runner-version (if (= version "dev")
                             "latest"
                             (str "master-" (subs version 0 8)))]

    (format
     "from registry.gitlab.com/genfhi/genfhi:app-runner-%s
COPY  app.json /app/app.json
ENV APP_BACKEND__APP_RESOURCE_PATH=/app
ENV SERVER__ENCRYPTION_TYPE=local
ENV APP_BACKEND__STORAGE_TYPE=file-system
CMD java -jar app-backend-standalone.jar app-backend"
     app-runner-version)))

(defn- docker-build-app [docker-client docker-local-dir implementation-guide]
  (let [docker-file-name    (str docker-local-dir "/Dockerfile")
        app-file-name       (str docker-local-dir "/app.json")
        docker-file-content (->docker-file-content version)]
    (io/make-parents docker-file-name)
    (io/make-parents app-file-name)
    (spit (io/file docker-file-name) docker-file-content)
    (spit (io/file app-file-name) (json/write-str implementation-guide))
    (.. docker-client
        (buildImageCmd (io/file docker-file-name))
        start)))

(defn ->application [fhir-client reference]
  (let [response (a/<!!
                  (client/invoke
                   fhir-client
                   workspace-ops/clientapp-download
                   {:client-app reference}))]
    (get-in response [:fhir/response :return])))

(defn- save-image-storage [storage identifier tar-stream]
  (let [result (store/put-item storage identifier tar-stream)]
    result))

(defn ->storage-identifier [app]
  (assert (some? app))
  (assert (some? (:id app)))
  (assert (some? (get-in app [:meta :versionId])))

  (str "images/" (:id app) "/" (get-in app [:meta :versionId])))

(defn- build-and-upload [storage docker-client dir app-impl identifier]
  (let [build-response    (docker-build-app
                           docker-client
                           dir
                           app-impl)]
    (with-open [image-tar-stream (.. docker-client
                                     (saveImageCmd (.awaitImageId build-response))
                                     exec)]
      (save-image-storage
       storage
       identifier
       image-tar-stream)
      (println "Done Saving Image:"))))

(defn- convert-encryption-to-local [{:keys [current-encryption local-encryption]}
                                    client-app]
  (let [decrypted-client-app (enc/decrypt-resource
                              current-encryption
                              client-app)]

    (enc/encrypt-resource
     local-encryption
     decrypted-client-app)))

(defn ->docker-app-building-op [docker-client ->storage local-dir]
  (->resource-operation
   "operation_definitions/genfhi/operation-docker-build.json"
   (fn [_this ctx request]
     (let [storage             (->storage (get request :fhir/workspace))
           fhir-client         (get ctx :fhir/client)
           parameters          (parameters/parameters->map (:fhir/data request))
           encryption-provider (get ctx :encryption-provider)
           app-impl            (->application
                                fhir-client
                                (parameters :client-app))
           is-encrypted?       (enc/has-encryptions? app-impl)
           local-encryption    (encryption-providers/->provider {:type "local"})
           app-impl            (if is-encrypted?
                                 (convert-encryption-to-local
                                  {:current-encryption encryption-provider
                                   :local-encryption local-encryption}
                                  app-impl)
                                 app-impl)
           client-app          (first (fp/evaluate "$this.entry.resource.where(resourceType='ClientApplication')"
                                                   app-impl))
           dir                 (str local-dir "/"
                                    (get request :fhir/workspace)
                                    "/"
                                    (get client-app :id)
                                    "/"
                                    (get-in client-app [:meta :versionId]))
           identifier          (->storage-identifier client-app)]

       ;; Because deriving new secret commenting out check to use existing cache.
       ;;(when (not (store/exists? storage identifier)))

       (build-and-upload storage docker-client dir app-impl identifier)

       (let [return {:downloadLocation
                     ;; Temp removing contenttype until figure out solution for mimetype validation.
                     {;;:contentType "application/x-tar"
                      :url (store/get-download-link storage (->storage-identifier client-app))}}]
         (if is-encrypted?
           (assoc return :encryption-secret (local-encryption/provider->key local-encryption))
           return))))))


