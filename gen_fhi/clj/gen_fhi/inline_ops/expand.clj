(ns gen-fhi.inline-ops.expand
  (:require
   [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
   [gen-fhi.operations.parameter-parsing.core :as parameters]
   [gen-fhi.fhir-terminology.protocols :as terminology]))

(defn ->valueset-expand []
  (->resource-operation
   "operation_definitions/hl7/operation-valueset-expand.json"
   (fn [_this ctx request]
     (let [terminology-provider (get ctx :terminology-provider)
           valueset-expand      (terminology/expand
                                 terminology-provider
                                 (parameters/parameters->map
                                  (:fhir/data request)))]
       {:return valueset-expand}))))

