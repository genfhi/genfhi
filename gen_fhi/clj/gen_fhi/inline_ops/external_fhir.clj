(ns gen-fhi.inline-ops.external-fhir
  (:require [clojure.data.json :as json]
            [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [clojure.core.async :refer [chan] :as a]
            [taoensso.timbre :as timbre]

            [gen-fhi.http :as http]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.interceptors.core :as it]
            [gen-fhi.fhir-client.utils :refer [fhir-req->http-req http-res->fhir-res]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.fhir-client.protocols :as client]
            [gen-fhi.encryption.core :as enc]
            [gen-fhi.auth.constants :as auth-constants]
            [gen-fhi.fhirpath.core :as fp]
            [async-errors.core :as ae]))

(defn- endpoint->interceptor [endpoint action-headers]
  (assert (some? endpoint) "Endpoint not found.")
  (let [api-url (:address endpoint)
        headers (reduce
                 (fn [acc [key val]]
                   (assoc acc key val))
                 {}
                 (map
                  #(clojure.string/split % #":")
                  (concat
                   (get endpoint :header [])
                   ;; Quick hack because by default not parsing to collection if many...
                   (if (coll? action-headers)
                     action-headers
                     [action-headers]))))]
    (it/->interceptor
     [{:error (fn [v]
                (timbre/error (:interceptors/exception v))
                v)
       :enter
       (it/alt-value
        (fn [fhir-req]
          [fhir-req
           (-> (fhir-req->http-req api-url fhir-req)
               (assoc
                :headers headers
                :as :text))]))}

      {:enter
       (it/alt-value
        (fn [[fhir-req http-req]]
          (let [fhir-response-chan (chan)]
            (try
              (http/request
               http-req
               (fn [http-res]
                 (try
                   (let [http-res (assoc http-res :body (json/read-str
                                                         (:body http-res)
                                                         :key-fn keyword))]
                     (a/>!! fhir-response-chan
                            (http-res->fhir-res fhir-req http-res)))
                   (catch Exception ex
                     (timbre/error ex)
                     (a/>!!
                      fhir-response-chan
                      (ex-info "OperationOutcome"
                               (oo/outcome-error
                                "exception"
                                (str (http-res :body)))))))))
              (catch Exception _ex
                (a/>!!
                 fhir-response-chan
                 (ex-info "OperationOutcome"
                          (oo/outcome-error
                           "exception"
                           "Failed remote request.")))))
            fhir-response-chan)))}])))

(defn- request-parameter->fhir-req [parameter]
  (let [request (json/read-str parameter :key-fn #(keyword "fhir" %))]
    ;; Need to alter request type to namespaced fhir keyword
    (assoc request :fhir/request-type (keyword "fhir" (:fhir/request-type request))
           :fhir/level (keyword "fhir" (:fhir/level request)))))

(defn oauth2-token-request [{:keys [token-url client-id client-secret resource audience]}]
  (let [auth-response-chan (chan)]
    (try
      (http/request
       {:method :post
        :url token-url
        :headers {"Content-Type" "application/x-www-form-urlencoded"}
        :body (str
               "client_id=" client-id
               (if resource (str "&resource=" resource) "")
               (if audience (str "&audience=" audience) "")
               "&client_secret=" client-secret
               "&grant_type=client_credentials")}
       (fn [res]
         (let [token-res (clojure.data.json/read-str (:body res)
                                                     :key-fn keyword)]
           (try
             (a/>!! auth-response-chan {:token token-res})
             (catch Exception ex
               (a/>!! auth-response-chan {:error ex}))))))
      (catch Exception ex
        (a/>!! auth-response-chan {:error ex})))
    auth-response-chan))

(defn endpoint-handler-auth [endpoint]
  (ae/go-try
   (let [auth-ext-expr "$this.extension.where(url=%authUrl).value"
         auth-type
         (or
          (first
           (fp/evaluate
            auth-ext-expr
            endpoint
            {:options/variables
             {:authUrl auth-constants/auth-type-url}}))
          "none")]
     (match auth-type
       "none"             endpoint
       ;; Should be performing most operations on the clientside except capability completions.
       "smart-ehr"        endpoint
       "smart-standalone" endpoint
       "oauth2-confidential"
       (let [token-url     (first (fp/evaluate auth-ext-expr
                                               endpoint
                                               {:options/variables {:authUrl auth-constants/auth-oauth2-token-url}}))
             resource      (first (fp/evaluate auth-ext-expr
                                               endpoint
                                               {:options/variables {:authUrl auth-constants/auth-oauth2-resource}}))
             audience      (first (fp/evaluate auth-ext-expr
                                               endpoint
                                               {:options/variables {:authUrl auth-constants/auth-oauth2-audience}}))
             client-id     (first (fp/evaluate auth-ext-expr
                                               endpoint
                                               {:options/variables {:authUrl auth-constants/auth-oauth2-client-id}}))
             client-secret (first (fp/evaluate auth-ext-expr
                                               endpoint
                                               {:options/variables {:authUrl auth-constants/auth-oauth2-client-secret}}))
             response (ae/<? (oauth2-token-request
                              {:audience      audience
                               :token-url     token-url
                               :client-id     client-id
                               :client-secret client-secret
                               :resource      resource}))]
         (if (get response :error)
           (oo/throw-outcome
            (oo/outcome-error
             "exception"
             "Failed to authorize"))
           (let [access_token (get-in response [:token :access_token])]
             (update
              endpoint
              :header
              (fn [headers]
                (conj headers
                      (str "Authorization: Bearer " access_token)))))))

;; Perform token exchange here or check cache if token exists.)))

       "basic" (update
                endpoint
                :header
                (fn [headers]
                  (let [basic-username (first (fp/evaluate auth-ext-expr
                                                           endpoint
                                                           {:options/variables {:authUrl auth-constants/auth-basic-username-url}}))
                        basic-password (first (fp/evaluate auth-ext-expr
                                                           endpoint
                                                           {:options/variables {:authUrl auth-constants/auth-basic-password-url}}))]
                    (conj headers
                          (str
                           "Authorization: Basic "
                           (.encodeToString
                            (java.util.Base64/getEncoder)
                            (.getBytes (str basic-username ":" basic-password))))))))))))

(defn- parameters->endpoint [encryption-provider fhir-client parameters]
  (ae/go-try
   (if-let [endpoint-resource (get parameters :endpointResource)]
     endpoint-resource
     (let [[_resource-type id] (string/split (get-in parameters [:endpoint :reference]) #"/")
           endpoint            (:fhir/response
                                (ae/<? (client/read
                                        fhir-client
                                        "Endpoint"
                                        id)))]
       (ae/<? (endpoint-handler-auth
               (if (some? encryption-provider)
                 (enc/decrypt-resource encryption-provider endpoint)
                 endpoint)))))))

(defn ->external-fhir-request []
  (->resource-operation
   "operation_definitions/genfhi/operation-external-fhir.json"
   (fn [_this ctx request]
     (ae/go-try
      (let [fhir-client         (get ctx :fhir/client)
            encryption-provider (get ctx :encryption-provider)
            parameters          (parameters/parameters->map (:fhir/data request))
            endpoint            (ae/<? (parameters->endpoint encryption-provider fhir-client parameters))
            headers             (get parameters :header [])
            interceptor         (endpoint->interceptor endpoint headers)
            fhir-request        (request-parameter->fhir-req (:request parameters))
            fhir-response       (ae/<? (interceptor fhir-request))]
        {:response (json/write-str fhir-response)})))))
