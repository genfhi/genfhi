(ns gen-fhi.inline-ops.invite-user
  (:require [clojure.core.match :refer [match]]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.auth0 :refer [->management-api ->token ->auth-api]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.operation-outcome.core :as oo])

  (:import (com.auth0.json.mgmt.users User)
           (com.auth0.exception Auth0Exception APIException)))

(defn fixed-length-password
  ([] (fixed-length-password 15))
  ([n]
   (let [chars (map char (range 33 127))
         password (take n (repeatedly #(rand-nth chars)))]
     (reduce str password))))

(def connection-name "Username-Password-Authentication")

(defn ->user [client-id  {:keys [workspace email]}]
  (doto (User.)
    (.setEmail email)
    (.setAppMetadata {client-id
                      {"workspaces"
                       [workspace]}})
    (.setConnection connection-name)
    (.setVerifyEmail true)
    (.setPassword (fixed-length-password 24))))

(defn ->invite-user [{:keys [domain app-client-id management-client-id management-client-secret]}]
  (->resource-operation
   "operation_definitions/genfhi/operation-invite-user.json"
   (fn [_this _ctx request]
     (let [workspace         (get request :fhir/workspace)
           parameters        (parameters/parameters->map (:fhir/data request))
           token             (->token domain management-client-id management-client-secret)
           management-client (->management-api domain token)
           auth-client       (->auth-api domain management-client-id management-client-secret)]
       (try
         (let [_user-create-res (.. management-client
                                    (users)
                                    (create (->user app-client-id {:workspace workspace
                                                                   :email (:email parameters)}))
                                    (execute))
               _reset-password-res (.. auth-client
                                       (resetPassword (:email parameters) connection-name)
                                       (execute))]
           {:status "success"})
         (catch APIException api-ex
           (match (.getStatusCode api-ex)
             409 (oo/throw-outcome
                  (oo/outcome-error
                   "conflict"
                   (str
                    "User already has a workspace with the email '"
                    (:email parameters)
                    "'. To add an existing user to multiple workspaces you must contact 'dev@genfhi.app'.")))
             :else (oo/throw-outcome
                    (oo/outcome-error
                     "invalid"
                     (str "Unable to create user with email '" (:email parameters) "'. Contact 'dev@genfhi.app' if you need assistance.")))))
         (catch Auth0Exception _auth0-ex
           (oo/throw-outcome
            (oo/outcome-fatal
             "invalid"
             (str "Unable to process request for user with email '" (:email parameters) "'")))))))))







