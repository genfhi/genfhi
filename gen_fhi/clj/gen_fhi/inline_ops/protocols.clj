(ns gen-fhi.inline-ops.protocols
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]))

(defprotocol InlineOp
  (get-definition [this])
  (execute! [this ctx parameters]))

(defn ->resource-operation [resource-uri executor]
  (let [operation-definition (json/read-str
                              (slurp (io/resource resource-uri))
                              :key-fn keyword)]
    (reify
      InlineOp
      (get-definition [_] operation-definition)
      (execute! [this ctx request]
        (executor this ctx request)))))
