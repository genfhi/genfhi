(ns gen-fhi.inline-ops.resolve-canonical
  (:require [clojure.core.async :as a]
            [clojure.data.json :as json]
            [taoensso.timbre :as timbre]

            [gen-fhi.http :as http]
            [gen-fhi.operation-outcome.core :as oo]
            [gen-fhi.operations.core :as workspace-ops]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.fhir-client.protocols :as client]
            [async-errors.core :as ae]))

(defn- resolve-endpoint [fhir-client parameter]
  (ae/go-try
   (let [response (ae/<?
                   (client/invoke
                    fhir-client
                    workspace-ops/external-fhir-request
                    {:endpoint   (:endpoint parameter)
                     :request (json/write-str
                               {:fhir/level :fhir/system
                                :fhir/request-type :fhir/search
                                :fhir/parameters {:url (:url parameter)}})}))]

     (try
       (-> (get-in response [:fhir/response :response])
           (json/read-str :key-fn keyword)
           (get :response)
           (first))
       (catch Exception e
         (timbre/error e)
         (oo/throw-outcome
          (oo/outcome-error
           "error"
           "Failed to resolve endpoint during canonical resolution")))))))

(defn- resolve-http [parameter]
  (let [response-chan (a/chan)]
    (try
      (http/request
       {:method :get
        :headers {"Content-Type" "application/json"}
        :url (:url parameter)}
       (fn [http-res]
         (try
           (a/>!! response-chan (json/read-str
                                 (:body http-res)
                                 :key-fn keyword))
           (catch Exception ex
             (timbre/error ex)
             (a/>!! response-chan nil)))))
      (catch Exception ex
        (timbre/error ex)
        (a/>!!
         (ex-info
          "OperationOutcome"
          (oo/outcome-error
           "error"
           "Failed to resolve canonical")))))
    response-chan))

(defn resolve-canonical [fhir-client parameter]
  (ae/go-try
   (let [endpoint-resolution (ae/<? (resolve-endpoint fhir-client parameter))]
     (if endpoint-resolution
       endpoint-resolution
       (ae/<? (resolve-http parameter))))))

(defn ->canonical-resolve []
  (->resource-operation
   "operation_definitions/genfhi/operation-canonical-resolve.json"
   (fn [_this ctx request]
     (ae/go-try
      (let [fhir-client     (get ctx :fhir/client)
            parameters      (parameters/parameters->map (:fhir/data request))
            resolution      (ae/<? (resolve-canonical fhir-client parameters))]
        (if (some? resolution)
          {:return resolution}
          {}))))))
