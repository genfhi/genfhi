(ns gen-fhi.inline-ops.validation
  (:require [clojure.core.match :refer [match]]

            [gen-fhi.questionnaire-validation.core :as questionnaire-validation]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.operation-outcome.core :as oo]))

(defn ->validate []
  (->resource-operation
   "operation_definitions/hl7/operation-validate.json"
   (fn [_this _ctx request]
     (let [params                         (parameters/parameters->map (:fhir/data request))
           resource                       (get params :resource)
           resource-type                  (get resource :resourceType)]
       (if (nil? resource-type)
         {:return (oo/outcome-error
                   "error"
                   "Invalid resource passed in.")}
         (try
           (match resource-type
             "QuestionnaireResponse" (questionnaire-validation/validate
                                      (first (get resource :contained))
                                      resource)
             :else                   (oo/outcome-validation
                                      (keyword "fhir-types" resource-type)
                                      resource))

           {:return (oo/outcome-information
                     "success"
                     "Validation was successful.")}
           (catch Exception ex
             (let [operation-outcome (ex-data ex)]
               {:return operation-outcome}))))))))
