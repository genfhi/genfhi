(ns gen-fhi.inline-ops.workspace-users
  (:require [gen-fhi.inline-ops.protocols :refer [->resource-operation]]
            [gen-fhi.auth0 :refer [->management-api ->token]])
  (:import  (com.auth0.client.mgmt.filter UserFilter)))

(defn user-workspace-query [app-client-id workspace]
  (str
   "app_metadata." app-client-id ".workspaces:\"" workspace "\""))

(defn get-workspace-users [management-client client-id workspace]
  (let [query (.withQuery (UserFilter.) (user-workspace-query client-id workspace))]
    (.. management-client
        (users)
        (list query)
        (execute))))

(defn auth0-results->Persons [auth0-user-page]
  (map
   (fn [user]
     {:resourceType "Person"
      :name         [(cond-> {:text   (or (.getName user) "Unknown")}
                       (some? (.getFamilyName user)) (assoc :family (.getFamilyName user))
                       (some? (.getGivenName user))  (assoc :given [(.getGivenName user)]))]
      :photo        (cond-> {:title "User picture"}
                      (some? (.getPicture user)) (assoc :url (.getPicture user)))
      :telecom      [(cond-> {:system  "email"}
                       (some? (.getEmail user)) (assoc :value (.getEmail user)))]})
   (.getItems auth0-user-page)))

(defn ->workspace-users [{:keys [domain app-client-id management-client-id management-client-secret]}]
  (->resource-operation
   "operation_definitions/genfhi/operation-workspace-users.json"
   (fn [_this _ctx request]
     (let [workspace         (get request :fhir/workspace)
           token (->token domain management-client-id management-client-secret)
           management-client (->management-api domain token)
           results (get-workspace-users management-client app-client-id workspace)]
       {:return (auth0-results->Persons results)}))))


