(ns gen-fhi.middleware.core
  (:require [org.httpkit.server :as http]))

(def workspace-key "https://genfhi.com/workspaces")
(defn- can-access-workspace? [ring-req]
  (let [user-authorized-workspaces (get-in ring-req [:claims workspace-key])
        workspace-attempting-to-access (get-in ring-req [:path-params :workspace])]
    (some #(= % workspace-attempting-to-access) user-authorized-workspaces)))

(defn process-workspace-middleware
  "Associates :workspace and :author keys on ring-req based on passed in fns."
  [{:keys [req->workspace req->author]}]
  (let [process (fn [req]
                  (assoc req
                         :workspace (req->workspace req)
                         :author (req->author req)))]
    (fn [handler]
      (fn
        ([ring-req respond raise]
         (handler
          (process ring-req)
          respond
          raise))
        ([ring-req]
         (handler
          (process ring-req)))))))

(defn validate-workspace-middleware [handler]
  (fn
    ([ring-req respond raise]
     (if (can-access-workspace? ring-req)
       (handler ring-req respond raise)
       (respond {:status 401 :body "Can't access workspace."})))
    ([ring-req]
     (if (can-access-workspace? ring-req)
       (handler ring-req)
       {:status 401 :body "Can't access workspace."}))))

(defn ->httpkit-async-middleware [handler]
  (fn [request]
    (let [response (http/as-channel
                    request
                    {:on-open
                     (fn [ch]
                       (handler
                        request
                        #(http/send! ch %)
                        #(http/close ch)))})]
      response)))

(defn ->middleware-logger [handler note]
  (fn [request respond raise]
    (println (str note ":received"))
    (handler request (fn [res]
                       (println (str note ":responded"))
                       (respond res))
             raise)))
