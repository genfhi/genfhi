(ns gen-fhi.storage.aws-s3.core
  (:require [cognitect.aws.client.api :as aws]
            [cognitect.aws.credentials :as credentials]

            [gen-fhi.storage.protocols :refer [Storage Downloader]]
            [gen-fhi.storage.aws-s3.pre-sign :refer [->pre-signer]]))

(defn- ->credentials-provider [access-key-id secret-access-key]
  (credentials/basic-credentials-provider
   {:access-key-id     access-key-id
    :secret-access-key secret-access-key}))

(defn- ->s3-client [{:keys [region access-key-id secret-access-key]}]
  (let [s3 (aws/client {:api                  :s3
                        :region               region
                        :credentials-provider (->credentials-provider access-key-id secret-access-key)})]
    (aws/validate-requests s3 true)
    s3))

(defn- ->identifier [workspace identifier]
  (str workspace "/" identifier))

(defrecord S3Storage [bucket-name workspace s3 pre-signer]
  Downloader
  (get-download-link [{:keys [pre-signer workspace]} identifier]
    (str (.url (pre-signer bucket-name (->identifier workspace identifier)))))

  Storage
  (list-items [{:keys [bucket-name workspace s3]} identifier]
    (let [response (aws/invoke s3 {:op :ListObjects
                                   :request
                                   {:Bucket bucket-name
                                    :Prefix (->identifier workspace identifier)}})]
      (map
       :Key
       (get response :Contents))))
  (exists?     [{:keys [bucket-name workspace s3]} identifier]
    (let [response (aws/invoke s3 {:op :ListObjectsV2
                                   :request {:Prefix (->identifier workspace identifier)
                                             :Bucket bucket-name}})]
      (< 0 (get response :KeyCount))))
  (get-item    [{:keys [bucket-name workspace s3]} identifier]
    (aws/invoke s3 {:op :GetObject
                    :request
                    {:Bucket bucket-name
                     :Key (->identifier workspace identifier)}}))
  (delete-item [{:keys [bucket-name workspace s3]} identifier]
    (aws/invoke s3 {:op :DeleteObject
                    :request
                    {:Bucket bucket-name
                     :Key (->identifier workspace identifier)}}))
  (put-item    [{:keys [bucket-name workspace s3]} identifier data]
    (aws/invoke s3 {:op :PutObject
                    :request
                    {:Bucket bucket-name :Key (->identifier workspace identifier)
                     :Body data}})))

(defn- create-workspaces-bucket [s3 region bucket-name]
  (let [current-buckets (aws/invoke s3 {:op :ListBuckets})
        bucket-exists?  (some #(= (:Name %) bucket-name)
                              (:Buckets current-buckets))]
    (when (not bucket-exists?)
      (aws/invoke s3 {:op :CreateBucket :request {:Bucket bucket-name
                                                  :CreateBucketConfiguration
                                                  {:LocationConstraint region}}}))))

(defn ->s3-storage [{:keys [bucket-name workspace region access-key-id secret-access-key]}]
  (let [s3 (->s3-client
            {:region region
             :access-key-id access-key-id
             :secret-access-key secret-access-key})
        ;;bucket-creation (create-workspaces-bucket s3 region bucket-name)
        pre-signer (->pre-signer {:access-key-id access-key-id :secret-access-key secret-access-key :region region})]
    (S3Storage. bucket-name workspace s3 pre-signer)))
