(ns gen-fhi.storage.aws-s3.pre-sign
  (:import [software.amazon.awssdk.auth.credentials StaticCredentialsProvider]
           [software.amazon.awssdk.auth.credentials AwsBasicCredentials]
           [software.amazon.awssdk.services.s3.model GetObjectRequest]
           [software.amazon.awssdk.services.s3.presigner.model GetObjectPresignRequest]
           [software.amazon.awssdk.services.s3.presigner S3Presigner]
           [software.amazon.awssdk.regions Region]))

(defn ->pre-signer [{:keys [access-key-id secret-access-key region]}]
  (fn [bucket-name key-name]
    (let [basic-auth         (AwsBasicCredentials/create access-key-id secret-access-key)
          static-credentials (StaticCredentialsProvider/create basic-auth)
          presigner          (.. S3Presigner
                                 (builder)
                                 (credentialsProvider static-credentials)
                                 (region (Region/of region))
                                 (build))
          get-object-request (.. GetObjectRequest builder
                                 (bucket bucket-name)
                                 (key key-name)
                                 build)
          get-object-presign-request (.. GetObjectPresignRequest builder
                                         (signatureDuration (java.time.Duration/ofMinutes 10))
                                         (getObjectRequest get-object-request)
                                         build)]
      (.presignGetObject presigner get-object-presign-request))))
      ;; System.out.println("Presigned URL: " + presignedGetObjectRequest.url()))));)))
