(ns gen-fhi.storage.protocols)

(defprotocol Storage
  (list-items [this identifier])
  (get-item [this identifier])
  (delete-item [this identifier])
  (put-item [this identifier data])
  (exists?   [this identifier]))


(defprotocol Downloader
  (get-download-link [this identifier]))
