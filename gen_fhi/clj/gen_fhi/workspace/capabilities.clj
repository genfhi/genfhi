(ns gen-fhi.workspace.capabilities)

(defn- full-capabilities []
  [{:code "read"}
   {:code "vread"}
   {:code "history"}
   {:code "search"}
   {:code "update"}
   {:code "delete"}
   {:code "patch"}
   {:code "create"}])

(def capabilities
  {:resourceType "CapabilityStatement"
   :kind         "capability"
   :fhirVersion  "4.0.1"
   :status       "active"
   :date         (.format (java.text.SimpleDateFormat. "yyyy-MM-dd") (java.util.Date.))
   :format       ["application/json" "application/edn" "application/fhir+json"]
   :rest
   [{:mode "server"
     :interaction  [{:code "search"} {:code "transaction"}]
     :resource
     [{:type "Questionnaire"
       :interaction (full-capabilities)}
      {:type "ClientApplication"
       :interaction (full-capabilities)}
      {:type "ImplementationGuide"
       :interaction (full-capabilities)}
      {:type "OperationDefinition"
       :interaction (full-capabilities)}
      {:type "Endpoint"
       :interaction
       [{:code "read"}
        {:code "vread"}
        {:code "history"}
        {:code "search"}
        {:code "update"}
        {:code "delete"}
        {:code "create"}]}
      {:type "Organization"
       :interaction (full-capabilities)}
      {:type "ValueSet"
       :interaction (full-capabilities)}
      {:type "CodeSystem"
       :interaction (full-capabilities)}]}]})
