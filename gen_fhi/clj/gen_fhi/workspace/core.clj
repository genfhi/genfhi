(ns gen-fhi.workspace.core
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.core.match :refer [match]]
            [org.httpkit.server :as http-kit]
            [taoensso.timbre :as timbre]
            [integrant.core :as ig]))

(defmethod ig/init-key :gen-fhi.workspace.core/http-kit [_ {:keys [handler port]}]
  (let [server (http-kit/run-server handler {:port port})]
    (timbre/info "workspace server initiated")
    server))

(defmethod ig/halt-key! :gen-fhi.workspace.core/http-kit [_ server]
  (server))

(def cli-options [])

(defn -main [& args]
  (let [config (ig/read-string (slurp (io/resource "gen_fhi/services.edn")))
        parsed-args (parse-opts args cli-options)]
    (ig/load-namespaces config)
    (match (:arguments parsed-args)
      ["workspace"] (ig/init config [:workspace-migrate/migrate
                                     :workspace-backend/server
                                     :workspace-backend/channel-server])
      ["migrate"]   (ig/init config [:workspace-migrate/migrate]))))
