(ns gen-fhi.workspace.database
  (:require [next.jdbc :as jdbc]
            [gen-fhi.resource-providers.postgres.core :as postgres]
            [gen-fhi.workspace.capabilities :as capabilities]))

(defn ->fhir-db [pg-config]
  (fn [workspace author]
    (postgres/->PGFHIRDatabase
     capabilities/capabilities
     (java.util.UUID/fromString workspace)
     author
     (jdbc/get-datasource
      (assoc
       pg-config
       :dbtype "postgres")))))

