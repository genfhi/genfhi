(ns gen-fhi.workspace.endpoint-environment
  (:require [gen-fhi.fhirpath.core :as fp]))

(defn apply-endpoint-environment
  "Merges contained endpoint based on environment provided.
  Environments are used as Organization references in managingOrganization."
  [environment endpoint]
  (let [endpoint (if (some? environment)
                   (let [override (first
                                   (fp/evaluate
                                    "$this.contained.where(managingOrganization.reference=%environment.reference)"
                                    endpoint
                                    {:options/variables {:environment environment}}))]

                     (merge
                      (dissoc endpoint :extension)
                      override))

                   endpoint)]
    (dissoc endpoint :contained)))
