(ns gen-fhi.workspace.environment
  (:require [config.core :refer [load-env]]))

(defn ->client-env
  "Public env vars sent to client"
  []
  (let [env (load-env)]
    (into
     {}
     (filter
      (fn [[key _]]
        (= (namespace key) "client"))
      env))))

(defn ->server-env
  "Private env vars on server"
  []
  (let [env (load-env)]
    env))
