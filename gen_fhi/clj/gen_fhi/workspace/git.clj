(ns gen-fhi.workspace.git
  (:import  [org.eclipse.jgit.api Git])
  (:require [clojure.java.io :as io]))

(defmacro ->git-sha [git-directory]
  (let [repo (.call (.setDirectory (Git/init) (io/file git-directory)))]
    (.getName (.next (.iterator (.call (.setMaxCount (.log repo) 1)))))))
