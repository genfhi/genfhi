(ns gen-fhi.workspace.handler
  (:require [clojure.core.match :refer [match]]
            [clojure.spec.alpha :as s]
            [integrant.core :as ig]
            [hiccup.page :as hiccup]
            [muuntaja.middleware :as muuntaja]
            [muuntaja.core :as m]
            [reitit.ring :as reitit]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.jwt :as jwt]
            [ring.middleware.gzip :as gzip]
            [ring.middleware.anti-forgery]
            [ring.middleware.session]
            [ring.redis.session :refer [redis-store]]
            [ring.middleware.params]
            [ring.middleware.keyword-params]
            [clj-json-patch.core :as patch]

            [gen-fhi.workspace.endpoint-environment :as ee]
            [gen-fhi.middleware.core :as middleware]
            [gen-fhi.workspace.environment :as env]
            [gen-fhi.workspace.database :as wdb]
            [gen-fhi.fhir-ops-executor.aws-lambda.core :as lambda]
            [gen-fhi.workspace.capabilities :as capabilities]
            [gen-fhi.resource-providers.multi-source.core :as multi-source]
            [gen-fhi.resource-providers.filesystem.core :as filesystem]
            [gen-fhi.file.core :as gf]
            [gen-fhi.storage.aws-s3.core :as s3]
            [gen-fhi.encryption.providers.core :as encryption-provider]
            [gen-fhi.docker.core :refer [->docker-client]]
            [gen-fhi.workspace.version :refer [version]]
            [gen-fhi.fhir-server.core :as fhir-server]
            [gen-fhi.inline-ops.core :as inline-op]
            [gen-fhi.fhir-terminology.providers.memory :refer [->MemoryTerminologyProvider]]
            [gen-fhi.fhir-ops-executor.handler :as op-handler]
            [gen-fhi.fhir-client.protocols :as fhir-client]
            [gen-fhi.encryption.core :as enc]
            [clojure.string :as string]))

(defn- ->s3-storage [env]
  (let [{:keys [server/aws-region
                server/aws-s3-access-key-id
                server/aws-s3-secret-access-key
                server/aws-s3-bucket-name]} env]
    (fn [workspace]
      (s3/->s3-storage
       {:workspace         workspace
        :region            aws-region
        :bucket-name       aws-s3-bucket-name
        :access-key-id     aws-s3-access-key-id
        :secret-access-key aws-s3-secret-access-key}))))

(defn- ->custom-operation-executioner [env]
  (let [{:keys [server/aws-lambda-access-key-id
                server/aws-lambda-secret-access-key
                server/aws-lambda-role
                server/aws-region
                client/auth0-client-id]} env]
    (when aws-lambda-access-key-id
      (lambda/->op-executor
       {:app-id            auth0-client-id
        :role              aws-lambda-role
        :region            aws-region
        :access-key-id     aws-lambda-access-key-id
        :secret-access-key aws-lambda-secret-access-key}))))

(defn- ->inline-ops [env]
  (filter
   some?
   [(inline-op/->validate)
    (inline-op/->capability-completion)
    (inline-op/->external-fhir-request)
    (inline-op/->valueset-expand)
    (inline-op/->clientapp-download)
    (when (some? (:server/docker-host env))
      (inline-op/->docker-app-building-op
       (->docker-client {:host (:server/docker-host env)})
       (->s3-storage env)
       (:server/docker-dir env)))
    (inline-op/->cloud-deploy (->s3-storage env))
    (inline-op/->cloud-undeploy (->s3-storage env))
    (inline-op/->workspace-users
     {:app-client-id            (:client/auth0-client-id env)
      :domain                   (:client/auth0-domain env)
      :management-client-id     (:server/auth0-management-client-id env)
      :management-client-secret (:server/auth0-management-client-secret env)})
    (inline-op/->invite-user
     {:app-client-id            (:client/auth0-client-id env)
      :domain                   (:client/auth0-domain env)
      :management-client-id     (:server/auth0-management-client-id env)
      :management-client-secret (:server/auth0-management-client-secret env)})
    (inline-op/->cloud-link (:server/public-app-host env) (->s3-storage env))
    (inline-op/->cloud-list (->s3-storage env))
    (inline-op/->canonical-resolve)]))

(defn- terminology-search
  "Override to search for all terminology when _all is present allows searching within memory provider."
  [{:keys [fhir-db terminology-provider]} req]
  (let [resource-type (get req :fhir/type)
        parameters (get req :fhir/parameters)]
    {:fhir/level :fhir/type
     :fhir/response-type :fhir/search
     :fhir/response
     (fhir-client/search
      (if (contains? parameters :_all)
        (get terminology-provider :source)
        fhir-db)
      resource-type
      (dissoc parameters :_all))}))

(defn- encrypt-resource
  "Overrides to do encryption on update, patch, creation ops."
  [{:keys [fhir-db encryption-provider]} req]
  (match req
    {:fhir/request-type :fhir/create
     :fhir/data         resource}       {:fhir/level         :fhir/type
                                         :fhir/response-type :fhir/create
                                         :fhir/response
                                         (fhir-client/create
                                          fhir-db
                                          (enc/encrypt-resource encryption-provider
                                                                resource))}
    {:fhir/request-type :fhir/patch
     :fhir/type resource-type
     :fhir/id id
     :fhir/data         patches}        (let [prev-resource (fhir-client/read fhir-db resource-type id)
                                              updated-resource (patch/patch prev-resource patches true)]
                                          {:fhir/level :fhir/instance
                                           :fhir/response-type :fhir/patch
                                           :fhir/response
                                           (fhir-client/update
                                            fhir-db
                                            resource-type
                                            id
                                            (enc/encrypt-resource
                                             encryption-provider
                                             updated-resource))})
    {:fhir/request-type :fhir/update
     :fhir/type         resource-type
     :fhir/id           id
     :fhir/data         resource}       {:fhir/level :fhir/instance
                                         :fhir/response-type :fhir/update
                                         :fhir/response
                                         (fhir-client/update
                                          fhir-db
                                          resource-type
                                          id
                                          (enc/encrypt-resource
                                           encryption-provider
                                           resource))}))

(defn- environment-overrides [{:keys [fhir-db]} req]
  (let [resource (fhir-client/read
                  fhir-db
                  (get req :fhir/type)
                  (get req :fhir/id))]
    {:fhir/level :fhir/instance
     :fhir/response-type :fhir/read
     :fhir/response
     (if-let [environ (get req :meta/environment)]
       (ee/apply-endpoint-environment
        {:reference environ}
        resource)
       resource)}))

(def custom-resource-handlers {:Endpoint            {:fhir/create encrypt-resource
                                                     :fhir/update encrypt-resource
                                                     :fhir/patch  encrypt-resource
                                                     :fhir/read   environment-overrides}
                               :OperationDefinition {:fhir/create encrypt-resource
                                                     :fhir/update encrypt-resource
                                                     :fhir/patch  encrypt-resource}
                               :ValueSet   {:fhir/search terminology-search}
                               :CodeSystem {:fhir/search terminology-search}})

(defn- ->encryption [env workspace]
  (let [{:keys [server/encryption-type
                server/encryption-local-secret-key
                server/aws-region
                server/aws-kms-access-key-id
                server/aws-kms-access-key-secret]} env]
    (when (some? encryption-type)
      (encryption-provider/->provider
       {:type   encryption-type
        :config
        (match encryption-type
          "local"
          {:secret-key        encryption-local-secret-key}
          "aws-kms"
          {:workspace         workspace
           :region            aws-region
           :access-key-id     aws-kms-access-key-id
           :access-key-secret aws-kms-access-key-secret})}))))

(defn- ->terminology-provider [fhir-db file-system-provider]
  (->MemoryTerminologyProvider
   (multi-source/->MultiSourceProvider
    ;; Order matters as to which one gets checked first.
    [{:id :filesystem
      :source file-system-provider}
     {:id :database
      :source fhir-db}])))

(defn- fhir-req->ctx [env]
  (let [inline-ops           (->inline-ops env)
        custom-executioner   (->custom-operation-executioner env)
        file-system-provider (filesystem/resource-paths->memory-client-provider
                              (gf/path->resources-uris "../cljc/definitions/resources" "terminology"))]
    (fn [req]
       ;; Insantiate db with workspace and author params
      (let [workspace (get req :fhir/workspace)
            author    (get req :author)
            fhir-db   ((wdb/->fhir-db
                        {:host     (:server/workspace-db-host env)
                         :port     (:server/workspace-db-port env)
                         :dbname   (:server/workspace-db-name env)
                         :user     (:server/workspace-db-username env)
                         :password (:server/workspace-db-password env)})
                       workspace
                       author)
            terminology-provider (->terminology-provider fhir-db file-system-provider)]

        (fhir-server/map->CTX
         {:capabilities             capabilities/capabilities
          :custom-resource-handlers custom-resource-handlers
          :fhir-db                  fhir-db
          :op-handler               (op-handler/->op-handler
                                     {:inline-ops         inline-ops
                                      :custom-executioner custom-executioner})
          :encryption-provider      (->encryption env (get req :fhir/workspace))
          :terminology-provider     terminology-provider})))))

(defn ->fhir-handler [env]
  (fhir-server/dynamic-spec-import)
  (s/assert :fhir-types/CapabilityStatement capabilities/capabilities)

  (fhir-server/create {:capabilities             capabilities/capabilities
                       :post-process-fhir-req      (fn [req fhir-req]
                                                     ;; Associate the workspace
                                                     (let [meta-keys (->>
                                                                      (get req :headers {})
                                                                      (filter
                                                                       (fn [[header _value]]
                                                                         (string/starts-with? header "genfhi-meta-")))
                                                                      (map (fn [[header value]]
                                                                             [(keyword
                                                                               "meta"
                                                                               (clojure.string/replace header "genfhi-meta-" ""))
                                                                              value]))
                                                                      (into {}))
                                                           workspace   (get-in req [:path-params :workspace])]
                                                       (-> fhir-req
                                                           (merge meta-keys)
                                                           (assoc :fhir/workspace workspace))))
                       :fhir-req->ctx            (fhir-req->ctx env)}))

(defn- heap-analytics [{:keys [id]}]
  [:script {:type "text/javascript"}
   (str
    "
  window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=document.createElement(\"script\");r.type=\"text/javascript\",r.async=!0,r.src=\"https://cdn.heapanalytics.com/js/heap-\"+e+\".js\";var a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(r,a);for(var n=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=[\"addEventProperties\",\"addUserProperties\",\"clearEventProperties\",\"identify\",\"resetIdentity\",\"removeEventProperty\",\"setEventProperties\",\"track\",\"unsetEventProperty\"],o=0;o<p.length;o++)heap[p[o]]=n(p[o])};
  heap.load(\"" id "\", {disableTextCapture: true});
")])

(defn- client-response [client-env]
  (let [csrf-token (force ring.middleware.anti-forgery/*anti-forgery-token*)]
    {:status 200
     :body
     (hiccup/html5
      [:head
       [:meta {:charset "UTF-8"}]
       [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]

       [:link {:rel "icon" :href "/assets/icon.svg" :type "image/svg"}]
       [:link {:rel "stylesheet" :href "/assets/antd.min.inc.css"}]
       [:link {:rel "stylesheet" :href (str "/assets/main." version ".css")}]

       (when (get client-env :client/heap-id)
         (heap-analytics {:id (client-env :client/heap-id)}))]
      [:body
       [:div#sente-csrf-token {:data-csrf-token csrf-token}]
       [:div {:id "app" :style "height: 100%;"}]
       [:script
        (str "window.env = " (slurp (m/encode "application/json" client-env)) ";"
             "window.version = '" version "';")]
       [:script {:src (str "/assets/cljs-out/js/workspace/main." version ".js")
                 :type "text/javascript"}]])}))

(defn- ->client-handler [client-env]
  (fn
    ([_req respond _raise]
     (respond (client-response client-env)))
    ([_req]
     (client-response client-env))))

(defmethod ig/init-key :gen-fhi.workspace.handler/handler
  [_ {:keys [:server/channel
             :server/jwt-config
             :server/allowed-origins]}]
  (let [server-env   (env/->server-env)
        fhir-handler      (->fhir-handler server-env)
        client-handler    (->client-handler (env/->client-env))
        shared-middleware [gzip/wrap-gzip
                           #(ring.middleware.session/wrap-session % {:store
                                                                     (redis-store
                                                                      {:pool {}
                                                                       :spec {:host (server-env :server/redis-host)
                                                                              :port (server-env :server/redis-port)
                                                                              :timeout-ms 5000}})})
                           ring.middleware.params/wrap-params
                           ring.middleware.keyword-params/wrap-keyword-params
                           #(wrap-cors
                             %
                             :access-control-allow-credentials "true"
                             :access-control-allow-origin      (map re-pattern allowed-origins)
                             :access-control-allow-methods     [:get :put :post :delete :patch])
                           ring.middleware.anti-forgery/wrap-anti-forgery]]
    (reitit/ring-handler
     (reitit/router
      [["/chsk/:workspace"                                 {:middleware (vec (concat
                                                                              shared-middleware
                                                                              [#(jwt/wrap-jwt % (merge
                                                                                                 jwt-config
                                                                                                 {:find-token-fn
                                                                                                  (fn [req]
                                                                                                    (-> req :params :jwt))}))
                                                                               middleware/validate-workspace-middleware]))
                                                            :get     (:ajax-get-or-ws-handshake-fn channel)
                                                            :post    (:ajax-post-fn channel)}]
       [""                                      {:middleware (vec (concat
                                                                   [middleware/->httpkit-async-middleware]
                                                                   shared-middleware))}
        ["/api"                                 {:middleware [#(jwt/wrap-jwt % jwt-config)
                                                              middleware/validate-workspace-middleware
                                                              muuntaja/wrap-format]}

         ["/v1/:workspace"
          (let [fhir-handlers {:get    fhir-handler
                               :post   fhir-handler
                               :patch  fhir-handler
                               :delete fhir-handler
                               :put    fhir-handler}]
            ["/fhir"
             ["" fhir-handlers]
             ["/*fhir-location" fhir-handlers]])]]

        ["/assets/*"                            (reitit/create-resource-handler)]
        ;; Frontend SPA routing
        ["/"                                    {:get client-handler}]
        ["/registration"                        {:get client-handler}]
        ["/error"                               {:get client-handler}]
        ["/w/:workspace"                        {:get client-handler}]
        ["/w/:workspace/{*path}"                {:get client-handler}]]]

      {:conflicts (constantly nil)})
     (reitit/redirect-trailing-slash-handler {:method :strip}))))
