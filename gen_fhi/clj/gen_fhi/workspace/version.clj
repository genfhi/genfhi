(ns gen-fhi.workspace.version
  (:require [config.core :refer [load-env]]
            [gen-fhi.workspace.git :refer [->git-sha]]))

(defonce version (if ((load-env) :dev)
                   "dev"
                   (->git-sha "../")))
