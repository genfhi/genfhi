(ns gen-fhi.workspace.ws.core
  (:require [integrant.core :as ig]
            [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]
            [taoensso.timbre :as timbre]

            [gen-fhi.resource-providers.ws :refer [get-patches-after]]
            [gen-fhi.workspace.database :as wdb]
            [gen-fhi.workspace.environment :as env]))

(defmethod ig/init-key :gen-fhi.workspace.ws.core/channel [_ _]
  (let [{:keys [ch-recv send-fn connected-uids
                ajax-post-fn ajax-get-or-ws-handshake-fn]}
        (sente/make-channel-socket!
         (get-sch-adapter)
         ;; For user id doing workspace | sub
         {:user-id-fn (fn [req]
                        (str
                         (-> req :path-params :workspace)
                         "|"
                         (-> req :claims :sub)))})]

    {:ajax-post-fn                ajax-post-fn
     :ajax-get-or-ws-handshake-fn ajax-get-or-ws-handshake-fn
     :ch-recv                     ch-recv
     :send-fn                     send-fn
     :connected-uids              connected-uids}))

(defmulti -event-msg-handler
  "Multi method for event messages."
  (fn [_ e]
    (:id e)))

(defmethod -event-msg-handler
  :fhir/get-patches-after
  [->fhir-db {:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn uid]}]
  (try
    (let [fhir-db (->fhir-db
                   (-> ring-req :path-params :workspace)
                   (-> ring-req :claims :sub))
          [_ event-data] event
          version-id    (:version-id event-data)
          patches
          (if (some? version-id)
            (get-patches-after
             fhir-db
             version-id)
            (get-patches-after
             fhir-db))]
      (when ?reply-fn
        (?reply-fn patches)))
    (catch Exception ex
      (timbre/error ex))))

(defmethod -event-msg-handler
  :default ; Default/fallback case (no other matching handler)
  [->fhir-db {:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
    ;; (debugf "Unhandled event: %s" event)
  (when ?reply-fn
    (?reply-fn {:umatched-event-as-echoed-from-server event})))

(defn event-msg-handler
  "Wraps `-event-msg-handler` with logging, error catching, etc."
  [->fhir-db {:as ev-msg :keys [id ?data event]}]
  (-event-msg-handler ->fhir-db ev-msg)) ; Handle event-msgs on a single thread
  ;; (future (-event-msg-handler ev-msg)) ; Handle event-msgs on a thread pool

(defmethod ig/init-key :gen-fhi.workspace.ws.core/channel-server [_ {:keys [:server/channel]}]
  (let [env (env/->server-env)]
    (timbre/info "channel message handler initiated")
    (sente/start-server-chsk-router!
     (:ch-recv channel)
     (partial event-msg-handler (wdb/->fhir-db
                                 {:host     (:server/workspace-db-host env)
                                  :port     (:server/workspace-db-port env)
                                  :dbname   (:server/workspace-db-name env)
                                  :user     (:server/workspace-db-username env)
                                  :password (:server/workspace-db-password env)})))))

(defmethod ig/halt-key! :gen-fhi.workspace.ws/channel-server [_ server]
  (server))
