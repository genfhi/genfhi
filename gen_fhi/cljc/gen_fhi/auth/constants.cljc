(ns gen-fhi.auth.constants)

(def auth-prefix "https://genfhi.com/Extension/auth")

(def auth-type-url (str auth-prefix "-type"))

;; Basic
(def auth-basic-username-url (str auth-prefix "-basic-username"))
(def auth-basic-password-url (str auth-prefix "-basic-password"))

;; SMART
(def auth-smart-client-id-url (str auth-prefix "-smart-client-id"))
(def auth-smart-scopes-url    (str auth-prefix "-smart-scopes"))

;; OAuth2
(def auth-oauth2-token-url     (str auth-prefix "-oauth2-token-url"))
(def auth-oauth2-client-id     (str auth-prefix "-oauth2-client-id"))
(def auth-oauth2-client-secret (str auth-prefix "-oauth2-client-secret"))
(def auth-oauth2-resource      (str auth-prefix "-oauth2-resource"))
(def auth-oauth2-audience      (str auth-prefix "-oauth2-audience"))
