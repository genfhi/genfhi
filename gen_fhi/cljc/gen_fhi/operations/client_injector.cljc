(ns gen-fhi.operations.client-injector
  (:require
   [async-errors.core :as ae]
   [gen-fhi.operations.protocols :as op]
   [gen-fhi.interceptors.core :as int]))

(defn- pre-process [op parameters]
  (if (satisfies? op/Operation op)
    (op/map->parameters op parameters "in")
    parameters))

(defn- post-process [op res]
  (let [parameters (get res :fhir/response)]
    (if (satisfies? op/Operation op)
      (assoc
       res
       :fhir/response
       (op/parameters->map op parameters))
      res)))

(defn- op->op-id [op]
  (if (satisfies? op/Operation op)
    (get (op/get-definition op) :code)
    op))

(defn op-processing
  "Given an interceptor process the invocation if pass in map convert it
  to parameters and convert return back to map."
  [interceptor req]
  (let [op  (get req :fhir/op)
        req (assoc req
                   :fhir/op (op->op-id op)
                   :fhir/data (pre-process
                               op
                               (get req :fhir/data)))
        res (interceptor req)]
    (if (int/is-async? res)
      (ae/go-try
        (let [res (ae/<? res)]
          (post-process op res)))
      (post-process op res))))
