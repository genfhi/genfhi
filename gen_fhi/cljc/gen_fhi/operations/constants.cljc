(ns gen-fhi.operations.constants)

(def operation-environment-variable-url
  "https://genfhi.com/OperationDefinition/environment-variable")

(def operation-environment-variable-value-url
  "https://genfhi.com/OperationDefinition/environment-variable-value")
