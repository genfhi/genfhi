(ns gen-fhi.operations.core
  #?(:cljs (:require-macros [gen-fhi.operations.macros :refer [->operations]]))
  
  #?(:cljs (:require [gen-fhi.operations.parameter-parsing.core])
     :clj  (:require [gen-fhi.operations.parameter-parsing.core]
                     [gen-fhi.operations.macros :refer [->operations]])))

(->operations "../cljc/definitions/resources" "operation_definitions")
