(ns gen-fhi.operations.macros
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]

            [gen-fhi.file.core :as gf]
            [gen-fhi.operations.parameter-parsing.core :as parameters]
            [gen-fhi.operations.protocols :refer [get-definition parameters->map map->parameters Operation]]))

(defmacro ->operation [resource-uri]
  (let [operation-definition (json/read-str
                              (slurp (io/resource resource-uri))
                              :key-fn keyword)]
    (assert (= "OperationDefinition" (get operation-definition :resourceType))
            (str "Operation read from uri '" resource-uri "' was not an operationdefinition"))
    `(def ~(symbol (get operation-definition :code))
       (reify
         Operation
         (get-definition [_#] ~operation-definition)
         (parameters->map [this# parameter-map#]
           (parameters/parameters->map parameter-map#))
         (map->parameters [this# parameter-map# direction#]
           (parameters/map->parameters (get-definition this#) parameter-map# direction#))))))

(defmacro ->operations [file-dir path]
  (let [resource-uris (gf/fn->resource-uris file-dir path)]
    `(do
       ~@(map
          (fn [resource-uri] `(->operation ~resource-uri))
          resource-uris))))
