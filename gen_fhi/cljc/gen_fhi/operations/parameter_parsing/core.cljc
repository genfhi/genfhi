(ns gen-fhi.operations.parameter-parsing.core
  (:require [clojure.string :as string]
            [gen-fhi.generated.r4.type-sets :refer [resource-types]]
            [gen-fhi.operations.parameter-parsing.util :refer [parameters+use->parameter-map]]
            [gen-fhi.operations.parameter-parsing.parameters-map :as p-to-map]))

(declare key+val->parameter)

(defn ^:private key+val->singular [parameter-def-map parameter-definition parameter val]
  (if (not (some? (get parameter-definition :type)))
    (assoc parameter :part
           (apply concat
                  (map
                   (fn [key+val]
                     (key+val->parameter parameter-def-map key+val))
                   val)))
    (let [parameter-type (get parameter-definition :type)]
      (if (some? (resource-types parameter-type))
        (assoc parameter :resource val)
        (let [value-keyword  (keyword (str "value" (string/capitalize parameter-type)))]
          (assoc parameter value-keyword val))))))

(defn ^:private key+val->parameter [parameter-def-map [key val]]
  (let [parameter            {:name (name key)}
        parameter-definition (get parameter-def-map key)]
    (assert (some? parameter-definition)
            (str "Parameter definition not found for name '" (name key) "'"))
    (if (not= "1" (get parameter-definition :max "1"))
      (map
       (partial key+val->singular parameter-def-map parameter-definition parameter)
       val)
      [(key+val->singular parameter-def-map parameter-definition parameter val)])))

(defn map->parameters [op map use]
  (let [parameter-def-map (parameters+use->parameter-map op use)
        parameters {:resourceType "Parameters" :parameter []}]
    (reduce
     (fn [parameters [key _]]
       (if (some? (get map key))
         (let [next-params (key+val->parameter parameter-def-map [key (get map key)])]
           (update
            parameters
            :parameter
            (fn [parameters]
              (concat parameters next-params))))
         parameters))
     parameters
     parameter-def-map)))

(def parameters->map p-to-map/parameters->map)
