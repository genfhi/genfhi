(ns gen-fhi.operations.parameter-parsing.parameters-map
  (:require [gen-fhi.fhirpath.core :as fp]))


(defn ^:private parameter->val [parameter]
  (if (:part parameter)
    (reduce
     (fn [m child-parameter]
       (assoc
        m
        (keyword (:name child-parameter))
        (parameter->val child-parameter)))
     {}
     (:part parameter))
    (or
     (first (fp/evaluate "$this.value" parameter))
     (first (fp/evaluate "$this.resource" parameter)))))

(defn parameters->map [parameters]
  (reduce
   (fn [m parameter]
     (assoc m (keyword (:name parameter))
            (if-let [prev-entry (get m (keyword (:name parameter)))]
              (if (or (vector? prev-entry) (list? prev-entry) (seq? prev-entry))
                (conj prev-entry (parameter->val parameter))
                (conj [prev-entry] (parameter->val parameter)))
              (parameter->val parameter))))
   {}
   (get parameters :parameter [])))
