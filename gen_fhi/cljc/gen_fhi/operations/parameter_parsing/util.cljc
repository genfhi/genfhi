(ns gen-fhi.operations.parameter-parsing.util)

(defn parameters->parameter-map [parameters]
  (reduce
   (fn [acc parameter]
     (assoc acc (keyword (:name parameter)) parameter))
   {}
   parameters))

(defn parameters->required-parameter-names [parameters]
  (into
   #{}
   (comp
    (filter #(< 0 (:min %)))
    (map   :name))
   parameters))

(defn ^:private parameter->parameter+nested-parameters [parameter]
  (if-let [nested-parameters (get parameter :part)]
    (apply concat (conj
                   (map parameter->parameter+nested-parameters nested-parameters)
                   [parameter]))
    [parameter]))

(defn op->parameter-list [op]
  (mapcat
   #(parameter->parameter+nested-parameters %)
   (get op :parameter [])))

(defn filter-parameters [op use]
  (filter
   #(= use (:use %))
   (op->parameter-list op)))

(defn parameters+use->parameter-map [op use]
  (parameters->parameter-map
   (filter-parameters op use)))

(defn required-parameters [op use]
  (parameters->required-parameter-names
   (filter op use)))
