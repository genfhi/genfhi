(ns gen-fhi.operations.protocols)

(defprotocol Operation
  (get-definition [this])
  (parameters->map [this parameter])
  (map->parameters [this parameter direction]))
