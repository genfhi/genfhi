(ns gen-fhi.operations.validation
  (:import [java.lang Integer])
  (:require [malli.core :as m]
            [malli.error :as me]
            [clojure.spec.alpha :as spec]

            [gen-fhi.fhir-specs.generate :refer [*terminology-provider*]]))

(defn- handle-cardinality [parameter validator]
  (let [max (get parameter :max)
        min (get parameter :min)
        parsed-max (try
                     #?(:cljs (js/parseInt max)
                        :clj (Integer/parseInt max))
                     #?(:clj
                        (catch Exception _e
                          0)
                        :cljs
                        (catch :default _e
                          0)))]
    (if (or (= max "*")
            (> parsed-max 1))
      (let [cardinal-validator [:sequential (cond-> {:min min}
                                              (not= "*" max) (assoc :max (Integer/parseInt max)))
                                validator]]
        ;; If passed in as a singular element it could be non wrapped in a vec
        ;; so check if single item (which won't be placed in a vec by default).
        (if (<= min 1)
          [:or
           validator
           cardinal-validator]
          cardinal-validator))
      validator)))

(defn- parameters->validator [parameters use]
  (->>
   parameters
   (filter #(= (:use %) use))
   (map
    (fn [parameter]
      (let [min (get parameter :min)
            is-required? (pos? min)]
        [(keyword (get parameter :name))
         (cond-> {:optional (not is-required?)})
         (handle-cardinality
          parameter
          (if (not-empty (get parameter :part))
            (into []
                  (concat
                   [:map {:closed true}]
                   (parameters->validator (get parameter :part) use)))
             ;; Use generated specs here. 
            [:fn {:error/fn
                  (fn [{:keys [value]} _]
                    (with-out-str
                      (clojure.spec.alpha/explain
                       (keyword "fhir-types" (get parameter :type))
                       value)))}

             (fn [v]
               ;; Setting binding to avoid issues on code checks off SD.
               (binding [*terminology-provider*
                         (fn [valueset strength code]
                           (string? code))]
                 (clojure.spec.alpha/valid?
                  (keyword "fhir-types" (get parameter :type))
                  v)))]))])))))

(defn op->mali-object
  "Note expects the map not a Parameter resource."
  [op use]
  (let [parameters (get op :parameter [])]
    (into []
          (concat
           [:map {:closed true}]
           (parameters->validator parameters use)))))

(defn op->param-validator [op use]
  (let [malli-obj (op->mali-object op use)]
    (fn [parameter]
      (let [validation (me/humanize (m/explain malli-obj parameter))]
        validation))))


