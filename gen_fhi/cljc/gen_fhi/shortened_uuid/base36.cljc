(ns gen-fhi.shortened-uuid.base36
  #?(:cljs (:require ["big-integer" :as big-integer])
     :clj  (:import [java.math BigInteger])))

(defn- big-int [value]
  #?(:cljs  (big-integer value)
     :clj   (BigInteger/valueOf value)))

(def UUID-BITLIMITS 128)
(def BASE (big-int 36))
(def DIGITS "0123456789abcdefghijklmnopqrstuvwxyz")

(defn- divmod [number]
  #?(:cljs (let [quot-remainder (.divmod number BASE)]
             [(.-quotient quot-remainder)
              (.-remainder quot-remainder)])
     :clj  (.divideAndRemainder number BASE)))

(defn encode [number]
  (when (< (.compareTo number (big-int 0)) 0)
    (throw (ex-info "number must not be negative" {:number number})))
  (loop [result ""
         number number]
    (let [divmod (divmod number)
          number (first divmod)
          digit  (second divmod)
          result (str (.charAt DIGITS digit) result)]

      (if (> (.compareTo number (big-int 0)) 0)
        (recur result number)
        (if (== 0 (count result))
          (.substring DIGITS 0 1)
          result)))))

(defn charAt [string index]
  (let [char-to-lookup (.charAt string (- (count string) index 1))]
    (.indexOf DIGITS (str char-to-lookup))))

(defn decode
  ([string] (decode string UUID-BITLIMITS))
  ([string bitLimit]
   (when (= 0 (count string))
     (throw (ex-info "String mut not be empty" {:string string})))
   (when (not (re-matches (re-pattern (str "[" DIGITS "]*")) string))
     (throw (ex-info "String contains illegal characters" {:string string})))
   (->> (range 0 (count string))
        (map
         (fn [index]
           (let [res (big-int (charAt string index))]
             (.multiply res (.pow BASE index)))))
        (reduce
         (fn [acc value]
           (let [sum (.add acc value)]
             (when (and (> bitLimit 0)
                        (> (.bitLength sum) bitLimit))
               (throw (ex-info "String contains more than 128bit information" {:string string})))
             sum))
         (big-int 0)))))
