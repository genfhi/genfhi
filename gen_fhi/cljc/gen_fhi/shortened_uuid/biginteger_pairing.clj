(ns gen-fhi.shortened-uuid.biginteger-pairing
  (:import [java.math BigInteger]))

(def HALF (.shiftLeft BigInteger/ONE 64))
(def MAX_LONG (BigInteger/valueOf Long/MAX_VALUE))

(defn- to-unsigned [value]
  (if (< (.signum value) 0)
    (.add value HALF)
    value))

(defn- to-signed   [value]
  (if (< (.compareTo MAX_LONG value) 0)
    (.subtract value HALF)
    value))

(defn pair [hi lo]
  (let [unsignedLo (to-unsigned lo)
        unsignedHi (to-unsigned hi)]
    (.add unsignedLo (.multiply unsignedHi HALF))))

(defn unpair [^BigInteger value]
  (let [parts    (.divideAndRemainder value HALF)
        signedHi (to-signed (first parts))
        signedLo (to-signed (second parts))]
    [signedHi signedLo]))

