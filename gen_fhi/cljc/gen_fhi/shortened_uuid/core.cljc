(ns gen-fhi.shortened-uuid.core
  (:require [gen-fhi.shortened-uuid.base36 :as base36]
            [gen-fhi.shortened-uuid.uuid-converter :as uuid-converter]))

;; Note because this could go in a domain need to use base 36 which is case insensitive
;; [requirement for domain names]
(defn uuid->suuid [uuid]
  (let [big-int (uuid-converter/to-big-integer uuid)
        encoded (base36/encode big-int)]
    encoded))
    

(defn suuid->uuid [suuid]
  (let [big-int (base36/decode suuid)]
    (uuid-converter/to-uuid big-int)))
