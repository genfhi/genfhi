(ns gen-fhi.shortened-uuid.uuid-converter
  #?(:clj (:import  [java.util UUID]
                    [java.math BigInteger]))
  #?(:cljs (:require ["big-integer" :as big-int])
     :clj
     (:require [gen-fhi.shortened-uuid.biginteger-pairing :as bigint-pairing])))

(defn to-big-integer [uuid]
  #?(:cljs (big-int (.replaceAll (str uuid) "-" "") 16)
     :clj
     (bigint-pairing/pair
      (BigInteger/valueOf (.getMostSignificantBits uuid))
      (BigInteger/valueOf (.getLeastSignificantBits uuid)))))

(defn to-uuid [value]
  #?(:cljs
     (let [uuid-no-dash (.toString value 16)]
       (str
        (.substring uuid-no-dash 0 8)
        "-"
        (.substring uuid-no-dash 8 12)
        "-"
        (.substring uuid-no-dash 12 16)
        "-"
        (.substring uuid-no-dash 16 20)
        "-"
        (.substring uuid-no-dash 20 (count uuid-no-dash))))
     :clj
     (let [unpaired (bigint-pairing/unpair value)]
       (UUID. (.longValueExact (first unpaired))
              (.longValueExact (second unpaired))))))
