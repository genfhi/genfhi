(ns gen-fhi.app-frontend.core
  (:require
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [clerk.core :as clerk]
   [accountant.core :as accountant]
   [re-frame.core :as re-frame]
   [gen-fhi.workspace.views.smart-login :as smart-login]

   ["uuid" :refer [v4]]
   ["dayjs" :as dayjs]
   ["dayjs/plugin/relativeTime" :as relative-time]

   [gen-fhi.db.log-db]
   [gen-fhi.db.core]
   [gen-fhi.patch-building.path :as path]
   [gen-fhi.application.core :refer [application]]
   [gen-fhi.workspace.components.query :refer [query]]

   [gen-fhi.app-frontend.router :as router]))

(.extend ^js/dayjs dayjs relative-time)

(def date-format "YYYY-MM-DDThh:mm:ss.SSSZ")

(defn client-check
  "Not sure on best approach here but for now not going to allow renders until client subd."
  [children]
  (let [c @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])]
    (when c
      [:<>
       children])))

(defn client-register
  ([children]
   [client-register {} children])
  ([_props children]
   (re-frame/dispatch [:gen-fhi.db.events.client/set-app-frontend-fhir-interceptor])
   children))

(defn app []
  (when-let [current-page @(re-frame/subscribe [:gen-fhi.db.subs.route/get-page])]
    [client-register
     [client-check
      [current-page]]]))

(defn application-instance []
  (let [resource-type @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :resource-type])
        id            @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :id])
        resource-path (path/->path resource-type id)]
    [:div {:style {:height "100vh"}}
     [:f> application {:is-editable? false
                       :path         resource-path}]]))

(defn  workspace []
  [query {:resource-type "ClientApplication" :parameters {}}
   (fn [{:keys [result re-query is-loading]}]
     [:div {:class "w-full flex items-center flex-col"}
      [:h2 {:className "text-xl text-gray-700 mb-2 mt-12"} "Applications"]
      [:div {:class "not-prose relative overflow-hidden border rounded"}
       [:table {:class "bg-slate-50 border-collapse table-auto text-sm rounded-xl"}
        [:thead
         [:tr
          [:th {:class "bg-slate-50 border-b font-medium p-2 text-slate-400  text-left"}
           "Name"]
          [:th {:class "bg-slate-50 border-b font-medium p-2 text-slate-400  text-left"}
           "Description"]
          [:th {:class "bg-slate-50 border-b font-medium p-2 text-slate-400 text-left"}
           "Last Updated"]]]
        [:tbody {:class "bg-white "}
         (map
          (fn [client-app]
            ^{:key (:id client-app)}
            [:tr {:class "hover:bg-blue-50 cursor-pointer"
                  :on-click (fn [_e]
                              (accountant/navigate!
                               (router/path-for
                                :instance
                                {:resource-type (:resourceType client-app)
                                 :id            (:id client-app)})))}
             [:td {:class "p-2"} (:name client-app)]
             [:td {:class "p-2"} (:description client-app)]
             [:td {:class "p-2"} (let [^js/dayjs dayjs-date (dayjs (get-in client-app [:meta :lastUpdated]) date-format)]
                                   (.fromNow dayjs-date))]])

          result)]]]])])

(defn mount []
  (rdom/render [app]
               (js/document.getElementById "app")))

(defn page-for [route]
  (case route
    :workspace    workspace
    :instance     application-instance
    :smart-login  smart-login/view))

(defn init! []
  (clerk/initialize!)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [route-match (router/match-by-path path)
            current-page (:name (:data  route-match))
            route-params (:path-params route-match)]

        (r/after-render clerk/after-render!)
        (re-frame/dispatch [:gen-fhi.db.events.ui/clear-ui])
        (re-frame/dispatch
         [:gen-fhi.db.events.route/set-route
          {:page          (page-for current-page)
           :id            current-page
           :params        route-params
           :query-params  (:query-params route-match)}])
        (clerk/navigate-page! path)))
    :path-exists?
    (fn [path]
      (boolean (router/match-by-path path)))})
  ;; Because different handling between app and workspace for smart.
  ;; Set on init the fn to deal with this.
  (re-frame/dispatch-sync [:gen-fhi.db.events.smart/set-smart-handler-app])
  (accountant/dispatch-current!)
  (mount))

(defn ^:after-load re-render []
  (init!))

(defonce start-up (do (init!) true))
