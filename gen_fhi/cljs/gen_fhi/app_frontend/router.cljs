(ns gen-fhi.app-frontend.router
  (:require [reitit.frontend :as reitit]))


(def router
  (reitit/router
   [[""                  :workspace
     ["/"                :workspace]
     ["/smart-login/:resource-type/:id/" :smart-login]
     ["/:resource-type/"
      [""                :type]
      [":id"             :instance]]]]
   {:conflicts nil}))

(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

(def match-by-path (partial reitit/match-by-path router))

