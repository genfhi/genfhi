(ns gen-fhi.application.core
  (:require [clojure.core.match :refer [match]]
            [reagent.core :as r]
            [re-frame.core :as re-frame]

            ["react-dnd-html5-backend" :refer [HTML5Backend]]
            ["react-dnd" :as dnd]
            ["antd" :refer [Spin]]
            ["@ant-design/icons" :refer [EllipsisOutlined LoadingOutlined]]
            [react :refer [useRef useContext]]
            [uuid :as uuid]

            [gen-fhi.application.subscriber-all :refer [sub-all-actions]]
            [gen-fhi.workspace.message-handler :as message-handler]
            [gen-fhi.application.providers :refer [IsEditableProvider is-editable-context]]
            [gen-fhi.workspace.constants.colors :as colors]
            [gen-fhi.workspace.utilities.throttle :refer [throttle]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.application.widgets.core :refer [widgets]]))

(message-handler/setup-listener)

(defn- resizer [{:keys [style item dir]}]
  [:div {:on-mouse-down (fn [_e]
                          (re-frame/dispatch [:gen-fhi.db.events.ui/set-dragging-item
                                              {:type :resize
                                               :item item
                                               :dir dir}]))
         :style (merge
                 {:cursor (match dir
                            (:or :top-left  :bottom-right) "nwse-resize"
                            (:or :top-right :bottom-left)  "nesw-resize")
                  :position "absolute"
                  :height "12px"
                  :width "12px"
                  :border "2px solid #ccc"
                  :background-color "white"
                  :z-index 2
                  :border-radius "12px"}
                 style)}])

(def is-movement-mutation? #{:resize :drag})

(defn display-item [_props]
  (let [is-hover (r/atom false)]
    (fn [{:keys [key item-path is-selected states-path]}]
      (let [is-editable?  (useContext is-editable-context)
            meta-item     @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value item-path])
            item          @(re-frame/subscribe [:gen-fhi.db.subs.item/get-item item-path states-path])
            dragging-mut  @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-dragging-mutation])
            is-hidden? (and (= (:id item) (get-in dragging-mut [:item :id]))
                            (is-movement-mutation? (get dragging-mut :type)))]
        [:div {:key key
               :on-mouse-over #(reset! is-hover true)
               :on-mouse-leave #(reset! is-hover false)
               :on-mouse-down  (fn [_e]
                                 (when is-editable?
                                   (re-frame/dispatch [:gen-fhi.db.events.ui/set-selected-item-path item-path])
                                   (re-frame/dispatch [:gen-fhi.db.events.ui/show-item-properties])))
               :class    (str "select-none " (when is-hidden? " invisible"))
               :style    {:position         "absolute"
                          :user-select      "none"
                          :left             (str (:x item) "px")
                          :top              (str (:y item) "px")
                          :height           (str (:height item) "px")
                          :width            (str (:width item) "px")
                          :box-sizing       "content-box"
                          :border           (str "1px solid "
                                                 (if (and
                                                      is-editable?
                                                      (or @is-hover is-selected dragging-mut))
                                                   colors/light-blue
                                                   "rgba(255,255,255,0)"))
                          :background-color "rgba(255,255,255,0)"}}

         ;; Check if any loading is happening 
         (when (contains? (set (vals (get item :loading))) true)
           [:div {:style {:top "calc(100% - 22px)"} ;; 20px is on height and 2px for space
                  :class "absolute right-0 z-10 h-5 w-5 flex items-center justify-center"}
            [:span {:title "Data is loading"}
             [:> LoadingOutlined {:class "text-xs text-blue-400" :spin true}]]])
         (when (and is-editable? @is-hover)
           [:<>
            [resizer {:item meta-item :dir :top-left
                      :style {:top "-5px"
                              :left "-5px"}}]
            [resizer {:item meta-item :dir :top-right
                      :style {:top "-5px"
                              :left (str (- (:width item) 7) "px")}}]
            [resizer {:item meta-item :dir :bottom-left
                      :style {:top (str (- (:height item) 7) "px")
                              :left "-5px"}}]
            [resizer {:item meta-item :dir :bottom-right :style {:top (str (- (:height item) 7) "px")
                                                                 :left (str (- (:width item) 7) "px")}}]])
         (when (and is-editable? (or @is-hover is-selected))
           [:div
            {:on-mouse-down (fn [_e]
                              (re-frame/dispatch [:gen-fhi.db.events.ui/set-selected-item-path item-path])
                              (re-frame/dispatch [:gen-fhi.db.events.ui/show-item-properties])
                              (re-frame/dispatch [:gen-fhi.db.events.ui/set-dragging-item
                                                  {:type :drag :item meta-item}]))
             :className "flex items-center justify-center text-white font-semibold bg-gray-400"
             :style {:cursor           "grab"
                     :position         "absolute"
                     :top              "-16px"
                     :left             "10px"
                     :z-index          1
                     :min-width        "40px"
                     :height           "15px"
                     :font-size        "10px"
                     :padding "4px"}}
            [:<>
             [:div {:style {:margin-right "4px" :display "flex" :flex-direction "column"}}
              [:> EllipsisOutlined {:style {:position "relative" :top "7px"}}]
              [:> EllipsisOutlined]
              [:> EllipsisOutlined {:style {:position "relative" :top "-7px"}}]]
             [:span (get meta-item :id)]]])

         [:div {:style {:overflow "auto" :height "100%"}}
          (let [widget     (widgets (:type item))
                item-props {:item-path item-path :item item :states-path states-path :is-editable? is-editable?}]
            (if (some? widget)
              [:f> (:render widget) item-props]
              [:span (str "unknown widget type " (:type item))]))]]))))

(defn display-item-placeholder
  "Puts a blue marker for current draging item."
  []
  (let [{:keys [item] :as dragging-mut} @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-dragging-mutation])]
    (when (and item (:x item) (:y item)
               (is-movement-mutation? (get dragging-mut :type)))
      [:div {:className "bg-blue-400"
             :style {:z-index          1
                     :position         "absolute"
                     :left             (str (:x item) "px")
                     :top              (str (:y item) "px")
                     :height           (str (:height item) "px")
                     :width            (str (:width item) "px")}}])))

(defn -application [{:keys [state-path]}]
  (let [states-path  (or state-path (path/->path "ApplicationState" (uuid/v4) "states"))]
    ;; Pre-populating with an initial state obj.
    (re-frame/dispatch [:gen-fhi.db.events.fhir/set-fhir-resource
                        {:fhir/response
                         {:resourceType "ApplicationState"
                          :id           (get (path/path-meta states-path) :id)
                          :states       []}}])
    (fn [{:keys [path]}]
      (let [is-editable?      (useContext is-editable-context)
            app-items-path    (path/descend path "manifest")
            grid-ref           (useRef)
            items              @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value app-items-path])
            item-mutation      @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-dragging-mutation])
            selected-item-path @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-selected-item-path])
            mutation-move      (throttle
                                (fn [grid-ref items item-mutation e]
                                  (let [bound-rect        (.getBoundingClientRect (.. grid-ref -current))
                                        top-offset        (.-top bound-rect)
                                        left-offset       (.-left bound-rect)
                                        cursor-x-absolute (+ (- (.-pageX e) left-offset) (.. grid-ref -current -scrollLeft))
                                        cursor-y-absolute (+ (- (.-pageY e) top-offset)  (.. grid-ref -current -scrollTop))
                                        visible-height    (+ (.. grid-ref -current -clientHeight) (.. grid-ref -current -scrollTop))
                                        visible-width     (+ (.. grid-ref -current -clientWidth) (.. grid-ref -current -scrollLeft))]

                                    (when item-mutation
                                      ;; Scroll Left
                                      (when (< cursor-x-absolute (+ (.. grid-ref -current -scrollLeft) 100))
                                        (.scrollBy (.. grid-ref -current)
                                                   #js {:left (* 2 (- cursor-x-absolute (+ (.. grid-ref -current -scrollLeft) 100)))
                                                        :behavior "smooth"}))

                                      ;; Scroll Right
                                      (when (> cursor-x-absolute (- visible-width 100))
                                        (.scrollBy (.. grid-ref -current)
                                                   #js {:left (* 2 (- cursor-x-absolute (- visible-width 100)))
                                                        :behavior "smooth"}))
                                      ;; Scroll Top
                                      (when (< cursor-y-absolute (+ (.. grid-ref -current -scrollTop) 100))
                                        (.scrollBy (.. grid-ref -current)
                                                   #js {:top (* 2 (- cursor-y-absolute (+ (.. grid-ref -current -scrollTop) 100)))
                                                        :behavior "smooth"}))

                                      ;; Scroll Bottom
                                      (when (> cursor-y-absolute (- visible-height 100))
                                        (.scrollBy (.. grid-ref -current)
                                                   #js {:top (* 2 (- cursor-y-absolute (- visible-height 100)))
                                                        :behavior "smooth"}))

                                      (re-frame/dispatch
                                       [:gen-fhi.db.events.ui/mutate-dragging-item
                                        items
                                        cursor-x-absolute
                                        cursor-y-absolute]))))
                                20)]
        [:div (let [grid-props          {:id "application"
                                         :ref grid-ref
                                         :tab-index "-1" ;; Needed to allow key tracking (div must be focusable)
                                         :className "bg-gray-50 w-full h-full"
                                         :style {:overflow         "auto"
                                                 :position         "relative"
                                                 ;; Show grid when dragging.
                                                 :background-image (if item-mutation
                                                                     "url('/assets/grid.svg')"
                                                                     "")}}
                    editable-grid-props {:on-mouse-up   (fn [_e]
                                                          (when item-mutation
                                                            (re-frame/dispatch [:gen-fhi.db.events.ui/place-dragging-item
                                                                                app-items-path
                                                                                items
                                                                                item-mutation])))
                                         :on-mouse-move (fn [e] (mutation-move grid-ref items item-mutation e))}]
                (if is-editable?
                  (merge grid-props editable-grid-props)
                  grid-props))
         [sub-all-actions {:meta-path app-items-path :state-path states-path}]
         (when is-editable?
           [display-item-placeholder])
         (map-indexed
          (fn [idx item]
            (let [item-path   (path/descend app-items-path idx)
                  is-selected (= (str item-path) (str selected-item-path))]
              [:f> display-item {:key (:id item)
                                 :item-path   item-path
                                 :states-path  states-path
                                 :is-selected is-selected}]))
          items)]))))

(defn application [{:keys [path is-editable?] :as props}]
  (let [is-smart?    @(re-frame/subscribe [:gen-fhi.db.subs.smart/is-legacy-smart? path])
        should-load-smart? (and is-smart?)] ;;(not is-editable?))]
    ;; Required because of Questionnaire editor.
    [:> dnd/DndProvider {:backend HTML5Backend}
     [:> IsEditableProvider {:value is-editable?}
      (if should-load-smart?
        (do
          (re-frame/dispatch [:gen-fhi.db.events.smart/register-smart {:p path}])
          (let [is-smart-loading? @(re-frame/subscribe [:gen-fhi.db.subs.smart/smart-loading? path])]
            (if is-smart-loading?
              [:div {:class "w-full h-full flex items-center justify-center"}
               [:div
                [:> Spin {:tip "Smart Loading"}]]]
              [:f> -application props])))
        [:f> -application props])]]))
