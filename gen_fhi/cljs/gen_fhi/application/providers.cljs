(ns gen-fhi.application.providers
  (:require
   [oops.core :as oops]
   
   ["react" :as react]))

(defonce is-editable-context (react/createContext false))

(def IsEditableProvider (oops/oget is-editable-context "Provider"))
(def IsEditableConsumer (oops/oget is-editable-context "Consumer"))
