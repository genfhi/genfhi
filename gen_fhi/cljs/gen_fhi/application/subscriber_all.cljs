(ns gen-fhi.application.subscriber-all
  (:require [re-frame.core :as re-frame]
            [gen-fhi.fhirpath.loc :as fp-loc]))

(defn sub-all-actions
  "Subscribes to all actions (neccessary for internal actions subscribing to actions needs a component tied to the action in rendering otherwise breaks.)"
  [{:keys [meta-path state-path]}]
  (let [actions @(re-frame/subscribe [:gen-fhi.db.subs.item/get-all-actions meta-path])]
    [:<>
     (doall
      (map
       (fn [action]
         (let [_sub-action @(re-frame/subscribe [:gen-fhi.db.subs.actions/get-action-data
                                                 {:client-path meta-path
                                                  :state-path  state-path
                                                  :action-id   (:id action)
                                                  :loc         (fp-loc/->loc)
                                                  :fetch?      false}])]
           [:span {:key (:id action) :class "hidden"} (str "subbed " (:id action))]))
       actions))]))
