(ns gen-fhi.application.widgets.button
  (:require ["fp-lang-codemirror" :refer [applicationXFhirQuery]]
            ["@codemirror/lang-css" :refer [cssLanguage]]

            [gen-fhi.application.widgets.type :refer [->widget]]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.workspace.utilities.events :as events]))

(def widget
  (->widget
   {:handlers   ["on-click"]
    :properties (concat
                 (:properties (meta button))
                 [{:name         "label"
                   :type         "string"
                   :editor-props {:label "Label"}}

                  {:name         "isDisabled"
                   :type         "boolean"
                   :editor-props {:label "Read-only?"}}

                  {:name         "style"
                   :type         "string"
                   :editor-props  {:label      "Style"
                                   :height     "80px"
                                   :extensions [(applicationXFhirQuery #js{:baseLanguage cssLanguage})]}}])

    :render (fn [{:keys [item item-path states-path]}]
              [button {:type       (:property/type item)
                       :className  "h-full w-full "
                       :STYLE      (or (get item :property/style) "")
                       :on-click   (fn [_e]
                                     (events/dispatch-handlers
                                      "on-click"
                                      {:item       item
                                       :item-path  item-path
                                       :state-path states-path}))
                       :disabled   (get item :property/isDisabled false)}
               (get item :property/label "")])}))
