(ns gen-fhi.application.widgets.code
  (:require [clojure.core.match :refer [match]]
            [re-frame.core :as re-frame]

            [gen-fhi.application.widgets.type :refer [->widget]]
            [gen-fhi.application.widgets.label-container :refer [label-container]]
            [gen-fhi.workspace.components.terminology :refer [code-select]]))

(defn- item->valueset-uri [item]
  (let [return (match (get item :version "v0")
                 "v0" (get-in item [:valueSet])
                 :else (or
                        (get item :binding)
                        (get-in item [:bindingV2 :url])))]
    (if (sequential? return)
      (first return)
      return)))

(defn- item->valueset-raw [item]
  (let [return (get-in item [:bindingV2 :valueset])]
    (if (sequential? return)
      (first return)
      return)))

(defn- item->valueset [item]
  (or (item->valueset-uri item)
      (item->valueset-raw item)))

(def widget
  (->widget
   {:properties [{:name         "label"
                  :type         "string"
                  :editor-props {:label "Label"}}

                 {:name         "labelPositioning"
                  :type         "code"
                  :editor-props {:label "Label Positioning"
                                 :valueset   "http://genfhi.com/label-positioning"
                                 :component-type :radio
                                 :default-value  "left"}}

                 {:name         "isDisabled"
                  :type         "boolean"
                  :editor-props {:label "Read-only?"}}

                 {:name         "value"
                  :type         "string"}]

    :render (fn code-item [{:keys [item-path item states-path]}]
              [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
               (if-let [value-set (item->valueset item)]
                 (let [value (get-in item [:property/value])]
                   [code-select {:value-set      value-set
                                 :raw-code?     true
                                 :value         (if (coll? value)
                                                  (first value)
                                                  value)

                                 :select-props {:style {:width "100%"}
                                                :size  "large"
                                                :disabled (get item :property/isDisabled)}

                                 :on-change    (fn [code]
                                                 (re-frame/dispatch
                                                  [:gen-fhi.db.events.mutate/modify-variable item-path states-path
                                                   (:id item)
                                                   :property/display
                                                   (get code :display "")])
                                                 (re-frame/dispatch
                                                  [:gen-fhi.db.events.mutate/modify-variable item-path states-path
                                                   (:id item)
                                                   :property/system
                                                   (get code :system "")])
                                                 (re-frame/dispatch
                                                  [:gen-fhi.db.events.mutate/modify-variable item-path states-path
                                                   (:id item)
                                                   :property/value
                                                   (get code :code "")]))}])
                 [:span "ValueSet must be set for code."])])}))
