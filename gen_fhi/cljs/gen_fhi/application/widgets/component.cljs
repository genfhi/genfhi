(ns gen-fhi.application.widgets.component
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame]

            ["fp-lang-codemirror" :refer [applicationXFhirQuery]]
            ["@codemirror/lang-javascript" :refer [javascriptLanguage]]
            ["@codemirror/lang-html" :refer [htmlLanguage]]

            [gen-fhi.application.widgets.type :refer [->widget]]
            [gen-fhi.db.util :as utils]
            [oops.core :as oops]))

(def ^{:private true} default-html "<body style='height: 100%; width: 100%; margin: 0px;'><div></div></body>")

(defn head-setup [item origin item-path states-path]
  (str "<head> 
<script crossorigin src=\"https://unpkg.com/@babel/standalone/babel.min.js\"></script>
<script crossorigin src=\"https://unpkg.com/react@18/umd/react.development.js\"></script>
<script crossorigin src=\"https://unpkg.com/react-dom@18/umd/react-dom.development.js\"></script>
<script>"
       (utils/custom-code-header origin item-path states-path)
       (utils/react-component-fns (:id item) origin states-path)
       "</script>
<script type=\"text/babel\">"
       (utils/react-component-connection (:id item) origin states-path)
       "</script> 
     </head>"))

(def widget
  (->widget
   {:properties [{:name         "props"
                  :type         "json"
                  :editor-props {:label "Data Model"
                                 :height "250px"
                                 :allow-modal true
                                 :extensions [(applicationXFhirQuery #js{:baseLanguage javascriptLanguage})]}}
                 {:name "value"
                  :type "string"
                  :editor-props {:label "HTML"
                                 :height "250px"
                                 :allow-modal true
                                 :extensions [(applicationXFhirQuery #js{:baseLanguage htmlLanguage})]}}]
    :render
    (fn [props]
      (let [prev-component-code (r/atom (get-in props [:item :property/value]))
            prev-component-props (r/atom nil)
            iframe-ref      (r/atom nil)
            has-mounted?    (r/atom false)]
        (fn [{:keys [item-path states-path item]}]
          (let [component-code (get item :property/value default-html)]
            (when (some? @iframe-ref)
              (set! (.-onload @iframe-ref)
                    (fn [_e]
                      (reset! has-mounted? true)
                      (oops/ocall @iframe-ref "contentWindow.postMessage"
                                  (clj->js
                                   (get-in item [:property/props])))
                      (reset! prev-component-props (get item :property/props)))))

           ;; [FIXME] Flashing during error.
            (when (and (or
                        (not= @prev-component-code component-code)
                        (not= @prev-component-props (get item :property/props)))
                       (some? (get item :error)))

              (reset! prev-component-code component-code)
              (reset! prev-component-props (get item :property/props))

              (re-frame/dispatch-sync
               [:gen-fhi.db.events.mutate/modify-variable
                item-path
                states-path
                (:id item)
                :error
                nil])
              (oops/ocall @iframe-ref "contentWindow.location.reload"))

            (when (and (not= @prev-component-props (get item :property/props))
                       @has-mounted?)
              (oops/ocall @iframe-ref "contentWindow.postMessage"
                          (clj->js
                           (get-in item [:property/props])))
              (reset! prev-component-props (get item :property/props)))

            [:iframe {:class "w-full h-full"
                      :ref (fn [ref]
                             (when (not @iframe-ref)
                               (reset! iframe-ref ref)))
                      :srcDoc (str
                               "<html style='height: 100%; width: 100%;'><body style='height:100%;width:100%;margin:0px;'>"
                               (head-setup
                                item
                                (.. js/window -location -origin)
                                item-path
                                states-path)
                               component-code
                               "</body></html>")}]))))}))
