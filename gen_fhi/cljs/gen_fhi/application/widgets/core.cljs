(ns gen-fhi.application.widgets.core
  (:require
   [gen-fhi.application.widgets.button   :as     button]
   [gen-fhi.application.widgets.code     :as     code]
   [gen-fhi.application.widgets.html :as html]
   [gen-fhi.application.widgets.image :as image]
   [gen-fhi.application.widgets.integer :as integer]
   [gen-fhi.application.widgets.label :as label]
   [gen-fhi.application.widgets.textarea :as textarea]
   [gen-fhi.application.widgets.json :as json]
   [gen-fhi.application.widgets.date :as date]
   [gen-fhi.application.widgets.component :as component]
   [gen-fhi.application.widgets.decimal :as decimal]
   [gen-fhi.application.widgets.list :as list]
   [gen-fhi.application.widgets.questionnaire :as questionnaire]))

(def widgets
  {"button"        button/widget
   "code"          code/widget
   "html"          html/widget
   "image"         image/widget
   "integer"       integer/widget
   "label"         label/widget
   "list"          list/widget
   "text"          textarea/widget
   "json"          json/widget
   "date"          date/widget
   "time"          date/widget
   "datetime"      date/widget
   "react"         component/widget
   "decimal"       decimal/widget
   "questionnaire" questionnaire/widget})

(defn get-full-metadata
  "Used for doc generation"
  []
  (clj->js
   (into
    {}
    (map
     (fn [[k v]]
       (let [data (dissoc v :render)
             ;; Because of circular inside of codemirror extensions
             ;; dissocing to allow stringify JSON.
             data (update data :properties
                          (fn [props]
                            (map
                             #(update % :editor-props dissoc :extensions)
                             props)))]
         [k data]))
     widgets))))
