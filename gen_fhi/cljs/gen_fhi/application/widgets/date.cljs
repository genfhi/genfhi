(ns gen-fhi.application.widgets.date
  (:require [re-frame.core :as re-frame]

            [gen-fhi.components.base.date :refer [date]]
            [gen-fhi.application.widgets.type :refer [->widget]]
            [gen-fhi.application.widgets.label-container :refer [label-container]]))

(def widget
  (->widget
   {:properties (concat
                 [{:name         "label"
                   :type         "string"
                   :editor-props {:label "Label"}}

                  {:name         "labelPositioning"
                   :type         "code"
                   :editor-props {:label "Label Positioning"
                                  :valueset   "http://genfhi.com/label-positioning"
                                  :component-type :radio
                                  :default-value  "left"}}]

                 (get (meta date) :properties []))

    :render
    (fn [{:keys [item-path item states-path]}]
      (let [value (if (coll? (get item :property/value))
                    (first (get item :property/value))
                    (get item :property/value))]

        [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
         [date {:type       (item :type)
                :value      value
                :isDisabled (get item :property/isDisabled false)
                :on-change  (fn [d _str]
                              (re-frame/dispatch
                               [:gen-fhi.db.events.mutate/modify-variable item-path states-path
                                (:id item)
                                :property/value
                                d]))}]]))}))
