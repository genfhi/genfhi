(ns gen-fhi.application.widgets.decimal
  (:require
   [re-frame.core :as re-frame]

   [gen-fhi.components.base.number :refer [number]]
   [gen-fhi.application.widgets.type :refer [->widget]]
   [gen-fhi.application.widgets.label-container :refer [label-container]]))

(def widget
  (->widget
   {:properties (concat
                 [{:name         "label"
                   :type         "string"
                   :editor-props {:label "Label"}}

                  {:name         "labelPositioning"
                   :type         "code"
                   :editor-props {:label "Label Positioning"
                                  :valueset   "http://genfhi.com/label-positioning"
                                  :component-type :radio
                                  :default-value  "left"}}]

                 (get (meta number) :properties [])

                 [{:name         "value"
                   :type         "decimal"}])

    :render (fn [{:keys [item-path item states-path]}]
              (let [value (get item :property/value "")
                    ;; Because value can be either coll (from fp evaluation) or direct value from
                    ;; state check if coll
                    value (if (coll? value) (first value) value)]
                [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
                 [number
                  {:type        "decimal"
                   :isDisabled  (get item :property/isDisabled false)
                   :value       value
                   :on-change   (fn [num]
                                  (re-frame/dispatch
                                   [:gen-fhi.db.events.mutate/modify-variable
                                    item-path
                                    states-path
                                    (:id item)
                                    :property/value
                                    num]))}]]))}))
