(ns gen-fhi.application.widgets.html
  (:require ["fp-lang-codemirror" :refer [applicationXFhirQuery]]
            ["@codemirror/lang-html" :refer [htmlLanguage]]

            [gen-fhi.db.util :as utils]
            [gen-fhi.application.widgets.type :refer [->widget]]))
  ;; (:require
  ;;  ["react-frame-component" :as react-frame]))

(def ^{:private true} default-html "<body style='height: 100%; width: 100%; margin: 0px;'><div></div></body>")

(defn- head-setup [origin item-path states-path]
  (str "<head><script>"
       (utils/variable-setup item-path states-path)
       (utils/custom-code-header origin item-path states-path)
       "</script></head>"))

(def widget
  (->widget
   {:properties [{:name "value"
                  :type "string"
                  :editor-props {:label "HTML"
                                 :height "250px"
                                 :allow-modal true
                                 :extensions [(applicationXFhirQuery #js{:baseLanguage htmlLanguage})]}}]

    :render (fn [{:keys [item-path states-path item is-editable?]}]
              (let [iframe [:iframe {:style {:height "100%" :width "100%" :border "0px"}
                                     :srcDoc (str
                                              "<html style='height: 100%; width: 100%;'>"
                                              (head-setup (.. js/window -location -origin)
                                                          item-path
                                                          states-path)
                                              "<body style='margin:0px;' onclick='setActiveItem()'>"
                                              (get-in item [:property/value] default-html)
                                              "</body>"
                                              "</html>")}]]

                [:div {:style {:height "100%" :border "1px solid #ddd"}}
                 (if is-editable?
                   [:<>
                    ;;[:div {:style {:position "absolute" :height "100%" :width "100%"}}]
                    iframe]
                   iframe)]))}))

