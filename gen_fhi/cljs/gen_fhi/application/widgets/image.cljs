(ns gen-fhi.application.widgets.image
  (:require
   ["fp-lang-codemirror" :refer [applicationXFhirQuery]]
   ["@codemirror/lang-css" :refer [cssLanguage]]

   [gen-fhi.application.widgets.type :refer [->widget]]
   [gen-fhi.application.widgets.label-container :refer [label-container]]))

(def widget
  (->widget
   {:properties [{:name         "label"
                  :type         "string"
                  :editor-props {:label "Label"}}

                 {:name         "labelPositioning"
                  :type         "code"
                  :editor-props {:label "Label Positioning"
                                 :valueset   "http://genfhi.com/label-positioning"
                                 :component-type :radio
                                 :default-value  "left"}}

                 {:name         "src"
                  :type         "string"
                  :editor-props  {:label "Source"}}

                 {:name         "style"
                  :type         "string"
                  :editor-props  {:label      "Style"
                                  :height     "80px"
                                  :extensions [(applicationXFhirQuery #js{:baseLanguage cssLanguage})]}}]

    :render (fn [{:keys [item]}]
              [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
               [:img {:src    (or (get item :property/src) "https://picsum.photos/200")
                      :STYLE  (str "width:100%;"
                                   (get item :property/style ""))
                      :height "100%"}]])}))
