(ns gen-fhi.application.widgets.json
  (:require
   [re-frame.core :as re-frame]

   ["@codemirror/lang-javascript" :refer [javascript]]
   ["@codemirror/basic-setup" :refer [basicSetup]]

   [gen-fhi.application.widgets.type :refer [->widget]]
   [gen-fhi.components.base.codemirror :refer [codemirror]]
   [gen-fhi.application.widgets.label-container :refer [label-container]]))

(defn value->json-value [value]
  (if (string? value)
    (try
      (.stringify js/JSON (.parse js/JSON value) nil 2)
      (catch :default _e
        value))
    (if (empty? value)
      ""
      (.stringify js/JSON (if (= 1 (count value))
                            (first
                             (clj->js
                              value))
                            (clj->js value))
                  nil
                  2))))

(def widget
  (->widget
   {:properties (concat
                 [{:name         "label"
                   :type         "string"
                   :editor-props {:label "Label"}}

                  {:name         "labelPositioning"
                   :type         "code"
                   :editor-props {:label "Label Positioning"
                                  :valueset   "http://genfhi.com/label-positioning"
                                  :component-type :radio
                                  :default-value  "left"}}]
                 (get
                  (meta codemirror)
                  :properties []))

                  ;; {:name         "isDisabled"
                  ;;  :type         "boolean"
                  ;;  :editor-props {:label "Read-only?"}}

                  ;; {:name         "value"
                  ;;  :type         "string"}])

    :render
    (fn [{:keys [item item-path states-path]}]
      [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
       [:div {:className "json-editor h-full w-full bg-white"}
        [:f> codemirror
         {:extensions [basicSetup (javascript #js{})]
          :value      (value->json-value (get item :property/value))

          :disabled?          (get item :property/disabled? false)
          :placeholder-string (get item :property/placeholder-string "")
          :height     "100%"
          :width      "100%"
          :theme      {"&.cm-editor" {:overflow "hidden"
                                      :padding "0px"}}

          :on-change (fn [value _view-update]
                       (when (not= value (value->json-value (get item :property/value)))
                         (re-frame/dispatch
                          [:gen-fhi.db.events.mutate/modify-variable
                           item-path
                           states-path
                           (:id item)
                           :property/value
                           value])))}]]])}))
