(ns gen-fhi.application.widgets.label
  (:require [clojure.core.match :refer [match]]

            [gen-fhi.application.widgets.type :refer [->widget]]))

(def widget
  (->widget
   {:properties [{:name         "label"
                  :type         "string"
                  :editor-props {:label "Label"}}

                 {:name         "labelPositioning"
                  :type         "code"
                  :editor-props {:label          "Label Positioning"
                                 :valueset       "http://genfhi.com/label-positioning"
                                 :component-type :radio
                                 :default-value  "left"}}]
    :render
    (fn [{:keys [item]}]
      [:label {:class "p-1"
               :style {:color           "#333"
                       :display         "flex"
                       :justify-content (match (get item :property/labelPositioning "left")
                                          "left" "flex-start"
                                          "right" "flex-end"
                                          "center" "center")
                       :align-items     "center"
                       :text-align      "center"
                       :width           "100%"
                       :height          "100%"}}
       (or  (get item :property/label) "Label")])}))
