(ns gen-fhi.application.widgets.label-container
  (:require [clojure.core.match :refer [match]]))

(def ^:private label-height "20px")

(defn label-container [{:keys [label position]} child]
  [:<>
   (when (some? label)
     [:div {:class "flex"
            :style {:justify-content (match (or position "left")
                                       "left" "flex-start"
                                       "right" "flex-end"
                                       "center" "center")}}
      [:label {:class "block text-slate-600 font-medium pb-1"
               :style {:height label-height}}
       label]])
   [:div {:style {:height (if (some? label) (str "calc(100% - " label-height ")") "100%")
                  :width "100%"}}
    child]])
