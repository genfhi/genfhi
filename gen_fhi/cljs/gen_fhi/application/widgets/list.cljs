(ns gen-fhi.application.widgets.list
  (:require [clojure.core.match :refer [match]]
            [clojure.string :as string]
            [reagent.core :as r]
            [re-frame.core :as re-frame]

            ["js-file-download" :as file-download-fn]
            ["react" :refer [useContext]]
            ["dayjs" :as dayjs]
            ["antd" :refer [Spin message]]
            ["showdown" :refer [Converter]]
            ["@codemirror/lang-javascript" :refer [javascript]]
            ["@genfhi/editor-setup" :refer [basicSetup]]

            [gen-fhi.application.widgets.type :refer [->widget]]
            [gen-fhi.components.base.codemirror :refer [codemirror]]
            [gen-fhi.workspace.utilities.events :as events]
            [gen-fhi.application.providers :refer [is-editable-context]]
            [gen-fhi.fhirpath.core :as fp]
            ;;[gen-fhi.application.widgets.date :refer [DatePicker]]
            [oops.core :as oops]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.application.widgets.label-container :refer [label-container]]))

(def date-format "YYYY-MM-DDThh:mm:ss.SSSZ")
(def ^:private instant-reg  #"([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\.[0-9]+)?(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))")
(def ^:private date-reg     #"([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)(-(0[1-9]|1[0-2])(-(0[1-9]|[1-2][0-9]|3[0-1]))?)?")
(def ^:private datetime-reg #"([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)(-(0[1-9]|1[0-2])(-(0[1-9]|[1-2][0-9]|3[0-1])(T([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\.[0-9]+)?(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00)))?)?)?")
(def ^:private time-reg     #"([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\.[0-9]+)?")

(defn- is-codeable-concept? [val]
  (contains? val :coding))

(defn- is-coding? [val]
  (contains? val :code))

(defn- is-identifier? [val]
  (and (contains? val :system) (contains? val :value)))

(defn- is-human-name? [val]
  (or (contains? val :given)
      (contains? val :family)))

(defn- is-address? [val]
  (or (contains? val :line) (contains? val :city) (contains? val :state)))

(defn- is-attachment? [val]
  (and (contains? val :contentType)
       (or (contains? val :url)
           (contains? val :data))))

(defn- is-quantity? [val]
  (and (contains? val :value)
       (or (contains? val :unit)
           (contains? val :system))))

(defn- is-reference [val]
  (contains? val :reference))

(defn- automatic->type [col res]
  (let [evaluation (get res :evaluation)]
    (if (not= (get col :type "automatic")  "automatic")
      (get col :type)
      (let [val    (first evaluation)
            result (cond
                     (and (string? val) (re-matches instant-reg val))  "datetime"
                     (and (string? val) (re-matches date-reg val))     "date"
                     (and (string? val) (re-matches datetime-reg val)) "datetime"
                     (and (string? val) (re-matches time-reg     val)) "time"

                     (and (map? val)    (is-reference val))            "reference"
                     (and (map? val)    (is-quantity? val))            "quantity"
                     (and (map? val)    (is-coding? val))              "coding"
                     (and (map? val)    (is-identifier? val))          "identifier"
                     (and (map? val)    (is-human-name? val))          "human-name"
                     (and (map? val)    (is-attachment? val))          "attachment"
                     (and (map? val)    (is-address? val))             "address"
                     (and (map? val)    (is-codeable-concept? val))    "codeable-concept"
                     (map? val)                                        "json"
                     :else                                             "text")]
        result))))

;; (defn- editable-date-item [{:keys [val]}]
;;   [:> DatePicker {:showTime true
;;                   :value (dayjs val)}])

(defn- ->copy-val-fn
  "Copies value to clipboard and alerts"
  [val]
  (fn [_e]
    (oops/ocall js/navigator "clipboard.writeText" (.stringify js/JSON (clj->js val)))
    (oops/ocall message "info" "Value copied")))

(defn- readonly-text-item [{:keys [display val]}]
  [:span {:className "hover:underline select-text"}
   (if (some? display)
     (str display)
     (str val))])

(defn-  readonly-date-item [{:keys [val display]}]
  [:span {:className "hover:underline select-text"}
   (let [^js/dayjs dayjs-date (dayjs val)]
     (.format dayjs-date display))])

(defn- readonly-codeable-item [{:keys [val]}]
  (let [text           (get val :text)
        coding-display (string/join "," (fp/evaluate "$this.coding.display" val))
        codes          (string/join "," (fp/evaluate "$this.coding.code" val))]

    [readonly-text-item {:val     val
                         :display (or text
                                      (if (empty? coding-display) nil coding-display)
                                      codes)}]))

(defn- file-download [{:keys [ext title data]}]
  [:button {:class "w-full p-1 bg-gray-50 hover:bg-gray-100 text-xs text-slate-700 hover:text-slate-800 rounded border border-slate-300"
            :on-click (fn [_e]
                        (file-download-fn
                         data
                         (str title
                              (.toISOString (new js/Date.))
                              "."
                              ext)))}
   "Download"])

(defn- readonly-base64binary [{:keys [val col]}]
  [file-download {:title (get col :name)
                  :data val
                  :ext "b64"}])

;;{:contentType "image/gif", :data "data"}
(defn- readonly-attachment [{:keys [val col]}]
  (if (string/starts-with?
       (get val :contentType "")
       "image/")
    [:img {:class "w-full"
           :src (if (get val :data)
                  (str
                   "data:"
                   (get val :contentTypeimage)
                   ";base64,"
                   (get val :data))
                  (get val :url))}]
    [file-download {:title (or (get val :title)
                               (get col :name))
                    :data (get val :data)
                    :ext  (last
                           (clojure.string/split
                            (get val :contentType)
                            #"/"))}]))

(defn- readonly-markdown [{:keys [val]}]
  (let [converter (Converter.)
        make-html (fn [markdown]
                    (.makeHtml converter markdown))
        prev-val (atom val)
        html (r/atom (make-html val))]
    (fn [{:keys [val]}]
      (when (not= @prev-val val)
        (reset! html (make-html val))
        (reset! prev-val val))
      [:iframe {:class "flex w-full overflow-auto"
                :srcDoc @html}])))

(defn- readonly-uri [{:keys [val]}]
  [:a {:target "_blank"
       :href val}
   val])

(defn- readonly-humanname [{:keys [val]}]
  [:span
   (str
    (clojure.string/join  ", " (get val :given))
    " "
    (get val :family))])

(defn- readonly-quantity [{:keys [val]}]
  [:span
   (:comparator val) " "
   (:value val) " "
   (or (:unit val) (:code val))])

(defn- readonly-reference [{:keys [val]}]
  [:span
   (or (:reference val) (:display val))])

(defn- readonly-json [{:keys [val]}]
  [:span
   (.stringify js/JSON (clj->js val))])

(defn- -list-item [_props]
  (let [is-json-active? (r/atom false)
        cell-ref        (r/atom nil)]
    (fn [{:keys [col val]}]
      [:div {:ref (fn [ref]
                    (when (nil? @cell-ref)
                      (reset! cell-ref ref)))
             :title     (.stringify js/JSON (clj->js val))}
       (when (and (some? val)
                  (some? @cell-ref)
                  @is-json-active?)
         [:div {:style {:position "relative" :z-index 1}}
          [:div {:autoFocus true
                 :onBlur  (fn [e]
                            (when (not (oops/ocall e "currentTarget.contains" (oops/oget e "relatedTarget")))
                              (reset! is-json-active? false)))
                 :style {:position "absolute" :background-color "white"
                         :min-width (oops/oget @cell-ref "clientWidth")
                         :min-height (oops/oget @cell-ref "clientHeight")
                         :max-width (+ 80 (oops/oget @cell-ref "clientWidth"))
                         :overflow "auto"}}
           [:f> codemirror
            {:auto-focus? true
             :extensions  [basicSetup (javascript #js{})]
             :value       (.stringify js/JSON (clj->js val))
             :disabled?   false
             :height      "100%"
             :width       "100%"
             :theme       {"&"           {:fontSize "12px"}
                           "&.cm-editor" {:overflow "hidden"
                                          :fontSize "12px"
                                          :padding "0px"}}}]]])
       [:div {:class "p-1 truncate text-slate-600 text-xs"
              :on-double-click
              (fn [_e]
                (reset! is-json-active? true))}
        (match (get col :type "text")
          "text"             [readonly-text-item {:val val}]

          "time"             [readonly-date-item {:val val :display "HH:mm:ss"}]
          "date"             [readonly-date-item {:val val :display "DD/MM/YYYY"}]
          "datetime"         [readonly-date-item {:val val :display "DD/MM/YYYY HH:mm:ss"}]
          "base64binary"     [readonly-base64binary {:val val :col col}]

          "decimal"          [readonly-text-item {:val val}]
       ;;"integer64"        [readonly-text-item {:val val}]
          "code"             [readonly-text-item {:val val}]
          "string"           [readonly-text-item {:val val}]
          "id"               [readonly-text-item {:val val}]
          "markdown"         [readonly-markdown  {:val val}]
          "uri"              [readonly-uri       {:val val}]

          "codeable-concept" [readonly-codeable-item {:val val}]
          "identifier"       [readonly-text-item {:val val :display (str (get val :system) ":" (get val :value))}]
          "coding"           [readonly-text-item {:val val :display (get val :code)}]
          "address"          [readonly-text-item {:val val :display (str
                                                                     (string/join "," (get val :line))
                                                                     " "
                                                                     (get val :city)
                                                                     ", "
                                                                     (get val :state)
                                                                     " "
                                                                     (get val :postalCode))}]

          "human-name"       [readonly-humanname  {:val val}]
          "attachment"       [readonly-attachment {:val val :col col}]
          "quantity"         [readonly-quantity   {:val val}]
          "reference"        [readonly-reference  {:val val}]
          "json"             [readonly-json       {:val val}]
          :else              [readonly-text-item  {:val val}])]])))

(defn- list-item [{:keys [col evaluation]}]
  (if-let [error (:error evaluation)]
    [:span {:class "bg-red-500 text-white"}
     (str error)]
    (let [evaluation (get evaluation :evaluation)
          is-many?   (> (count evaluation) 1)]
      (if is-many?
        [:<>
         (map-indexed
          (fn [i val]
            (let [last? (= i (- (count evaluation) 1))]
              ^{:key i}
              [:div {:class (when (not last?)
                              "mb-1 border border-t-0 border-r-0 border-l-0 border-slate-300")}
               [-list-item {:col col :val val}]]))
          evaluation)]
        [-list-item {:col col :val (first evaluation)}]))))

(defn empty-list [children]
  [:div {:style {:flex 1 :display "flex" :align-items "center" :justify-content "center"}}
   [:div
    {:className "flex items-center justify-center bg-gray-100"
     :style {:height "40px" :width "180px"}}
    [:span {:className "text-gray-500 font-semibold text-xs"}
     children]]])

(defn is-resource? [val]
  (contains? val :resourceType))

(def default-column-width 140)

(defn- columns->summation-to-point [columns i]
  (reduce
   (fn [sum column] (+ sum (js/parseInt (get column :width default-column-width))))
   0
   (subvec columns 0 (+ i 1))))

(defn- apply-col-mutation->cols [item columns mutation]
  (if (and (= (get-in mutation [:item :id])
              (get item :id))
           (= (get mutation :type) :column-resize))

    (into
     []
     (map
      (fn [column]
        (if (= (get column :key) (get-in mutation [:column :key]))
          (with-meta
            (assoc column :width
                   (str (.max js/Math 80
                              (js/parseInt
                               (+ (js/parseInt (get-in mutation [:column :width] (get mutation :default-width)))
                                  (- (get mutation :end)
                                     (get mutation :start)))))))
            {:resizing? true})
          column))
      columns))
    columns))

(defn- list-cell [{:keys [first? last? column item-path states-path row]}]
  (let [evaluation @(re-frame/subscribe
                     [:gen-fhi.db.subs.item/evaluate-expression
                      {:expression (get column :dataIndex)
                       :p item-path
                       :state-path states-path
                       :ctx row}])]
    [:td {:key (:key column)
          :className (str "border border-slate-300 p-0"
                          (when first?
                            " border-l-0")
                          (when last?
                            " border-r-0"))}
     ^{:key (get column :key)}
     [list-item {:col (assoc column :type (automatic->type column evaluation))
                 :evaluation evaluation}]]))

(defn- list-row [{:keys  [item-id selected? columns row item-path states-path]}]
  [:tr
   {:className (str "cursor-pointer"
                    (if selected?
                      " bg-blue-50 "
                      ""))
    :on-click (fn [_e]
                (re-frame/dispatch
                 [:gen-fhi.db.events.mutate/modify-variable
                  item-path
                  states-path
                  item-id
                  :selected
                  row]))}

   (map-indexed
    (fn [i column]
      ^{:key (get column :key)}
      [list-cell {:first?      (= 0 i)
                  :last?       (= i (- (count columns) 1))
                  :column      column
                  :item-path   item-path
                  :states-path states-path
                  :row         row}])
    columns)])

(defn- filter-hidden-columns [columns hidden-col-keys]
  (into
   []
   (sequence
    (comp
     ;; Associate index key into col because of hidden columns
     ;; hidden columns would change real path later (when I map over index)
     ;; so nead to preserve here.
     (map-indexed (fn [i v]
                    (assoc v :index i)))
     (filter
      #(not (contains? hidden-col-keys (:key %)))))
    columns)))

(def default-count-per-page 20)

(defn- ->pages-to-show [{:keys [page-range current-page pages]}]
  (let [page-seq      (map #(+ 1 %) (range page-range))
        pages-to-show (->> (concat
                            (reverse (map #(- current-page %) page-seq))
                            [current-page]
                            (map #(+ current-page %) page-seq)))
        filtered-pages-to-show (filter #(and (< % (count pages))
                                             (>= % 0))
                                       pages-to-show)]
    (if (empty? filtered-pages-to-show)
      '(0)
      filtered-pages-to-show)))

(defn- page-item [{:keys [current-page page on-click]}]
  [:div {:on-click (fn [_e] (on-click page))
         :class    (str "cursor-pointer px-2 mr-1 flex justify-center items-center border "
                        (if (= current-page page)
                          "text-blue-400 border-blue-400"
                          "text-slate-400 border-slate-400"))}
   (+ 1 page)])

(defn- pagination [{:keys [item p state-path]}]
  (let [;; Setting a min value to 1 as zero will cause the creation of an infinite (which gets sequenced in range ... ) 
        count-per-page  (max 1 (get-in item [:pagination :countPerPage] [default-count-per-page]))
        current-offset  (get item :offset 0)
        current-page    (/ current-offset count-per-page)
        pages           (range
                         (Math/ceil
                          (/
                           (get-in item [:pagination :total])
                           count-per-page)))
        page-range      1
        pages-to-show   (->pages-to-show {:page-range page-range :current-page current-page :pages pages})
        on-click        (fn [page]
                          (re-frame/dispatch
                           [:gen-fhi.db.events.mutate/modify-variable
                            p
                            state-path
                            (:id item)
                            :offset
                            (* count-per-page page)])
                          (events/dispatch-handlers
                           "on-pagination"
                           {:item       item
                            :item-path  p
                            :state-path state-path}))]
    [:div {:class "bg-gray-100 flex justify-end items-center p-1 overflow-x-auto overflow-y-hidden"}
     (when (and (< page-range (count pages-to-show))
                (not (some #{0} pages-to-show)))
       [:<>
        [page-item {:on-click     on-click
                    :current-page current-page
                    :page         0}]
        [:span {:class "text-slate-400 mr-1"} "..."]])
     (map
      (fn [page]
        ^{:key page}
        [page-item {:on-click on-click
                    :current-page current-page
                    :page page}])
      pages-to-show)
     (when (and (< page-range (count pages-to-show))
                (not (some #{(- (count pages) 1)}
                           pages-to-show)))
       [:<>
        [:span {:class "text-slate-400 mr-1"} "..."]
        [page-item {:on-click     on-click
                    :current-page current-page
                    :page         (- (count pages) 1)}]])]))

(def widget
  (->widget
   {:properties [{:name         "label"
                  :type         "string"
                  :editor-props {:label "Label"}}

                 {:name         "labelPositioning"
                  :type         "code"
                  :editor-props {:label "Label Positioning"
                                 :valueset   "http://genfhi.com/label-positioning"
                                 :component-type :radio
                                 :default-value  "left"}}

                 {:name         "value"
                  :type         "json"
                  :editor-props {:label "Data"}}]

    :render
    (fn [_props]
      (let [container-ref     (r/atom nil)]
        (fn [{:keys [item item-path states-path]}]
          (let [data-loading?     (get-in item [:loading :property/value] false)
                data              (get item :property/value [])
                dragging-mutation @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-dragging-mutation])
                filtered-columns  (filter-hidden-columns (get item :column []) (set (get item :hiddenColumns [])))
                columns           (apply-col-mutation->cols item filtered-columns dragging-mutation)

                editable?         (useContext is-editable-context)
                selected          (get item :selected)]

            ;; If active selected item does not exist in the set then set it to nil.
            ;; If it's a resource use the id as opposed to value based equality
            ;; Prevents deselection of item when retriggering queries for value
            ;; being changed on selected.
            (when (and (some?  selected)
                       (empty? (filter #(= % selected) data)))
              (if (is-resource? (get item :selected))
                (if-let [selected-item (first (filter #(= (get % :id) (get selected :id)) data))]
                  (re-frame/dispatch [:gen-fhi.db.events.mutate/modify-variable item-path states-path (:id item) :selected selected-item])
                  (re-frame/dispatch [:gen-fhi.db.events.mutate/modify-variable item-path states-path (:id item) :selected nil]))

                (re-frame/dispatch [:gen-fhi.db.events.mutate/modify-variable item-path states-path (:id item) :selected nil])))
            (when editable?
              (re-frame/dispatch [:gen-fhi.db.events.item/generate-columns
                                  {:p          item-path
                                   :state-path states-path}]))
            [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
             [:> Spin {:wrapperClassName "customized-ant-spinner"
                       :spinning data-loading?}
              [:div {:className "flex flex-col h-full w-full bg-white border border-slate-300"}
               (cond
                 (empty? columns)
                 [:div {:class "flex items-center justify-center flex-1"}
                  [empty-list "No columns"]]
                 (empty? data)
                 [:div {:class "flex items-center justify-center flex-1"}
                  [empty-list "No data"]]
                 :else
                 [:div {:ref (fn [ref]
                               (when (and ref (nil? @container-ref))
                                 (reset! container-ref ref)))
                        :className "overflow-auto h-full w-full"}
                  [:table {:className "table-fixed border-collapse text-slate-600 mr-8 border-l-0 border-b-0 border-t-0 border border-slate-300"
                           :style {:width (str (columns->summation-to-point
                                                columns
                                                (- (count columns) 1))
                                               "px")}}
                   [:colgroup
                    (map
                     (fn [column]
                       (let [is-resizing? (get (meta column) :resizing?)]
                         [:col {:key (get column :key)
                                :span "1"
                                :class (when is-resizing? "border-r-2 border-blue-400")
                                :style {:width (str (get column :width default-column-width) "px")}}]))
                     columns)]

                   [:thead {:className ""}
                    [:tr {:style {}}
                     (map-indexed
                      (fn [i column]
                        [:th {:key       (:key column)
                              :class (str
                                      "bg-gray-100 border-t-0 border border-slate-300 "
                                      (when (= 0 i)
                                        " border-l-0")
                                      (if (= i (- (count columns) 1))
                                        " text-center border-r-0"
                                        " text-left"))
                              :style {:position "sticky"
                                      :top 0
                                      :z-index 1}}
                         [:span {:class "text-slate-600 text-xs"}
                          (get column :title)]
                         (when editable?
                           [:div {:on-mouse-down (fn []
                                                   (re-frame/dispatch [:gen-fhi.db.events.ui/set-dragging-item
                                                                       {:type :column-resize
                                                                        :path (path/extend-path item-path "column" (get column :index))
                                                                        :column column
                                                                        :item    item
                                                                        :default-width default-column-width
                                                                        :start   (- (columns->summation-to-point columns i)
                                                                                    (.. @container-ref -scrollLeft))
                                                                        :end     (- (columns->summation-to-point columns i)
                                                                                    (.. @container-ref -scrollLeft))}]))

                                  :style {:position "absolute"
                                          :width "4px"
                                          :height "100%"
                                          :top "0"
                                          :right "0"
                                          :cursor "ew-resize"}}])])

                      columns)]]
                   [:tbody
                    (map-indexed
                     (fn [_row-i row]
                       ^{:key (hash row)}
                       [list-row {:item-id (:id item)
                                  :selected? (= (:selected item)
                                                row)
                                  :columns columns
                                  :item-path item-path
                                  :states-path states-path
                                  :row row}])
                     data)]]])
               (when (some? (get item :pagination))
                 [pagination {:item item :state-path states-path :p item-path}])]]]))))}))





