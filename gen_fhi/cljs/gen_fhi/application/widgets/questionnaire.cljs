(ns gen-fhi.application.widgets.questionnaire
  (:require [gen-fhi.questionnaire.core :refer [Questionnaire]]
            [gen-fhi.application.widgets.label-container :refer [label-container]]
            [gen-fhi.application.widgets.type :refer [->widget]]
            [re-frame.core :as re-frame]
            [gen-fhi.patch-building.path :as path]))

(def widget
  (->widget
   {:properties (concat
                 [{:name         "label"
                   :type         "string"
                   :editor-props {:label "Label"}}

                  {:name         "labelPositioning"
                   :type         "code"
                   :editor-props {:label "Label Positioning"
                                  :valueset   "http://genfhi.com/label-positioning"
                                  :component-type :radio
                                  :default-value  "left"}}

                  {:name         "questionnaire"
                   :type         "json"
                   :editor-props {:label "Questionnaire"}}

                  {:name         "questionnaireResponse"
                   :type         "json"
                   :editor-props {:label "Questionnaire Response"}}])

    :render (fn [{:keys [item-path item states-path]}]
              [label-container {:label (get item :property/label)
                                :position (get item :property/labelPositioning)}
               [:div {:class "p-2 bg-white h-full overflow-auto border"}
                (let [questionnaire
                      (if-let [q-reference (get-in item [:property/questionnaireLocal])]
                        @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/reference->path q-reference)])
                        (get item :property/questionnaire))]

                  (when (= "Questionnaire" (get questionnaire :resourceType))
                    [:f> Questionnaire
                     {:key                     (get item :id)
                      :on-change-event        (fn [questionnaire-response]
                                                (println "change triggered qr:")
                                                [:gen-fhi.db.events.mutate/modify-variable
                                                 item-path
                                                 states-path
                                                 (:id item)
                                                 :property/questionnaireResponse
                                                 questionnaire-response])
                      :editable?              false
                      :questionnaire          (if (list? questionnaire)
                                                (first questionnaire)
                                                questionnaire)
                      :questionnaire-response (if (list? (get item :property/questionnaire))
                                                (first (get item :property/questionnaireResponse))
                                                (get item :property/questionnaireResponse))}]))]])}))
