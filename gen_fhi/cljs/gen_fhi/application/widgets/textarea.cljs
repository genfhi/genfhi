(ns gen-fhi.application.widgets.textarea
  (:require
   [re-frame.core :as re-frame]

   ["fp-lang-codemirror" :refer [applicationXFhirQuery]]
   ["@codemirror/lang-css" :refer [cssLanguage]]

   [gen-fhi.application.widgets.type :refer [->widget]]
   [gen-fhi.application.widgets.label-container :refer [label-container]]))

(def widget
  (->widget
   {:properties [{:name         "label"
                  :type         "string"
                  :editor-props {:label "Label"}}

                 {:name         "labelPositioning"
                  :type         "code"
                  :editor-props {:label "Label Positioning"
                                 :valueset  "http://genfhi.com/label-positioning"
                                 :component-type :radio
                                 :default-value  "left"}}

                 {:name         "isDisabled"
                  :type         "boolean"
                  :editor-props {:label "Read-only?"}}

                 {:name         "value"
                  :type         "string"}

                 {:name         "style"
                  :type         "string"
                  :editor-props  {:label      "Style"
                                  :height     "80px"
                                  :extensions [(applicationXFhirQuery #js{:baseLanguage cssLanguage})]}}]

    :render
    (fn [{:keys [item-path item states-path]}]
      [label-container {:label (get item :property/label) :position (get item :property/labelPositioning)}
       [:textarea {:disabled     (get item :property/isDisabled false)
                   :on-change    (fn [e]
                                   (re-frame/dispatch
                                    [:gen-fhi.db.events.mutate/modify-variable
                                     item-path
                                     states-path
                                     (:id item)
                                     :property/value
                                     (.. e -target -value)]))
                   :value        (get item :property/value "")
                   :className    "ant-input"
                   :STYLE        (str "resize: none; width: 100%; height: 100%;"
                                      (get item :property/style ""))}]])}))
