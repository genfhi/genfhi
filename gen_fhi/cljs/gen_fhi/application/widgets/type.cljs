(ns gen-fhi.application.widgets.type
  (:require [cljs.spec.alpha :as spec]))

(defrecord Widget [handlers properties render])

(spec/def ::name string?)
(spec/def ::type #{"json" "string" "integer" "decimal" "number" "boolean" "code"})
(spec/def ::hide? boolean?)
(spec/def ::property (spec/keys :req-un [::name ::type]
                                :opt-un [::hide?]))

(spec/def ::properties (spec/coll-of ::property))
(spec/def ::handlers (spec/coll-of string?))
(spec/def ::render   fn?)

(spec/def ::widget-schema (spec/keys
                           :req-un [::properties ::render]
                           :opt-un [::handlers]))

(defn ->widget [props]
  {:pre [(spec/valid? ::widget-schema props)]}
  (map->Widget props))
