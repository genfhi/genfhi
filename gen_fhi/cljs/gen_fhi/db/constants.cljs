(ns gen-fhi.db.constants)

(def workspace-claim "https://genfhi.com/workspaces")

(def channel-key :channel)
(def patch-channel-key :stop-patch-channel)
(def client-key :client-int)



;; Used for caching fhir-requests (gets)
;; Can't use this for mutations as this is value based checking on set 
(def in-flight-fhir-requests :in-flight-fhir-request)

(def temp-queue :temp-queue)

(def pending-queue :pending-queue)
(def inflight-queu :inflight-queue)
(def response-queu :response-queue)

(def movement-increment 10)
(def maximum-y 20)
(def maximum-x 10)

;; [TODO] should consider different values for different items.
(def min-item-height 40)
(def min-item-width  60)
