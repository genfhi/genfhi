(ns gen-fhi.db.events.action
  (:require [re-frame.core :as re-frame]

            [gen-fhi.workspace.utilities.alerts :as alerts]
            [gen-fhi.db.events.mutate :refer [apply-mutation]]
            [gen-fhi.db.util :as utils]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.fhirpath.loc :as fp-loc]))

(re-frame/reg-event-fx
 ::move-action
 [(re-frame/inject-cofx
   :inject/subscription
   (fn [[_ path]]
     (let [{:keys [resource-type id]} (path/path-meta path)]
       [:gen-fhi.db.subs.fhir/get-fhir-resource resource-type id {:apply-temps? true}])))
  (re-frame/inject-cofx
   :inject/subscription
   (fn [[_ path]]
     [:gen-fhi.db.subs.temp-edits/get-applied-edit path]))]
 (fn [{:keys [db gen-fhi.db.subs.temp-edits/get-applied-edit :gen-fhi.db.subs.fhir/get-fhir-resource]} [_ from to]]
   ;; The reason to apply immediattely is that this is being called from a dispatch-sync on move
   ;; And it blinks if it's not going to happen immediattely on move.
   ;; 
   ;; Additionally we apply the edit because the edit ias associated to a path in the array
   ;; On move that edit will apply so as the indices change it doesn't get applied to a
   ;; different action.
   {:db (apply-mutation {:db       db
                         :resource get-fhir-resource
                         :temp?    false
                         :mutation [{:path from
                                     :type :replace
                                     :value get-applied-edit}
                                    {:type :move
                                     :from from
                                     :path to}]})
    :dispatch [:gen-fhi.db.events.temp-edits/remove-temp-mutation from true]}))

(re-frame/reg-event-fx
 ::set-action-response
 (fn [{:keys [db]} [_ client-app-path action fhir-response :as _arg]]
   (let [{:keys [id]}  (path/path-meta client-app-path)
         fhir-response (utils/external-parameter->fhir-response fhir-response)]
     (cond
       ;; Because smart returns the :fhir/error correctly vs remote
       ;; which returns "error" becasue of json serialiazation.
       (or (= :fhir/error (:response-type fhir-response))
           (= "error" (:response-type fhir-response)))
       (do
         (alerts/issue-message
          (:id action)
          (alerts/fhir-response->issue-string
           fhir-response))
         {:db db})
       :else
       (do
         (when (= "manual" (:trigger action))
           (alerts/success-message (:id action)))
         {:db
          (-> (utils/set-action-response db {:action action
                                             :client-id id
                                             :response fhir-response}))})))))

(re-frame/reg-event-fx
 ::trigger-action
 [(re-frame/inject-cofx
   :inject/subscription
   (fn [[_ {:keys [id p state-path loc] :or {loc (fp-loc/->loc)}}]]
     [:gen-fhi.db.subs.item/get-action
      {:action-id   id
       :client-path p
       :state-path  state-path
       :loc         loc}]))]
 (fn [{:keys [db gen-fhi.db.subs.item/get-action]} [_ {:keys [p state-path]}]]
   (let [action          (get get-action :evaluation)
         is-disabled?    (get action :isDisabled)]
     (if is-disabled?
       ;; When it's disabled clear any cached actions.
       {:dispatch [::set-action-response
                   p
                   action
                   nil]}
       (utils/action->dispatch-request
        db
        {:action          (get get-action :evaluation)
         :client-app-path p
         :item-state-path state-path})))))

;; Set loading state on state.
(re-frame/reg-event-db
 ::set-loading
 (fn [db [_ state-path action-id loading]]
   (let [{:keys [resource-type id]} (path/path-meta state-path)]
     (update-in
      db
      [:fhir-store resource-type id :loading]
      (fn [loading-states]
        (if (some? loading-states)
          (if loading
            (conj loading-states (keyword action-id))
            (disj loading-states (keyword action-id)))
          (if loading
            #{(keyword action-id)}
            #{})))))))

