(ns gen-fhi.db.events.client
  (:require
   [re-frame.core :as re-frame]
   [oops.core :as oops]
   [clojure.string :as string]

   [gen-fhi.interceptors.core :as it]
   [gen-fhi.db.constants :as c]
   [gen-fhi.workspace.client :as client]
   [gen-fhi.workspace.env :refer [env]]
   [gen-fhi.workspace.suuid :refer [suuid->uuid]]))

(defn- workspace-api-endpoint [workspace]
  (string/replace (env :client/api-url) ":workspace" workspace))

(defn- workspace-associate-meta-information [workspace-suuid fhir-request]
  (let [environment
        (.getItem
         js/sessionStorage
         (str workspace-suuid "/environment"))]
    (if (contains? fhir-request :meta/environment)
       ;; Do nothing
      fhir-request
      (assoc
       fhir-request
       :meta/environment
       environment))))

(re-frame/reg-event-db
 ::set-fhir-interceptor
 (fn [db [_ workspace-suuid get-access-token]]
   (let [workspace-uuid (suuid->uuid workspace-suuid)
         api-url         (workspace-api-endpoint workspace-uuid)
         client-int (client/interceptor
                     api-url
                     get-access-token
                     {:pre
                      [{:enter
                        (it/alt-value
                         (fn [fhir-req]
                           (workspace-associate-meta-information
                            workspace-suuid
                            fhir-req)))}]})]
     (set! (.-fhir-client js/window) client-int)
     (assoc db c/client-key client-int))))

(defn spoof-access-token []
  (js/Promise. (fn [resolve _reject]
                 (resolve "hello"))))

(re-frame/reg-event-db
 ::set-app-frontend-fhir-interceptor
 (fn [db [_]]
   (let [api-url    (str (oops/oget js/window "location" "origin")
                         "/api/v1/fhir/")
         client-int (client/interceptor api-url spoof-access-token)]

     (assoc db c/client-key client-int))))
