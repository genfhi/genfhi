(ns gen-fhi.db.events.core
  (:require
   [gen-fhi.db.events.temp-edits]
   [gen-fhi.db.events.client]
   [gen-fhi.db.events.fhir]
   [gen-fhi.db.events.action]
   [gen-fhi.db.events.mutate]
   [gen-fhi.db.events.operation]
   [gen-fhi.db.events.route]
   [gen-fhi.db.events.sync]
   [gen-fhi.db.events.ui]
   [gen-fhi.db.events.smart]
   [gen-fhi.db.events.item]
   [gen-fhi.db.events.environment]))

