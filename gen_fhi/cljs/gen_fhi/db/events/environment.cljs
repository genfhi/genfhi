(ns gen-fhi.db.events.environment
  (:require
   [re-frame.core :as re-frame]))

(defn- workspace-suuid->environment-key [workspace-suuid]
  (str workspace-suuid "/environment"))

(defn- load-environment-from-session [workspace-suuid]
  (or (.getItem
       js/sessionStorage
       (workspace-suuid->environment-key workspace-suuid))
      nil))

(defn- set-environment-to-session [workspace-suuid environment]
  (.setItem
   js/sessionStorage
   (workspace-suuid->environment-key workspace-suuid)
   environment))

(defn- remove-environment-session [workspace-suuid]
  (.removeItem
   js/sessionStorage
   (workspace-suuid->environment-key workspace-suuid)))

(re-frame/reg-event-db
 ::set-endpoint-environment
 (fn [db [_ workspace-suuid environment]]
   (if (nil? environment)
     (do
       (remove-environment-session workspace-suuid)
       (dissoc db :environment))
     (do
       (set-environment-to-session workspace-suuid environment)
       (assoc db :environment environment)))))

;; Initial load
(re-frame/reg-event-fx
 ::load-endpoint-environment
 (fn [_fx [_ workspace-suuid]]
   (let [environment (load-environment-from-session workspace-suuid)]
     {:dispatch [::set-endpoint-environment
                 workspace-suuid
                 environment]})))


