(ns gen-fhi.db.events.fhir
  (:require [re-frame.core :as re-frame]

            [gen-fhi.workspace.constants.smart :as smart-const]
            [gen-fhi.workspace.smart-client :as smart]
            [gen-fhi.db.constants :as c]
            [gen-fhi.patch-building.path :as path]))

(re-frame/reg-event-db
 ::remove-in-flight
 (fn [db [_ fhir-request]]
   (assoc-in
    db
    [c/in-flight-fhir-requests]
    (disj (get db c/in-flight-fhir-requests) fhir-request))))

(re-frame/reg-event-db
 ::set-fhir-resource
 (fn [db [_ fhir-response]]
   (let [resource (:fhir/response fhir-response)]
     (assoc-in
      db
      [:fhir-store (:resourceType resource) (:id resource)]
      resource))))

;; Currently used in questionnaire keying of the questionnaire components key
(re-frame/reg-event-db
 ::set-fhir-resource-temp
 (fn [db [_ resource resource-type key]]
   (assoc-in
    db
    [:fhir-store (:resourceType resource) key]
    resource)))

(re-frame/reg-event-fx
 ::fhir-request
 (fn [cofx [_ {:keys [on-start on-success on-failure on-finish request]}]]
   (let [in-flight (or (get-in cofx [:db c/in-flight-fhir-requests]) #{})]
     ;; Don't allow duplicate requests if they are identical.
     (when (not (contains? in-flight request))
       {:db (assoc
             (:db cofx)
             c/in-flight-fhir-requests
             (conj in-flight request))
        :gen-fhi.db.fx.fhir/fhir-request-fx
        {:request request
         :on-success on-success
         :on-failure on-failure
         :on-start   on-start
         :on-finish  on-finish}}))))

(re-frame/reg-event-fx
 ::external-smart-request
 [(re-frame/inject-cofx
   :inject/subscription
   (fn [[_ client-path _opts]]
     (let [{:keys [resource-type id]} (path/path-meta client-path)]
       [:gen-fhi.db.subs.smart/smart-client
        (path/->path resource-type id)])))]
 (fn [{:keys [:gen-fhi.db.subs.smart/smart-client]}
      [_ _client-path {:keys [header on-success on-failure on-start on-finish request]}]]
   (if (nil? smart-client)
     (when (some? on-failure)
       (re-frame/dispatch (conj on-failure "Error occured on external smart request.")))
     (let [interceptor (smart/->interceptor smart-client {:headers header})]
       {:gen-fhi.db.fx.fhir/smart-fhir-request-fx
        {:interceptor interceptor
         :on-success  on-success
         :on-failure  on-failure
         :on-start    on-start
         :on-finish   on-finish
         :request     request}}))))

(re-frame/reg-event-fx
 ::external-fhir-request
 [(re-frame/inject-cofx
   :inject/subscription
   (fn [[_ client-path]]
     (let [{:keys [resource-type id]} (path/path-meta client-path)]
       [:gen-fhi.db.subs.smart/is-legacy-smart? (path/->path resource-type id)])))]
 (fn [{:keys [:gen-fhi.db.subs.smart/is-legacy-smart?]}
      [_ client-path {:keys [endpoint-ref-string header on-success on-failure on-start on-finish request]}]]
   (if (and (= smart-const/smart-key endpoint-ref-string) is-legacy-smart?)
     {:dispatch [::external-smart-request
                 client-path
                 {:header     header
                  :on-success on-success
                  :on-failure on-failure
                  :on-start   on-start
                  :on-finish  on-finish
                  :request    request}]}
     {:gen-fhi.db.fx.fhir/endpoint-fx
      {:endpoint-ref-string endpoint-ref-string
       :on-success          on-success
       :on-failure          on-failure
       :on-start            on-start
       :on-finish           on-finish
       :header              header
       :request             request}})))

