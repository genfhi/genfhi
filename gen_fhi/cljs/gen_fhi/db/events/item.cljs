(ns gen-fhi.db.events.item
  (:require [re-frame.core :as re-frame]
            [clojure.core.match :refer [match]]
            [clojure.set :as set]

            [gen-fhi.fhirpath.parser :as fp-parser]
            ;; [gen-fhi.db.generated-columns :refer [search-parameters]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.x-fhir-query.core :as x-fhir-query]
            [gen-fhi.fhirpath.core :as fp]))

(defn parameter-expressions->columns [parameter-expressions]
  (map
   (fn [[key val]]
     {:key       (name key)
      :title     (name key)
      :generated true
      :type      "automatic"
      :dataIndex {:language "text/fhirpath"
                  :expression val}})
   parameter-expressions))

(defn- data->new-column-keys [data]
  (set (apply concat (map keys data))))

(defn ->next-columns [item]
  (let [cur-generated-columns    (set
                                  (sequence
                                   (comp
                                    (filter #(:generated %))
                                    (map :key)
                                    (map keyword))
                                   (get item :column)))
        column-keys              (data->new-column-keys (get item :property/value []))
        new-columns              (set/difference column-keys cur-generated-columns)
        remove-columns           (set/difference cur-generated-columns column-keys)
        return                   (into
                                  []
                                  (concat
                                   (filter
                                    #(not (remove-columns (keyword (get % :key))))
                                    (get item :column))
                                   ;; New columns
                                   (parameter-expressions->columns
                                    ;; map representing column-name and expression
                                    (reduce
                                     (fn [acc key]
                                       (let [is-escaped? (nil? (re-matches #"([a-zA-Z]|_|[0-9])*" (name key)))]
                                         (if is-escaped?
                                           (assoc acc key (str "$this.`" (name key) "`"))
                                           (assoc acc key (str "$this." (name key))))))
                                     {}
                                     new-columns))))]
    return))

(defn- new-columns? [columns data]
  (when (some? (first data))
    (let [generated-keys (set
                          (sequence
                           (comp
                            (filter :generated)
                            (map    :key)
                            (map    keyword))
                           columns))
          are-new?       (not= (data->new-column-keys data)
                               generated-keys)]
      are-new?)))

(re-frame/reg-event-fx
 ::generate-columns
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ {:keys [p state-path]}]]
    [:gen-fhi.db.subs.item/get-item
     p
     state-path]))
 (fn [{:keys [gen-fhi.db.subs.item/get-item]} [_ {:keys [p]}]]
   (when (new-columns? (get get-item :column) (get get-item :property/value))
     {:dispatch
      [:gen-fhi.db.events.mutate/mutate
       {:type :replace
        :path (path/descend p "column")
        :value (->next-columns get-item)}]})))

(re-frame/reg-event-fx
 ::trigger-event-handler
 (fn [_fx [_ handler item-path states-path]]
   (match (get handler :type)
     "trigger-action" {:dispatch [:gen-fhi.db.events.action/trigger-action
                                  {:id         (:action handler)
                                   :p          item-path
                                   :state-path states-path}]}
     :else            (throw (ex-info "Unknown event" {:event (get handler :type)})))))

(re-frame/reg-event-fx
 ::hide-column
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ p _column-key]]
    [:gen-fhi.db.subs.value/get-value
     (:parent (path/ascend (:parent (path/ascend p))))]))
 (fn [{:keys [gen-fhi.db.subs.value/get-value]} [_ p column-key]]

   {:dispatch
    [:gen-fhi.db.events.mutate/mutate
     {:type :replace
      :path (path/descend
             (:parent (path/ascend (:parent (path/ascend p))))
             "hiddenColumns")
      :value (into
              []
              (conj
               (set (get get-value :hiddenColumns []))
               column-key))}]}))

(re-frame/reg-event-fx
 ::show-column
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ p _column-key]]
    [:gen-fhi.db.subs.value/get-value
     (:parent (path/ascend (:parent (path/ascend p))))]))
 (fn [{:keys [gen-fhi.db.subs.value/get-value]} [_ p column-key]]
   {:dispatch
    [:gen-fhi.db.events.mutate/mutate
     {:type :replace
      :path (path/descend
             (:parent (path/ascend (:parent (path/ascend p))))
             "hiddenColumns")
      :value (into
              []
              (filter
               #(not= % column-key)
               (set (get get-value :hiddenColumns []))))}]}))

(defn- value->variable-locs
  ([value id]
   (value->variable-locs [] value id))
  ([path value id]
   (cond
     (vector? value)
     (apply concat
            (map-indexed
             #(value->variable-locs (conj path %1) %2 id)
             value))
     (map? value)
     (match (get value :language)
       "text/fhirpath"  (let [vars (fp/->variables (get value :expression))]
                          (if (vars id)
                            [path]
                            []))
       "application/x-fhir-query" (let [vars (x-fhir-query/->variables (get value :expression))]
                                    (if (vars id)
                                      [path]
                                      []))
       :else (apply
              concat
              (map
               (fn [[key value]]
                 (value->variable-locs
                  (conj path key)
                  value
                  id))
               value)))

     :else [])))

(defn- action-id-locs [resource old-id]
  (concat
   (fp/locations
    "$this.action.handler.action.where($this = %variableId)"
    resource
    {:options/variables {:variableId old-id}})
   (fp/locations
    "$this.manifest.handler.action.where($this = %variableId)"
    resource
    {:options/variables {:variableId old-id}})))

(re-frame/reg-event-fx
 ::replace-id
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ p _old-id _new-id]]
    (let [{:keys [resource-type id]} (path/path-meta p)]
      [:gen-fhi.db.subs.value/get-value (path/->path resource-type id)])))
 (fn [{:keys [gen-fhi.db.subs.value/get-value]} [_ p old-id new-id]]
   (let [root-path (path/path->root p)
         variable-paths (value->variable-locs get-value (keyword old-id))
         action-id-locs (action-id-locs get-value old-id)]
     (when (not-empty variable-paths)
       {:dispatch-n
        (concat
         (map
          (fn [loc]
            [:gen-fhi.db.events.mutate/mutate
             {:type :replace
              :value new-id
              :path (apply
                     (partial path/extend-path root-path)
                     loc)}])
          action-id-locs)
         (map
          (fn [var-path]
            (let [expression-path (conj var-path :expression)
                  expr (get-in get-value var-path)]
              [:gen-fhi.db.events.mutate/mutate
               {:type :replace
                :value (match (:language expr)
                         "application/x-fhir-query"
                         (x-fhir-query/replace-variable
                          (:expression expr)
                          old-id
                          new-id)
                         "text/fhirpath"
                         (fp-parser/replace-variable
                          (:expression expr)
                          old-id
                          new-id)
                         :else (expr :expression))
                :path (apply
                       (partial path/extend-path root-path)
                       expression-path)}]))

          variable-paths))}))))
