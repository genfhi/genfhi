(ns gen-fhi.db.events.mutate
  (:require
   [clj-json-patch.core :as patch]
   [re-frame.core :as re-frame]

   [gen-fhi.db.constants :as c]
   [gen-fhi.patch-building.core :as pb]
   [gen-fhi.patch-building.path :as path]
   [gen-fhi.db.subs.patches :as patches]))

(defn ->pending-key [resource-type id]
  (str resource-type "/" id))

(defn- should-remove?
  ([v]
   (should-remove? v true))
  ([v remove-empty?]
   (cond
     (= "" v) remove-empty?
     (nil? v) remove-empty?
     :else    false)))

(defn- process-value
  "remove parameters that are empty etc..."
  [value]
  (cond
    (map? value)        (into
                         {}
                         (->> (map (fn [[k v]] [k (process-value v)]) value)
                              (filter (fn [[_k v]] (not (should-remove? v))))))
    (sequential? value)  (into
                          []
                          (->> (map process-value value)
                               (filter #(not (should-remove? %)))))
    :else                value))

(defn- process-mutation [mutation]
  (if (:value mutation)
    (update mutation :value (fn [v] (process-value v)))
    mutation))

(defn mutation->json-patches
  "Alter mutation into series of json patches"
  [resource mutation]
  (let [mutation (process-mutation mutation)
        patches (pb/build-patches resource mutation)]
    patches))

(defn- mutate-local [db mutation]
  (let [{:keys [resource-type id]} (path/path-meta (:path mutation))]
    (update-in
     db
     [:fhir-store resource-type id]
     (fn [resource]
       (let [patches-to-apply (mutation->json-patches resource mutation)]
         (patch/patch
          resource
          patches-to-apply
          true))))))

(re-frame/reg-event-db
 ::mutate-local
 (fn [db [_ mutation]]
   (mutate-local db mutation)))

(re-frame/reg-event-fx
 ::modify-variable
 [(re-frame/inject-cofx
   :inject/subscription
   (fn [[_ _item-path states-path item-id  _key _value]]
     [:gen-fhi.db.subs.item/get-state-path states-path item-id]))
  (re-frame/inject-cofx
   :inject/subscription
   (fn [[_ _item-path states-path item-id  _key _value]]
     [:gen-fhi.db.subs.item/get-item-state states-path item-id]))
  (re-frame/inject-cofx
   :inject/subscription
   (fn [[_ item-path states-path item-id  _key _value]]
     [:gen-fhi.db.subs.item/get-variable {:p item-path :state-path states-path :variable-id item-id :evaled? true}]))]

 (fn [{:keys [db
              gen-fhi.db.subs.item/get-variable
              :gen-fhi.db.subs.item/get-state-path
              :gen-fhi.db.subs.item/get-item-state]}
      [_ _item-path _states-path item-id key value]]
   (let [key      (keyword key)
         mutation {:type :replace
                   :path get-state-path
                   :value
                   ;; Need to reuse the old state if present.
                   (assoc (or get-item-state {:id item-id})
                          key
                          ;; Place initial on to detect if expression has changed relative to when
                          ;; state has changed within get-item sub.
                          {:initial (get-in get-variable [:default-values key])
                           :value   value})}]
     ;; Cannot just dispatch to mutate-local. Reason is because dispatch will
     ;; put the event at the bottom of the queue so events that are dependent
     ;; on this one in queue will have stale state.
     ;; Example of this would be running dispatch on modify-variable immediatly from iframe.
     ;; If this calls out to re-dispatch those subsequent events will have old data.
     {:db (mutate-local db mutation)})))

(defn patches->patches-op-merged
  "Given series of patches reduce by combining those operations that are the same together
  [NOTE only do this for replace ops, Add and Deletion ops cannot be combined."
  [patches]
  (reduce
   (fn [reduced-patches cur-patch]
     (let [lpatch (last reduced-patches)]
       (if (and (= "replace" (get cur-patch :op))
                (= (get cur-patch :op) (get lpatch :op))
                (= (get cur-patch :path) (get lpatch :path)))
         (conj (pop reduced-patches) cur-patch)
         (conj reduced-patches cur-patch))))
   []
   patches))

(defn- -dedupe-mutation-ops [mutations mutation]
  (let [mutations (into [] mutations)
        last-mutation (last mutations)]
    (if (and (= :replace (:type last-mutation))
             (= (:type last-mutation) (:type mutation))
             (= (:path last-mutation) (:path mutation)))
      (conj
       (pop mutations)
       mutation)
      (conj
       mutations
       mutation))))

(defn- -apply-mutation [{:keys [db mutation temp? retry? resource]}]
  (let [{:keys [resource-type id]} (path/path-meta (:path mutation))
        resource-key               (->pending-key resource-type id)
        patches                    (mutation->json-patches resource mutation)]

    {:resource (patch/patch resource patches true)
     :db
     (if temp?
       ;; For temp because want the ability to filter out a mutation
       ;; Not going to convert to patches.
       (update-in
        db
        [c/temp-queue resource-key]
        (fn [prev-mutations]
          (-dedupe-mutation-ops
           prev-mutations
           mutation)))

       (update-in
        db
        [c/pending-queue resource-key]
        (fn [request]
          (if (nil? request)
            {:mutations [mutation]
             :request
             {:fhir/request-type :fhir/patch
              :fhir/level        :fhir/instance
              :fhir/type         resource-type
              :fhir/id           id
              :fhir/data         (mutation->json-patches resource mutation)}}
            (-> request
                (update :mutations (fn [prev-mutations]
                                     (if retry? ;; If it's a retry concat to the front
                                       (concat [mutation] prev-mutations)
                                       (-dedupe-mutation-ops
                                        prev-mutations
                                        mutation))))
                (assoc-in
                 [:request :fhir/data]
                 (into
                  []
                  (let [prev-patches (get-in request [:request :fhir/data] [])
                        patches      (mutation->json-patches resource mutation)]
                    (patches->patches-op-merged
                     ;; If retry patches should come prev so applied first.
                     (if retry?
                       (concat patches prev-patches)
                       (concat prev-patches patches)))))))))))}))

(defn apply-mutation
  "Apply mutation outside of dispatch (used for those that require immediate mutation in fx db."
  [{:keys [db mutation resource temp? retry?]}]
  (if (sequential? mutation)
    (:db
     (reduce
      (fn [{:keys [db resource]} mutation]
        (-apply-mutation {:db       db
                          :mutation mutation
                          :temp?    temp?
                          :retry?   retry?
                          :resource resource}))
      {:db db :resource resource}
      mutation))
    (:db
     (-apply-mutation
      {:db       db
       :mutation mutation
       :temp?    temp?
       :retry?   retry?
       :resource resource}))))

(defn- mutation->get-fhir-resource-sub [mutation temp?]
  (let [{:keys [resource-type id]} (path/path-meta (:path mutation))]
    [:gen-fhi.db.subs.fhir/get-fhir-resource resource-type id {:apply-temps? temp?}]))

(re-frame/reg-event-fx
 ::mutate
 [(re-frame/inject-cofx
   :inject/subscription
   (fn [[_ mutation {:keys [temp?] :or {temp? false}}]]
     (if (sequential? mutation)
       (do (assert (= 1 (count (set (map #(path/path-meta (:path %)) mutation)))))
           (mutation->get-fhir-resource-sub (first mutation) temp?))
       (mutation->get-fhir-resource-sub mutation temp?))))]

 (fn [{:keys [db :gen-fhi.db.subs.fhir/get-fhir-resource]} [_ mutation {:keys [temp?]
                                                                        :or {temp? false}
                                                                        :as opts}]]
   {:db (apply-mutation {:db       db
                         :mutation mutation
                         :temp?    temp?
                         :resource get-fhir-resource})}))

(defn re-process-requests
  "Reprocess requests, note that for retries because we want new pendings to take precedence we concat them to the front.
  during patch building it will apply mutations in order they appear meaning the last mutation will win."
  [db requests {:keys [retry?] :or {retry? false}}]
  (let [return (reduce
                (fn [db request]
                  (let [resource-type (get-in request [:request :fhir/type])
                        id            (get-in request [:request :fhir/id])

                        inflight-queue (patches/db->inflight-queue db)
                        response-queue (patches/db->response-queue db)

                        resource-patches (patches/->resource-patches
                                          resource-type
                                          id
                                          response-queue
                                          inflight-queue)
                        resource         (get-in db [:fhir-store resource-type id]
                                                 resource-patches)
                        return-db        (apply-mutation
                                          {:db db
                                           :mutation (get request :mutations)
                                           :temp? false
                                           :retry? retry?
                                           :resource resource})]
                    return-db))
                db
                requests)]
    return))

(defn re-apply-pending [db resource-type id]
  (let [pending-key (->pending-key resource-type id)]
    (if-let [pending-request (get-in db [c/pending-queue pending-key])]
      (re-process-requests
       (assoc-in db [c/pending-queue pending-key] nil)
       [pending-request]
       {:retry? false})
      db)))
