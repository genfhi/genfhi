(ns gen-fhi.db.events.operation
  (:require [re-frame.core :as re-frame]

            [gen-fhi.db.util :as util]))

(re-frame/reg-event-db
 ::set-cached-query
 (fn [db [_ fhir-request fhir-response]]
   (if (= (:fhir/response-type fhir-response) :fhir/error)
     (util/alert-operation-outcome fhir-response)
     (assoc-in
      db
      [:cached-queries fhir-request]
      fhir-response))))

(re-frame/reg-event-fx
 ::trigger-cached-query
 (fn [_fx [_ request]]
   {:dispatch
    [:gen-fhi.db.events.fhir/fhir-request
     {:on-success [::set-cached-query request]
      :request   request}]}))
