(ns gen-fhi.db.events.route
  (:require
   [goog.object :as gobj]
   [accountant.core :as accountant]
   [re-frame.core :as re-frame]

   [gen-fhi.db.constants :as c]
   [gen-fhi.workspace.router :as router]
   [gen-fhi.workspace.suuid :refer [uuid->suuid]]))

(re-frame/reg-event-db
 ::set-route
 (fn [db [_ route]]
   (assoc db :route route)))

;; Because redirectcallback is async and sets state while rendering
;; Check if already workspace before redirecting...
(re-frame/reg-event-fx
 ::redirect-workspace
 (re-frame/inject-cofx
  :inject/subscription
  (fn [_]
    [:gen-fhi.db.subs.route/get-parameter :workspace]))
 (fn [{:keys [gen-fhi.db.subs.route/get-parameter]} [_ user]]
   (let [workspaces (or (js->clj (gobj/get user c/workspace-claim))
                        [])]
     (when (nil? get-parameter)
       (if (empty? workspaces)
         (accountant/navigate! (router/path-for :empty-workspace))
         (accountant/navigate! (router/path-for :workspace {:workspace (uuid->suuid (first workspaces))})))))))
