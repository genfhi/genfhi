(ns gen-fhi.db.events.smart
  (:require
   [re-frame.core :as re-frame]
   [goog.object :as obj]
   [clojure.core.match :refer [match]]

   ["fhirclient" :as fhir-client]

   [gen-fhi.db.util :as db-utils]
   [gen-fhi.db.subs.smart :as smart-sub]
   [gen-fhi.workspace.utilities.resources :refer [endpoint->auth-type]]
   [gen-fhi.fhirpath.core :as fp]
   [gen-fhi.auth.constants :as auth-constants]
   [gen-fhi.patch-building.path :as path]
   [gen-fhi.app-frontend.router :as app-router]
   [gen-fhi.workspace.router    :as workspace-router]
   [oops.core :as oops]))

(defn resource->smart-config
  ([resource]
   (resource->smart-config resource false))
  ([resource dev-environment?]
   (when resource
     (match (:resourceType resource)
       "Endpoint"     (let [auth-type (endpoint->auth-type resource)]
                        (cond-> {:clientId (first (fp/evaluate
                                                   "$this.extension.where(url=%extUrl).value"
                                                   resource
                                                   {:options/variables {:extUrl auth-constants/auth-smart-client-id-url}}))
                                 :scope    (first (fp/evaluate
                                                   "$this.extension.where(url=%extUrl).value"
                                                   resource
                                                   {:options/variables {:extUrl auth-constants/auth-smart-scopes-url}}))}
                          ;; Only put an iss when in dev environment or
                          ;; standalone for EHR launches should be nil
                          (or dev-environment?
                              (= auth-type "smart-standalone"))
                          (assoc :iss (get resource :address))))
       "ClientApplication" (get resource :smart)))))

(defn- smart-finish [{:keys [path config on-success on-failure on-finish]}]
  (-> (oops/ocall fhir-client "oauth2.init" (clj->js config) path)
      (.then
       (fn [smart-client]
         (when on-success
           (re-frame/dispatch (conj on-success smart-client)))))
      (.catch
       (fn [e]
         (.error js/console e)
         (when on-failure
           (re-frame/dispatch (conj on-failure e)))))
      (.finally
       (fn []
         (when on-finish
           (db-utils/dispatch on-finish))))))

(defn- ->on-message [{:keys [p on-success on-failure on-finish]}]
  (letfn [(on-message [message]
            (match (obj/getValueByKeys message "data" "type")
              "auth-completed"
              ;; Because multiple callers can be registered ensure the right one
              ;; Is getting instantiated..

              (when (= (str p) (obj/getValueByKeys message "data" "path"))
                (.log js/console "auth-complete:endpoint:" p)
                (.removeEventListener js/window "message" on-message)
                  ;; Brittle code looks at internals of fhir-client and sets
                  ;; storage in such a way that oauth.init fhir-client uses
                  ;; the sessionstorage cache...
                (.setItem js/sessionStorage
                          (obj/getValueByKeys message "data" "path")
                          (obj/getValueByKeys message "data" "key"))
                (.setItem js/sessionStorage
                          (.parse js/JSON (obj/getValueByKeys message "data" "key"))
                          (.stringify js/JSON (obj/getValueByKeys message "data" "auth")))
                ;; (.log js/console js/sessionStorage)
                (smart-finish {:on-success on-success
                               :on-failure on-failure
                               :on-finish  on-finish
                               :path       (obj/getValueByKeys message "data" "path")
                               :config     (obj/getValueByKeys message "data" "config")}))
              :else            nil))]
    on-message))

(defn- clear-session-storage
  "Clear sessionStorage for smart client so doesn't
  use cache on reinstantiation of smart client."
  [client-path]
  ;; Internal details of session removal...
  (when-let [unparsed-key (.getItem js/sessionStorage client-path)]
    (let [key    (.parse js/JSON unparsed-key)
          _state (.getItem js/sessionStorage key)]
      (.removeItem js/sessionStorage client-path)
      (.removeItem js/sessionStorage key))))

(re-frame/reg-event-db
 ::set-smart-handler
 (fn [db [_ smart-router-fn]]
   (assoc
    db
    :smart-handler
    smart-router-fn)))

(defn- app-smart-router [props]
  (let [{:keys [resource on-success on-failure on-finish p]} props
        smart-auth-route   (app-router/path-for :smart-login
                                                {:resource-type (:resourceType resource)
                                                 :id            (:id resource)})
        config (resource->smart-config resource)]
    (smart-finish {:path p
                   :config     (assoc
                                config
                                :redirectUri
                                (oops/oget js/window "location" "href"))
                   :on-success on-success
                   :on-failure on-failure
                   :on-finish  on-finish})))
    ;; (.addEventListener js/window "message" (->on-message props))
    ;; (.open js/window smart-auth-route)))

(re-frame/reg-event-fx
 ::set-smart-handler-app
 (fn [_fx _event]
   {:dispatch [::set-smart-handler app-smart-router]}))

(defn workspace-smart-router [props]
  (let [{:keys [resource]} props
        workspace          (get-in (workspace-router/match-by-path (.. js/window -location -pathname)) [:parameters :path :workspace])
        smart-auth-route   (workspace-router/path-for :smart-login
                                                      {:workspace     workspace
                                                       :resource-type (:resourceType resource)
                                                       :id            (:id resource)})]
    (clear-session-storage (path/->path (:resourceType resource) (:id resource)))
    (.addEventListener js/window "message" (->on-message props))
    (.open js/window smart-auth-route)))

(re-frame/reg-event-fx
 ::set-smart-handler-workspace
 (fn [_fx _event]
   {:dispatch [::set-smart-handler workspace-smart-router]}))

(re-frame/reg-fx
 ::smart-login-fx
 (fn [{:keys [handler on-start] :as props}]
   (when on-start   (db-utils/dispatch on-start))
   (assert (fn? handler)
           "Smart handler fn must registered via ::set-smart-handler!")
   (handler props)))

(re-frame/reg-event-db
 ::set-smart-loading?
 (fn [db [_ client-path loading?]]
   (let [{:keys [resource-type id]} (path/path-meta client-path)]
     (assoc-in db [:smart resource-type id :loading?] loading?))))

(re-frame/reg-event-db
 ::set-smart-client
 (fn [db [_ client-path smart-client]]
   (let [{:keys [resource-type id]} (path/path-meta client-path)]
     (-> db
         (assoc-in [:smart resource-type id :client]
                   smart-client)
         (assoc-in [:smart resource-type id :context]
                   (smart-sub/map->SmartContext
                    {:clientId  (oops/oget smart-client "state.clientId")
                     :encounter {:id           (oops/oget smart-client "encounter.id")}
                     :patient   {:id           (oops/oget smart-client "patient.id")}
                     :user      {:id           (oops/oget smart-client "user.id")
                                 :resourceType (oops/oget smart-client "user.resourceType")}}))))))

(re-frame/reg-event-fx
 ::register-smart
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ {:keys [p]}]]
    (let [{:keys [resource-type id]} (path/path-meta p)
          client-resource-path (path/->path resource-type id)]
      [:gen-fhi.db.subs.value/get-value client-resource-path])))
 (fn [{:keys [db gen-fhi.db.subs.value/get-value]} [_ {:keys [p post-registration]}]]
   (let [resource get-value
         {:keys [resource-type id]} (path/path-meta p)]
     (if (or
             ;; Check to make sure the resources are aligned.
          (not= (get-in db [:smart resource-type id :endpoint]) resource)
          (and (not (some? (get-in db [:smart resource-type id :client])))
               (not (get-in db [:smart resource-type id :loading?] false))))
       (do
        ;; Needs to be set immediately to avoid race conditions.
         {:db (-> db
                  (assoc-in [:smart resource-type id :endpoint] resource)
                  (assoc-in [:smart resource-type id :loading?] true))
          ::smart-login-fx
          {:handler    (get db :smart-handler)
           :p          p
           :resource   resource
           :on-start   [::set-smart-loading? p true]
           :on-failure [::set-smart-loading? p false]
           :on-success [::set-smart-client   p]
           :on-finish  (conj
                        [[::set-smart-loading? p false]]
                        post-registration)}})
       (when post-registration
         (re-frame/dispatch post-registration))))))
