(ns gen-fhi.db.events.sync
  (:require
   [clj-json-patch.core :as patch]
   [cljs.core.async :as a :refer-macros [go-loop]]
   [re-frame.core :as re-frame]
   [taoensso.sente :as sente]
   [taoensso.timbre :refer-macros [info  warn]]

   [gen-fhi.db.constants :as c]
   [gen-fhi.db.events.mutate :as mutate]))

;; Sets up loop that sends patches to server from queus. Does so by using PATCH http method.
(re-frame/reg-event-fx
 ::create-sync-loop
 (fn [_ [_]]
   {:gen-fhi.db.fx.sync/create-sync-loop-fx {}}))

(defn patches->latest-version-id [patches]
  (:version_id (last patches)))

(defn- channel->get-latest-patches
  "[TODO] Should be moved to an effect."
  [send-fn]
  (let [latest-version-id (atom nil)
        stop-chan         (a/chan)]
    (go-loop []
      (a/alt!
        stop-chan nil
        (a/timeout 200)
        (let [current-version-id @latest-version-id]
          (try
            (send-fn (if (some? current-version-id)
                       [:fhir/get-patches-after {:version-id current-version-id}]
                       [:fhir/get-patches-after])
                     200
                     (fn [reply]
                       (when (and (sente/cb-success? reply)
                                  (not (empty? reply)))
                         (reset! latest-version-id (patches->latest-version-id reply))
                         (re-frame/dispatch [::process-patches reply]))))
            (catch js/Error e
              (.error js/console e)))
          (recur))))
    stop-chan))

(defn filter-queue
  "Filter queue in channel if it's less than the version id.
  Version ID is an incrementing integer so should always use the highest value here.
  (Could occur if received response back before channel for a local patch)."
  [db channel-latest-version-id]
  (update
   db
   c/response-queu
   (fn [response-queue]
     (filter #(> (js/parseInt (get-in % [:version-id]))
                 (js/parseInt channel-latest-version-id))
             response-queue))))

(re-frame/reg-event-fx
 ::process-patches
 (fn [cofx [_ patches]]
   (reduce
    (fn [cofx patch]
      (let [id (:id patch)
            version-expected (:prev_version_id patch)
            resource-type (:resource_type patch)]
        ;; Resource could not exist in the local db.
        (if-let  [local-version-id (get-in (:db cofx) [:fhir-store resource-type id :meta :versionId])]
          (if (= (js/parseInt local-version-id) (js/parseInt version-expected))
            (assoc
             cofx
             :db
             (-> (get cofx :db)
                  ;; Apply the patch on the resource
                 (update-in
                  [:fhir-store resource-type id]
                  (fn [resource]
                    (-> (patch/patch resource (:patches patch) true)
                         ;; Assoc metadata from remote to local
                         ;; Includes non user mutated data like versionid.
                        (assoc :meta (:meta patch)))))
                  ;; Reprocess the pending requests post patch applied on local.
                 (mutate/re-apply-pending
                  resource-type
                  id)))

            (if (> local-version-id version-expected)
              (do (info (str "discarding patch for " resource-type ":" id ":" (:version_id patch)))
                  cofx)
              (do
                (warn "Versions didn't match refetching the resource and ignoring patch." local-version-id version-expected)
                (update cofx :fx (fn [fx]
                                   (let [request {:fhir/level        :fhir/instance
                                                  :fhir/request-type :fhir/read
                                                  :fhir/type         resource-type
                                                  :fhir/id           id}]
                                     (conj fx [:dispatch [:gen-fhi.db.events.fhir/fhir-request
                                                          {:on-success [:gen-fhi.db.events.fhir/set-fhir-resource]
                                                           :request request}]])))))))
          (do
            (warn "resource not in local cache so ignoring patch")
            cofx))))
    {:db (filter-queue (:db cofx) (patches->latest-version-id patches))
     :fx []}
    patches)))

;; Using the ws repeatedly query up for the latest series of patches. 

(re-frame/reg-event-db
 ::-set-channel
 (fn [db [_ ws-data]]
   (let [stop-patch-channel (channel->get-latest-patches (:send-fn ws-data))]
     (assoc db
            c/channel-key ws-data
            c/patch-channel-key stop-patch-channel))))

(re-frame/reg-event-fx
 ::register-channel
 (fn [_cofx [_ workspace get-access-token]]
   {:gen-fhi.db.fx.ws/register-channel-fx {:workspace workspace
                                           :get-access-token get-access-token
                                           :on-success [::-set-channel]}}))
