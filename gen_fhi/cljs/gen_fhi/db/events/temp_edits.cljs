(ns gen-fhi.db.events.temp-edits
  (:require [re-frame.core :as re-frame]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.db.constants :as c]
            [clojure.string :as string]))

(re-frame/reg-event-fx
 ::set-temp-mutation
 (fn [_fx [_ mutation]]
   {:dispatch [:gen-fhi.db.events.mutate/mutate mutation {:temp? true}]}))

(re-frame/reg-event-fx
 ::remove-temp-mutation
 (fn [{:keys [db]} [_ path tmp-only?]]
   (let [{:keys [resource-type id]} (path/path-meta path)
         fx {:db (update-in
                  db
                  [c/temp-queue (str resource-type "/" id)]
                  (fn [mutations]
                    (let [filtered-mutations
                          (filter
                           (fn [mutation]
                             (not (string/starts-with? (:path mutation) path)))
                           mutations)]
                      (into [] filtered-mutations))))}]
     (if (true? tmp-only?)
       fx
       (assoc fx :dispatch-n [[:gen-fhi.db.events.mutate/mutate {:path path :type :remove}]])))))

(re-frame/reg-event-fx
 ::apply-temp-mutation
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ path]]
    [:gen-fhi.db.subs.temp-edits/get-applied-edit
     path]))
 (fn [{:keys [gen-fhi.db.subs.temp-edits/get-applied-edit]} [_ path]]
   {:dispatch-n [[:gen-fhi.db.events.temp-edits/remove-temp-mutation path true]
                 [:gen-fhi.db.events.mutate/mutate {:path path
                                                    :type :replace
                                                    :value get-applied-edit}]]}))

