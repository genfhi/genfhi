(ns gen-fhi.db.events.ui
  (:require
   [clojure.core.match :refer [match]]
   [re-frame.core :as re-frame]

   [gen-fhi.db.constants :as c]
   [gen-fhi.patch-building.path :as path]))

(re-frame/reg-event-db
 ::set-dragging-item
 (fn [db [_ mutation]]
   (assoc-in db [:ui :dragging-mutation] mutation)))

;; For additional props defined in components putting a seperate area
(re-frame/reg-event-db
 ::ui-change-component-value
 (fn [db [_ key value]]
   (assoc-in db [:ui :component key] value)))

(re-frame/reg-event-db
 ::flip-show-bar
 (fn [db [_ direction]]
   (update-in db [:ui :bar direction]
              (fn [showing?]
                (not showing?)))))

(re-frame/reg-event-db
 ::set-show-bar
 (fn [db [_ direction show?]]
   (assoc-in db [:ui :bar direction]
             show?)))

(re-frame/reg-event-db
 ::update-dragging-coordinates
 (fn [db [_ coordinates]]
   (assoc-in db [:ui :dragging-coordinates]
             coordinates)))

(re-frame/reg-event-db
 ::clear-ui
 (fn [db]
   (assoc db :ui {})))

(re-frame/reg-event-fx
 ::show-item-properties
 (fn [_ _]
   {:dispatch-n [;;[:gen-fhi.db.events.ui/set-show-bar :right true]
                 [:gen-fhi.db.events.ui/ui-change-component-value :item-sidebar "item-properties"]]}))

(re-frame/reg-event-fx
 ::show-component-adder
 (fn [_ _]
   {:dispatch-n [[:gen-fhi.db.events.ui/set-show-bar :right true]
                 [:gen-fhi.db.events.ui/ui-change-component-value :item-sidebar "components"]]}))

;; On item placement take temp (items being moved out the way) and convert them to mutations.
(defn ui-item-temps->mutations [p items db]
  (let [item-id->path (->> (map-indexed (fn [idx item] [idx item]) items)
                           (reduce (fn [acc [idx item]] (assoc acc (:id item) (path/descend p idx))) {}))
        item-temp-states  (vals (get-in db [:ui :temp-item-states] []))]

    (map
     (fn [item]
       [:gen-fhi.db.events.mutate/mutate
        {:path (item-id->path (:id item))
         :type :replace
         :value item}])
     item-temp-states)))

(def min-col-width 80)

;; UI ephemeral.
(re-frame/reg-event-fx
 ::place-dragging-item
 (fn [{:keys [db]} [_ p items item-mutation]]
   ;; Means need to move existing items position
   (match [(get item-mutation :type)]
     [:column-resize] {:dispatch-n [[::remove-temp-ui-draging-mutations]
                                    [:gen-fhi.db.events.mutate/mutate {:path (path/descend (get item-mutation :path) "width")
                                                                       :type :replace
                                                                       :value (str
                                                                               (.max js/Math
                                                                                     min-col-width
                                                                                     (js/parseInt
                                                                                      (+ (js/parseInt (get-in item-mutation [:column :width] (get item-mutation :default-width)))
                                                                                         (- (get item-mutation :end)
                                                                                            (get item-mutation :start))))))}]]}
     :else
     (let [dragging-item (:item item-mutation)]
       (if (and (:id dragging-item) (:x dragging-item) (:y dragging-item))
         (let [idx (first (keep-indexed
                           (fn [idx item]
                             (when (= (:id dragging-item) (:id item))
                               idx))
                           items))]
           (assert (some? idx) (str "Unknown id for replacing item " (:id dragging-item)))
           {:dispatch-n (concat
                         [[::remove-temp-ui-draging-mutations]
                          [:gen-fhi.db.events.mutate/mutate {:path (path/descend p idx) :type :replace :value dragging-item}]]
                         (ui-item-temps->mutations p items db))})

         (let [rand-number (.round js/Math (* (.random js/Math) 100000))
               new-id      (str (:type dragging-item) rand-number)

               idx    (count items)]
           {:dispatch-n (concat
                         [[::remove-temp-ui-draging-mutations]
                          [:gen-fhi.db.events.mutate/mutate {:path (path/descend p idx)
                                                             :type :add
                                                             :value (assoc dragging-item :id new-id)}]]
                         (ui-item-temps->mutations p items db))}))))))

(re-frame/reg-event-fx
 ::remove-selected-item
 (fn [{:keys [db]} [_]]
   (when (get-in db [:ui :selected-item])
     {:db
      (update db :ui dissoc :selected-item)
      :dispatch
      [:gen-fhi.db.events.mutate/mutate {:path (get-in db [:ui :selected-item])
                                         :type :remove}]})))

(re-frame/reg-event-db
 ::remove-temp-ui-draging-mutations
 (fn [db [_]]
   (-> db
       (update :ui dissoc :dragging-mutation)
       (update :ui dissoc :temp-item-states))))

(defn- round-to-movement-increment [val]
  (->> (/ val c/movement-increment)
       (.round js/Math)
       (* c/movement-increment)))

(defn- update-item-dimensions
  "Assumes pos from bottom-right"
  [dir cursor-x cursor-y item]
  (let [item-x   (+ (:x item) (:width item))
        item-y   (+ (:y item) (:height item))
        cursor-x (round-to-movement-increment cursor-x)
        cursor-y (round-to-movement-increment cursor-y)
        offset-x (- cursor-x item-x)
        offset-y (- cursor-y item-y)
        calculated-width  (round-to-movement-increment (+ (:width item) offset-x))
        calculated-height (round-to-movement-increment (+ (:height item) offset-y))]

    (match dir
      :top-left  (let [new-width (max c/min-item-width
                                      (+ (- (:x item) cursor-x) (:width item)))
                       new-height (max c/min-item-height
                                       (+ (- (:y item) cursor-y) (:height item)))]
                   (assoc item
                          :y      (- (:y item) (- new-height (:height item)))
                          :x      (- (:x item) (- new-width (:width item)))
                          :width  new-width
                          :height new-height))
      :top-right  (let [new-height (max c/min-item-height
                                        (+ (- (:y item) cursor-y) (:height item)))]
                    (assoc item
                           :y      (- (:y item) (- new-height (:height item)))
                           :width  (max c/min-item-width calculated-width)
                           :height new-height))
      :bottom-right (assoc item
                           :width  (max c/min-item-width calculated-width)
                           :height (max c/min-item-height calculated-height))
      :bottom-left  (let [new-width (max c/min-item-width
                                         (+ (- (:x item) cursor-x) (:width item)))]
                      (assoc item
                             :x      (- (:x item) (- new-width (:width item)))
                             :width  new-width
                             :height (max c/min-item-height
                                          (- cursor-y (:y item)))))

      :else (throw (ex-message (str "Bad direction: '" dir "'"))))))

(defn- update-item-coordinates [cursor-x cursor-y item]
  (assoc item
         ;; (Note minus 30 here because chevron offset by 10 and width is 40
         :x (max c/maximum-x (round-to-movement-increment (- cursor-x 30)))
         :y (max c/maximum-y (round-to-movement-increment cursor-y))))

(defn- item-collision? [item1 item2]
  (let [collision? (and
                    (< (:x item1) (+ (:x item2) (:width item2)))
                    (> (+ (:x item1) (:width item1)) (:x item2))
                    (< (:y item1)  (+ (:y item2)  (:height item2)))
                    (> (+ (:height item1)  (:y item1)) (:y item2)))]

    collision?))

(defn item-collisions [app-items item-to-detect]
  (->> app-items
       (filter #(not= (:id %) (:id item-to-detect)))
       (filter
        (partial item-collision? item-to-detect))
       (map (fn [i]
              (assoc i :y (+ (:y item-to-detect)
                             (:height item-to-detect)))))
       (reduce (fn [acc i] (assoc acc (:id i) i)) {})))

(defn sort-by-precedence [id->init-item items]
  (sort (fn [i1 i2]
          (< (get (id->init-item (:id i1)) :y)
             (get (id->init-item (:id i2)) :y)))
        items))

(defn- items+altered-item->item-movements [app-items altered-item]
  (let [app-items     (->> (filter #(not= (:id %) (:id altered-item)) app-items))
        id->init-item (reduce #(assoc %1 (:id %2) %2) {} app-items)]

    (loop [item-movements  {}
           items-to-detect [altered-item]]
      (if-let [item-to-check (first items-to-detect)]
        (let [item-collisions (item-collisions
                               ;; merge temp states into items
                               (vals (merge id->init-item item-movements))
                               ;; Merge the current item with it's next position (if there)
                               (merge item-to-check
                                      (get item-movements (:id item-to-check))))]
          (recur (merge item-movements item-collisions)
                 (sort-by-precedence id->init-item (concat (rest items-to-detect) (vals item-collisions)))))
        item-movements))))

(re-frame/reg-event-db
 ::mutate-dragging-item
 (fn [db [_ items x y]]
   (let [mutation (get-in db [:ui :dragging-mutation])]
     (when mutation
       (match [(:type mutation) x y]
         [_ nil nil] (assoc-in db [:ui :dragging-mutation] mutation)
         [:drag x y]          (let [mutation (update mutation :item #(update-item-coordinates x y %))]
                                (->
                                 db
                                 (assoc-in
                                  [:ui :temp-item-states]
                                  (items+altered-item->item-movements
                                   items
                                   (get mutation :item)))
                                 (assoc-in
                                  [:ui :dragging-mutation]
                                  mutation)))

         [:resize x y]        (let [mutation (update mutation :item #(update-item-dimensions (:dir mutation) x y %))]
                                (->
                                 db
                                 (assoc-in
                                  [:ui :temp-item-states]
                                  (items+altered-item->item-movements
                                   items
                                   (get mutation :item)))
                                 (assoc-in
                                  [:ui :dragging-mutation]
                                  mutation)))
         [:column-resize x y] (assoc-in
                               db
                               [:ui :dragging-mutation :end]
                               (-  x (get-in mutation [:item :x])))

         :else                (throw
                               (js/Error.
                                (str
                                 "Unknown mutation "
                                 (:type mutation)))))))))

(re-frame/reg-event-db
 ::set-selected-item-path
 (fn [db [_ item-path]]
   (assoc-in db [:ui :selected-item] item-path)))

(re-frame/reg-event-db
 ::deselect-item-path
 (fn [db [_]]
   (update db :ui dissoc :selected-item)))

(re-frame/reg-event-db
 ::set-active-modal
 (fn [db [_ active-modal]]
   (assoc-in db [:ui :active-modal] active-modal)))
