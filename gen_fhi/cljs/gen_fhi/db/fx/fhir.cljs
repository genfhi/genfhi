(ns gen-fhi.db.fx.fhir
  (:require   [cljs.core.async :as a]
              [re-frame.core :as re-frame]
              [clojure.string :as string]
              [clojure.core.match :refer [match]]

              [gen-fhi.db.util :as db-utils]
              [gen-fhi.fhir-client.protocols :as client]
              [gen-fhi.workspace.client :as workspace-client]
              [gen-fhi.db.constants :as c]
              [gen-fhi.fhirpath.core :as fp]
              [gen-fhi.auth.constants :as auth-constants]
              [gen-fhi.patch-building.path :as path]))

(re-frame/reg-fx
 ::fhir-request-fx
 (fn [{:keys [request on-success on-failure on-start on-finish]}]
   (let [interceptor (get @re-frame.db/app-db c/client-key)]
     (assert
      (fn? interceptor)
      "Must register an interceptor via :gen-fhi.db.events.client/set-fhir-interceptor before calling fhir-request-fx.")
     (try
       (when (some? on-start)
         (db-utils/dispatch on-start))
       (let [response (interceptor request)]
         (a/take! response
                  (fn [res]
                    (when (some? on-success)
                      (cond
                        (fn? on-success) (on-success res)
                        (sequential? on-success) (db-utils/dispatch (conj on-success res))))
                    ;; Put here so only triggers after successful query (if in finally statement triggers immediately).
                    (db-utils/dispatch [:gen-fhi.db.events.fhir/remove-in-flight request])
                    (when (some? on-finish)
                      (db-utils/dispatch on-finish)))))
       (catch js/Error e
         (when (some? on-failure)
           (db-utils/dispatch (conj on-failure e)))
         (db-utils/dispatch [:gen-fhi.db.events.fhir/remove-in-flight request])
         (when (some? on-finish)
           (db-utils/dispatch on-finish)))))))

(defn- endpoint-string-ref->path [endpoint-ref-string]
  (let [[resource-type id] (string/split endpoint-ref-string #"/")]
    (path/->path resource-type id)))

(defn- get-endpoint-resource [fhir-client endpoint-ref-string]
  (a/go
    (let [{:keys [resource-type id]} (path/path-meta (endpoint-string-ref->path endpoint-ref-string))
          endpoint-local             (get-in @re-frame.db/app-db [:fhir-store resource-type id])]
      (if (some? endpoint-local)
        endpoint-local
        (let [endpoint-remote (a/<! (client/read fhir-client resource-type id))]
          (swap! re-frame.db/app-db #(assoc-in
                                      %
                                      [:fhir-store resource-type id]
                                      (get endpoint-remote :fhir/response)))
          (get-in @re-frame.db/app-db [:fhir-store resource-type id]))))))

(defn- fhir-request->external-op-request [endpoint external-request header]
  {:fhir/request-type :fhir/invoke
   :fhir/level        :fhir/system
   :fhir/op           "external-fhir-request"
   :fhir/data         {:resourceType "Parameters"
                       :parameter
                       (concat
                        (map
                         (fn [h] {:name "header" :valueString h})
                         header)
                        [{:name "endpoint"
                          :valueReference {:reference endpoint}}
                         {:name "request"
                          :valueString (.stringify js/JSON (clj->js external-request))}])}})

(re-frame/reg-fx
 ::endpoint-fx
 (fn [{:keys [endpoint-ref-string] :as props}]
   (let [interceptor        (get @re-frame.db/app-db c/client-key)
         fhir-client        (workspace-client/create interceptor)]
     (assert
      (fn? interceptor)
      "Must register an interceptor via :gen-fhi.db.events.client/set-fhir-interceptor before calling fhir-request-fx.")
     (a/take!
      (get-endpoint-resource fhir-client endpoint-ref-string)
      (fn [endpoint]
        (match (or (first (fp/evaluate "$this.extension.where(url=%extUrl).value" endpoint
                                       {:options/variables {:extUrl auth-constants/auth-type-url}}))
                   "none")

          (:or "smart-standalone"
               "smart-ehr")
          (db-utils/dispatch [:gen-fhi.db.events.smart/register-smart
                              {:p (path/->path
                                   (get endpoint :resourceType)
                                   (get endpoint :id))
                               :post-registration
                               [:gen-fhi.db.events.fhir/external-smart-request
                                (endpoint-string-ref->path endpoint-ref-string)
                                (update
                                 (select-keys
                                  props
                                  [:on-success
                                   :on-failure
                                   :on-start
                                   :on-finish
                                   :request
                                   :header])
                                 :header
                                 (fn [headers]
                                   (concat headers
                                           (:header endpoint))))]}])

          ;; Default Dispatch as a server side request. Uses the $external-fhir-request operation.
          :else              (let [request (fhir-request->external-op-request
                                            endpoint-ref-string
                                            (get props :request)
                                            (get props :header))]
                               (db-utils/dispatch
                                [:gen-fhi.db.events.fhir/fhir-request
                                 (-> (select-keys
                                      props
                                      [:on-success
                                       :on-failure
                                       :on-start
                                       :on-finish])
                                     (assoc :request request)
                                     (assoc :endpoint endpoint))]))))))))

(re-frame/reg-fx
 ::smart-fhir-request-fx
 (fn [{:keys [interceptor request on-success on-failure on-start on-finish]}]
   (assert
    (fn? interceptor)
    "Must register an interceptor via :gen-fhi.db.events.client/set-fhir-interceptor before calling fhir-request-fx.")
   (try
     (when (some? on-start)
       (db-utils/dispatch on-start))
     (let [response (interceptor request)]
       (a/take! response
                (fn [res]
                  (cond
                    (fn? on-success) (on-success res)
                    (sequential? on-success) (db-utils/dispatch (conj on-success res)))
                  ;; Put here so only triggers after successful query (if in finally statement triggers immediatlly).
                  (when (some? on-finish)
                    (db-utils/dispatch on-finish)))))

     (catch js/Error e
       (when (some? on-failure)
         (db-utils/dispatch (conj on-failure e)))
       (when (some? on-finish)
         (db-utils/dispatch on-finish))))))

