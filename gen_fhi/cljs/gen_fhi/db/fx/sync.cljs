(ns gen-fhi.db.fx.sync
  (:require
   [re-frame.core :as re-frame]
   [cljs.core.async :as a :refer-macros [go-loop]]
   [oops.core :as oops]
   [taoensso.timbre :refer-macros [info]]
   [reagent.core :as r]
   [clojure.pprint]

   ["@ant-design/icons" :refer [ExclamationCircleOutlined]]
   ["antd" :refer [notification]]

   [gen-fhi.db.constants :as c]
   [gen-fhi.db.events.mutate :refer [->pending-key] :as mutate]
   [gen-fhi.db.util :as util]))

(defn db->local-fhir-resource [db resource-type id]
  (get-in db [:fhir-store resource-type id]))

(defn print-queues [message]
  (info message)
  (info (c/pending-queue @re-frame.db/app-db))
  (info (c/inflight-queu @re-frame.db/app-db))
  (info (c/response-queu @re-frame.db/app-db)))

(defn- ->requests-to-send
  "Should only send one patch request at a time for a resource to guarantee correct ordering."
  [inflight-queue pending-queue]
  (let [pending-fhir-requests (vals pending-queue)
        inflight-resources (set (map
                                 #(->pending-key (get-in % [:request :fhir/type])
                                                 (get-in % [:request :fhir/id]))
                                 inflight-queue))]
    (->> pending-fhir-requests
         (filter
          (fn [pending-request]
            (not (contains?
                  inflight-resources
                  (->pending-key (get-in pending-request [:request :fhir/type])
                                 (get-in pending-request [:request :fhir/id]))))))
         (map #(assoc % :internal/id (rand-int 100000000))))))

(defn associate-version-match [db request]
  (let [resource-type  (get-in request [:request :fhir/type])
        id             (get-in request [:request :fhir/id])
        local-resource (db->local-fhir-resource db resource-type id)]
    (assoc-in
     request
     [:request :fhir/if-match]
     (get-in local-resource [:meta :versionId]))))



;; Uniform representation
;; {:version_id :internal/id :patch}))
(re-frame/reg-fx
 ::create-sync-loop-fx
 (fn [{:keys []}]
   (let [exit-chan (a/chan)]
     (go-loop []
       (a/alt!
         exit-chan nil
         (a/timeout 1000)
         (let [db             @re-frame.db/app-db
               interceptor    (get db c/client-key)
               pending-queue  (get db c/pending-queue)
               inflight-queue (get db c/inflight-queu)
               requests       (->> (->requests-to-send inflight-queue pending-queue)
                                   (map (partial associate-version-match db)))]
           (when (seq requests)
             (swap!
              re-frame.db/app-db
              (fn [db]
                (-> db
                    (assoc  c/pending-queue {})
                    (update c/inflight-queu (fn [inflight] (concat inflight requests)))))))

           ;;(print-queues "InFlight")
           (let [results (a/<! (a/reduce
                                (fn [result value]
                                  (conj result value))
                                []
                                (a/merge
                                 (map
                                  (fn [request]
                                    (a/go
                                      (let [res (a/<! (interceptor (get request :request)))]
                                        ;; [TODO] need to use a fhir-response crafting to determine error.
                                        (if (= :fhir/error (:fhir/response-type res))
                                          ;; On auth issue reload page to force reauth
                                          (cond
                                            (or (= 403 (:http/status res))
                                                (= 401 (:http/status res))) (do
                                                                              ;; Set to nil so no warning to user on auth issue...
                                                                              (set! (.-onbeforeunload js/window) nil)
                                                                              (oops/ocall js/window "location.reload"))
                                            (= 409 (:http/status res)) (assoc request :error true :retry? true)

                                            :else (do
                                                    (util/alert-operation-outcome res)
                                                    (assoc request :error true)))
                                          ;; Using the same request but including the version-id
                                          (assoc
                                           request
                                           :version-id (get-in res [:fhir/response :meta :versionId]))))))
                                  requests))))]

             (when (seq results)
               (swap!
                re-frame.db/app-db
                (fn [db]
                  (let [inflight-filtered (reduce
                                           (fn [inflight-queue response]
                                             (filter #(not= (% :internal/id) (response :internal/id)) inflight-queue))
                                           (get db c/inflight-queu [])
                                           results)
                        retry-requests    (filter #(:retry? %) results)]

                    (-> (mutate/re-process-requests db retry-requests {:retry? true})
                        (assoc
                         c/inflight-queu
                         inflight-filtered

                         c/response-queu
                         (concat
                          (get db c/response-queu [])
                          (filter #(not (= true (:error %))) results))))))))

             ;;(print-queues "Response")
             (recur)))))
     exit-chan)))

