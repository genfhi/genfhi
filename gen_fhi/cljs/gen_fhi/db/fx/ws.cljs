(ns gen-fhi.db.fx.ws
  (:require
   [cljs.core.async :as a]
   [gen-fhi.db.constants :as c]
   [gen-fhi.workspace.suuid :refer [suuid->uuid]]
   [gen-fhi.workspace.ws :refer [create-workspace-channel]]
   [re-frame.core :as re-frame]
   [taoensso.sente :as sente]))

(re-frame/reg-fx
 ::register-channel-fx
 (fn [{:keys [workspace get-access-token on-success on-failure]}]
   (try
     (let [db  @re-frame.db/app-db]
       (when-let [_existing-ws (get db c/channel-key)]
         ;; Disconnect from a previously existing channel.
         (a/put! (get-in db c/patch-channel-key) 1) ;; Kill the patch channel query
         (sente/chsk-disconnect! (get-in db [c/channel-key :chsk])))
       (a/take! (create-workspace-channel (suuid->uuid workspace) get-access-token)
                (fn [res]
                  (re-frame/dispatch (conj on-success res)))))
     (catch js/Error _e
       (when on-failure
         (re-frame/dispatch (conj on-failure)))))))
