(ns gen-fhi.db.log-db
  (:require [re-frame.db :as db]))

(defn log-db []
  (.log js/console (clj->js @db/app-db)))

(set! (.-logDB js/window) log-db)
