(ns gen-fhi.db.questionnaire.constants)

(def ordinal-expression-url
  "http://hl7.org/fhir/StructureDefinition/ordinalValue")

(def calculated-expression-url
  "http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-calculatedExpression")

(def enablewhen-expression-url
  "http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-enableWhenExpression")

(def initial-expression-url
  "http://hl7.org/fhir/uv/sdc/StructureDefinition/sdc-questionnaire-initialExpression")

(def variable-expression-url
  "http://hl7.org/fhir/StructureDefinition/variable")
