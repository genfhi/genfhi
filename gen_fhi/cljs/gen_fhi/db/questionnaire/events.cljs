(ns gen-fhi.db.questionnaire.events
  (:require [re-frame.core :as re-frame]
            [gen-fhi.patch-building.path :as path]))

(defn- find-questionnaire-item
  "Finds Questionnaire item recursively looping over items"
  [cur link-id p]
  (if (= (get cur :linkId) link-id)
    p
    (loop [idx 0
           child-items (get cur :item [])]
      (if (empty? child-items)
        nil
        (if-let [p (find-questionnaire-item
                    (first child-items)
                    link-id
                    (path/extend-path p "item" idx))]
          p
          (recur
           (inc idx)
           (rest child-items)))))))

(re-frame/reg-event-fx
 ::set-selected-item-by-link-id
 (re-frame/inject-cofx
  :inject/subscription
  (fn [[_ {:keys [p]}]]
    (let [{:keys [resource-type id]} (path/path-meta p)]
      [:gen-fhi.db.subs.value/get-value (path/->path resource-type id)])))
 (fn [{:keys [gen-fhi.db.subs.value/get-value]} [_ {:keys [p link-id]}]]
   (let [{:keys [resource-type id]} (path/path-meta p)
         p                          (find-questionnaire-item
                                     get-value
                                     link-id
                                     (path/->path resource-type id))]
     {:dispatch [:gen-fhi.db.events.ui/set-selected-item-path p]})))

(re-frame/reg-event-fx
 ::register-questionnaire-on-change
 (fn [_cofx [_ id sub event-fn]]
   {:gen-fhi.db.fx.track/register
    {:id           id
     :subscription sub
     :event-fn     event-fn
     :dispatch-first? false}}))

(re-frame/reg-event-fx
 ::dispose-questionnaire-on-change
 (fn [_cofx [_ id]]
   {:gen-fhi.db.fx.track/dispose {:id id}}))
