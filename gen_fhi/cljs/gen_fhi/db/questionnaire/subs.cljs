(ns gen-fhi.db.questionnaire.subs
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]
            [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [taoensso.timbre :refer-macros [warn]]

            [gen-fhi.patch-building.path :as path]
            [gen-fhi.fhirpath.loc :as fp-loc]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.db.questionnaire.constants :as constants]
            [gen-fhi.db.questionnaire.utilities :as q-utils]))

(declare evaluate-expression-unsafe)

(defn- items->map [acc cur-pointer items]
  (->> items
       (map-indexed (fn [i x] [i x]))
       (reduce
        (fn [acc [i item]]
          (let [pointer (path/descend cur-pointer i)
                acc     (assoc acc (item :linkId) pointer)]

            (items->map
             acc
             (path/descend pointer :item)
             (get item :item []))))
        acc)))

(defn- move-up-item
  "Given a Path move up to the next item in a questionnaire. Does so by checking parent field is :item."
  [p]
  (when (some? p)
    (loop [p (:parent (path/ascend p))]
      (if (or (nil? p)
              (= :item (as-> p p
                         (:parent (path/ascend p))
                         (when (some? p) (:field (path/ascend p))))))
        p
        (recur (:parent (path/ascend p)))))))

(defn- goto-item [p]
  (if (= :item (as-> p p
                 (:parent (path/ascend p))
                 (when (some? p) (:field (path/ascend p)))))
    p
    (move-up-item p)))

;; Need to ascend up the chain searching for extensions
(re-frame/reg-sub-raw
 ::get-all-sd-variables
 (fn [_ [_ p]]
   (r/reaction
    (let [cur @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p {:fetch-remote? false}])]
      (concat
       (let [variable-extensions (->>
                                  (get cur :extension [])
                                  (filter #(= (:url %) constants/variable-expression-url))
                                  (map :valueExpression))]
         variable-extensions)
       (if-let [parent-item-p (move-up-item p)]
         @(re-frame/subscribe [::get-all-sd-variables parent-item-p])
         []))))))

(re-frame/reg-sub
 ::expr->issue
 (fn [[_ operation-pointer]]
   (let [{:keys [resource-type id]} (path/path-meta operation-pointer)]
     (assert (= "OperationOutcome" resource-type) "operation-pointer get-issue-map must be pointing to OperationOutcome")
     [(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                           (path/->path :OperationOutcome id)
                           {:fetch-remote? false}])]))
 (fn [[operation-outcome]]
   (reduce
    (fn [expr->issue issue]
      (reduce
       (fn [expr->issue expression]
         (assoc expr->issue expression issue))
       expr->issue
       (get issue :expression)))
    {}
    (get operation-outcome :issue))))

(re-frame/reg-sub
 ::pointer->issues
 (fn [[_ operation-pointer _pointer]]
   [(re-frame/subscribe [::expr->issue operation-pointer])])
 (fn [[expr->issue] [_ _operation-pointer pointer]]
   (let [expression (path/to-expression pointer)]
     (expr->issue expression))))

(re-frame/reg-sub-raw
 ::find-sd-variables
 (fn [_ [_ p variable-name]]
   (r/reaction
    (let [cur @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p {:fetch-remote? false}])]
      (let [variable-expression (->>
                                 (get cur :extension [])
                                 (filter #(= (:url %) constants/variable-expression-url))
                                 (map :valueExpression)
                                 (filter #(= variable-name (:name %)))
                                 (first))]
        (if variable-expression
          variable-expression
          (when-let [parent-item-p (move-up-item p)]
            @(re-frame/subscribe [::find-sd-variables parent-item-p variable-name]))))))))

(re-frame/reg-sub
 ::get-variables
 (fn [[_ q-pointer]]
   [(re-frame/subscribe [::questionnaire-linkid-map q-pointer])
    (re-frame/subscribe [::get-all-sd-variables     q-pointer])])
 (fn [[linkid-map sd-variables]]
   (concat
    [{:label "questionnaire" :type "variable"}]
    [{:label "qitem"         :type "variable"}]
    [{:label "resource"      :type "variable"}]
    (map (fn [expression]
           {:label (:name expression) :type "variable"})
         sd-variables)
    (map (fn [k]
           {:label (name k) :type "variable"})
         (fp/->environment-variables-ids))
    (map (fn [k]
           {:label k :type "variable"})
         (keys linkid-map)))))

(re-frame/reg-sub
 ::questionnaire-linkid-map
 (fn [[_ q-pointer]]
   (let [q-root (path/path->root q-pointer)]
     [(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-root {:fetch-remote? false}])]))
 (fn [[questionnaire] [_ q-pointer]]
   (let [root-pointer (path/path->root q-pointer)]
     (items->map {} (path/descend root-pointer :item) (get questionnaire :item [])))))

; Returns all pointers for questionnaire-response items based on questionnaire-item linkId connection.
(re-frame/reg-sub
 ::get-questionnaire-response-pointers
 (fn [[_ q-pointer qr-pointer]]
   [(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-pointer {:fetch-remote? false}])
    (re-frame/subscribe [:gen-fhi.db.subs.value/get-value qr-pointer {:fetch-remote? false}])])
 (fn [[questionnaire-item questionnaire-response-items] [_ q-pointer qr-pointer]]
   ;; Run sanity check assertions.
   (assert (= :item (:field (path/ascend qr-pointer))))

   ;; Could be nil here if questionnaire-item is malformed
   (if (nil? (:linkId questionnaire-item))
     (do
       (warn (str "Questionnaire item didn't have linkId at :'" q-pointer "'"))
       [])
     (let [qr-paths (eduction
                     (comp
                      (map-indexed (fn [i v] [i v]))
                      (filter (fn [[_i qr-item]] (= (:linkId questionnaire-item) (:linkId qr-item))))
                      (map    (fn [[i _qr-item]] (path/descend qr-pointer i))))
                     questionnaire-response-items)]

       (if (empty? qr-paths)
         [(path/extend-path qr-pointer {:linkId (get questionnaire-item :linkId)})]
         qr-paths)))))

(defn- is-expression? [value]
  (and (contains? value :language)
       (contains? value :expression)))

(defn- throw-circular-loc-error [loc]
  (throw (ex-info
          (str "Failed Circular\n" (fp-loc/loc->str loc))
          {:loc loc})))

(re-frame/reg-sub-raw
 ::get-variable
 (fn [_ [_ {:keys [p qr-path variable-id]}]]
   (r/reaction
    (let [linkid-map      @(re-frame/subscribe [::questionnaire-linkid-map p])
          qr-item-pointer @(re-frame/subscribe [::qr-item-traversal-for-link-id qr-path variable-id #{}])]
      (if-let [item-p (get linkid-map (name variable-id))]
        (merge
         @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value item-p {:fetch-remote? false}])
         (when qr-item-pointer @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value qr-item-pointer {:fetch-remote? false}])))
        (let [variable-expression @(re-frame/subscribe [::find-sd-variables p (name variable-id)])]
          (when variable-expression
            (let [result @(re-frame/subscribe [::evaluate-expression {:expression variable-expression :p p :qr-path qr-path}])]
              (if-let [error (:error result)]
                (throw (ex-info
                        (str "Error with variable '" (name variable-id) "': '" (ex-message error) "'")
                        {:variable variable-id}))
                (:evaluation result))))))))))

(defn- item-path+qr-path+visited->fp-options
  ([{:keys [p qr-path loc] :as opts}]
   {:pre [(some? loc)]}
   (let [root-questionnaire-path  (path/path->root p)]
         ;;root-qr-path (path/path->root qr-path)]
     {:fp-node/loc       loc
      ;; :fp-node/get-value (fn [loc ctx field]
      ;;                      (let [value (get ctx (keyword "property" (name field)) (get ctx field))]
      ;;                        (if (is-expression? value)
      ;;                          (if (fp-loc/circular? loc)
      ;;                            (throw-circular-loc-error loc)
      ;;                            (:evaluation
      ;;                             (evaluate-expression-unsafe
      ;;                              value
      ;;                              (assoc opts :loc loc))))
      ;;                          value)))
      :options/variables
      (fn [loc id]
        (if (fp-loc/circular? loc)
          (throw-circular-loc-error loc)
          (cond
            ;; See https://build.fhir.org/ig/HL7/sdc/expressions.html#fhirpath-supplements
            (= id :qitem)
            (let [q-item-p (goto-item p)]
              @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                    q-item-p
                                    {:fetch-remote? false}]))
            (= id :questionnaire)
            @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                  root-questionnaire-path
                                  {:fetch-remote? false}])

            (= id :resource)
            @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                  (path/path->root qr-path)
                                  {:fetch-remote? false}])
            :else
            (let [var   @(re-frame/subscribe
                          [::get-variable
                           {:p           p
                            :qr-path     qr-path
                            :variable-id id
                            :loc         loc}])]
              var))))})))

(re-frame/reg-sub-raw
 ::evaluate-expression
 (fn [_ [_ {:keys [expression p qr-path ctx]}]]
   (r/reaction
    (try
      (let [result (fp/evaluate
                    (:expression expression)
                    ctx
                    (item-path+qr-path+visited->fp-options
                     {:p p :qr-path qr-path :loc (fp-loc/->loc)}))]

        {:evaluation result})

      (catch :default e
        {:error e})))))

(defn- search-collection [pointers-to-search search-link-id visited-paths]
  (loop [remaining-pointers-to-search pointers-to-search]
    (if (empty? remaining-pointers-to-search)
      nil
      (if-let [found @(re-frame/subscribe [::qr-item-traversal-for-link-id
                                           (first remaining-pointers-to-search)
                                           search-link-id
                                           (apply conj
                                                  visited-paths
                                                  (disj
                                                   pointers-to-search
                                                   (first remaining-pointers-to-search)))])]
        found
        (recur (rest remaining-pointers-to-search))))))

(defn- pre-process-qr-pointer
  "qr-pointer expected to be at an item location if it's not raise until it is or at root"
  [p]
  (loop [p p]
    (let [{:keys [parent]} (path/ascend p)]
      (if (or (nil? parent)
              (= :item (:field (path/ascend parent))))
        p
        (recur (:parent (path/ascend p)))))))

(re-frame/reg-sub-raw
 ::qr-item-traversal-for-link-id
 (fn [_ [_ qr-pointer search-linkid visited-paths]]
   ;; Need handle case where
   (r/reaction
    (let [qr-pointer (pre-process-qr-pointer qr-pointer)
          qr-item|resource @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value qr-pointer {:fetch-remote? false}])]
      (if (visited-paths qr-pointer)
        nil
        (if (= (:linkId qr-item|resource) (name search-linkid))
          qr-pointer
          (let [ancestor-pointer (try (-> qr-pointer (path/ascend) :parent (path/ascend) :parent)
                                      (catch :default _e
                                        nil))
                ancestor-value (when ancestor-pointer
                                 @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value ancestor-pointer {:fetch-remote? false}]))
                ;; Can only have siblings if ancestor exists.
                sibling-pointers (if ancestor-pointer
                                   (disj (apply sorted-set (map  #(path/extend-path ancestor-pointer "item" %)
                                                                 (range (count (get ancestor-value :item [])))))
                                         qr-pointer)
                                   #{})
                child-pointers   (disj (apply sorted-set (map  #(path/extend-path qr-pointer "item" %)
                                                               (range (count (get qr-item|resource :item [])))))
                                       qr-pointer)
                visited-paths    (conj visited-paths qr-pointer)]

            ;; Ancestor search
            (if-let [found-root (when (and (not (contains? visited-paths ancestor-pointer))
                                           (some? ancestor-pointer))
                                  @(re-frame/subscribe [::qr-item-traversal-for-link-id
                                                        ancestor-pointer
                                                        search-linkid
                                                        (apply conj visited-paths sibling-pointers)]))]
              found-root
              ;; sibling-search
              (if-let [found-sibling (search-collection sibling-pointers search-linkid visited-paths)]
                found-sibling
                (when-let [found-child (search-collection child-pointers search-linkid (apply
                                                                                        conj
                                                                                        visited-paths
                                                                                        sibling-pointers))]
                  found-child))))))))))

(defn- expression->answer [expression {:keys [q-pointer qr-pointer q-item]}]
  (let [result (when expression
                 (-> @(re-frame/subscribe [::evaluate-expression
                                           {:expression expression
                                            :p          q-pointer
                                            :qr-path    qr-pointer
                                            :ctx        nil}])
                     (:evaluation)
                     (first)))]
    (when result
      {(keyword (str "value" (string/capitalize
                              (q-utils/q-item-type->fhir-primitive
                               (get q-item :type)))))
       ;; Need to set to nil as equality checks fail on NaN
       ;; So could cause constant repeat.
       ;; set to nil to avoid this...
       (if (.isNaN js/Number result)
         nil
         result)})))

(defn- ->answer [answer {:keys [q-item] :as expression-options}]
  (let [calculated-expression     (->> (get q-item :extension [])
                                       (filter #(= (:url %) constants/calculated-expression-url))
                                       (first)
                                       :valueExpression)

        initial-expression        (->> (get q-item :extension [])
                                       (filter #(= (:url %) constants/initial-expression-url))
                                       (first)
                                       :valueExpression)]

    (cond
      (some? calculated-expression)     (expression->answer calculated-expression expression-options)
      (some? answer)                    answer
      (some? (first (:initial q-item))) (first (:initial q-item))
      :else                             (if (some? initial-expression)
                                          (expression->answer initial-expression expression-options)
                                          nil))))

(defn- evaluate-enablewhen-condition [q-pointer qr-pointer condition]
  (let [item       @(re-frame/subscribe [::get-variable
                                         {:p           (path/path->root q-pointer)
                                          :qr-path     qr-pointer
                                          :variable-id (:question condition)}])]
    (if (nil? item)
      true
      (let [type       (q-utils/q-item-type->fhir-primitive
                        (get item :type))
            value-key  (keyword (str "value"  (string/capitalize type)))
            answer-key (keyword (str "answer" (string/capitalize type)))]

        (match (:operator condition)
          "<=" (<=     (get-in item [:answer 0 value-key])            (get condition answer-key))
          ">=" (>=     (get-in item [:answer 0 value-key])            (get condition answer-key))
          ">"  (>      (get-in item [:answer 0 value-key])            (get condition answer-key))
          "<"  (<      (get-in item [:answer 0 value-key])            (get condition answer-key))
          "!=" (not (= (get-in item [:answer 0 value-key])            (get condition answer-key)))
          "="  (=      (get-in item [:answer 0 value-key])            (get condition answer-key))
          "exists"     (= (some? (get-in item [:answer 0 value-key])) (get condition :answerBoolean))
          :else        false)))))

(defn- ->is-enabled? [{:keys [q-item q-pointer qr-pointer]}]
  (let [enablewhen-expression (->> (get q-item :extension [])
                                   (filter #(= (:url %) constants/enablewhen-expression-url))
                                   (first)
                                   :valueExpression)]

    (if enablewhen-expression
      (-> @(re-frame/subscribe [::evaluate-expression
                                {:expression enablewhen-expression
                                 :p q-pointer
                                 :qr-path qr-pointer
                                 :ctx     nil}])
          (:evaluation)
          (first))
      (let [enable-behavior (get q-item :enableBehavior "all")]
        (if (empty? (get q-item :enableWhen))
          true
          (reduce
           (fn [is-enabled? condition]
             (let [result (evaluate-enablewhen-condition
                           q-pointer
                           qr-pointer
                           condition)]
               (if (= "all" enable-behavior)
                 (and is-enabled? result)
                 (or  is-enabled? result))))
           (evaluate-enablewhen-condition q-pointer qr-pointer (first (get q-item :enableWhen)))
           (rest (get q-item :enableWhen))))))))

(re-frame/reg-sub-raw
 ::get-derived-items
 (fn [_ [_ q-pointer qr-pointer]]
   (r/reaction
    (let [qr-pointers               @(re-frame/subscribe [::get-questionnaire-response-pointers q-pointer qr-pointer])
          q-item                    @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-pointer {:fetch-remote? false}])
          output (doall
                  (mapcat
                   (fn [qr-pointer]
                     (let [qr-item @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value qr-pointer {:fetch-remote? false}])]
                       (map
                        (fn [i]
                          (let [derived-answer (->answer ;; Have check in place otherwise nth will throw
                                                (if (seq (get qr-item :answer))
                                                  (nth (get qr-item :answer []) i)
                                                  nil)
                                                {:q-pointer q-pointer
                                                 :qr-pointer qr-pointer
                                                 :q-item q-item})]

                            (cond-> (assoc q-item
                                           :enabled?            (->is-enabled? {:q-item     q-item
                                                                                :q-pointer  q-pointer
                                                                                :qr-pointer qr-pointer})

                                           :meta/q-pointer      q-pointer
                                           :meta/qr-pointer     qr-pointer
                                           :meta/answer-pointer (path/extend-path qr-pointer
                                                                                  :answer
                                                                                  i))
                              (some? derived-answer)               (merge derived-answer))))

                        (range (max 1 (count (get qr-item :answer [])))))))
                   qr-pointers))]
      output))))

(defn- derived->qr-item [derived-item]
  {:linkId (get derived-item :linkId)
   :text (get derived-item :text)})
   ;;:meta/answer-pointer (get derived-item :meta/answer-pointer)})

(defn- derived-item->answer-value [derived-item]
  (let [value-keyword (keyword (str "value" (string/capitalize (q-utils/q-item-type->fhir-primitive (get derived-item :type)))))]
    (if (some? (get derived-item value-keyword))
      [{value-keyword (get derived-item value-keyword)}]
      [])))

(re-frame/reg-sub-raw
 ::get-derived-q-items
 (fn [_ [_ q-pointer qr-pointer]]
   (r/reaction
    (let [derived-items @(re-frame/subscribe [::get-derived-items q-pointer qr-pointer])
          is-group?     (= "group" (get (first derived-items) :type))]
      (if (not is-group?)
        (let [qr-item (derived->qr-item (first derived-items))
              answers (into [] (mapcat derived-item->answer-value derived-items))]
          [(if (empty? answers)
             qr-item
             (assoc
              qr-item
              :answer
              answers))])

        ;; Groups repeat at the item level not the answer level (which is the case for repeat integer as an ex).
        (into
         []
         (map
          (fn [derived-item]
            (let [q-pointer  (get derived-item :meta/q-pointer)
                  qr-pointer (get derived-item :meta/qr-pointer)
                  qr-item    (derived->qr-item derived-item)]
              (if (empty? (get derived-item :item))
                qr-item
               ;; Handle the nested values.
                (assoc
                 qr-item
                 :item
                 (into
                  []
                  (doall
                   (mapcat
                    (fn [i] @(re-frame/subscribe
                              [::get-derived-q-items
                               (path/extend-path q-pointer :item i)
                               (path/extend-path qr-pointer :item)]))
                    (range (count (get derived-item :item))))))))))
          derived-items)))))))

(re-frame/reg-sub-raw
 ::get-derived-questionnaire-response
 (fn [_ [_ q-pointer qr-pointer]]
   (r/reaction
    (assert (nil? (:parent (path/ascend qr-pointer))) "q-pointer should be at root")
    (let [{:keys [id]}  (path/path-meta qr-pointer)
          questionnaire @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-pointer {:fetch-remote? false}])]
      (when (and (some? questionnaire) (= "Questionnaire" (:resourceType questionnaire)))
        (cond-> {:id           id
                 :resourceType "QuestionnaireResponse"
                 :status       "in-progress"
                 :item
                 (mapcat
                  (fn [i]
                    @(re-frame/subscribe [::get-derived-q-items
                                          (path/extend-path q-pointer :item i)
                                          (path/extend-path qr-pointer :item)]))
                  (range (count (:item questionnaire))))}
          (some? (:url questionnaire)) (assoc :questionnaire (:url questionnaire))))))))
