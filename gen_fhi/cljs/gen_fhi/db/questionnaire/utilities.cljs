(ns gen-fhi.db.questionnaire.utilities
  (:require [clojure.core.match :refer [match]]))

(defn q-item-type->fhir-primitive [item-type]
  (match item-type
    "group"       ""

    "display"     ""

    "date"        "date"

    "dateTime"    "dateTime"

    "time"        "time"

    "boolean"     "boolean"

    "string"      "string"

    "text"        "string"

    "url"         "uri"

    "integer"     "integer"

    "decimal"     "decimal"

    "choice"      "coding"

    "open-choice" "coding"

    "attachment"  "attachment"

    "reference"   "reference"

    "quantity"    "quantity"))

