(ns gen-fhi.db.subs.actions
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]

            [gen-fhi.workspace.constants.smart :as smart-constant]
            [gen-fhi.patch-building.path :as path]))

(re-frame/reg-sub
 ::all-cached-actions-data
 (fn [app-db [_]]
   (get app-db :action-response {})))

(re-frame/reg-sub
 ::get-cached-actions-app
 (fn []
   (re-frame/subscribe [:gen-fhi.db.subs.actions/all-cached-actions-data]))
 (fn [action-responses [_ client-app-path]]
   (let [{:keys [resource-type id]} (path/path-meta client-app-path)]
     (assert (= resource-type "ClientApplication")
             (str
              "Cached action data only available for ClientApp resource type not '"
              resource-type "'"))
     (get action-responses id))))

(defn parameter->fhir-response [parameter]
  (js->clj (.parse js/JSON (:valueString parameter)) :keywordize-keys true))

(re-frame/reg-sub
 ::get-cached-action
 (fn [[_ client-app-path]]
   (re-frame/subscribe [:gen-fhi.db.subs.actions/get-cached-actions-app client-app-path]))
 (fn [app-actions [_ _client-app-path action-id]]
   (get app-actions action-id)))

(defn client-path->manifest-path [client-path]
  (let [{:keys [resource-type id]} (path/path-meta client-path)]
    (path/->path resource-type id "manifest")))

(defn item-state-path->state-path-generic [item-state-path]
  (let [{:keys [resource-type id]} (path/path-meta item-state-path)]
    (path/->path resource-type id "states")))

(re-frame/reg-sub-raw
 ::get-action-data
 (fn
   [_app-db [_ {:keys [client-path state-path action-id fetch? loc]
                :or   {fetch? true}}]]
   (r/reaction
    (when (some? action-id)
      (let [states-path                (item-state-path->state-path-generic state-path)
            cached-data                @(re-frame/subscribe [:gen-fhi.db.subs.actions/get-cached-action client-path action-id])
            result                     @(re-frame/subscribe
                                         [:gen-fhi.db.subs.item/get-action
                                          {:client-path   (client-path->manifest-path client-path)
                                           :state-path    states-path
                                           :action-id     action-id
                                           :loc           loc}])]
        ;; Error stored under :error keyword.
        (if-let [error (get result :error)]
          {:error error}
          (let [action                    (:evaluation result)
               ;; Because We want to retrigger when endpoint data changes IE when it's loaded the client,
               ;; Hard a dependency to it smart cached data here.
                endpoint-path             (if (= (get action :endpoint :reference) smart-constant/smart-key)
                                            client-path
                                            (path/reference->path (get action :endpoint)))
                _smart-info               (when endpoint-path
                                            @(re-frame/subscribe [:gen-fhi.db.subs.smart/path->smart-info endpoint-path]))]
            (when (and (= (get action :trigger) "automatic")
                       (not= action (get cached-data :query))
                       ;; For subscriber-all component don't initiate fetches.
                       ;; Strictly used for setting up subs correctly on injection.
                       fetch?)
              (re-frame/dispatch [:gen-fhi.db.events.action/trigger-action {:id         action-id
                                                                            :p          client-path
                                                                            :state-path states-path
                                                                            :loc        loc}]))
              ;; (.log js/console
              ;;       (clj->js action)
              ;;       "!="
              ;;       (clj->js (get-in cached-data [:query]))
              ;;       (not= action (get cached-data :query))))

            {:result (get  cached-data :response)})))))))
