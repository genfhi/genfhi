(ns gen-fhi.db.subs.client
  (:require
   [gen-fhi.db.constants :as constants]
   [gen-fhi.workspace.client :as client]
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::get-fhir-interceptor
 (fn [db _]
   (get db constants/client-key)))

(re-frame/reg-sub
 ::get-fhir-client
 (fn []
   (re-frame/subscribe [::get-fhir-interceptor]))
 (fn [int _]
   (when int
     (client/create int))))
