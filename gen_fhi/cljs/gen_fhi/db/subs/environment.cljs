(ns gen-fhi.db.subs.environment
  (:require [re-frame.core :as re-frame]))


(re-frame/reg-sub
 ::environment
 (fn [db]
   (get db :environment)))
