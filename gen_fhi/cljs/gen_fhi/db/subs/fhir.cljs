(ns gen-fhi.db.subs.fhir
  (:require [re-frame.core :as re-frame]
            [clj-json-patch.core :as patch]
            [taoensso.timbre :refer-macros [error]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.views.instance.application-editor.upgrades.resource
             :as resource-upgrades]))

(re-frame/reg-sub
 ::get-fhir-store
 (fn [db]
   (:fhir-store db)))

(re-frame/reg-sub
 ::get-local-fhir-resource
 (fn [_]
   (re-frame/subscribe [::get-fhir-store]))
 (fn [fhir-store [_ resource-type id]]
   (get-in fhir-store [resource-type id])))

(defn- resource+patches->patched-resource [resource patches]
  (try
    (patch/patch resource patches  true)
    (catch js/Error e
      nil
      (error "Failed to apply patch: " e
             (str patches)))))

(defn apply-patches [resource patches]
  (loop [resource resource
         patch-idx 0]
    (if (< patch-idx  (count patches))
      (recur
       (if-let [applied-resource (resource+patches->patched-resource
                                  resource
                                  (get (nth patches patch-idx) :fhir/data []))]
         applied-resource
         resource)
       (+ 1 patch-idx))
      resource)))

;; Used in get-resource-temp-edits because otherwise circular dependency.
(re-frame/reg-sub
 ::get-fhir-resource-no-temp
 (fn [[_ resource-type id]]
   [(re-frame/subscribe [::get-local-fhir-resource resource-type id])
    (re-frame/subscribe [:gen-fhi.db.subs.patches/get-resource-local-patches-to-apply resource-type id])])
 (fn [[resource sync-patches] [_ _resource-type _id]]
   (resource-upgrades/upgrade-to-latest
    (apply-patches resource sync-patches))))

(re-frame/reg-sub
 ::get-fhir-resource
 (fn [[_ resource-type id]]
   [(re-frame/subscribe [::get-local-fhir-resource resource-type id])
    (re-frame/subscribe [:gen-fhi.db.subs.patches/get-resource-local-patches-to-apply resource-type id])
    (re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-resource-temp-edit (path/->path resource-type id)])])
 (fn [[resource sync-patches temp-edits] [_ _resource-type _id {:keys [apply-temps?] :or {apply-temps? false}}]]
   (resource-upgrades/upgrade-to-latest
    (cond-> resource
      true         (apply-patches sync-patches)
      apply-temps? (apply-patches temp-edits)))))

(re-frame/reg-sub
 ::get-extension-path-by-url
 (fn [[_ extensions-path]]
   (re-frame/subscribe [:gen-fhi.db.subs.value/get-value extensions-path]))
 (fn [extensions [_ path ext-url]]
   (let [ext-idx  (or
                   (first
                    (keep-indexed
                     (fn [idx ext]
                       (when (= ext-url (:url ext))
                         idx))
                     extensions))
                   0)]
     (path/descend path ext-idx))))
