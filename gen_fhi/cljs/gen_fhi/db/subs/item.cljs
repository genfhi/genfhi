(ns gen-fhi.db.subs.item
  (:require [clojure.core.match :refer [match]]
            [clojure.set :as set]
            [reagent.core :as r]
            [re-frame.core :as re-frame]
            [clojure.string :as string]

            [gen-fhi.db.subs.smart :refer [map->SmartContext]]
            [gen-fhi.x-fhir-query.core :as x-fhir-query]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.fhirpath.loc :as fp-loc]
            [gen-fhi.patch-building.path :as path]))

(declare evaluate-expression-unsafe)
(declare item-path+state-path+visited->fp-options)

(re-frame/reg-sub
 ::state-path-hash
 (fn [[_ state-path]]
   (re-frame/subscribe [:gen-fhi.db.subs.value/get-value state-path {:fetch-remote? false}]))
 (fn [states [_ state-path]]
   (reduce-kv
    (fn [state-hash index value]
      (assoc state-hash (:id value) (path/descend state-path index)))
    {}
    states)))

(re-frame/reg-sub
 ::get-state-path
 (fn [[_ state-path _item-id]]
   (re-frame/subscribe [::state-path-hash state-path]))
 (fn [state-paths-hash [_ state-path item-id]]
   (let [cur-state-path (get state-paths-hash item-id)]
     (if (some? cur-state-path)
       cur-state-path
       (path/descend
        state-path
        (count (vals state-paths-hash)))))))

(re-frame/reg-sub-raw
 ::get-item-state
 (fn [_app-db [_ state-path item-id]]
   (r/reaction
    (let [state-path @(re-frame/subscribe [::get-state-path state-path item-id])]
      (when state-path
        @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value state-path {:fetch-remote? false}]))))))

(re-frame/reg-sub
 ::get-item-meta-path
 (fn [[_ meta-path]]
   (re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                        (path/descend
                         (path/path->root meta-path)
                         "manifest")]))

 (fn [item-metas [_ meta-path item-id]]
   (let [index (first
                (->> item-metas
                     (map-indexed vector)
                     (filter (fn [[_index item]]
                               (= (:id item) (name item-id))))
                     (map first)))]
     (when index
       (path/extend-path (path/path->root meta-path) "manifest" index)))))

(re-frame/reg-sub-raw
 ::get-all-actions
 (fn [_app-db [_ resource-path]]
   (r/reaction
    (let [{:keys [resource-type id]} (path/path-meta resource-path)
          actions-path (path/->path resource-type id "action")]
      @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value actions-path])))))

(re-frame/reg-sub
 ::get-action-path
 (fn [[_ resource-path]]
   (re-frame/subscribe [::get-all-actions resource-path]))
 (fn [actions [_ resource-path action-id]]
   (let [{:keys [resource-type id]} (path/path-meta resource-path)
         index (first
                (->> actions
                     (map-indexed vector)
                     (filter (fn [[_index action]]
                               (let [action-id (try (name action-id) (catch js/Error _e nil))]
                                 (= (:id action) (name action-id)))))
                     (map first)))]
     (when index
       (path/->path resource-type id "action" index)))))

;; Used on codemirror autocompletions
(re-frame/reg-sub-raw
 ::get-all-variable-names
 (fn [_app-db [_ meta-path]]
   (r/reaction
    (let [{:keys [resource-type id]} (path/path-meta meta-path)
          actions                    @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path resource-type id "action")])
          is-smart                   @(re-frame/subscribe [:gen-fhi.db.subs.smart/is-legacy-smart? meta-path])
          items                      @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path resource-type id "manifest")])
          environment-variables      (fp/->environment-variables-ids)]

      (concat
       [{:label "endpoint" :type "variable" :detail (str "[SMART] Endpoint smart information.")}]
       (if is-smart
         [{:label "smart" :type "variable" :detail (str "[SMART] SMART on fhir context.")}]
         [])
       (map
        (fn [v] {:label (name v) :type "variable" :detail (str "[environment]<" (name v) ">")})
        environment-variables)
       (map
        (fn [action] {:label (:id action) :type "variable" :detail (str "[action]<" (:type action) ">")})
        (or actions []))
       (map
        (fn [item] {:label (:id item) :type "variable" :detail (str "[item]<" (:type item) ">")})
        (or items [])))))))

(re-frame/reg-sub-raw
 ::get-variable
 (fn [_app-db [_ {:keys [p state-path variable-id loc evaled?] :or {evaled? false}}]]
   (r/reaction
    ;; Allow smart context extracted through %smart variable id.
    (cond
      (= :endpoint variable-id)
      (let [smart-infos @(re-frame/subscribe [:gen-fhi.db.subs.smart/get-all-endpoint-smart-context p])]
        smart-infos)
      (= :smart variable-id)
      (let [smart-info @(re-frame/subscribe [:gen-fhi.db.subs.smart/smart-context p])]
        (if (some? smart-info)
          smart-info
          (map->SmartContext
           {:clientId  nil
            :encounter {:id nil}
            :patient   {:id nil}
            :user      {:id nil
                        :resourceType nil}})))

      :else
      (if-let [item-variable-meta-path @(re-frame/subscribe [::get-item-meta-path p variable-id])]
        ;; [FIXME] Hack used in modify-variable which requires the default values present.
        (if evaled?
          @(re-frame/subscribe [::get-item item-variable-meta-path state-path])
          @(re-frame/subscribe [::get-item-no-eval item-variable-meta-path state-path]))
        (if-let [_action-path  @(re-frame/subscribe [::get-action-path p variable-id])]
          (let [action-data    @(re-frame/subscribe [:gen-fhi.db.subs.actions/get-action-data
                                                     {:client-path   p
                                                      :state-path    state-path
                                                      :action-id     variable-id
                                                      :loc           loc}])
                ;; Request metadata
                action-request @(re-frame/subscribe [::get-action
                                                     {:client-path p
                                                      :state-path state-path
                                                      :action-id variable-id
                                                      :loc loc}])]
            (if (contains? action-data :error)
              (throw (get action-data :error))
              ;; IF no error present return the result key
              (assoc
               (get action-data :result)
               :request
               (:evaluation action-request))))
          nil))))))

(defn- expression->post-process-type [expression]
  (let [post-processing-ext-type (first (fp/evaluate "$this.extension.where(url='https://genfhi.com/Extension/post-processing').value" expression))]
    post-processing-ext-type))

(defn- query-result->json [expression value]
  (match (:language expression)
    (:or
     "application/x-fhir-query"
     "genfhi/post-json-x-fhir-query-process") (js->clj (.parse js/JSON value) :keywordize-keys true)
    :else                                     value))

(defn- process-expression-json
  "derives values from expressions off of extensions"
  [value opts ctx]
  (cond
    (sequential? value)
    (into [] (map #(process-expression-json % opts ctx) value))
    (map? value)
    (let [extension-locs
          (fp/locations
           "$this.descendants().extension.where(url=%extUrl)"
           value
           {:options/variables {:extUrl "https://genfhi.com/Expression/derived"}})]
      (reduce
       (fn [value extension-loc]
         (let [expression (get-in value (concat extension-loc [:valueExpression]))
               result     (:evaluation (evaluate-expression-unsafe expression opts ctx))
               ;; Pop off extension and index
               root-path (pop (pop extension-loc))]
           ;; Need to handle case where _field for primitives...
           (if (string/starts-with? (name (last root-path))  "_")
             (->
              ;; (update-in value (pop root-path) (fn [v] (dissoc v (last root-path))))
              (assoc-in
               value
               (concat (pop root-path) [(keyword (subs
                                                  (name (last root-path))
                                                  1))])
               result))
             (assoc-in value root-path result))))
       value
       extension-locs))))

(defn- ->post-process
  ([expression value]
   (let [post-process-type (expression->post-process-type expression)]
     (match post-process-type
       "json"    (query-result->json expression value)
       "string"  value
       "integer" (js/parseInt value)
       "decimal" (js/parseFloat value)
       "boolean" (= "true" value)
       :else     value))))

(defn- eval->string [fp-eval]
  (string/join
   ","
   (map
    (fn [val]
      (cond
        (string? val) val
        :else (.stringify js/JSON (clj->js val))))
    fp-eval)))

(defn- processing-type->eval-string [type]
  (match type
    (:or "javascript" "json") (fn [evaluation]
                                (let [result (.stringify js/JSON (clj->js evaluation))]
                                  result))
    :else                      eval->string))

(defrecord EvaluateExpressionOpts [p state-path loc variables])

(defn- is-expression? [value]
  (and (contains? value :language)
       (contains? value :expression)))

(defn- throw-circular-loc-error [loc]
  (throw (ex-info
          (str "Failed Circular\n" (fp-loc/loc->str loc))
          {:loc loc})))

(defn- item-path+state-path+visited->fp-options
  ([{:keys [p state-path loc variables!] :as opts}]
   {:pre [(some? loc)]}
   (let [root-meta-path  (path/path->root p)
         root-state-path (path/path->root state-path)]
     {:fp-node/loc       loc
      :fp-node/get-value (fn [loc ctx field]
                           (let [value (get ctx
                                            (keyword "property" (name field))
                                            (get ctx field))]
                             (if (is-expression? value)
                               (if (fp-loc/circular? loc)
                                 (throw-circular-loc-error loc)
                                 (:evaluation
                                  (evaluate-expression-unsafe
                                   value
                                   (assoc opts :loc loc))))
                               value)))
      :options/variables
      (fn [loc id]
        (if (fp-loc/circular? loc)
          (throw-circular-loc-error loc)
          (let [var   @(re-frame/subscribe
                        [::get-variable
                         {:p          (path/descend root-meta-path "manifest")
                          :state-path (path/descend root-state-path "states")
                          :variable-id id
                          :loc         loc}])]

            (swap! variables! (fn [vars] (conj vars id)))
            var)))})))

(defn- evaluate-expression-unsafe
  ([expression opts]
   (evaluate-expression-unsafe expression opts nil))
  ([expression
    {:keys [loc variables!] :or {variables! (atom #{})
                                 loc (fp-loc/->loc)} :as opts}
    ctx]
   (let [fp-options (item-path+state-path+visited->fp-options (assoc opts
                                                                     :variables! variables!
                                                                     :loc loc))]
     (when expression
       (let [post-process-type (expression->post-process-type expression)
             language          (:language expression)
             evaluation
             (match language
              ;; Because of lazy evaluation need to force eval so try catch can work.
              ;; Otherwise execution may happen elsewhere...
               "text/fhirpath"            (into '() (fp/evaluate
                                                     (:expression expression)
                                                     ctx
                                                     fp-options))
               "genfhi/post-json-x-fhir-query-process"
               (-> (query-result->json expression (:expression expression))
                   (process-expression-json opts ctx))
               "application/x-fhir-query" (x-fhir-query/execute
                                           (:expression   expression)
                                           {:eval->string (processing-type->eval-string post-process-type)
                                            :fp-options   (assoc
                                                           fp-options
                                                           :options/allow-singular?
                                                           (or (= "json"       post-process-type)
                                                               (= "javascript" post-process-type)))})
               :else (throw (js/Error. (str "Invalid language " language))))]
         {:variables @variables!
          :evaluation (->post-process expression evaluation)})))))

(defn- evaluate-expression [expression opts]
  (try
    (evaluate-expression-unsafe expression opts)
    (catch :default e
      (.error js/console e)
      nil)))

(defn- item->derive-exp-locs* [cur-loc val]
  (cond
    (is-expression? val)       [cur-loc]
    ;; Columns will be evaluated in the context of a row...
    (= (last cur-loc) :column) []
    (map? val)                 (mapcat
                                (fn [[key val]]
                                  (item->derive-exp-locs*
                                   (conj cur-loc key)
                                   val))
                                val)
    (sequential? val)          (mapcat
                                identity
                                (map-indexed
                                 (fn [i val]
                                   (item->derive-exp-locs*
                                    (conj cur-loc i)
                                    val))
                                 val))
    :else                      []))

(defn- item->derive-exp-locs [item]
  (item->derive-exp-locs* [] item))

(re-frame/reg-sub
 ::get-remote-loading-states
 (fn [[_ state-path]]
   (let [{:keys [resource-type id]} (path/path-meta state-path)]
     (re-frame/subscribe [:gen-fhi.db.subs.fhir/get-fhir-resource resource-type id])))
 (fn [state-resource]
   (get state-resource :loading #{})))

(defn- properties-vec->map [properties]
  (reduce
   (fn [value property]
     (assoc
      value
      (keyword "property" (get property :name))
      ; If nested properties than convert nested properties to map.
      (if (seq (get property :property))
        (properties-vec->map (get property :property))
        (first (fp/evaluate "$this.value" property)))))
   {}
   properties))

(defn- assoc-properties-as-keys [value]
  (-> value
      (merge (properties-vec->map (get value :property [])))
      (dissoc :property)))

(defn- derive-properties [{:keys [p state-path value]}]
  (let [loading-remotes     @(re-frame/subscribe [::get-remote-loading-states state-path])
        opts                {:p             p
                             :state-path    state-path}

        value               (assoc-properties-as-keys value)
        ;; Hash map for what expressions are loading.
        expression-locs     (item->derive-exp-locs value)
        evaled-value         (transduce
                              (comp
                               (map
                                (fn [expression-loc]
                                  (let [expression (get-in value expression-loc)
                                        evaluation (evaluate-expression expression opts)]
                                    [expression-loc evaluation]))))
                              (fn
                                ([value] value)
                                ([value [expression-loc expression-eval]]
                                 (-> value
                                     (assoc-in expression-loc (:evaluation expression-eval))
                                     ;; Set if the expression is loading
                                     (assoc-in (concat [:loading] expression-loc)
                                               (some? (seq (clojure.set/intersection
                                                            (:variables expression-eval)
                                                            loading-remotes)))))))
                              value
                              expression-locs)]
    evaled-value))

(defn- evaluate-action-unsafe [action opts]
  (let [location-evaled (:evaluation
                         (evaluate-expression-unsafe
                          (get action :location)
                          opts))

        body-evaled     (:evaluation
                         (evaluate-expression-unsafe
                          (get action :body)
                          opts))
        is-disabled     (:evaluation
                         (evaluate-expression-unsafe
                          (get action :isDisabled)
                          opts))]
    (assoc action
           :location      location-evaled
           :body          body-evaled
           :isDisabled    is-disabled)))

(re-frame/reg-sub-raw
 ::evaluate-expression
 (fn [_ [_ {:keys [expression p state-path ctx]}]]
   (r/reaction
    (let [opts {:p p :state-path state-path}]
      (try
        {:evaluation (:evaluation (evaluate-expression-unsafe expression opts ctx))}
        (catch :default e
          {:error e}))))))

(re-frame/reg-sub-raw
 ::get-action
 (fn [_app-db [_ {:keys [client-path state-path action-id loc]}]]
   (assert (some? loc) "get-action NO LOC")
   (r/reaction
    (try
      (assert (some? loc) "get-action NO LOC")
      ;; Because an action can be deleted and still registered (in the all register action component)
      ;; Check to see if a path exists first before calling get-value
      (when-let [action-path @(re-frame/subscribe [::get-action-path client-path action-id])]
        (let [action      @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value action-path])
              opts        {:p          client-path
                           :state-path state-path
                           :loc        loc}
              result (evaluate-action-unsafe action opts)]
          {:evaluation result}))
      (catch :default e
        (.error js/console e)
        {:error e})))))

(re-frame/reg-sub
 ::expression-variables
 (fn [_app-db [_ expression]]
   (try
     (match (get expression :language)
       "text/fhirpath"            (fp/->variables (get expression :expression))
       "application/x-fhir-query" (x-fhir-query/->variables (get expression :expression)))
     (catch js/Error _e
       #{}))))

(re-frame/reg-sub
 ::get-temp-item-states
 (fn []
   (re-frame/subscribe [:gen-fhi.db.subs.ui/get-ui]))
 (fn [ui [_]]
   (get ui :temp-item-states {})))

(re-frame/reg-sub
 ::get-temp-item-state
 (fn []
   (re-frame/subscribe [::get-temp-item-states]))
 (fn [temp-item-states [_ item-id]]
   (get temp-item-states item-id)))

;; For recursive expression resolve to the items value that is not revaluated
;; (would enlessly loop in that case). An example where this is used is in pagination
;; With a list referencing a query and the query referencing back to the list for the
;; current offset.
(re-frame/reg-sub-raw
 ::get-item-no-eval
 (fn [_app-db [_ item-meta-path states-path]]
   (r/reaction
    (let [item            @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value item-meta-path])
          item-temp-state (select-keys @(re-frame/subscribe [::get-temp-item-state (:id item)]) [:x :y :height :width])
          item-state      @(re-frame/subscribe [::get-item-state states-path (:id item)])
          item-state      (into
                           {}
                           (map
                            (fn [[key val]]
                              [key
                               ;; State vals are maps with :value and :initial keys.
                               ;; Choose which one is available.
                               (cond
                                 (map? val)
                                 (or (get val :value)
                                     (get val :initial))
                                 ;; ID on state is a raw value not sure how best to handle this?
                                 (string? val) val)])
                            item-state))]
      ;; In order where item-temp-state takes precedence then item-state than raw-item
      (merge
       (assoc-properties-as-keys item)
       item-state
       item-temp-state)))))

(re-frame/reg-sub-raw
 ::get-item
 (fn [_app-db [_ item-meta-path states-path]]
   (r/reaction
    (let [item                  @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value item-meta-path])
          ;; Temporary UI states (for example when item moving and collisions).
          item-temp-state       (select-keys @(re-frame/subscribe [::get-temp-item-state (:id item)]) [:x :y :height :width])
          item-state            @(re-frame/subscribe [::get-item-state states-path (:id item)])
          default-values        (derive-properties
                                 {:p         item-meta-path
                                  :state-path states-path
                                  :value      item})]

      ;; Reduce the state to see if the initial value matches expression if it doesn't use the expression
      ;; Else use the state.
      (-> (reduce
           (fn [default-values state-key]
             ;; Because derived property could be an empty string check it here.
             (if (match [(get-in item-state [state-key :initial])
                         (get default-values state-key)]
                   [nil ""] true
                   ["" nil] true
                   [a b]    (= a b))
               (assoc default-values state-key (get-in item-state [state-key :value]))
               default-values))
           default-values
           (keys item-state))

          ;; Default values used in mutate.cljs to say which value came from expression
          ;; (if expresison changes we want to use the expression as opposed to state).
          (assoc :default-values default-values)
          ;; Merge in the temp states such as collision movements and resizes etc... which
          ;; haven't been placed yet.
          (merge item-temp-state))))))
