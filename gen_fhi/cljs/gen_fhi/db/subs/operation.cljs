(ns gen-fhi.db.subs.operation
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]
            [gen-fhi.operations.parameter-parsing.parameters-map
             :as param-parsing]))

(re-frame/reg-sub
 ::get-cached-queries
 (fn [app-db [_]]
   (get-in app-db [:cached-queries])))

(re-frame/reg-sub
 ::get-cached-query-local
 (fn [[_ _request]]
   (re-frame/subscribe [::get-cached-queries]))
 (fn [cached-queries [_ request]]
   (get cached-queries request)))

(re-frame/reg-sub-raw
 ::get-cached-query
 (fn [_app-db [_ request]]
   (r/reaction
    (let [cached-data @(re-frame/subscribe [::get-cached-query-local request])]
      (if (some? cached-data)
        cached-data
        (re-frame/dispatch
         [:gen-fhi.db.events.operation/trigger-cached-query
          request]))))))

(re-frame/reg-sub-raw
 ::get-operation-response
 (fn [_app-db [_ operation parameters]]
   (r/reaction
    (let [environment @(re-frame/subscribe [:gen-fhi.db.subs.environment/environment])
          request     {:fhir/request-type :fhir/invoke
                       :fhir/level        :fhir/system
                       :fhir/op           operation
                       :fhir/data         parameters
                       :meta/environment  environment}
          cached-data @(re-frame/subscribe [::get-cached-query-local request])]
      (if (some? cached-data)
        (let [operation-response (:fhir/response cached-data)]
          (param-parsing/parameters->map
           operation-response))
        (re-frame/dispatch
         [:gen-fhi.db.events.operation/trigger-cached-query
          request]))))))
