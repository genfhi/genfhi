(ns gen-fhi.db.subs.patches
  (:require
   [gen-fhi.db.constants :as constants]
   [re-frame.core :as re-frame]))

(defn db->pending-queue [db]
  (or (vals (get db constants/pending-queue {})) []))

(defn db->inflight-queue [db]
  (get db constants/inflight-queu []))

(defn db->response-queue [db]
  (get db constants/response-queu []))

(defn ->resource-patches
  "Ordering matters should be response,inflight then pending for concatenation order."
  [resource-type id & queues]
  (map
   :request
   (mapcat
    (fn [queue]
      (filter
       #(and (= (get-in % [:request :fhir/type]) resource-type)
             (= (get-in % [:request :fhir/id]) id))
       queue))
    queues)))

(re-frame/reg-sub
 ::get-pending-queue
 (fn [db]
   (db->pending-queue db)))

(re-frame/reg-sub
 ::get-inflight-queue
 (fn [db]
   (db->inflight-queue db)))

(re-frame/reg-sub
 ::get-response-queue
 (fn [db]
   (db->response-queue db)))

(re-frame/reg-sub
 ::is-syncing
 (fn [_]
   [(re-frame/subscribe [::get-pending-queue])
    (re-frame/subscribe [::get-inflight-queue])
    (re-frame/subscribe [::get-response-queue])])
 (fn [[pending in-flight response]]
   (< 0 (+ (count pending) (count in-flight) (count response)))))

(re-frame/reg-sub
 ::get-resource-local-patches-to-apply
 (fn [_]
   [(re-frame/subscribe [::get-pending-queue])
    (re-frame/subscribe [::get-inflight-queue])
    (re-frame/subscribe [::get-response-queue])])
 (fn [[pending-queue inflight-queue response-queue] [_ resource-type id]]
   (->resource-patches
    resource-type
    id
    response-queue
    inflight-queue
    pending-queue)))

