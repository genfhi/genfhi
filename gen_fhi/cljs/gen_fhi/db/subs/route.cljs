(ns gen-fhi.db.subs.route
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::get-route
 (fn [db]
   (get db :route {})))

(re-frame/reg-sub
 ::get-page
 (fn []
   (re-frame/subscribe [::get-route]))
 (fn [route]
   (get route :page)))

(re-frame/reg-sub
 ::get-route-id
 (fn []
   (re-frame/subscribe [::get-route]))
 (fn [route]
   (get route :id)))

(re-frame/reg-sub
 ::get-parameters
 (fn []
   (re-frame/subscribe [::get-route]))
 (fn [route]
   (get route :params)))

(re-frame/reg-sub
 ::get-query-parameters
 (fn []
   (re-frame/subscribe [::get-route]))
 (fn [route]
   (get route :query-params)))

(re-frame/reg-sub
 ::get-parameter
 (fn []
   (re-frame/subscribe [::get-parameters]))
 (fn [parameters [_ parameter-key]]
   (get parameters parameter-key)))
