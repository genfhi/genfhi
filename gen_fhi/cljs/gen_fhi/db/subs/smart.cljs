(ns gen-fhi.db.subs.smart
  (:require [re-frame.core :as re-frame]

            [gen-fhi.patch-building.path :as path]))

(defrecord SmartContext [clientId encounter patient user])

(re-frame/reg-sub
 ::smart-infos
 (fn [db [_]]
   (get db :smart)))

;; Used in variable %endpoint so filtering on
;; the endpoints used in the app (passed in p parameter).
(re-frame/reg-sub
 ::get-all-endpoint-smart-context
 (fn [[_ p]]
   (let [{:keys [resource-type id]} (path/path-meta p)
         root-path (path/->path resource-type id)]
     [(re-frame/subscribe [::smart-infos])
      (re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/descend
                                                             root-path
                                                             "action")])]))

 (fn [[smart-infos app-actions] [_]]
   (let [endpoints-used (set (->> app-actions
                                  (map #(get-in % [:endpoint :reference]))
                                  (filter some?)))]

     (into
      {}
      (->> (get smart-infos "Endpoint")
           (filter (fn [[key _smart-info]]
                     (endpoints-used
                      (str "Endpoint/" key))))
           (map
            (fn [[key smart-info]]
              [(keyword key)
               (assoc
                (get smart-info :context)
                :endpoint
                (get smart-info :endpoint))])))))))

(re-frame/reg-sub
 ::smart-info
 (fn []
   (re-frame/subscribe [::smart-infos]))
 (fn [smarts [_ resource-type id]]
   (get-in smarts [resource-type id])))

(re-frame/reg-sub
 ::path->smart-info
 (fn [[_ p]]
   (let [{:keys [resource-type id]} (path/path-meta p)]
     (re-frame/subscribe [::smart-info resource-type id])))
 (fn [smart-info [_ _path]]
   smart-info))

(re-frame/reg-sub
 ::smart-client
 (fn [[_ client-path]]
   (let [{:keys [resource-type id]} (path/path-meta client-path)]
     [(re-frame/subscribe [::smart-info resource-type id])
      (re-frame/subscribe [::smart-loading? client-path])]))
 (fn [[smart-info _is-loading?] [_ _client-path]]
   (let [smart-client (get smart-info :client)]
     smart-client)))

(re-frame/reg-sub
 ::smart-context
 (fn [[_ client-path]]
   (let [{:keys [resource-type id]} (path/path-meta client-path)]
     [(re-frame/subscribe [::smart-info resource-type id])]))
 (fn [[smart-info] [_ _client-path]]
   (let [smart-info (get smart-info :context)]
     smart-info)))

(re-frame/reg-sub
 ::smart-loading?
 (fn [[_ client-path]]
   (let [{:keys [resource-type id]} (path/path-meta client-path)]
     (re-frame/subscribe [::smart-info resource-type id])))
 (fn [smart-info [_ _ _]]
   (get smart-info :loading? true)))

;; Old SMART impl used the clientapplication smart key as opposed to endpoint.
(re-frame/reg-sub
 ::is-legacy-smart?
 (fn [[_ client-path]]
   (let [{:keys [resource-type id]} (path/path-meta client-path)]
     (re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path resource-type id "smart")])))
 (fn [smart-config _]
   (some? (get smart-config :clientId))))
