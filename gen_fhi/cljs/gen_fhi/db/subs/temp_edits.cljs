(ns gen-fhi.db.subs.temp-edits
  (:require [re-frame.core :as re-frame]
            [gen-fhi.patch-building.path :as path]
            [clojure.string :as string]
            [gen-fhi.db.constants :as c]
            [gen-fhi.db.events.mutate :as mutate]
            [clj-json-patch.core :as patch]))

(re-frame/reg-sub
 ::get-all-temp-edits
 (fn [db [_]]
   (get db c/temp-queue)))

(re-frame/reg-sub
 ::get-resource-temp-edit
 (fn [[_ path]]
   (let [{:keys [resource-type id]} (path/path-meta path)]
     [(re-frame/subscribe [:gen-fhi.db.subs.fhir/get-fhir-resource-no-temp resource-type id])
      (re-frame/subscribe [::get-all-temp-edits])]))
 (fn [[resource temp-edits] [_ path]]
   (let [{:keys [resource-type id]} (path/path-meta path)
         mutations (get temp-edits (str resource-type "/" id))

         patches (get
                  (reduce
                   (fn [{:keys [resource patches]} mutation]
                     (let [new-patches (mutate/mutation->json-patches resource mutation)]
                       {:resource (patch/patch resource patches true)
                        :patches (into
                                  []
                                  (concat
                                   patches
                                   new-patches))}))
                   {:resource resource :patches []}
                   mutations)
                  :patches
                  [])]
     [{:internal/id       (rand-int 100000000)
       :fhir/request-type :fhir/patch
       :fhir/level        :fhir/instance
       :fhir/type         resource-type
       :fhir/id           id
       :fhir/data patches}])))

(re-frame/reg-sub
 ::get-temp-edit
 (fn [[_ path]]
   (let [{:keys [resource-type id]} (path/path-meta path)]
     [(re-frame/subscribe [:gen-fhi.db.subs.fhir/get-fhir-resource-no-temp resource-type id])
      (re-frame/subscribe [::get-resource-temp-edit path])]))
 (fn [[resource resource-edits] [_ path]]
   (when-let [patches (apply concat (map :fhir/data resource-edits))]
     (let [json-pointer  (path/to-jsonpointer path resource)]
       (filter
        (fn [patch]
          (string/starts-with?
           (:path patch)
           json-pointer))
        patches)))))

(re-frame/reg-sub
 ::get-applied-edit
 (fn [[_ path]]
   [(re-frame/subscribe [:gen-fhi.db.subs.value/get-value path {:apply-temps? true}])])
 (fn [[value] _path]
   value))

(re-frame/reg-sub
 ::any-temp-edits?
 (fn [[_ _path]]
   (re-frame/subscribe [::get-all-temp-edits]))
 (fn [temp-edits _]
   (some? (not-empty (apply concat (vals temp-edits))))))

(re-frame/reg-sub
 ::temp-edits?
 (fn [[_ path]]
   (re-frame/subscribe [::get-temp-edit path]))
 (fn [temp-edits _]
   (not-empty temp-edits)))
