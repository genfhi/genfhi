(ns gen-fhi.db.subs.ui
  (:require [goog.object :as gobj]
            [re-frame.core :as re-frame]

            [gen-fhi.workspace.suuid :refer [uuid->suuid]]
            [gen-fhi.db.constants :as c]))

(re-frame/reg-sub
 ::get-workspaces
 (fn [_db [_ user]]
   (let [workspaces (or (js->clj (gobj/get user c/workspace-claim))
                        [])]
     (map
      uuid->suuid
      workspaces))))

(re-frame/reg-sub
 ::get-ui
 (fn [db]
   (get db :ui {})))

(re-frame/reg-sub
 ::get-dragging-coordinates
 (fn []
   (re-frame/subscribe [::get-ui]))
 (fn [ui [_]]
   (get ui :dragging-coordinates)))

(re-frame/reg-sub
 ::get-ui-component
 (fn []
   (re-frame/subscribe [::get-ui]))
 (fn [ui [_]]
   (get ui :component)))

(re-frame/reg-sub
 ::get-component-ui-property
 (fn []
   (re-frame/subscribe [::get-ui-component]))
 (fn [component-ui [_ property]]
   (get component-ui property)))

(re-frame/reg-sub
 ::get-display-bar
 (fn []
   (re-frame/subscribe [::get-ui]))
 (fn [ui [_ direction]]
   (get-in ui [:bar direction] false)))

(re-frame/reg-sub
 ::get-dragging-mutation
 (fn []
   (re-frame/subscribe [::get-ui]))
 (fn [ui [_]]
   (get ui :dragging-mutation)))

(re-frame/reg-sub
 ::get-selected-item-path
 (fn []
   (re-frame/subscribe [::get-ui]))
 (fn [ui [_]]
   (get ui :selected-item)))

(re-frame/reg-sub
 ::active-modal
 (fn []
   (re-frame/subscribe [::get-ui]))
 (fn [ui [_]]
   (get ui :active-modal)))
