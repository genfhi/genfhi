(ns gen-fhi.db.subs.value
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]

            [gen-fhi.patch-building.path :as path]))

(defn parse-int [field]
  (let [int-field (js/parseInt field)]
    (when (not (js/isNaN int-field))
      int-field)))

(defn- default-get-root [path {:keys [apply-temps? fetch-remote?]}]
  (let [{:keys [resource-type id]} (path/path-meta path)
        resource                   @(re-frame/subscribe [:gen-fhi.db.subs.fhir/get-fhir-resource resource-type id {:apply-temps? apply-temps?}])]
    (if (some? resource)
      resource
      (when fetch-remote?
        (let [request {:fhir/level        :fhir/instance
                       :fhir/request-type :fhir/read
                       :fhir/type         resource-type
                       :fhir/id           id
                           ;; Always when fetching to fhir-store use the base.
                       :meta/environment  nil}]
          (re-frame/dispatch [:gen-fhi.db.events.fhir/fhir-request
                              {:on-success [:gen-fhi.db.events.fhir/set-fhir-resource]
                               :request request}]))))))

;; Given a path represented using [gen-fhi.patch-building.path] will attempt 
;; to pull from local cache and subscribe to field. If not in local cache will request resource
;; using path meta data <ResourceType|id*fields>. 

(re-frame/reg-sub-raw
 ::get-value
 (fn
   [_app-db [_ path {:as opts
                     :keys [apply-temps?
                            fetch-remote?]
                     :or {apply-temps?   false
                          fetch-remote?  true}}]]
   (r/reaction         ;; wrap the computation in a reaction
    ;; Sanity checking that the path is not nil.
    (when (nil? path)
      (.error js/console "received nil path")
      (.trace js/console))

    (let [{:keys [parent field]} (path/ascend path)]
      (if (some? parent)
        (let [parent-value @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value parent opts])]
          (if (nil? parent-value)
            nil
            (let [field     (path/field->resolved-field field parent-value)]
              (if (number? field)
                (if (< field (count parent-value))
                  (nth parent-value field)
                  nil)
                (get parent-value field)))))

        (default-get-root path {:apply-temps? apply-temps?
                                :fetch-remote? fetch-remote?}))))))

