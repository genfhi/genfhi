(ns gen-fhi.db.util
  (:require [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [re-frame.core :as re-frame]
            [re-frame.db :as app-db]
            [reagent.core :as r]

            ["antd" :refer [notification]]
            ["@ant-design/icons" :refer [ExclamationCircleOutlined]]

            [gen-fhi.workspace.utilities.alerts :as alerts]
            [gen-fhi.patch-building.path :as path]))

(defn variable-setup [item-path states-path]
  (str "
var statePath = '" states-path "';
var itemPath = '" item-path "';"))

(defn custom-code-header [origin item-path states-path]
  (str (variable-setup item-path states-path) "
function modifyState(variable, key, value) {
  window.parent.postMessage({type: 'modify-variable',
                             'item-path': itemPath,
                             'state-path': statePath,
                             variable: variable,
                             key: \"property/\" +  key,
                             value: value}, '" origin "');
};
function triggerAction(variable) {
  window.parent.postMessage({type: 'trigger-action',
                             'item-path': itemPath,
                             'state-path': statePath,
                             variable: variable}, '" origin "');
};
function setActiveItem(variable) {
  window.parent.postMessage({type: 'set-selected-item-path',
                             'item-path': itemPath}, '" origin "');
};
"))

(defn react-component-fns [item-id origin states-path]
  (let [props-states-path (path/descend states-path "props")]
    (str "
function modifyProps(value) {
  window.parent.postMessage({type: 'modify-variable',
                             'item-path': itemPath,
                             'state-path': '" states-path "',
                             variable: '" item-id "',
                             key: \"property/props\",
                             value: value}, '" origin "')};
")))

(defn react-component-connection [item-id origin states-path]
  (str
   "
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { props: props, hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { error: error.toString(), hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
     console.log(error);
      window.parent.postMessage({type: 'modify-variable',
                             'item-path': itemPath,
                             'state-path': '" states-path "',
                             variable: '" item-id "',
                             key: \"error\",
                             value: error}, '" origin "');
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <span style={{color: 'red'}}>{this.state.error}</span>;
    }

    return this.props.children;
  }
}

function Connect(Component){
   return class extends React.Component {
      constructor(props) {
        super(props);
        this.state = {props: {}};
      }
      componentDidMount() {
        this.callback = (event) => { this.setState({props: event.data})};
        window.addEventListener(\"message\", this.callback);
      }
      componentWillUnmount() {
        window.removeEventListener(\"message\", this.callback)
       }
      render(){
         return <ErrorBoundary><Component props={this.state.props}/></ErrorBoundary>
       }
   }

}
"))

(defn set-action-response [db {:keys [client-id action response]}]
  (assoc-in
   db
   [:action-response client-id (keyword (get action :id))]
   {:query          action
    :response       response}))

(defn dispatch
  "Event can be non-existant or be either a singular event or a sequence of events."
  [event]
  (when event
    (if (coll? (first event))
      (doseq [e event]
        (re-frame/dispatch e))
      (re-frame/dispatch event))))

(defn- paramter-string->parameter-map [parameter-string]
  (->>
   (string/split parameter-string "&")
   (filter not-empty)
   (map #(string/split % "="))
   (reduce
    (fn [parameter-map [param-name param-value]]
      (assoc parameter-map (keyword param-name) (or param-value "''")))
    {})))

(defn action->body
  "Can be a string or map depending on post-processing settings."
  [action]
  (cond
    (string? action) (js->clj
                      (.parse
                       js/JSON
                       (get action :body)))
    :else            (get action :body)))

(defn- action->fhir-request [action]
  (let [query             (get action :location)
        [path parameters] (string/split query #"\?")
        paths             (string/split path #"/")
        parameters        (paramter-string->parameter-map parameters)]

    (match (get action :type)
      "operation"        (match (get action :level)
                           "system"
                           {:fhir/request-type :fhir/invoke
                            :fhir/level        :fhir/system
                            :fhir/parameters   parameters
                            :fhir/op           (get-in action [:operation :name])
                            :fhir/data         (action->body action)}
                           "resource"
                           {:fhir/request-type :fhir/invoke
                            :fhir/level        :fhir/type
                            :fhir/type         (first paths)
                            :fhir/parameters   parameters
                            :fhir/op           (get-in action [:operation :name])
                            :fhir/data         (action->body action)})

      "search-system"    {:fhir/request-type :fhir/search
                          :fhir/level        :fhir/system
                          :fhir/parameters   parameters}

      "create"           {:fhir/request-type :fhir/create
                          :fhir/level        :fhir/type
                          :fhir/type         (first paths)
                          :fhir/parameters   parameters
                          :fhir/data         (action->body action)}

      "read"             {:fhir/request-type :fhir/read
                          :fhir/level        :fhir/instance
                          :fhir/type         (first paths)
                          :fhir/id           (second paths)}

      "delete"           {:fhir/request-type :fhir/delete
                          :fhir/level        :fhir/instance
                          :fhir/type         (first paths)
                          :fhir/id           (second paths)
                          :fhir/parameters   parameters}

      "patch"            {:fhir/request-type :fhir/patch
                          :fhir/level        :fhir/instance
                          :fhir/type         (first paths)
                          :fhir/id           (second paths)
                          :fhir/parameters   parameters
                          :fhir/data         (action->body action)}

      "update"           {:fhir/request-type :fhir/update
                          :fhir/level        :fhir/instance
                          :fhir/type         (first paths)
                          :fhir/id           (second paths)
                          :fhir/parameters   parameters
                          :fhir/data         (action->body action)}

      "search-type"      {:fhir/request-type :fhir/search
                          :fhir/level        :fhir/type
                          :fhir/type         (first paths)
                          :fhir/parameters   parameters}
      :else              nil)))

(defn- create-handler-triggers [client-path state-path handlers]
  (let [create-handler (fn [event-type handlers]
                         (->> handlers
                              (filter #(= event-type (:eventType %)))
                              (map    (fn [handler]
                                        [:gen-fhi.db.events.item/trigger-event-handler handler client-path state-path]))))]
    {:on-start  (create-handler "on-start" handlers)
     :on-finish (create-handler "on-finish" handlers)}))

(defn- parameter->fhir-response [parameter]
  (js->clj (.parse js/JSON (:valueString parameter)) :keywordize-keys true))

(defn external-parameter->fhir-response [response]
  (if (= "external-fhir-request" (get response :fhir/op))
    (when-let [response-param (first
                               (filter
                                #(= "response" (:name %))
                                (get-in response [:fhir/response :parameter])))]
      (parameter->fhir-response response-param))
    ;; Remove namespaces so aligned with parsing of external-fhir-request 
    (reduce
     (fn [acc [k v]]
       (assoc acc k v))
     {}
     (map
      (fn [[k v]]
        [(keyword (name k)) v])
      response))))

(defn- on-success-callback [client-app-path action fhir-response]
  (let [{:keys [id]}  (path/path-meta client-app-path)
        fhir-response (external-parameter->fhir-response fhir-response)]

    (cond
       ;; Because smart returns the :fhir/error correctly vs remote
       ;; which returns "error" becasue of json serialiazation.
      (or (= :fhir/error (:response-type fhir-response))
          (= "error" (:response-type fhir-response)))
      (alerts/issue-message
       (:id action)
       (alerts/fhir-response->issue-string
        fhir-response))
      :else
      (do
        (when (= "manual" (:trigger action))
          (alerts/success-message (:id action)))
        {:db
         (-> (reset! app-db/app-db
                     (set-action-response
                      @app-db/app-db
                      {:action action
                       :client-id id
                       :response fhir-response})))}))))

(defn- execute-custom-code [db {:keys [action p state-path on-start on-finish]}]
  (let [{:keys [id]}  (path/path-meta p)]
    (try
      (when (not-empty on-start)
        (dispatch on-start))
      (let [code (get action :body)
            executor (js/Function. (str (custom-code-header
                                         (.. js/window -location -origin)
                                         p
                                         state-path)
                                        code))
            executor-res (js->clj (executor) :keywordize-keys true)]
        (when (not-empty on-finish)
          (dispatch on-finish))
        {:db
         (set-action-response db
                              {:client-id id
                               :action action
                               :response {:level "system"
                                          :response-type "invoke"
                                          :response executor-res}})})
      (catch :default e
        (when (not-empty on-finish)
          (dispatch on-finish))
        (do
          (alerts/issue-message
           (get action :id)
           (str e))
          {:db db})))))

(defn action->dispatch-request [db {:keys [action client-app-path item-state-path]}]
  (let [action-id (keyword (get action :id))
        handlers  (create-handler-triggers
                   client-app-path
                   item-state-path
                   (get action :handler []))]
    (match (get action :actionType "fhir-rest")
      "custom-code" (execute-custom-code
                     db
                     {:action     action
                      :p          client-app-path
                      :state-path item-state-path
                      :on-start   (get handlers :on-start)
                      :on-finish  (get handlers :on-finish)})
      "fhir-rest"
      (let [external-fhir-request (action->fhir-request action)]
        (when external-fhir-request
         {:dispatch
          [:gen-fhi.db.events.fhir/external-fhir-request
           client-app-path
           {:endpoint-ref-string (get-in action [:endpoint :reference])
            :request             external-fhir-request
            :header              (get action :header [])
            :on-start            (concat
                                  [[:gen-fhi.db.events.action/set-loading item-state-path action-id true]]
                                  (get handlers :on-start))
            :on-finish           (concat
                                  [[:gen-fhi.db.events.action/set-loading item-state-path action-id false]]
                                  (get handlers :on-finish))
            :on-success          (fn [response]
                                   (on-success-callback
                                    client-app-path
                                    action
                                    response))}]})))))

(defn alert-operation-outcome [fhir-response]
  (when (= (:fhir/response-type fhir-response) :fhir/error)
    (.open
     notification
     (clj->js
      (reduce
       (fn [notification issue]
         (assoc
          notification
          :description
          (str
           (:description notification)
           " "
           (get-in issue [:details :text])
           " "
           (:diagnostics issue))))
       {:message "Issue"
        :icon (r/as-element [:> ExclamationCircleOutlined {:style {:color "red"}}])
        :description ""}
       (:fhir/issue fhir-response))))))
