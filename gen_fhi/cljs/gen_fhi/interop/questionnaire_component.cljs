(ns gen-fhi.interop.questionnaire-component
  (:require
   [gen-fhi.db.core]
   [reagent.core :as r]

   [gen-fhi.questionnaire.core :refer [Questionnaire]]))

(defn QuestionnaireInterop [{:keys [id questionnaire questionnaireResponse onChange expand]}]
  [:f> Questionnaire
   {:key                    id
    :on-change-event        (fn [questionnaire-response]
                              (when onChange
                                (onChange (clj->js questionnaire-response))))
    :expand                 expand
    :editable?              false
    :questionnaire          (js->clj questionnaire :keywordize-keys true)
    :questionnaire-response (js->clj questionnaireResponse :keywordize-keys true)}])

(def ^:export QuestionnaireReact (r/reactify-component QuestionnaireInterop))
