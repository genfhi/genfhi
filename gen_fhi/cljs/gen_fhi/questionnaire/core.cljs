(ns gen-fhi.questionnaire.core
  (:require [clojure.core.match :refer [match]]
            [re-frame.core :as re-frame]
            [oops.core :as oops]
            [clojure.string :as string]
            [cljs.core.async :as a]

            ["@ant-design/icons" :refer [WarningOutlined]]
            ["antd" :refer [Select Radio Space]]
            ["react" :refer [useRef useEffect]]
            ["react-dnd" :as dnd]

            [gen-fhi.operations.core :as workspace-ops]
            [gen-fhi.fhir-client.protocols :as fhir-client]
            [gen-fhi.workspace.components.terminology :refer [open-code-select code-select code-radio]]
            [gen-fhi.components.base.number :refer [number]]
            [gen-fhi.components.base.date :refer [date]]
            [gen-fhi.db.questionnaire.utilities :as q-utils]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.utilities.mutations :as mutations]
            [gen-fhi.fhirpath.core :as fp]))

(declare questionnaire-item)

(defn q-dropper [q-pointer editable?]
  (dnd/useDrop (clj->js
                {:accept "questionnaire-move-item"
                 :drop (fn [item monitor]
                         (when (oops/ocall monitor "isOver" #js{:shallow true})
                           (match (get item :type)
                             :add (mutations/add
                                   q-pointer
                                   (get item :q-item)
                                   {:sync? true})
                             :move (mutations/move
                                    q-pointer
                                    (get item :pointer)
                                    {:sync? true}))
                           (re-frame/dispatch [:gen-fhi.db.questionnaire.events/set-selected-item-by-link-id
                                               {:p       q-pointer
                                                :link-id (get-in item [:q-item :linkId])}])))

                          ;; Set as the current item

                 :collect (fn [monitor]
                            (let [return
                                  {:item          (oops/ocall monitor "getItem")
                                   :canDrop       (oops/ocall monitor "canDrop")
                                   :isOver        (oops/ocall monitor "isOver")
                                   :isOverCurrent (oops/ocall monitor "isOver" #js{:shallow true})}]
                              return))})))

(defn- empty-space-dnd [{:keys [q-pointer editable?]}]
  (let [[drop-props drop] (q-dropper q-pointer editable?)
        inner-or-same-loc? (and (some? (get-in drop-props [:item :pointer]))
                                (string/starts-with? q-pointer (get-in drop-props [:item :pointer])))]
    [:div {:ref   drop}
     (cond
       (:isOverCurrent drop-props)    [:div {:class "ml-2 bg-blue-200 h-[53px] -top-[26px]"}
                                       ;; Used because otherwise will flash so put a buffer in so that the placeholder remains when switched off the absolute.
                                       [:div {:class "absolute bg-transparent h-6 w-full -top-3 "}]]
       (and (:canDrop drop-props)
            (not inner-or-same-loc?)) [:div {:class "absolute bg-transparent h-4 w-full -top-2 "}]
       :else                          nil)]))

(defn- wrap-children [{:keys [on-item-click outcome-pointer q-pointer qr-pointer editable? expand]} main-item]
  (let [item           @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-pointer {:fetch-remote? false}])
        children-items (get item :item)]
    [:<>
     main-item
     (when (not-empty children-items)
       (doall
        (map-indexed
         (fn [i child-item]
           (let [q-pointer  (path/extend-path q-pointer "item" i)
                 qr-pointer (path/extend-path qr-pointer "item")]
             ^{:key (:linkId child-item)}
             [questionnaire-item {:expand          expand
                                  :editable?       editable?
                                  :on-item-click   on-item-click
                                  :outcome-pointer outcome-pointer
                                  :q-pointer       q-pointer
                                  :qr-pointer      qr-pointer}]))
         children-items)))
     ;; May need to remove this conditional down the road.
     [:div {:class "relative"}
      (when (and editable?
                 (or (= "group" (:type item))
                     (some? (:resourceType item))))
        [:f> empty-space-dnd {:editable? editable?
                              :q-pointer (path/extend-path q-pointer "item" (count children-items))}])]]))

(defn- item->item-control [item]
  (first (fp/evaluate
          "$this.extension.where(url=%url).valueCodeableConcept.coding.code"
          item
          {:options/variables
           {:url "http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl"}})))

(defn- choice-control [{:keys [type item-control value read-only? value-set on-change expand]}]
  (match [item-control type]
    ["radio-button" _] [code-radio
                        {:value-set      value-set
                         :expand         expand
                         :raw-code?      true
                         :value          (get value :code)
                         :select-props   {:allowClear true
                                          :style {:width "100%"}
                                          :size  "small"
                                          :disabled read-only?}
                         :on-change      (fn [coding]
                                           (on-change coding))}]
    [_ "open-choice"]  [open-code-select
                        {:value-set      value-set
                         :raw-code?      true
                         :expand         expand
                         :value          (get value :code)
                         :select-props   {:allowClear true
                                          :style {:width "100%"}
                                          :size  "small"
                                          :disabled read-only?}
                         :on-change      (fn [coding]
                                           (on-change coding))}
                        [:span "ValueSet must be set for code."]]
    [_ "choice"]       [code-select
                        {:value-set      value-set
                         :raw-code?      true
                         :expand         expand
                         :value          (get value :code)
                         :select-props   {:allowClear true
                                          :style {:width "100%"}
                                          :size  "small"
                                          :disabled read-only?}
                         :on-change      (fn [coding]
                                           (on-change coding))}
                        [:span "ValueSet must be set for code."]]))

(defn answer-option [{:keys [item item-control value on-change]}]
  (let [fhir-type    (q-utils/q-item-type->fhir-primitive (:type item))
        value-field  (keyword (str "value" (string/capitalize fhir-type)))
        value         (if (or (= (:type item) "choice") (= (:type item) "open-choice"))
                        (get value :code)
                        value)
        on-change    (fn [v]
                       (let [v (if (or (= (:type item) "choice") (= (:type item) "open-choice"))
                                 (first
                                  (filter
                                   #(= (:code %) v)
                                   (map value-field (get item :answerOption))))
                                 v)]
                         (on-change v)))]
    (match [item-control]
      ["radio-button"]
      [:> (.-Group Radio) {:className "flex flex-1"
                           :value value
                           :on-change (fn [e] (on-change (.. e -target -value)))}
       [:> Space {:className "border p-1 w-full " :direction "vertical"}
        (map
         (fn [answer-option]
           ^{:key (str answer-option)}
           [:> Radio {:value (if (= "coding" fhir-type)
                               (get-in answer-option [value-field :code])
                               (get answer-option value-field))}
            [:span {:id (str "radio-choice-" (get-in answer-option [value-field :code]))}
             (if (= "coding" fhir-type)
               (str (or (get-in answer-option [value-field :display])
                        (get-in answer-option [value-field :code])))
               (str (get answer-option value-field)))]])
         (get item :answerOption))]]
      :else
      [:> Select {:allowClear   true
                  :class        "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded overflow-hidden"
                  :filterOption (fn [input option]
                                  (string/includes?
                                   (.toLowerCase (str (.-children option)))
                                   (.toLowerCase (str input))))
                  :value        value
                  :on-change    on-change}
       (doall
        (map
         (fn [answer-option]
           [:> (.-Option Select) {:key   (str (get answer-option value-field))
                                  :value (if (= "coding" fhir-type)
                                           (get-in answer-option [value-field :code])
                                           (get answer-option value-field))}
            (if (= "coding" fhir-type)
              (str (or (get-in answer-option [value-field :display])
                       (get-in answer-option [value-field :code])))
              (str (get answer-option value-field)))])
         (get item :answerOption)))])))

(defn questionnaire-control [{:keys [item read-only? value on-change expand]}]
  (let [item-control (item->item-control item)]

    (if (get item :answerOption)
      [answer-option {:item         item
                      :value        value
                      :item-control item-control
                      :on-change    on-change}]
      (match (:type item)
        "group"         [:<>]
        "display"       [:<>]
        "date"          [date {:type "date"
                               :isDisabled read-only?
                               :value      value
                               :on-change  (fn [date]
                                             (on-change date))}]

        "dateTime"      [date {:type "datetime"
                               :isDisabled read-only?
                               :value      value
                               :on-change  (fn [date-time]
                                             (on-change date-time))}]
        "time"          [date {:type "time"
                               :isDisabled read-only?
                               :value      value
                               :on-change  (fn [time]
                                             (on-change time))}]

        "boolean"       [:input {:type      "checkbox"
                                 :disabled  read-only?
                                 :checked   value
                                 :on-change (fn [e]
                                              (on-change (oops/oget e "target" "checked")))}]

        "string"        [:input {:className "ant-input w-full h-full"
                                 :disabled  read-only?
                                 :value     value
                                 :on-change (fn [e]
                                              (on-change (oops/oget e "target" "value")))}]

        "text"          [:textarea {:className "ant-input resize-none w-full h-full"
                                    :disabled  read-only?
                                    :value     value
                                    :on-change (fn [e]
                                                 (on-change (oops/oget e "target" "value")))}]

        "url"           [:input {:className "ant-input w-full h-full text-blue-400 underline"
                                 :disabled  read-only?
                                 :value     value
                                 :on-change (fn [e]
                                              (on-change
                                               (oops/oget e "target" "value")))}]

        "integer"       [number {:isDisabled read-only?
                                 :type       "integer"
                                 :value      value
                                 :on-change  (fn [num]
                                               (on-change num))}]

        "decimal"       [number {:isDisabled read-only?
                                 :type       "decimal"
                                 :value      value
                                 :on-change  (fn [num]
                                               (on-change num))}]

        (:or
         "choice"
         "open-choice") [choice-control {:type         (:type item)
                                         :expand       expand
                                         :item-control item-control
                                         :value        value
                                         :read-only?   read-only?
                                         :value-set    (:answerValueSet item)
                                         :on-change    on-change}]

        "attachment"  [:input {:type      "file"
                               :disabled  read-only?
                               :on-change (fn [e]
                                            (let [file-reader (js/FileReader.)]
                                              (oops/oset!
                                               file-reader :onload
                                               (fn [e]
                                                 (on-change
                                                  {:data   (oops/oget e "target" "result")})))

                                              (oops/ocall
                                               file-reader
                                               "readAsBinaryString"
                                               (first
                                                (oops/oget e "currentTarget" "files")))))}]

        "reference"   [:<>]
        "quantity"    [:<>]

        :else       [:div
                     [:span (str "unknown type:'" type "'")]]))))

(defn draggable-questionnaire-item [{:keys [q-pointer item editable?]} children]
  (let [ref                        (useRef)
        [_collected drag _preview] (dnd/useDrag #js{:type "questionnaire-move-item"
                                                    :canDrag (fn [_monitor] editable?)
                                                    :item {:type :move
                                                           :pointer q-pointer
                                                           :q-item  item}})]
    (drag ref)
    [:div {:ref   ref
           :data-id q-pointer}
     children]))

(defn- questionnaire-item-inner [{:keys [q-pointer
                                         item
                                         editable?
                                         selected-item-p
                                         on-item-click
                                         derived-items
                                         outcome-pointer
                                         issue
                                         expand]}]
  [:div {:class "relative"}
   (when editable?
     [:f> empty-space-dnd {:editable? editable?
                           :q-pointer q-pointer}])
   [:div {:on-click (fn [e]
                      (oops/ocall e "stopPropagation")
                      (when on-item-click
                        (on-item-click q-pointer item)))}
    [:div {:class (str "ml-2 border "
                       (cond
                         (= selected-item-p q-pointer) "rounded border-dashed border-blue-300 "
                         :else                         "border-transparent ")
                       (when (not (:enabled? (first derived-items)))
                         (if editable?
                           "bg-gray-100"
                           "hidden")))}
     [:div {:class (str "flex "
                        (if (= "group" (:type item))
                          "font-semibold"
                          "font-medium"))}
      [:div
       (when-let [prefix (get item :prefix)]
         [:span {:class "text-gray-400 mr-1"}
          prefix])]
      [:div {:class "flex-col flex flex-1"}
       [:div
        [:span (get item :text (get item :linkId))]
        (when (get item :required false)
          [:span {:class "ml-1 text-red-400"} "*"])]
       [:div {:class (str "font-normal border  "
                          (cond (some? issue) " border-red-400 "
                                :else         " border-transparent "))}
        (doall
         (map-indexed
          (fn [i {:keys [meta/qr-pointer meta/answer-pointer] :as derived-item}]
                ;; Because qr-pointer may change when artificial path can't use as key for rendering issues
                ;; losing focus
            ^{:key i} ;;qr-pointer}
            [wrap-children {:expand          expand
                            :outcome-pointer outcome-pointer
                            :on-item-click   on-item-click
                            :q-pointer       q-pointer
                            :qr-pointer      qr-pointer
                            :editable?       editable?}

             [:div {:class "mb-2"}

              (let [fhir-type   (q-utils/q-item-type->fhir-primitive (get item :type))
                    value-field (keyword (str "value" (string/capitalize fhir-type)))]
                [questionnaire-control
                 {:expand       expand
                  :item         item
                  :read-only?   (get derived-item :readOnly)
                  :value        (get derived-item value-field)
                  :on-change    (fn [value]
                                  (mutations/replace
                                   (path/extend-path answer-pointer value-field)
                                   value
                                   {:local? true}))}])]])
          derived-items))
        (when (get item :repeats false)
          [:div {:class "mt-2"}
           [:div
            [:span {:className "font-medium inline-block cursor-pointer hover:text-blue-500 p-1"
                    :on-click
                    (fn [_e]
                      (let [last-qr-pointer (:meta/qr-pointer (last derived-items))
                            {:keys [parent field]} (path/ascend last-qr-pointer)]
                           ;; If map means it's a search query.
                        (if (map? field)
                            ;; Add twice because already displaying the group when QR empty
                            ;; So means two should now be present instead of 1.
                          (do
                            (mutations/add
                             (path/descend parent "-")
                             {:linkId (get item :linkId)}
                             {:local? true})
                            (mutations/add
                             (path/descend parent "-")
                             {:linkId (get item :linkId)}
                             {:local? true}))
                          (mutations/add
                           (path/descend parent "-")
                           {:linkId (get item :linkId)}
                           {:local? true}))))}
             (str "+ " (get item :text (get item :linkId)))]]])
        (when (some? issue)
          [:div
           [:span {:class "text-red-500 flex items-center ml-2"}
            [:> WarningOutlined {:class "mr-1"}]
            (str (:diagnostics issue))]])]]]]]])

(defn- questionnaire-item
  "Seperating out inner portion with root due to rendering issues on React interop."
  [{:keys [q-pointer outcome-pointer qr-pointer editable? on-item-click expand]}]
  (assert (some? q-pointer))
  (assert (some? qr-pointer))

  (let [item                       @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-pointer {:fetch-remote? false}])
        issue                      @(re-frame/subscribe [:gen-fhi.db.questionnaire.subs/pointer->issues
                                                         outcome-pointer
                                                         q-pointer])
        derived-items               @(re-frame/subscribe
                                      [:gen-fhi.db.questionnaire.subs/get-derived-items
                                       q-pointer
                                       qr-pointer])
        selected-item-p            @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-selected-item-path])
        inner                      [questionnaire-item-inner {:selected-item-p selected-item-p
                                                              :q-pointer q-pointer
                                                              :item item
                                                              :editable? editable?
                                                              :on-item-click on-item-click
                                                              :derived-items derived-items
                                                              :outcome-pointer outcome-pointer
                                                              :issue issue
                                                              :expand expand}]]

    (if editable?
      [:f> draggable-questionnaire-item {:item item
                                         :q-pointer q-pointer
                                         :editable? editable?}
       inner]
      inner)))

(defn QuestionnaireSubmit [{:keys [q-pointer qr-pointer on-submit]}]
  (let [client                 @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        questionnaire          @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value q-pointer {:fetch-remote? false}])
        questionnaire-response @(re-frame/subscribe
                                 [:gen-fhi.db.questionnaire.subs/get-derived-questionnaire-response
                                  q-pointer qr-pointer])]
    [:button {:class "bg-blue-500 text-white p-2 rounded"
              :on-click (fn [_e]
                          (a/take!
                           (fhir-client/invoke
                            client
                            workspace-ops/validate
                            {:resource (assoc questionnaire-response :contained [questionnaire])})
                           (fn [{:keys [:fhir/response]}]
                             (let [operation-outcome (:return response)]
                               (on-submit
                                questionnaire-response
                                operation-outcome)))))}
     "Submit"]))

(defn Questionnaire [{:keys [key
                             editable?
                             questionnaire
                             questionnaire-response
                             on-item-click
                             on-change-event
                             on-submit
                             expand
                             operation-outcome]}]

  (let [questionnaire-response
        (if (and (map? questionnaire-response)
                 (= "QuestionnaireResponse" (:resourceType questionnaire-response)))
          questionnaire-response
          {:id            key
           :resourceType  "QuestionnaireResponse"
           :questionnaire (:url questionnaire)
           :status        "in-progress"})
        q-pointer                      (path/->path :Questionnaire key)
        qr-pointer                     (path/->path :QuestionnaireResponse key)
        operation-outcome-pointer      (path/->path :OperationOutcome key)
        local-operation-outcome        @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                                             operation-outcome-pointer
                                                             {:fetch-remote? false}])
        local-questionnaire-response   @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                                             qr-pointer
                                                             {:fetch-remote? false}])
        local-questionnaire             @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                                              q-pointer
                                                              {:fetch-remote? false}])]

    (useEffect
     (fn []
       (when on-change-event
         (re-frame/dispatch
          [:gen-fhi.db.questionnaire.events/register-questionnaire-on-change
           key
           [:gen-fhi.db.questionnaire.subs/get-derived-questionnaire-response q-pointer qr-pointer]
           on-change-event]))
       (fn []
         (re-frame/dispatch [:gen-fhi.db.questionnaire.events/dispose-questionnaire-on-change key])))
     #js[on-change-event])

    (when (and (some? operation-outcome) (not= operation-outcome local-operation-outcome))
      (println "dispatch operation-outcome")
      (re-frame/dispatch-sync [:gen-fhi.db.events.fhir/set-fhir-resource-temp
                               operation-outcome
                               "OperationOutcome"
                               key]))

    (when (not= questionnaire local-questionnaire)
      (println "dispatch questionnaire")
      (re-frame/dispatch-sync [:gen-fhi.db.events.fhir/set-fhir-resource-temp
                               questionnaire
                               "Questionnaire"
                               key])
      ;; Reset the QR as this is a new Questionnaire
      (re-frame/dispatch-sync [:gen-fhi.db.events.fhir/set-fhir-resource-temp
                               (assoc questionnaire-response :questionnaire (:url questionnaire))
                               "QuestionnaireResponse" key]))

    (when (not= (:id questionnaire-response) (:id local-questionnaire-response))
      (println "dispatch questionnaire-response")
      (re-frame/dispatch-sync [:gen-fhi.db.events.fhir/set-fhir-resource-temp
                               (assoc questionnaire-response :questionnaire (:url questionnaire))
                               "QuestionnaireResponse"
                               key]))

    [wrap-children {:on-item-click   on-item-click
                    :q-pointer       q-pointer
                    :qr-pointer      qr-pointer
                    :outcome-pointer operation-outcome-pointer
                    :editable?       editable?
                    :expand          expand}
     (if editable?
       [:input {:value (get questionnaire :title)
                :on-change (fn [e]
                             (mutations/replace
                              (path/descend q-pointer :title)
                              (oops/oget e "target.value")))
                :class "w-full font-semibold text-lg"}]
       [:h1 {:class "font-semibold text-lg"}
        (get questionnaire :title)])]))



