(ns gen-fhi.workspace.client
  (:require
   [cljs-http.client :as http]
   [cljs.core.async.interop :refer-macros [<p!]]
   [cljs.core.async :as a]

   ;[async-errors.core :as ae]
   [gen-fhi.operations.client-injector :refer [op-processing]]
   [gen-fhi.fhir-client.protocols :refer [Client] :as fhir-client]
   [gen-fhi.fhir-client.utils
    :refer [fhir-req->http-req http-res->fhir-res]]
   [gen-fhi.interceptors.core :as it]
   [gen-fhi.workspace.constants.core :refer [?csrf-token]]))

(defn interceptor
  ([api-url get-access-token]
   (interceptor api-url get-access-token {}))
  ([api-url get-access-token {:keys [pre] :or {pre []}}]
   (it/->interceptor
    (concat
     pre
     [{:enter
       (it/alt-value
        (fn [fhir-req]
          [fhir-req (fhir-req->http-req api-url fhir-req)]))}
      {:enter (it/alt-value
               (fn [[fhir-req http-req]]
                 (a/go
                  (let [token    (<p! (get-access-token))
                        http-res (a/<! (http/request
                                        (update
                                         http-req
                                         :headers
                                         (fn [headers]
                                           (merge
                                            headers
                                            {"Authorization" (str "Bearer " token)
                                             "Content-Type" "application/json"
                                             "X-CSRF-Token" ?csrf-token})))))
                        return   (http-res->fhir-res fhir-req http-res)]
                    return))))}]))))

(defn create [interceptor]

  (reify
    Client
    (request     [this request] (interceptor request))
    (metadata    [this]
      (interceptor {:fhir/request-type :fhir/capabilities
                    :fhir/level        :fhir/system}))

    (invoke      [this op parameters]
      (op-processing interceptor
                     {:fhir/request-type :fhir/invoke
                      :fhir/level        :fhir/system
                      :fhir/op           op
                      :fhir/data         parameters}))

    (invoke      [this op type parameters]
      (op-processing interceptor
                     {:fhir/request-type :fhir/invoke
                      :fhir/level        :fhir/type
                      :fhir/op           op
                      :fhir/type         type
                      :fhir/data         parameters}))

    (invoke      [this op type id parameters]
      (op-processing interceptor
                     {:fhir/request-type :fhir/invoke
                      :fhir/level        :fhir/instance
                      :fhir/op           op
                      :fhir/type         type
                      :fhir/id           id
                      :fhir/data         parameters}))

    (search [this parameters]
      (interceptor {:fhir/level        :fhir/system
                    :fhir/request-type :fhir/search
                    :fhir/parameters   parameters}))

    (search [this resource-type parameters]
      (interceptor {:fhir/level        :fhir/type
                    :fhir/request-type :fhir/search
                    :fhir/type         resource-type
                    :fhir/parameters   parameters}))

    (create [this resource]
      (interceptor {:fhir/level        :fhir/type
                    :fhir/request-type :fhir/create
                    :fhir/type         (:resourceType resource)
                    :fhir/data         resource}))

    (batch [this bundle]
      (throw (ex-message "Not Implemented")))

    (transaction [this bundle]
      (interceptor {:fhir/level        :fhir/system
                    :fhir/request-type :fhir/transaction
                    :fhir/data         bundle}))

    (history [this]
      (throw (ex-message "Not Implemented")))

    (history [this type]
      (throw (ex-message "Not Implemented")))

    (history [this resource-type id]
      (throw (ex-message "Not Implemented")))

    (read [this resource-type id]
      (interceptor {:fhir/request-type :fhir/read
                    :fhir/level        :fhir/instance
                    :fhir/type         resource-type
                    :fhir/id           id}))

    (update [this type id resource]
      (fhir-client/update this type id resource {}))

    (update [this type id resource constraints]
      (interceptor {:fhir/request-type :fhir/update
                    :fhir/level        :fhir/instance
                    :fhir/type          type
                    :fhir/id            id
                    :fhir/data         resource}))

    (delete [this type id]
      (interceptor {:fhir/request-type :fhir/delete
                    :fhir/level        :fhir/instance
                    :fhir/type         type
                    :fhir/id           id}))

    (patch [this type id patches]
      (fhir-client/patch this type id patches {}))

    (patch [this type id patches constraints]
      (throw (ex-message "Not Implemented")))

    (vread       [this resource-type id vid]
      (throw (ex-message "Not Implemented")))))
