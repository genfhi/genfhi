(ns gen-fhi.workspace.components.capability-completions
  (:require [clojure.string :as string]
            [re-frame.core :as re-frame]
            [reagent.core :as r]

            ["antd" :refer [AutoComplete Select]]

            [oops.core :as oops]))

(defn- endpoint-parameter [endpoint]
  (if (= "Endpoint" (get endpoint :resourceType))
    {:name "endpointResource"
     :resource endpoint}
    {:name "endpoint"
     :valueReference endpoint}))

(defn resource-autocomplete [{:keys [placeholder className style endpoint value on-change]}]
  (let [parameter              (when endpoint
                                 @(re-frame/subscribe
                                   [:gen-fhi.db.subs.operation/get-operation-response
                                    "capability-completion"
                                    {:resourceType "Parameters"
                                     :parameter
                                     [(endpoint-parameter endpoint)
                                      {:name        "level"
                                       :valueString "system"}
                                      {:name "type"
                                       :valueString "resources"}]}]))
        {:keys [resourceTypes]} parameter]
    [:> AutoComplete
     (merge {:showSearch   true
             :loading     (nil? parameter)
             :style        style
             :className   (str "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded "
                               className)
             :value        value
             :on-change   on-change
             :options      (map
                            (fn [resourceType]
                              {:label resourceType
                               :value resourceType})
                            resourceTypes)
             :filterOption (fn [input option]
                             (string/includes?
                              (.toLowerCase (oops/oget option "value"))
                              (.toLowerCase input)))

             :placeholder (or placeholder "Key")})]))

(defn search-parameter-autocomplete [{:keys [value placeholder style className endpoint level resource-type on-change]}]
  (let [parameter                (when endpoint
                                   @(re-frame/subscribe
                                     [:gen-fhi.db.subs.operation/get-operation-response
                                      "capability-completion"
                                      {:resourceType "Parameters"
                                       :parameter
                                       (concat
                                        [(endpoint-parameter endpoint)
                                         {:name        "level"
                                          :valueString level}
                                         {:name "type"
                                          :valueString "search-parameters"}]
                                        (when (= "resource" level)
                                          [{:name "resourceType"
                                            :valueString resource-type}]))}]))
        {:keys [searchParameters]} parameter]
    [:> AutoComplete
     (merge {:showSearch   true
             :loading      (nil? parameter)
             :style        style
             :className   (str "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded "
                               className)
             :value        value
             :on-change    on-change
             :options      (map
                            (fn [param]
                              {:label (r/as-element [:div
                                                     [:div {:className "flex text-slate-700 items-center"}
                                                      [:div {:style {:font-size "10px"} :className "w-11/12"}
                                                       [:span (:name param)]]
                                                      [:div {:style {:font-size "10px"} :className "flex justify-end text-slate-400"}
                                                       [:span "<" (:type param) ">"]]]
                                                     [:div {:title (get param :documentation "")
                                                            :style {:font-size "8px"}
                                                            :className  "text-slate-400"}
                                                      (get param :documentation "")]])

                               :value (:name param)})
                            searchParameters)
             :filterOption (fn [input option]
                             (string/includes?
                              (.toLowerCase (oops/oget option "value"))
                              (.toLowerCase input)))

             :placeholder (or placeholder "Parameter")})]))

(defn interactions [{:keys [value placeholder style className endpoint level resource-type on-change]}]
  (let [parameter              (when endpoint
                                 @(re-frame/subscribe
                                   [:gen-fhi.db.subs.operation/get-operation-response
                                    "capability-completion"
                                    {:resourceType "Parameters"
                                     :parameter
                                     (concat
                                      [(endpoint-parameter endpoint)
                                       {:name        "level"
                                        :valueString level}
                                       {:name "type"
                                        :valueString "interactions"}]
                                      (when (= level "resource")
                                        [{:name "resourceType"
                                          :valueString resource-type}]))}]))
        {:keys [interactions]} parameter
        interactions           (filter some? (if (sequential? interactions) interactions [interactions]))]
    [:> Select
     (merge {:showSearch   true
             :loading      (nil? parameter)
             :style        style
             :className   (str "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded "
                               className)
             :value        value
             :on-change    on-change
             :options      (map
                            (fn [interaction]
                              {:label interaction

                               :value interaction})
                            interactions)
             :filterOption (fn [input option]
                             (string/includes?
                              (.toLowerCase (oops/oget option "value"))
                              (.toLowerCase input)))

             :placeholder (or placeholder "Interaction")})]))

(defn operation-autocomplete []
  (fn [{:keys [placeholder style className level endpoint resource-type value on-change]}]
    (let [parameter          (when endpoint
                               @(re-frame/subscribe
                                 [:gen-fhi.db.subs.operation/get-operation-response
                                  "capability-completion"
                                  {:resourceType "Parameters"
                                   :parameter
                                   (concat
                                    [(endpoint-parameter endpoint)
                                     {:name "type"
                                      :valueString "operations"}
                                     {:name "level" :valueString level}]

                                    (cond
                                      (= level "system") []
                                      (= level "resource")   [{:name "resourceType"
                                                               :valueString resource-type}]
                                      :else (throw (ex-info (str "Failure incorrect level of '" level "'") {}))))}]))
          {:keys [operations]} parameter]
      [:> AutoComplete
       (merge {:showSearch   true
               :loading      (nil? parameter)
               :style        style
               :className   (str "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded "
                                 className)
               :value        value
               :on-change    (fn [v]
                               (on-change
                                (or (first (filter #(= (:definition %) v) operations))
                                    ;; If non existant in completion just default to a spoof
                                    ;; value for filtering and client overrides.
                                    {:name v
                                     :definition v})))
               :options      (map
                              (fn [operation]
                                {:label
                                 (r/as-element
                                  [:div
                                   [:div {:className "flex text-slate-700 items-center"}
                                    [:div {:style {:font-size "10px"} :className "flex flex-row w-11/12"}
                                     [:div {:class "flex flex-1"}
                                      [:span {:class "font-semibold"} (:name operation)]]
                                     [:div
                                      [:span
                                       (:definition operation)]]]]
                                   [:div {:title (get operation :documentation "")
                                          :style {:font-size "8px"}
                                          :className  "text-slate-400"}
                                    (get operation :documentation "")]])

                                 :value (:definition operation)})
                              operations)
               :filterOption (fn [input option]
                               (string/includes?
                                (.toLowerCase (oops/oget option "value"))
                                (.toLowerCase input)))

               :placeholder (or placeholder "Operation")})])))
