(ns gen-fhi.workspace.components.completion
  (:require [reagent.core :as r]
            [oops.core :as oops]))

(defn- spinner [{:keys [class]}]
  [:svg {:class (str "animate-spin h-4 w-4 text-blue-400 " class) :viewBox "0 0 24 24"}
   [:circle
    {:class "opacity-25"
     :fill   "none"
     :cx "12"
     :cy "12"
     :r "10"
     :stroke "currentColor"
     :stroke-width "4"}]
   [:path
    {:class "opacity-75"
     :fill "currentColor"
     :d "M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"}]]) 

(defn completion [_props]
  (let [ref                (r/atom nil)
        is-focused         (r/atom false)
        is-options-hovered (r/atom false)]
    (fn [{:keys [id input-class options value on-change loading?]}]
      (let [bounds (when @ref (oops/ocall  @ref "getBoundingClientRect"))]
        [:div {:class "relative"}
         [:div {:class "flex"}
          [:input {:id        (str id)
                   :value     value
                   :on-change (fn [e]  (on-change (oops/oget e "target.value")))
                   :on-focus  (fn [_e] (reset! is-focused true))
                   :on-blur   (fn [_e] (reset! is-focused false))
                   :ref       (fn [el]
                                (when (nil? @ref)
                                  (reset! ref el)))
                   :class     (str "flex flex-1 "input-class)}]
          (when loading?
           [:div {:class "flex mr-2 items-center justify-center"}
            [spinner]])]
         (when (and (not loading?) @ref (or @is-focused @is-options-hovered) (not-empty options))
           [:div {:class "bg-white max-h-56 overflow-y-auto border-t-0 border border-slate-400"
                  :style {:position "absolute"
                          :z-index "1"
                          :width (oops/oget bounds "width")
                          :top   (oops/oget bounds "height")}}
                          ;;:left (oops/oget bounds "left")}}
            (map
             (fn [option]
               ^{:key (:value option)}
               [:div {:class          "cursor-pointer p-1 hover:bg-gray-100 "
                      :on-mouse-leave (fn [_e] (reset! is-options-hovered false))
                      :on-mouse-enter (fn [_e] (reset! is-options-hovered true))
                      :on-click       (fn [_e]
                                        (on-change (:value option))
                                        (reset! is-options-hovered false)
                                        (reset! is-focused false))}
                [:div
                 [:span {:class "text-slate-700"}
                  (or (:display option) (:value option))]]
                [:div {:style {:font-size "8px"} :class "text-xs text-slate-500"}
                 [:span
                  (:secondary option)]]]) 
                 
             options)])]))))
