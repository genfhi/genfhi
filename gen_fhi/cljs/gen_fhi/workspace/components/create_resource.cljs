(ns gen-fhi.workspace.components.create-resource
  (:require-macros [gen-fhi.workspace.utilities.inline-load :refer [load-resource]])
  (:require
   [cljs.core.async :as a]
   [accountant.core :as accountant]
   [re-frame.core :as re-frame]
   [reagent.core :as r]
   [clojure.string :as string]
   [oops.core :as oops]

   ["uuid" :as uuid]
   ["antd" :refer [Dropdown Menu]]

   [gen-fhi.workspace.templates.core :as template]
   [gen-fhi.workspace.utilities.alerts :as alerts]
   [gen-fhi.workspace.components.editors.core :refer [raw-text-editor]]
   [gen-fhi.fhir-client.protocols :refer [create]]
   [gen-fhi.components.base.button :refer [button ->button-class]]
   [gen-fhi.workspace.router :as router]
   [gen-fhi.workspace.utilities.naming :as naming]))

(defn resource-create-menu [{:keys [resource-types cb]}]
  [:div {:class "border border-slate-200"}
   [:> Menu
    (map
     (fn [resource-type]
       [:> (.-Item Menu) {:key      resource-type
                          :on-click (fn [] (cb resource-type))}
        resource-type])
     resource-types)]])

(def default-resource
  {"ValueSet"
   (fn []
     (let [generated-identifier (uuid/v4)]
       {:name "New ValueSet"
        :resourceType "ValueSet"
        :status "draft"
        :url (str "https://genfhi.com/ValueSet/" generated-identifier)}))

   "Questionnaire"
   (fn []
     (let [generated-identifier (uuid/v4)]
       {:resourceType "Questionnaire"
        :url          (str "https://genfhi.com/Questionnaire/" generated-identifier)
        :title        "Blank Questionnaire"
        :description  "Blank questionnaire"
        :status       "active"}))
     ;;(load-resource "gen_fhi/workspace/data/questionnaires/example.json"))

   "CodeSystem"
   (fn []
     (let [generated-identifier (uuid/v4)]
       {:resourceType "CodeSystem"
        :name (str "New CodeSystem")
        :title (str "New CodeSystem")
        :url   (str "https://genfhi.com/CodeSystem/" generated-identifier)
        :status "draft"
        :content "complete"
        :concept [{:code "OPERATIONS",
                   :display "Operations",
                   :definition "Healthcare Operations"}
                  {:code "SYSADMIN",
                   :display "Sysadmin",
                   :definition "System Administration"}]}))

   "OperationDefinition"
   (fn []
     {:system true,
      :instance false,
      :name "Nested Parameters Op",
      :description "New Operation Definition testing nested parameters"
      :type false,
      :resourceType "OperationDefinition",
      :extension
      [{:url "https://genfhi.com/OperationDefinition/op-code",
        :valueExpression
        {:language "javascript",
         :expression
         "function(input, onSuccess, onError) {\n    onSuccess(\n     {\n       testing: [\n         {'param-nested': input.parameter[\"param-input\"]},\n         {'param-nested': \"ayod\"}\n       ],\n       \"my-int\": 18\n     });\n}"}}],
      :status "draft",
      :kind "operation",
      :code "nested-param",
      :parameter
      [{:max "*",
        :min 0,
        :use "out",
        :name "testing",
        :part
        [{:max "1",
          :min 0,
          :use "out",
          :name "param-nested",
          :part [],
          :type "string"}]}
       {:max "1", :min 0, :use "out", :name "my-int", :type "integer"}
       {:max "1", :min 0, :use "in", :name "param-input", :type "string"}]})
   "ImplementationGuide"
   (fn []
     {:url "https://genfhi.com/test"
      :name "test"
      :packageId "test"
      :status "active"
      :fhirVersion ["r4"]
      :resourceType "ImplementationGuide"})
   "ClientApplication"
   (fn []
     {:name "New App"
      :version "v6"
      :description "New application"
      :resourceType "ClientApplication"})
   "Endpoint"
   (fn []
     {:name "FHIR API"
      :resourceType "Endpoint"
      :status "active"
      :connectionType {:code "hl7-fhir-rest"}
      :address "http://fhir-api.com"
      :payloadType [{:coding [{:code "urn:ihe:pcc:cm:2008"}]}]
      :header ["Content-Type:application/json"]})})

(defn resource-type->default-resource [resource-type]
  (assert (some? (get default-resource resource-type))
          (str "No default resource for type '" resource-type "'"))
  ((get default-resource resource-type)))

(defn- create-resource
  [client resource params]
  (a/take!
   (create client resource)
   (fn [res]
     (let [{:keys [resourceType id]} (:fhir/response res)]
       (if (some? (:fhir/issue res))
         (alerts/issue-message
          (str
           "Faied to create "
           (naming/resource-type->display-name (get resource :resourceType)))
          (alerts/fhir-response->issue-string
           res))
         (do
           (alerts/success-message (str "Creating a new " (naming/resource-type->display-name resourceType)))
           (accountant/navigate!
            (router/path-for
             :instance
             (merge
              params
              {:modifiers ""
               :resource-type resourceType
               :id             id})))
           (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :left    true])
           (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :bottom  true])
           (re-frame/dispatch [:gen-fhi.db.events.ui/show-component-adder])))))))

(defn create-resource-btn-multiple [{:keys [resource-types]}]
  (let [on-resource-create (fn [resource-type]
                             (re-frame/dispatch
                              [:gen-fhi.db.events.ui/set-active-modal
                               {:type :create-modal
                                :data {:type resource-type}}]))
        dropdown-menu      (r/as-element
                            [resource-create-menu
                             {:resource-types resource-types
                              :cb              on-resource-create}])]
    [:> Dropdown {:placement "bottomRight" :overlay dropdown-menu :trigger ["click"]}
     [:button {:class (->button-class :primary)}
      "Create"]]))

(defn create-resource-btn-single [{:keys [resource-types]}]
  (let [on-resource-create (fn [resource-type]
                             (re-frame/dispatch
                              [:gen-fhi.db.events.ui/set-active-modal
                               {:type :create-modal
                                :data {:type resource-type}}]))]

    [button {:on-click #(on-resource-create (first resource-types))}
     "Create"]))

(defn create-resource-btn [{:keys [resource-types] :as props}]
  (if (< 1 (count resource-types))
    [create-resource-btn-multiple props]
    [create-resource-btn-single props]))

(def create-form-types
  {"Questionnaire"       [{:property :title       :type "text"}
                          {:property :url         :type "text"}
                          {:property :description :type "text"}]
   "ClientApplication"   [{:property :name        :type "text"}
                          {:property :description :type "text"}]
   "Endpoint"            [{:property :name        :type "text"}
                          {:property :address     :type "text"}]
   "ValueSet"            [{:property :name        :type "text"}
                          {:property :url         :type "text"}]
   "CodeSystem"          [{:property :name        :type "text"}
                          {:property :url         :type "text"}]
   "OperationDefinition" [{:property :name        :type "text"}
                          {:property :description :type "text"}
                          {:property :code        :type "text"}]})

(defn- create-resource-blank [{:keys [type]}]
  (let [state            (r/atom {})
        default-resource ((get default-resource type (fn [])))
        c                @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        params           @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])]
    (fn [{:keys [type]}]
      (let [properties (get create-form-types (name type))
            cur-state @state]
        [:div
         (map
          (fn [property]
            (let [value (get cur-state (get property :property))
                  placeholder (get default-resource (get property :property))]
              ^{:key (get property :property)}
              [:div {:class "mb-1"}
               [raw-text-editor
                {:label       (string/capitalize
                               (name (get property :property)))
                 :placeholder placeholder
                 :value       value
                 :on-change   (fn [e]
                                (swap!
                                 state
                                 #(assoc
                                   %
                                   (get property :property)
                                   (oops/oget e "target.value"))))}]]))
          properties)
         [:div {:class "mt-2 flex justify-end"}
          [button {:type :primary
                   :on-click (fn [_e]
                               (create-resource
                                c
                                (merge default-resource @state)
                                params))}
           "Create"]]]))))

(defn- chunk-vector
  "Creates chunked array based on number."
  [vector number]
  (loop [chunks []
         vector (vec vector)]
    (if (empty? vector)
      chunks
      (let [idx (min (count vector) number)]
        (recur
         (conj
          chunks
          (subvec vector 0 idx))
         (subvec vector idx (count vector)))))))

(defn- import-resource [{:keys []}]
  (let [state            (r/atom {})
        c                @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        params           @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])]
    (fn [_props]
      [:div {:class "flex justify-center items-center"}
       [:div {:class "flex flex-1"}
        [:input {:on-change (fn [e]
                              (let [file-reader (js/FileReader.)]
                                (oops/oset! file-reader :onload
                                            (fn [e]
                                              (try
                                                (reset!
                                                 state
                                                 (js->clj
                                                  (.parse js/JSON (.. e -target -result))
                                                  :keywordize-keys true))
                                                (catch :default e
                                                  (.error js/console e)))))
                                (.readAsBinaryString
                                 file-reader
                                 (first (.. e -currentTarget -files)))))
                 :type "file"}]]
       [button {:type :primary
                :on-click (fn [_e]
                            (create-resource
                             c
                             @state
                             params))}
        "Import"]])))

(defn create-resource-form [{:keys [type disable-import?]}]
  (let [c                 @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        type-templates    (get template/templates (keyword type))]
    (fn [{:keys [type]}]
      (let [params           @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])]
        [:div
         [create-resource-blank {:type type}]
         (when (not disable-import?)
           [:div {:class "mt-4 border border-l-0 border-b-0 border-r-0"}
            [:div {:class "mt-4 mb-2"}
             [:span {:class "font-semibold text-base"}
              "Import " (naming/resource-type->display-name type)]]
            [import-resource]])
         (when (not-empty type-templates)
           [:div {:class "mt-4 border border-l-0 border-b-0 border-r-0"}
            [:div {:class "mt-4 mb-2 text-base font-semibold"}
             [:span "Create from templates"]]
            [:div {:class "flex flex-col"}
             (map
              (fn [row]
                [:div {:class "flex flex-1"}
                 (map
                  (fn [template]
                    [:div {:class "cursor-pointer p-2 flex flex-col items-center w-1/3 mr-1 bg-gray-100 hover:bg-blue-50"}
                     [:div {:class "flex flex-col items-center w-full p-2"
                            :on-click (fn [_e]
                                        (template/create c template params))}
                      [:div {:class "text-gray-600 font-semibold mb-1"}
                       (get template :name)]
                      [:div {:class "text-xs font-normal"}
                       [:ul
                        (map
                         (fn [[resource-type count]]
                           [:li
                            [:span {:class "font-semibold underline"}
                             count]
                            [:span
                             (str " " resource-type)]])
                         (template/template->resource-count template))]]]
                     [:div {:class "p-2 flex justify-center items-center"}
                      [:span {:class "text-xs"}
                       (get template :description)]]])
                  row)])
              (chunk-vector
               (concat
                ;;[{:name "TESTING"}]
                type-templates)
               3))]])]))))
