(ns gen-fhi.workspace.components.dnd
  (:require [re-frame.core :as re-frame]
            ["react-beautiful-dnd" :refer [DragDropContext Droppable Draggable]]
            [oops.core :as oops]
            [reagent.core :as r]))

(def query-attr "data-rbd-drag-handle-draggable-id")

(defn- on-drag-update [update]
  (when (oops/oget update "destination")
    (let [draggable-id (oops/oget update "draggableId")
          destination-index (oops/oget update "destination.index")
          dom-query (str "[" query-attr "=\"" draggable-id "\"]")
          dragged-dom (.querySelector js/document dom-query)]
      (when dragged-dom
        (let [client-y (+
                        (-> (.getComputedStyle
                             js/window
                             (oops/oget dragged-dom "parentNode"))
                            (oops/oget "paddingTop")
                            (js/parseFloat))
                        (reduce
                         (fn [total curr]
                           (let [style (or (.-currentStyle curr)
                                           (.getComputedStyle js/window curr))
                                 margin-bottom (js/parseFloat (oops/oget style "marginBottom"))]
                             (+ total (oops/oget curr "clientHeight") margin-bottom)))
                         0
                         (subvec (into
                                  []
                                  (oops/oget dragged-dom "parentNode.children"))
                                 0
                                 destination-index)))]
          (re-frame/dispatch
           [:gen-fhi.db.events.ui/update-dragging-coordinates
            {:client-height "5px" ;;(oops/oget dragged-dom "clientHeight")
             :client-width  (oops/oget dragged-dom "clientWidth")
             :client-y      client-y
             :client-x      (-> (.getComputedStyle
                                 js/window
                                 (oops/oget dragged-dom "parentNode"))
                                (oops/oget "paddingLeft"))}]))))))

(defn droppable [props children]
  (let [dragging-placeholder-coordinate @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-dragging-coordinates])]
    [:div {:class "relative"}
     [:> Droppable props
      (fn [provided snapshot]
        (r/as-element
         [:div (js->clj (.assign js/Object
                                 (oops/oget provided "droppableProps")
                                 #js {:ref (oops/oget provided "innerRef")}))
          children
          (oops/oget provided "placeholder")
          (when (oops/oget snapshot "isDraggingOver")
            ;;border border-dashed border-orange-300
            [:div {:class "bg-orange-50"
                   :style {:position "absolute",
                           :top (:client-y dragging-placeholder-coordinate)
                           :left (:client-x dragging-placeholder-coordinate)
                           :height (:client-height dragging-placeholder-coordinate)
                           :width (:client-width dragging-placeholder-coordinate)}}])]))]]))

(defn draggable [props children]
  [:> Draggable props
   (fn [provided snapshot]
     (r/as-element
      [:div (js->clj
             (.assign js/Object
                      (oops/oget provided "draggableProps")
                      (oops/oget provided "dragHandleProps")
                      #js{:ref (oops/oget provided "innerRef")}))
       (children provided snapshot)]))])

(defn drag-drop-context [props children]
  [:> DragDropContext (merge
                       props
                       (when (not (:disable-custom-on-drag props))
                         {:on-drag-update on-drag-update}))
   children])
