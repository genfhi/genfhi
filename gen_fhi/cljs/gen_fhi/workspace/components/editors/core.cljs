(ns gen-fhi.workspace.components.editors.core
  (:require [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [re-frame.core :as re-frame]
            [oops.core :as oops]
            [reagent.core :as r]

            ["@ant-design/icons" :refer [ExpandAltOutlined]]
            ["@codemirror/autocomplete" :refer [autocompletion]]
            ["@genfhi/editor-setup" :refer [basicSetup]]
            ["antd" :refer [Select]]
            ["fp-lang-codemirror" :refer [FP]]

            [gen-fhi.fhirpath.core :as fp]

            [gen-fhi.workspace.components.helper :refer [helper-text]]
            [gen-fhi.components.base.codemirror :refer [codemirror]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.components.reference :refer [reference-select]]
            [gen-fhi.workspace.components.terminology :refer [valueset-search code-radio code-select]]
            [gen-fhi.workspace.components.render-path :refer [editor-for-path]]
            [gen-fhi.workspace.utilities.mutations :as mutations]))

(defn- is-escaped? [word]
  (nil? (re-matches #"([a-zA-Z]|_|[0-9])*" word)))

(defn- escape [word]
  (if (is-escaped? word)
    (str "`" word "`")
    word))

(defn- variable-completion [context variables word]
  (if (and (not (oops/oget context "explicit"))
           (= (oops/oget word "from") (oops/oget word "to")))
    nil
    #js{:from (+ 1 (oops/oget word "from"))
        :options (clj->js (map
                           ;; Need to make sure variable gets escaped when neccessary
                           #(assoc % :apply (escape (get % :label)))
                           variables))}))

(defn- dot-completion [fp-ctx word]
  (let [supported-fns-fp (fp/->supported-functions)
        active-fields    (if (map? (first fp-ctx))
                           (keys (first fp-ctx))
                           nil)]
    #js {:from (+ 1 (oops/oget word "from"))
         :options (clj->js
                   (concat
                    (map
                     (fn [field-key]
                       {:label (name field-key)
                        :boost 10
                        :detail (let [val (get (first fp-ctx) field-key)]
                                  (cond
                                    (list? val)   "<list>"
                                    (vector? val) "<vector>"
                                    (map? val)    "<map>"
                                    (string? val) "<string>"
                                    (number? val) "<number>"
                                    :else "<unknown>"))
                        :type "property"
                        :apply (fn [view _completion from to]
                                 (let [field-val (escape (name field-key))]
                                   (oops/ocall view
                                               "dispatch"
                                               (clj->js {:changes   {:from from :to to :insert field-val}
                                                         :selection {:anchor (+ from (count field-val))}
                                                         :userEvent "input.complete"}))))})

                     active-fields)
                    (map
                     (fn [fp-key]
                       {:label (name fp-key)
                        :type "function"
                        :detail (get supported-fns-fp fp-key)
                        :apply (fn [view _completion from to]
                                 (oops/ocall view
                                             "dispatch"
                                             (clj->js {:changes {:from from :to to :insert (str (name fp-key) "()")}
                                                       :selection {:anchor (+ 1 from (count (name fp-key)))}
                                                       :userEvent "input.complete"})))})
                     (keys supported-fns-fp))))}))

(defn- expression-eval
  "Eval exp calling child with ctx and result."
  [{:keys [ctx p state-path expression]} children]
  (let [language        (get expression :language)

        eval-result-raw (when (and state-path language)
                          @(re-frame/subscribe [:gen-fhi.db.subs.item/evaluate-expression
                                                {:expression expression
                                                 :p p
                                                 :state-path state-path
                                                 :ctx ctx}]))

        eval-result     (if (string? eval-result-raw)
                          (list eval-result-raw)
                          eval-result-raw)]
    (children eval-result)))

;; The way this works is checking if there is two brackets before
;; And does a negative look ahead to confirm their are no double bracket closing.
(def inside-expression-reg "(?:\\{\\{)(?:(?!\\}\\}).)*")

(defn- ->auto-completer
  "Codemirror auto-completions completions"
  [{:keys [ctx language variables evaluate-ctx]}]
  (fn [context]
    (if (and (= language "application/x-fhir-query")
             (not (oops/ocall context "matchBefore"  (re-pattern inside-expression-reg))))
      nil
      (if-let [word (oops/ocall context "matchBefore"  #"%\w*")]
        (variable-completion context variables word)
        (if-let [word (oops/ocall context "matchBefore" #"\.\w*")]
          (let [expression  (cond
                              (= language "application/x-fhir-query") (string/replace
                                                                       (oops/oget (oops/ocall context
                                                                                              "matchBefore"
                                                                                              (re-pattern inside-expression-reg))
                                                                                  "text")
                                                                       "{{"
                                                                       "")
                              (= language "text/fhirpath")            (oops/oget (oops/ocall context "matchBefore" #".*") "text"))
                ctx-expression (subs expression 0 (string/last-index-of expression "."))
                fp-ctx         (when (fn? evaluate-ctx) (evaluate-ctx ctx-expression))]
            (dot-completion (get fp-ctx :evaluation) word))
          nil)))))

(defn -label
  ([content]
   [-label {} content])
  ([{:keys [tooltip hide-label?]} content]
   (when (not hide-label?)
     [:label {:class "flex items-center"}
       content
       (when tooltip
         [:span {:class "ml-2 inline-flex justify-center items-center"}
          [helper-text {:content tooltip}]])])))

(def ^{:private true} input-class "p-2 w-full border border-slate-300 hover:border-blue-400 focus:border-blue-400 active:border-blue-400 rounded ")

(defn raw-textarea-editor [{:keys [on-blur className container-style input-style  hide-label? label  on-change value tooltip]}]
  [:div {:style (merge {:width "100%"} container-style)}
   [-label {:tooltip tooltip
            :hide-label? hide-label?} label]
   [:textarea {:class-name (str input-class
                                className)
               :style     (merge {:resize "none" :outline-width "0"} (or input-style {}))
               :value     value
               :on-change on-change
               :on-blur   on-blur}]])

(defn raw-text-editor [{:keys [tooltip input-type readonly? on-blur auto-focus placeholder container-style input-style  hide-label? label on-change value className]}]
  [:div {:style (merge {:width "100%"} container-style)}
   [-label {:tooltip tooltip
            :hide-label? hide-label?} label]
   [:input {:type        (or "text" input-type)
            :readOnly    readonly?
            :class-name  (str input-class
                              className)

            :auto-focus  auto-focus
            :placeholder placeholder
            :style       (or input-style {})
            :value       value
            :on-change   on-change
            :on-blur     (or on-blur identity)}]])

(defn text-form-blur []
  (let [value!      (r/atom nil)
        ;; Hold previous property derived from item.
        prev-value! (r/atom nil)]
    (fn [{:keys [placeholder className p input-style container-style hide-label?]}]
      (let [{:keys [field]} (path/ascend p)
            value @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])]
        (when (not= @prev-value! value)
          (reset! prev-value! value)
          (reset! value! value))
        [raw-text-editor {:className       className
                          :hide-label?     hide-label?
                          :placeholder     placeholder
                          :label           (string/capitalize (name field))
                          :container-style container-style
                          :input-style     (or input-style {})
                          :value           @value!
                          :on-change       (fn [e]
                                             (reset! value! (.. e -target -value)))
                          :on-blur         (fn []
                                             (re-frame/dispatch
                                              [:gen-fhi.db.events.item/replace-id p value @value!])

                                             (mutations/replace p @value!))}]))))

(defn text-form [{:keys [tooltip placeholder className p input-style container-style hide-label?]}]
  (let [{:keys [field]} (path/ascend p)]
    [editor-for-path {:path p}
     (fn [value {:keys [on-change]}]
       [raw-text-editor {:tooltip tooltip
                         :className className
                         :hide-label? hide-label?
                         :placeholder placeholder
                         :label (string/capitalize (name field))
                         :container-style container-style
                         :input-style (or input-style {})
                         :value      value
                         :on-change
                         (fn [e]
                           (let [value (.. e -target -value)]
                             (on-change value)))}])]))

(defn textarea-form [{:keys [tooltip p input-style container-style hide-label?]}]
  (let [{:keys [field]} (path/ascend p)]
    [editor-for-path {:path p}
     (fn [value {:keys [on-change]}]
       [raw-textarea-editor {:tooltip tooltip
                             :hide-label? hide-label?
                             :label (string/capitalize (name field))
                             :container-style container-style
                             :input-style (or input-style {})
                             :value      value
                             :on-change
                             (fn [e]
                               (let [value (.. e -target -value)]
                                 (on-change value)))}])]))

(defn boolean-form [{:keys [tooltip p input-style container-style hide-label?]}]
  (let [{:keys [field]} (path/ascend p)]
    [editor-for-path {:path p}
     (fn [value {:keys [on-change]}]
       [:div {:style (merge {:width "100%"} container-style)}
        [:div
         [-label {:tooltip tooltip
                  :hide-label? hide-label?}
          (string/capitalize (name field))]]
        [:input {:type "checkbox"
                 :style (or input-style {})
                 :checked value
                 :on-change
                 (fn [e]
                   (let [value (.. e -target -checked)]
                     (on-change value)))}]])]))

(defn code-select-form [{:keys [tooltip
                                filter-concepts
                                label component-type hide-label? p container-style valueset select-props default-value
                                descendent-of]}]
  (let [component-type  (or component-type :select)
        {:keys [field]} (path/ascend p)]
    [editor-for-path {:path p}
     (fn [value {:keys [on-change]}]
       (let [value (or value default-value)]
         [:div {:class "w-full " :style container-style}
          [-label {:tooltip tooltip
                   :hide-label? hide-label?} (or label
                                                 (string/capitalize (name field)))]
          (match component-type
            :radio
            [code-radio {:value-set  valueset
                         :on-change (fn [code]
                                      (on-change code))
                         :filter-concepts filter-concepts
                         :descendent-of descendent-of
                         :value     value}]
            :select
            [code-select {:value-set  valueset
                          :on-change (fn [code]
                                       (on-change code))
                          :filter-concepts filter-concepts
                          :value     value
                          :descendent-of descendent-of
                          :select-props select-props}])]))]))

(defn code-expression-form [{:keys [tooltip p state-path ctx language label component-type hide-label? container-style valueset select-props default-value]}]
  (let [component-type  (or component-type :select)
        {:keys [field]} (path/ascend p)]
    [editor-for-path {:path p}
     (fn [value {:keys [on-change]}]
       [expression-eval
        {:ctx ctx
         :p p
         :state-path state-path
         :expression value}
        (fn [result]
          (let [result (or (:evaluation result) default-value)]
            [:div {:style (merge {:width "100%"} container-style)}
             [-label {:tooltip tooltip
                      :hide-label? hide-label?} (or label
                                                    (string/capitalize (name field)))]
             (match component-type
               :radio
               [code-radio {:value-set  valueset
                            :on-change  (fn [code]
                                          (on-change {:language   language
                                                      :expression code}))
                            :value      result}]
               :select
               [code-select {:value-set    valueset
                             :on-change    (fn [code]
                                             (on-change {:language language
                                                         :expression code}))
                             :value        result
                             :select-props select-props}])]))])]))

(defn reference-select-form [{:keys [tooltip label hide-label? p container-style resource-type search-key select-props value-fn]}]
  (let [value-fn        (or value-fn constantly)
        {:keys [field]} (path/ascend p)]

    [editor-for-path {:path p}
     (fn [reference {:keys [on-change]}]
       [:div {:style (merge {:width "100%"} container-style)}
        [-label {:tooltip tooltip
                 :hide-label? hide-label?} (or label
                                               (string/capitalize (name field)))]
        [reference-select {:reference (value-fn reference)
                           :search-key search-key
                           :resource-type resource-type
                           :select-props
                           (merge select-props
                                  {:on-change (fn [reference]
                                                (on-change {:reference reference}))})}]])]))

(defn textarea [props]
  [:textarea  props])

(defn- language-tag [{:keys [style]} children]
  [:a
   {:tabIndex "-1"
    :target "_blank"
    :href (cond
            (= children "text/fhirpath")
            "https://hl7.org/fhirpath/"
            (= children "application/x-fhir-query")
            "http://build.fhir.org/ig/HL7/sdc/expressions.html#x-fhir-query-enhancements"
            :else "http://build.fhir.org/ig/HL7/sdc/expressions.html")
    :className "hover:text-blue-800 text-gray-700"
    :style (merge
            {:font-size "6px"}
            style)}
   children])

(defn expression-results [{:keys [style result]}]
  [:div  {:className " w-full border border-t-0 "
          :style (merge {:position "absolute"
                         :z-index 5
                         :overflow-y "auto"
                         :overflow-x "hidden"
                         :max-height "300px"
                         :min-height "35px"}
                        style)}
   ;; Could have a result non sequential such as json
   (if-let [error (:error result)]
     [:div {:class "p-2 bg-red-100 text-red-900 border-red-400"}
      [:div {:style {:font-weight "500" :display "flex" :font-size "12px"}}
       [:div {:style {:display "flex" :flex 1}}
        [:span "Error"]]]
      [:pre {:class "whitespace-pre-wrap" :style {:font-size "11px"}}
       (str (ex-message error))]]

     (let [evaluation (:evaluation result)
           ;; Quick hack fix tomorrow
           evaluation (if (sequential? evaluation) evaluation [evaluation])]
       (if evaluation
         [:div {:class "p-2 bg-blue-100 text-blue-900 border-blue-400"}
          [:div {:class "font-semibold text-xs flex"}
           [:div {:style {:display "flex" :flex 1}}
            [:span "Result"]]
           [:div {:class "flex mr-1 font-medium"}
            [:span  "Type: "
             (str
              (cond
                (sequential? (:evaluation result)) "List"
                (map? (:evaluation result)) "Map"
                (string? (:evaluation result)) "String"
                (number? (:evaluation result)) "Number"
                (boolean? (:evaluation result)) "Boolean"
                :else "Unknown")
              ", ")]]
           [:div {:style {:display "flex"}}
            [:span  "Count: " (count evaluation)]]]
          [:pre {:style {:font-size "11px"}}
           (str
            (->> (take 3 evaluation)
                 (map #(if (string? %) % (.stringify js/JSON (clj->js %) nil 2)))
                 (clj->js)))
           (when (< 3 (count evaluation))
             "\n[Only displaying first 3 results]")]]
         "<empty>")))])

(defn raw-expression-editor [{:keys [tooltip language extensions variables max-height max-width ctx evaluate-ctx]}]
  (let [current-value  (atom nil)
        root           (r/atom nil)
        focused?       (r/atom false)
        passed-extensions-atom (r/atom extensions)
        variables-atom (r/atom variables)
        create-ext     (fn [extensions variables]
                         (concat
                          (or extensions [])
                          [basicSetup
                           (FP)
                           (autocompletion #js{:override #js[(->auto-completer
                                                              (merge
                                                               {:evaluate-ctx evaluate-ctx
                                                                :ctx          ctx
                                                                :language     language
                                                                :variables    (or variables {})}))]})]))
        extensions-atom  (r/atom (create-ext extensions variables))]
    (fn [{:keys [on-expand placeholder variables key label container-style extensions language value on-change hide-label? hide-eval? eval-style exp-results height width]}]
      (reset! current-value value)
      ;; Quick hack to prevent flashing of dropdown unless real change has occured.
      (when (or (not= variables @variables-atom)
                (not= extensions @passed-extensions-atom))
        (reset! passed-extensions-atom extensions)
        (reset! variables-atom variables)
        (reset! extensions-atom (create-ext extensions variables)))

      (let [min-height "40px"]
        [:div {:style (merge {:min-height min-height
                              :height (str height (if (and (not hide-label?)
                                                           label)
                                                    "+ 22px"
                                                    ""))
                              :width (or "100%" width)
                              :position "relative"}
                             container-style)
               :on-focus   #(reset! focused? true)
               :on-blur    #(reset!  focused? false)}

         [:div {:style {:position "absolute"
                        :left 0
                        :right 0
                        :bottom 0
                        :top 0}}
          [:div
           [-label {:tooltip tooltip
                    :height "22px"
                    :hide-label? hide-label?}
            label]]
          [:div {:class "bg-white"
                 :style {:position "relative"}
                 :ref (fn [ref]
                        (when (nil? @root)
                          (reset! root ref)))}
           [:div {:style {:position "absolute"
                          :top "-7px"
                          :right "3px"
                          :z-index 1}}
            [language-tag {:className "mr-1"}

             language]]
           (when on-expand
             [:div
              {:className "text-xs text-gray-500 hover:text-blue-500"
               :style {:position "absolute" :bottom "6px" :right "3px" :z-index 1}}
              [:> ExpandAltOutlined {:on-click on-expand}]])
           [:f> codemirror
            {:key                key
             ;; If height is minimal don't show scrollbar because overlaps content
             ;; too much.
             :theme              (if (and (some? @root)
                                          (< (oops/oget @root "clientHeight") 60))
                                   {".cm-scroller" {"overflow" "hidden"}}
                                   {})
             :height             height
             :width              width
             :max-height         max-height
             :max-width          max-width
             :placeholder-string placeholder
             :extensions         @extensions-atom
             :value              (get value :expression "")
             :on-change          (fn [code _view-update]
                                   (when (and (some? on-change)
                                              (or (not= code "") (some? @current-value)))
                                     (on-change code _view-update)))}]
           (when (and @focused? (some? exp-results) (not hide-eval?))
             [expression-results {:style eval-style
                                  :result exp-results}])]]]))))

(defn ->post-processing-extension [value-code]
  {:url "https://genfhi.com/Extension/post-processing"
   :valueCode value-code})

(defn assoc-post-processing [post-processing expression]
  (update expression
          :extension
          (fn [extensions_]
            (let [extensions (into [] (filter
                                       (fn [ext] (not= "https://genfhi.com/Extension/post-processing" (get ext :url)))
                                       extensions_))]
              (if (some? post-processing)
                (conj extensions (->post-processing-extension post-processing))
                extensions)))))

(defn valueset-select-expression
  "Used to evaluate valueset expression and present a dropdown selection."
  [{:keys [tooltip p state-path hide-label? label]}]

  (let [value           @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        {:keys [field]} (path/ascend p)]
    [expression-eval {:p p :state-path state-path :expression value}
     (fn [results]
       (let [url (if (sequential? (:evaluation results))
                   (first results)
                   (:evaluation results))]
         [:<>
          [-label {:tooltip tooltip
                   :hide-label? hide-label?} (or label (string/capitalize (name field)))]
          [valueset-search
           {:url       url
            :class     input-class
            :on-change (fn [valueset-or-url]
                         (if (= "" valueset-or-url)
                           (mutations/remove p)
                           ;; Build out an expression as a fhirpath string from the url. 
                           (mutations/replace
                            p
                            (let [url (if (= "ValueSet" (get valueset-or-url :resourceType))
                                        (get valueset-or-url :url)
                                        valueset-or-url)]
                              (assoc-post-processing
                               "json"
                               {:expression (str "\"" url "\"")
                                :language   "application/x-fhir-query"})))))}]]))]))

(defn valueset-select-url
  "Used to evaluate valueset expression and present a dropdown selection."
  [{:keys [tooltip p hide-label? label]}]

  (let [url             @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        {:keys [field]} (path/ascend p)]
    [:<>
     [-label {:tooltip tooltip
              :hide-label? hide-label?} (or label (string/capitalize (name field)))]
     [valueset-search
      {:url       url
       :class     input-class
       :on-change (fn [valueset-or-url]
                    (if (= "" valueset-or-url)
                      (mutations/remove p)
                        ;; Build out an expression as a fhirpath string from the url. 
                      (mutations/replace
                       p
                       (let [url (if (= "ValueSet" (get valueset-or-url :resourceType))
                                   (get valueset-or-url :url)
                                   valueset-or-url)]
                         url))))}]]))

(defn dynamic-expression "Used to evaluate any exp displaying results and ctx"
  [{:keys [tooltip p state-path label language on-change value theme ctx height width extensions]}]
  (let [language  (or language "text/fhirpath")
        variables @(re-frame/subscribe [:gen-fhi.db.subs.item/get-all-variable-names p])]
    [expression-eval
     {:p p
      :ctx ctx
      :state-path state-path
      :expression (assoc value :language language)}
     (fn [results]
       [:<>
        [raw-expression-editor {:tooltip tooltip
                                :extensions   extensions
                                :ctx          ctx
                                :evaluate-ctx (fn [ctx-expression]
                                                @(re-frame/subscribe
                                                  [:gen-fhi.db.subs.item/evaluate-expression
                                                   {:expression
                                                    {:expression ctx-expression
                                                     :language   "text/fhirpath"}
                                                    :ctx         ctx
                                                    :p           p
                                                    :state-path  state-path}]))
                                :key          (str p)
                                :label        label
                                :exp-results  results
                                :variables    variables
                                :value        value
                                :language     language
                                :theme        theme
                                :height       height
                                :width        width
                                :on-change    on-change}]])]))

(defn expression-form [{:keys [tooltip
                               extensions
                               allow-modal
                               hide-label?
                               p
                               container-style
                               label
                               language
                               state-path
                               before-on-change
                               max-height
                               max-width
                               post-processing
                               height
                               ctx] :as props}]
  (let [language        (or language "text/fhirpath")
        {:keys [field]} (path/ascend p)
        expression      @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        variables       @(re-frame/subscribe [:gen-fhi.db.subs.item/get-all-variable-names p])]
    [expression-eval
     {:ctx ctx
      :p p
      :state-path state-path
      :expression expression}
     (fn [results]
       [:<>
        [:div {:style (merge {:width "100%" :position "relative"} container-style)}

         [-label {:tooltip tooltip
                  :hide-label? hide-label?} (or label (string/capitalize (name field)))]
         [:<>
          [raw-expression-editor {:key (str p)
                                  :height height
                                  :evaluate-ctx (fn [ctx-expression]
                                                  @(re-frame/subscribe
                                                    [:gen-fhi.db.subs.item/evaluate-expression
                                                     {:expression
                                                      {:expression ctx-expression
                                                       :language   "text/fhirpath"}
                                                      :ctx         ctx
                                                      :p           p
                                                      :state-path  state-path}]))
                                  :extensions extensions
                                  :max-height max-height
                                  :max-width  max-width
                                  :exp-results results
                                  :variables   variables
                                  :on-expand   (when allow-modal
                                                 (fn []
                                                   (re-frame/dispatch
                                                    [:gen-fhi.db.events.ui/set-active-modal
                                                     {:type :code-editor
                                                      :data {:expression-form-props (select-keys props [:allow-modal? :ctx :post-processing])
                                                             :p p
                                                             :extensions extensions
                                                             :state-path state-path
                                                             :language language}}])))
                                  :value       expression
                                  :language    language
                                  :on-change   (fn [code _view-update]
                                                 (when before-on-change (before-on-change code _view-update))
                                                 (if (= code "")
                                                   (mutations/remove p {:sync? true :remove-empty true})
                                                   ;; Don't replace the whole object because would overwrite potential reference.
                                                   (mutations/replace
                                                    p
                                                    (assoc-post-processing
                                                     post-processing
                                                     (assoc expression
                                                            :expression code
                                                            :language (or language "text/fhirpath")))
                                                    {:sync? true :remove-empty true})))}]]]])]))

(defn number-form [{:keys [tooltip p label container-style hide-label? className auto-focus placeholder input-style on-blur]}]
  (let [{:keys [field]} (path/ascend p)]
    [editor-for-path {:path p}
     (fn [value {:keys [on-change]}]
       [:div {:style (merge {:width "100%"} container-style)}
        [-label {:tooltip tooltip
                 :hide-label? hide-label?} (or label (string/capitalize (name field)))]
        [:div
         [:input
          {:class-name   (str input-class
                              className)
           :type "number"
           :auto-focus   auto-focus
           :placeholder placeholder
           :style       (or input-style {})
           :value       value
           :on-change   (fn [e]
                          (on-change (js/parseInt (oops/oget e "target.value"))))
           :on-blur     (or on-blur identity)}]]])]))

(defn action-reference [{:keys [tooltip hide-label? p label value on-change]}]
  (let [{:keys [resource-type id]} (path/path-meta p)
        action-path                (path/->path resource-type id "action")
        actions                    @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value action-path])]
    [:div
     [:div
      [-label {:tooltip tooltip
               :hide-label? hide-label?} label]]
     [:> Select {:className "flex items-center w-full mr-1 border border-slate-300 hover:border-blue-400 rounded"
                 :placeholder "Select an action"
                 :value       value
                 :style       {:width "100%"}
                 :on-change   on-change}
      (map
       (fn [action]
         [:> (.-Option Select) {:key (:id action) :value (:id action)}
          (:id action)])
       actions)]]))
