(ns gen-fhi.workspace.components.editors.questionnaire
  (:require [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.components.editors.core :as editors]
            [clojure.string :as string]
            [re-frame.core :as re-frame]
            [gen-fhi.workspace.utilities.mutations :as mutations]))

(defn- questionnaire-expression-eval
  "Eval exp calling child with ctx and result."
  [{:keys [ctx p qr-path expression]} children]
  (let [language        (get expression :language)
        eval-result-raw (when (and qr-path language)
                          @(re-frame/subscribe [:gen-fhi.db.questionnaire.subs/evaluate-expression
                                                {:expression expression
                                                 :p p
                                                 :qr-path qr-path
                                                 :ctx ctx}]))
        eval-result     (if (string? eval-result-raw)
                          (list eval-result-raw)
                          eval-result-raw)]

    (children eval-result)))

(defn expression-form [{:keys [p
                               remove-expression-on-empty?
                               tooltip
                               state-path
                               extensions
                               ;;allow-modal
                               hide-label?

                               container-style
                               label
                               language

                               before-on-change
                               max-height
                               max-width
                               post-processing
                               height
                               ctx] :as props
                        :or {remove-expression-on-empty? true}}]
  (let [language        (or language "text/fhirpath")
        {:keys [field]} (path/ascend p)
        expression      @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        variables       (when (some? p) @(re-frame/subscribe [:gen-fhi.db.questionnaire.subs/get-variables p]))]
    [questionnaire-expression-eval
     {:ctx        ctx
      :p          p
      :qr-path    state-path
      :expression expression}
     (fn [results]
       [:<>
        [:div {:style (merge {:width "100%" :position "relative"} container-style)}
         [editors/-label {:tooltip tooltip
                          :hide-label? hide-label?} (or label (string/capitalize (name field)))]
         [:<>
          [editors/raw-expression-editor {:key            (str p)
                                          :height         height
                                          :evaluate-ctx   (fn [ctx-expression]
                                                            (let [return @(re-frame/subscribe [:gen-fhi.db.questionnaire.subs/evaluate-expression
                                                                                               {:expression {:expression ctx-expression :language "text/fhirpath"}
                                                                                                :p p
                                                                                                :qr-path state-path
                                                                                                :ctx ctx}])]
                                                              return))
                                          :extensions     extensions
                                          :max-height     max-height
                                          :max-width      max-width
                                          :exp-results    results
                                          :variables      variables
                                          ;; :on-expand   (when allow-modal
                                          ;;                (fn []
                                          ;;                  (re-frame/dispatch
                                          ;;                   [:gen-fhi.db.events.ui/set-active-modal
                                          ;;                    {:type :code-editor
                                          ;;                     :data {:expression-form-props (select-keys props [:allow-modal? :ctx :post-processing])
                                          ;;                            :p p
                                          ;;                            :extensions extensions
                                          ;;                            :state-path state-path
                                          ;;                            :language language}}])))
                                          :value          expression
                                          :language       language
                                          :on-change      (fn [code _view-update]
                                                            (when before-on-change (before-on-change code _view-update))
                                                            (if (= code "")

                                                              (mutations/remove
                                                               (if remove-expression-on-empty?
                                                                 p
                                                                 (path/descend p :expression))
                                                               {:sync? true :remove-empty true})
                                                              ;; Don't replace the whole object because would overwrite potential reference.
                                                              (mutations/replace
                                                               p
                                                               (editors/assoc-post-processing
                                                                post-processing
                                                                (assoc expression
                                                                       :expression code
                                                                       :language (or language "text/fhirpath")))
                                                               {:sync? true :remove-empty true})))}]]]])]))
