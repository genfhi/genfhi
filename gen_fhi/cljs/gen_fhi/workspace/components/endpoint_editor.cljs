(ns gen-fhi.workspace.components.endpoint-editor
  (:require [clojure.string :as string]
            [re-frame.core :as re-frame]
            [cljs.core.async :refer [take!]]
            [reagent.core :as r]

            [antd :refer [Select]]

            [gen-fhi.workspace.constants.smart :refer [smart-key]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.fhir-client.protocols :refer [search]]))

(defn reference->path [reference]
  (when-let [reference-string (get reference :reference)]
    (let [[resource-type id] (string/split reference-string #"/")]
      (path/->path resource-type id))))

(defn- display-resource-option [{:keys [resource]}]
  [:div {:class "flex flex-1"}
   [:div {:class "flex flex-1"}
    (get resource :name)]
   [:div {:class "flex ml-2 text-gray-300"}
    (:id resource)]])

(defn endpoint-select [{:keys []}]
  (let [loading?     (r/atom false)
        query-result (r/atom [])
        c            @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        on-search    (fn [value]
                       (reset! loading? true)
                       (take!
                        (search
                         c
                         "Endpoint"
                         (assoc {} :name value))
                        (fn [{:keys [:fhir/response]}]
                          (reset!
                           loading?
                           false)
                          (reset!
                           query-result
                           response))))]
    (on-search "")

    (fn [{:keys [client-path reference select-props class]}]
      (let [is-smart?         @(re-frame/subscribe  [:gen-fhi.db.subs.smart/is-legacy-smart? client-path])]
        [:> Select (merge {:defaultValue (get reference :reference nil)
                           :on-search    on-search
                           :filterOption (fn [_input _option] true) ;; Always show filter comes from api query
                           :style {:min-width "200px"}
                           :showSearch   true
                           :loading      @loading?
                           :className   (str "flex items-center pl-1 pr-1 w-full border border-slate-300 hover:border-blue-400 rounded " class)
                           :placeholder  "Search for reference"}
                          select-props)

         (when is-smart?
           [:> (.-Option Select) {:key smart-key :value smart-key}
            smart-key])

         (->> @query-result
              (map
               (fn [resource]
                 [:> (.-Option Select)
                  {:key (:id resource)
                   :value (str (:resourceType resource) "/" (:id resource))}
                  (r/as-element [display-resource-option {:resource resource}])])))]))))


