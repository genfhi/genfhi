(ns gen-fhi.workspace.components.helper
  (:require
   ["@ant-design/icons" :refer [QuestionCircleOutlined]]
   ["antd" :refer [Popover]]))

(defn helper-text [{:keys [icon-class  content placement]
                    :or {placement "topRight"
                         icon-class "text-slate-300 underline hover:underline hover:text-blue-400"}}]
  [:> Popover {:placement placement
               :content   content}
   [:div {:class "inline-block"}
    [:div {:class "h-full flex items-center justify-center"}
     [:> QuestionCircleOutlined {:class icon-class}]]]])


