(ns gen-fhi.workspace.components.layout
  (:require [re-frame.core :as re-frame]))

(def side-bar-width "270px")
(def sidebar-props {:style {:width side-bar-width
                            :overflow      "auto"}})

(defn layout
  ([children]
   [layout {} children])
  ([{:keys [left-sidebar right-sidebar bottom-bar]} children]
   (let [left-sidebar-showing  @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :left])
         right-sidebar-showing @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :right])
         bottom-bar-showing    @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :bottom])]
     [:div {:class "flex flex-1 h-full"}
      (when (and left-sidebar left-sidebar-showing)
        left-sidebar)
      [:div {:style {:flex-direction "column" :display "flex" :flex 1 :height "100%" :overflow "auto"}}
       children
       (when (and bottom-bar bottom-bar-showing)
         bottom-bar)]
      (when (and right-sidebar right-sidebar-showing)
        right-sidebar)])))

(defn left-sidebar
  ([children]
   [left-sidebar {} children])
  ([props children]
   [:div (merge sidebar-props
                {:className "select-none z-50 border border-y-0 border-l-0 border-gray-300"
                 :style (merge (:style sidebar-props))}
                props)
    children]))

(defn right-sidebar
  ([children]
   [right-sidebar {} children])
  ([props children]
   (let [right-sidebar-showing  @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :right])]
     (when right-sidebar-showing
       [:div (merge sidebar-props
                    {:className "select-none z-50 border border-y-0 border-r-0 border-gray-300"
                     :style (merge (:style sidebar-props))}
                    props)
        children]))))

(defn bottom-bar
  ([children]
   [bottom-bar {} children])
  ([props children]
   (let [bottom-bar-showing  @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :bottom])]
     (when bottom-bar-showing
       [:div (merge {:className "select-none z-50 w-full border border-x-0 border-b-0 border-gray-300"
                     :style {:height "250px" :overflow "auto"}}
                    props)
        children]))))
