(ns gen-fhi.workspace.components.list)

(defn list-item [{:keys [left-icon right-icon class-name on-click primary-text secondary-text style is-active?]}]
  [:div {:className (str "flex items-center truncate p-1 border border-x-0 border-t-0 border-gray-100 hover:bg-blue-100 hover:text-blue-500"
                         (if is-active?
                           " text-blue-500 bg-blue-100"
                           "")
                         class-name)
         :style (merge
                 {:cursor           "grab"
                  :text-overflow    "ellipsis"}
                 style)
         :on-click on-click}
   (when left-icon
     [:span {:class "flex mr-1"}
      left-icon])
   [:span {:class "flex mr-1 font-semibold" :style {:font-size "12px"}}
    primary-text]
   [:span {:class "flex mr-1 truncate" :style {:font-size "10px"}}
    secondary-text]
   [:span {:class "flex flex-1 justify-end"}
    (when right-icon
      right-icon)]])
