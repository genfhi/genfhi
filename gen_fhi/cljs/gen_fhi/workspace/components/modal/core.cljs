(ns gen-fhi.workspace.components.modal.core
  "Shared modal"
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]
            [cljs.core.async :refer [take!]]
            [clojure.core.match :refer [match]]
            [oops.core :as oops]
            [clojure.string :as string]

            [antd :refer [Breadcrumb Modal notification Spin message]]
            ["@codemirror/view" :refer [EditorView keymap]]
            ["@codemirror/commands" :refer [indentWithTab]]
            ["@ant-design/icons" :refer [CloseOutlined ExclamationCircleOutlined CheckCircleOutlined]]
            ["js-file-download" :as file-download]

            [gen-fhi.workspace.utilities.naming :as naming]
            [gen-fhi.workspace.components.create-resource :refer [create-resource-form]]
            [gen-fhi.workspace.components.parameter-editor :refer [parameters-editor]]
            [gen-fhi.workspace.router :as router]
            [gen-fhi.workspace.utilities.resources :refer [endpoint->auth-type]]
            [gen-fhi.operations.parameter-parsing.parameters-map :as param-parsing]
            [gen-fhi.fhir-client.protocols :refer [invoke]]
            [gen-fhi.workspace.components.tabs :refer [tabs tab-pane]]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.workspace.utilities.mutations :as mutations]
            [gen-fhi.workspace.components.terminology :refer [code-select]]
            [gen-fhi.workspace.components.editors.core :refer [expression-form action-reference raw-text-editor dynamic-expression]]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.workspace.components.modal.patch-editor :refer [patch-editor-modal]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.operations.core :as workspace-ops]))

(defn- error-notification [message response]
  (.open
   notification
   (clj->js
    (reduce
     (fn [notification issue]
       (assoc
        notification
        :description
        (str (:description notification) " " (:diagnostics issue))))
     {:message message
      :icon (r/as-element [:> ExclamationCircleOutlined {:style {:color "red"}}])
      :description ""}
     (:fhir/issue response)))))

(defn ctx+column-expression->column-exp-ctx [ctx column-expression]
  (try
    (let [eval-exp (fp/evaluate
                    (get column-expression :expression "$this")
                    ctx)]
      {:evaluation eval-exp})
    (catch js/Error e
      {:error e})))

(defn- -modal [{:keys [id modal-content modal-props]} children]
  [:> Modal (merge
             {:visible (if (sequential? id)
                         (contains? (set id) (:type modal-content))
                         (= (:type modal-content) id))
              :onCancel (fn [_e]
                          (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil])
                          (when-let [on-close (get modal-content :on-close)]
                            (on-close)))
              :header   nil
              :footer   nil}
             modal-props)
   children])

(defn- operation-invoke-modal [_props]
  (let [client           @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        input-parameter  (r/atom {:resourceType "Parameters"})
        loading?         (r/atom false)
        output-parameter (r/atom nil)]
    (fn [{:keys [modal-content]}]
      [-modal {:id :operation-invoke
               :modal-content modal-content}
       [:div {:class "p-6"}
        [:div {:class "mb-4 mt-4 mr-4"}
         [tabs {:tab-direction :left
                :active "input"
                :tab-right-content
                [:div {:class "p-1"}
                 [button {:type     :primary
                          :disabled @loading?
                          :on-click (fn [_e]
                                      (try
                                        (let [input @input-parameter]
                                          (reset! loading? true)
                                          (take!
                                           (invoke
                                            client
                                            (get-in modal-content [:data :operation :code])
                                            input)
                                           (fn [parameter-response]
                                             (reset! loading? false)
                                             (reset! output-parameter
                                                     (:fhir/response parameter-response))
                                             (if (= (:fhir/response-type parameter-response) :fhir/error)
                                               (error-notification "Failed to invoke Operation" parameter-response)
                                               (oops/ocall message "success" (str "$" (get-in modal-content [:data :operation :code])
                                                                                  " has finished."))))))

                                        (catch :default e
                                          (.open notification #js{:icon (r/as-element [:> ExclamationCircleOutlined {:style {:color "red"}}])
                                                                  :message (str e)}))))}

                  "Invoke"]]}
          [:f> tab-pane {:key "input" :name "Input"}
           [:div {:class "mt-2"}
            [parameters-editor
             {:use "in"
              :operation-definition (get-in modal-content [:data :operation])
              :on-change            (fn [parameter]
                                      (reset! input-parameter parameter))
              :value                @input-parameter}]]]

          [:f> tab-pane {:key "output" :name "Output"}
           [:div {:class "mt-2"}
            [:> Spin {:spinning @loading?}
             [parameters-editor {:use "out"
                                 :readonly? true
                                 :operation-definition (get modal-content :operation)
                                 :value @output-parameter}]]]]]]]])))

(defn- column-editor-modal [{:keys [modal-content]}]
  (let [state  (r/atom (get-in modal-content [:data :column]))]
    (fn [{:keys [modal-content]}]
      (let [{:keys [column column-path state-path ctx]} (get modal-content :data)]

        (when (and (some? column) (not= (:key column) (get @state :key)))
          (reset! state column))
        [-modal {:id :column-editor
                 :modal-content modal-content
                 :modal-props
                 {:title      "Column Editor"
                  :footer     [(r/as-element
                                [button
                                 {:type :danger
                                  :className "mr-2"
                                  :on-click
                                  (fn [_e]
                                    (re-frame/dispatch
                                     [:gen-fhi.db.events.ui/set-active-modal nil])
                                    (re-frame/dispatch
                                     [:gen-fhi.db.events.mutate/mutate
                                      {:type :remove
                                       :path column-path}]))}

                                 "Delete"])
                               (r/as-element
                                [button
                                 {:on-click
                                  (fn [_e]
                                    (mutations/replace column-path @state)
                                    (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil]))}
                                 "Update"])]
                  ;; Close button was not aligned properly ...
                  :close-icon (r/as-element
                               [:div {:style {:display "flex" :align-items "center" :justify-content "center" :height "100%" :width "100%"}}
                                [:> CloseOutlined]])}}

         [:div {:className "mb-8 p-2"}
          [:div {:className "mb-1"}
           [:label "Type"]
           [code-select {:value-set  "http://genfhi.com/ValueSet/application-widget-column-type-valueset"
                         :on-change (fn [code] (swap! state #(assoc % :type code)))
                         :value     (get @state :type)
                         :select-props {:style {:width "100%"}}}]]
          [:div {:className "mb-1"}
           [raw-text-editor        {:key             (str column-path "-name")
                                    :auto-focus      true
                                    :label           "Name"
                                    :value           (:title @state)
                                    :on-change       (fn [e]
                                                       (swap!
                                                        state
                                                        (fn [state]
                                                          (assoc state :title (.. e -target -value)))))}]]

          [:div {:className "mb-1"}
           [dynamic-expression  {:p           (path/descend column-path "expression")
                                 :state-path  state-path
                                 :label       "Expression"
                                 :key         (str column-path "-expression")
                                 :height      "40px"
                                 :ctx         ctx
                                 :value       (:dataIndex @state)
                                 :on-change   (fn [code _view-update]
                                                (swap!
                                                 state
                                                 (fn [state] (assoc-in state [:dataIndex :expression] code))))}]]]]))))

(defn- column-add-modal [{:keys [modal-content]}]
  (let [state (r/atom (get-in modal-content [:data :column]))]
    (fn [{:keys [modal-content]}]
      (let [{:keys [column-path state-path ctx]} (:data modal-content)]

        (when (not= (get @state :key) (get-in modal-content [:data :column :key]))
          (reset! state (get-in modal-content [:data :column])))
        [-modal {:id :column-add
                 :modal-content modal-content
                 :modal-props
                 {:title      "Create Column"
                  :footer     [(r/as-element
                                [button
                                 {:on-click
                                  (fn [_e]
                                    (mutations/add  column-path  @state {:sync? true})
                                    (mutations/move
                                     (path/descend (:parent (path/ascend column-path)) 0)
                                     column-path
                                     {:sync? true})
                                    (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil]))}
                                 "Create"])]
                  ;; Close button was not aligned properly ...
                  :close-icon (r/as-element
                               [:div {:style {:display "flex" :align-items "center" :justify-content "center" :height "100%" :width "100%"}}
                                [:> CloseOutlined]])}}
         [:div {:className "mb-8 p-2"}
          [:div {:className "mb-1"}
           [:label "Type"]
           [code-select {:value-set  "http://genfhi.com/ValueSet/application-widget-column-type-valueset"
                         :on-change (fn [code] (swap! state #(assoc % :type code)))
                         :value     (get @state :type)
                         :select-props {:style {:width "100%"}}}]]
          [:div {:className "mb-1"}
           [raw-text-editor        {:key             (str column-path "-name")
                                    :label           "Name"
                                    :auto-focus      true
                                    :placeholder     "Column name"
                                    :value           (:title @state)
                                    :on-change       (fn [e]
                                                       (swap!
                                                        state
                                                        (fn [state]
                                                          (assoc state :title (.. e -target -value)))))}]]
          [:div {:className "mb-1"}
           [dynamic-expression  {:p           (path/descend column-path "expression")
                                 :state-path  state-path
                                 :label       "Expression"
                                 :key         (str column-path "-expression")
                                 :height      "40px"
                                 :ctx         ctx
                                 :value       (:dataIndex @state)
                                 :on-change   (fn [code _view-update]
                                                (swap!
                                                 state
                                                 (fn [state]
                                                   (assoc
                                                    state
                                                    :dataIndex
                                                    {:expression code
                                                     :language   "text/fhirpath"}))))}]]]]))))

(defn- handler-modal [{:keys [modal-content]}]
  (let [state (r/atom (get modal-content :data))]
    (fn [{:keys [modal-content]}]
      (when (not= (str (:handler-path @state)) (str (get-in modal-content [:data :handler-path])))
        (reset! state (get modal-content :data)))
      (let [{:keys [on-change on-delete handler-type handler-path item-type]} (get modal-content :data)]
        [-modal {:id [:handler-editor :handler-add]
                 :modal-content modal-content
                 :modal-props
                 {:title    (if (= :handler-add (:type modal-content))
                              "Create Handler"
                              "Edit Handler")
                  :footer   (if (= :handler-add (:type modal-content))
                              [(r/as-element
                                [button
                                 {:type :secondary
                                  :className "mr-2"
                                  :on-click
                                  (fn [_e]
                                    (reset! state nil)
                                    (re-frame/dispatch
                                     [:gen-fhi.db.events.ui/set-active-modal nil]))}
                                 "Cancel"])
                               (r/as-element
                                [button
                                 {:type "primary"
                                  :on-click
                                  (fn [_e]
                                    (on-change (:handler @state))
                                    (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil]))}
                                 "Create"])]
                              [(r/as-element
                                [button
                                 {:type :danger
                                  :className "mr-2"
                                  :on-click
                                  (fn [_e]
                                    (reset! state nil)
                                    (re-frame/dispatch
                                     [:gen-fhi.db.events.ui/set-active-modal nil])
                                    (on-delete))}
                                 "Delete"])
                               (r/as-element
                                [button
                                 {:on-click
                                  (fn [_e]
                                    (on-change (:handler @state))
                                    (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil]))}
                                 "Update"])])}}

         [:div {:key "content" :class "p-2"}
          [:div
           [:label "Event"]
           [code-select {:value-set      (match handler-type
                                           :widget "http://genfhi.com/ValueSet/application-widget-event-types-valueset"
                                           :action "http://genfhi.com/ValueSet/application-action-event-types-valueset")
                         :descendent-of item-type
                         :on-change     (fn [code] (swap! state #(assoc-in % [:handler :eventType] code)))
                         :value         (get-in @state [:handler :eventType])
                         :select-props {:style {:width "100%"}}}]
           [:label "On event"]
           [code-select {:value-set  "http://genfhi.com/ValueSet/application-widget-handler-type-valueset"
                         :on-change (fn [code] (swap! state #(assoc-in % [:handler :type] code)))
                         :value     (get-in @state [:handler :type])
                         :select-props {:style {:width "100%"}}}]]

          [action-reference {:key (str handler-path "-action")
                             :p handler-path
                             :label "Handler"
                             :value (get-in @state [:handler :action])
                             :on-change (fn [action-id] (swap! state #(assoc-in % [:handler :action] action-id)))}]]]))))

(defn- code-editor-title [{:keys [p]}]
  (let [{:keys [parent field]} (path/ascend p)
        resource               @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/path->root p)])
        parent                 @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value parent])
        title (or (:title resource) (:name resource) (str (:resourceType resource) (:id resource)))]
    (if (and (some? (:id parent))
             (not= (:id resource) (:id parent)))
      [:> Breadcrumb
       [:> (.-Item Breadcrumb)
        title]
       [:> (.-Item Breadcrumb)
        (:id parent)]
       [:> (.-Item Breadcrumb)
        (name field)]]
      [:> Breadcrumb
       [:> (.-Item Breadcrumb)
        title]
       [:> (.-Item Breadcrumb)
        (name field)]])))

(def code-editor-theme (oops/ocall  EditorView "theme" (clj->js
                                                        {"&"
                                                         {:min-height "150px"
                                                          :max-height "600px"}

                                                         "&.cm-editor"
                                                         {:border "0px"}})))

(defn- code-editor-modal [{:keys [modal-content]}]
  (let [{:keys [language p state-path extensions expression-form-props]} (get modal-content :data)]
    [-modal {:id :code-editor
             :modal-content modal-content
             :modal-props
             {:close-icon (r/as-element [:<>])
              :style    {:minWidth "800px"}
              :footer   nil}}
     [:div
      [:div {:class "border p-1 pr-2 pl-2 flex justify-center items-center"}
       [code-editor-title {:p p}]
       [:div {:class "flex flex-1 justify-end"}
        [button {:on-click #(re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil])}
         "Finish"]]]
      [:div {:class "expression-modal"}
       [expression-form (merge
                         expression-form-props
                         {:language language
                          :extensions (concat
                                       ;; Allow tabbing in modal by default.
                                       [(oops/ocall keymap "of"
                                                    #js[indentWithTab])
                                        code-editor-theme]
                                       extensions)
                          :hide-label? true
                          :p p
                          :state-path state-path})]]]]))

(defn- docker-pane [{:keys [id]}]
  (let [c @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        docker-link (r/atom nil)
        secret      (r/atom nil)
        loading     (r/atom false)]
    (fn []
      [:div {:className "p-4 flex flex-col justify-center items-center"}
       (cond
         (some? @docker-link)
         [:div {:class "flex justify-center items-center flex-col w-full"}
          [button {:type :secondary}
           [:a {:target "_blank" :href @docker-link}
            "Download Image"]]
          [:div {:class "w-full"}
           [:div {:class "m-2"}
            [:span "After downloading the image run the following command: "]
            [:div {:class "m-1"}
             [:code {:class "border p-1 select-text"}
              "docker load -i IMAGE_FILE_NAME"]]]
           [:div {:class "m-2"}
            [:span "To run the loaded image: "]
            [:div {:class "m-1 border p-1"}
             [:code {:class "select-text"}
              (if-let [secret @secret]
                (str "docker run -p 8001:8001 " "-e \"server__encryption_local_secret_key=" secret "\" LOADED_IMAGE_ID")
                "docker run -p 8001:8001 LOADED_IMAGE_ID")]]]]]
         @loading             [:> Spin {:spinning @loading :tip "Creating Image"}]
         :else                [:div
                               [:div {:class "mb-4"}
                                [:span {:class "text-slate-700"}
                                 (str "Create a docker image of your application that "
                                      "you can deploy on your own infrastructure.")]]
                               [button
                                {:on-click (fn [_e]
                                             (reset! loading true)
                                             (take!
                                              (invoke
                                               c
                                               "docker-clientapp-build"
                                               {:resourceType "Parameters"
                                                :parameter
                                                [{:name "client-app"
                                                  :valueReference {:reference (str "ClientApplication/" id)}}]})
                                              (fn [response]
                                                (reset! loading false)
                                                (if (= :fhir/error (:fhir/response-type response))
                                                  (error-notification "Failed to Download App" response)
                                                  (let [parameters        (param-parsing/parameters->map (:fhir/response response))
                                                        download-link     (get-in parameters [:downloadLocation :url])
                                                        encryption-secret (get-in parameters [:encryption-secret])]
                                                    (reset! docker-link download-link)
                                                    (reset! secret encryption-secret))))))}
                                "Generate Image"]])])))

(defn- ref->path [reference]
  (let [ref-split (string/split (get reference :reference) #"/")]
    (when (= 2 (count ref-split))
      (apply path/->path ref-split))))

(defn- endpoint-deployment-table [{:keys [id]}]
  (let [cur-workspace @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :workspace])
        client-app    @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path "ClientApplication" id)])
        endpoints     (->> (get client-app :action [])
                           (map :endpoint)
                           (map ref->path)
                           (filter some?)
                           (set)
                           (map (fn [path]
                                  @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value path])))
                           (filter (fn [endpoint]
                                     (let [auth-type (endpoint->auth-type endpoint)]
                                       (or (= auth-type "smart-ehr")
                                           (= auth-type "smart-standalone"))))))]
    (when (seq endpoints)
      [:div
       [:div {:class "mb-2 mt-4"}
        [:span {:class "font-semibold text-slate-700"}
         "Update the following endpoints redirect url with cloud link above."]]
       [:table {:class "w-full table-fixed border border-slate-300 "}
        [:thead {:class "bg-gray-100 border-t-0 text-left border border-slate-300"}
         [:th "Name"]]
        [:tbody
         (map
          (fn [endpoint]
            (let [url
                  (router/path-for
                   :instance
                   {:workspace cur-workspace
                    :modifiers ""
                    :resource-type "Endpoint"
                    :id (get endpoint :id)})]
              ^{:key (get endpoint :id)}
              [:tr  {:class "border border-slate-300"}
               [:td
                [:a {:tabIndex "-1"
                     :target "_blank"
                     :class "w-full"
                     :href url}
                 (get endpoint :name)]]]))
          endpoints)]]])))

(defn- cloud-deploy-pane [{:keys [id]}]
  (let [c @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        link (r/atom nil)
        loading? (r/atom true)
        cloud-deploy (fn []
                       (reset! loading? true)
                       (take!
                        (invoke
                         c
                         "cloud-deploy"
                         {:resourceType "Parameters"
                          :parameter
                          [{:name "client-app"
                            :valueReference {:reference (str "ClientApplication/" id)}}]})
                        (fn [response]
                          (reset! loading? false)
                          (if (= :fhir/error (:fhir/response-type response))
                            (error-notification "Failed to deploy App" response)
                            (let [cloud-link (:return
                                              (param-parsing/parameters->map
                                               (:fhir/response response)))]
                              (reset! link cloud-link))))))]

    (take!
     (invoke
      c
      "cloud-link"
      {:resourceType "Parameters"
       :parameter
       [{:name "client-app"
         :valueReference {:reference (str "ClientApplication/" id)}}]})
     (fn [response]
       (reset! loading? false)
       (if (= :fhir/error (:fhir/response-type response))
         (error-notification "Failed to pull cloud link" response)
         (let [parameters    (param-parsing/parameters->map (:fhir/response response))
               cloud-link (get parameters :return)]
           (reset! link cloud-link)))))

    (fn [{:keys [id]}]
      [:div {:className "p-4 flex justify-center items-center"}
       (if @loading?
         [:> Spin {:tip "Checking link"}]
         (if (some? @link)
           [:div {:class "flex flex-1 flex-col"}
            [:div {:class "flex flex-1 flex-col"}
             [:div {:class "mb-2"}
              [:label {:class "text-blue-500 text-lg font-semibold"} "Published App"]]
             [:div {:class "flex mb-4"}
              [:div {:class "flex flex-1 mr-1"}
               [raw-text-editor {:value @link}]]
              [button {:type :primary
                       :on-click (fn [_e]
                                   (oops/ocall js/navigator "clipboard.writeText" @link)
                                   (oops/ocall message "info" "Value copied"))}
               "Copy"]]
             [:div {:class "flex"}
              [button {:type :danger
                       :on-click (fn [_e]
                                   (reset! loading? true)
                                   (take!
                                    (invoke
                                     c
                                     "cloud-undeploy"
                                     {:resourceType "Parameters"
                                      :parameter
                                      [{:name "client-app"
                                        :valueReference {:reference (str "ClientApplication/" id)}}]})
                                    (fn [response]
                                      (reset! loading? false)
                                      (if (= :fhir/error (:fhir/response-type response))
                                        (error-notification "Failed to pull cloud link" response)
                                        (let [parameters    (param-parsing/parameters->map (:fhir/response response))]
                                          (when (= "success" (:return parameters))
                                            (reset! link nil)))))))}

               "Unpublish"]
              [:div {:class "flex flex-1 justify-end"}
               [button {:type :primary
                        :on-click (fn [_e] (cloud-deploy))}
                "Update to Latest"]]]]
            [endpoint-deployment-table {:id id}]]
           [:div {:class "flex flex-1 flex-col"}
            [:div {:class "mb-4"}
             [:span {:class "text-slate-700"}
              "Deploy your application with a publically available link."]]
            [:div
             [button {:type :primary
                      :on-click (fn [_e] (cloud-deploy))}
              "Deploy as Public App"]]]))])))

(defn- deployment-modal [{:keys [modal-content]}]
  (let [c      @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        state  (r/atom (:data modal-content))]
    (fn [{:keys [modal-content]}]
      (when (not= (:id @state) (get-in modal-content [:data :id]))
        (reset! state (get modal-content :data)))
      [-modal {:id :deployment
               :modal-content modal-content
               :modal-props
               {:close-icon (r/as-element
                             [:div {:class "flex items-center justify-center h-full w-full z-30"}
                              [:> CloseOutlined]])}}
       [:div {:class "p-6"}
        [tabs {:tab-direction :left :active "public"}
         [:f> tab-pane {:key "public" :name "Public"}
          [cloud-deploy-pane {:id (get-in modal-content [:data :id])}]]
         [:f> tab-pane {:key "docker" :name "Docker"}
          [docker-pane {:id (get-in modal-content [:data :id])}]]
         [:f> tab-pane {:key "manifest" :name "App Manifest"}
          [:div {:className "p-4 flex flex-col"}
           [:div {:class "mb-4"}
            [:span {:class "text-slate-700"}
             "Download your application data. Can store your application externally or load into an existing docker container."]]
           [:div
            [button {:type     :primary
                     :on-click (fn [_e]
                                 (take!
                                  (invoke
                                   c
                                   "clientapp-download"
                                   {:resourceType "Parameters"
                                    :parameter
                                    [{:name "client-app"
                                      :valueReference {:reference (str "ClientApplication/" (get-in modal-content [:data :id]))}}]})
                                  (fn [response]
                                    (if (= :fhir/error (:fhir/response-type response))
                                      (error-notification "Failed to Download App" response)
                                      (let [bundle (:return
                                                    (param-parsing/parameters->map
                                                     (:fhir/response response)))]
                                        (file-download
                                         (.stringify js/JSON (clj->js bundle) nil 2)
                                         (str "genfhi-client-app-" (.toISOString (new js/Date.)) ".json")))))))}

             "Download Manifest"]]]]]]])))

(defn- create-resource-modal [_props]
  (fn [{:keys [modal-content]}]
    [-modal {:id :create-modal
             :modal-content modal-content}
     [:div {:class "p-6 text-slate-600"}
      [:div {:class "mb-2"}
       [:span {:class "font-semibold text-base"}
        (str "Create a new " (naming/resource-type->display-name (get-in modal-content [:data :type])))]]
      [create-resource-form {:type (get-in modal-content [:data :type])
                             :on-finish (get-in modal-content [:data :on-finish])
                             :disable-import? (get-in modal-content [:data :disable-import?])}]]]))

(defn- invite-user-modal [_props]
  (let [c      @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        state  (r/atom {})
        loading? (r/atom false)]
    (fn [{:keys [modal-content]}]
      [-modal {:id :invite-user
               :modal-content modal-content}
       [:div {:class "p-6 text-slate-600"}
        [:div {:class "mt-2 mb-1"}
         [:span {:class "text-lg font-semibold"}
          "Invite User"]]
        [:div {:class "mb-2"}
         [raw-text-editor
          {:label "User Email"
           :input-type "email"
           :value (:email @state)
           :on-change (fn [e]
                        (swap!
                         state
                         (fn [s]
                           (assoc s :email
                                  (.. e -target -value)))))}]]
        [:div {:class "flex flex-1 items-center"}
         [:> Spin {:spinning @loading?}
          (r/as-element
           [button {:type :primary
                    :on-click (fn [_e]
                                (reset! loading? true)
                                (take!
                                 (invoke
                                  c
                                  workspace-ops/invite-user
                                  {:email (:email @state)})
                                 (fn [response]
                                   (reset! loading? false)
                                   (if (= :fhir/error (:fhir/response-type response))
                                     (error-notification
                                      "Failed to invite user"
                                      response)
                                     (do

                                       (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil])
                                       (.open
                                        notification
                                        (clj->js
                                         {:icon (r/as-element [:> CheckCircleOutlined {:style {:color "green"}}])
                                          :message "User invited"
                                          :description (str "User with email '" (:email @state) "'. Has been created. Have the new user check their inbox for an email to set their password. Note the email may take a few minutes to send.")}))
                                       (reset! state {}))))))}

            "Invite"])]]]])))

(defn modal []
  (let [modal-content @(re-frame/subscribe [:gen-fhi.db.subs.ui/active-modal])]
    (match (:type modal-content)
      :create-modal        [create-resource-modal  {:modal-content modal-content}]
      :code-editor         [code-editor-modal      {:modal-content modal-content}]
      :patch-editor        [patch-editor-modal     {:modal-content modal-content}]
      :deployment          [deployment-modal       {:modal-content modal-content}]
      (:or
       :handler-editor
       :handler-add)       [handler-modal          {:modal-content modal-content}]
      :column-editor       [column-editor-modal    {:modal-content modal-content}]
      :column-add          [column-add-modal       {:modal-content modal-content}]
      :operation-invoke    [operation-invoke-modal {:modal-content modal-content}]
      :invite-user         [invite-user-modal      {:modal-content modal-content}]
      :else                [:<>])))
