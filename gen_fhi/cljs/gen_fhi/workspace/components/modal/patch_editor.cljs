(ns gen-fhi.workspace.components.modal.patch-editor
  (:require [clojure.core.match :refer [match]]
            [reagent.core :as r]
            [re-frame.core :as re-frame]

            ["antd" :refer [Modal]]

            [gen-fhi.workspace.constants.extensions :refer [expression-value-extension-url]]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.workspace.components.terminology :refer [code-select]]
            [gen-fhi.workspace.components.editors.core :refer [dynamic-expression] :as editors]
            [gen-fhi.fhirpath.core :as fp]))

(defn patch-editor-modal [{:keys [modal-content]}]
  (let [row (r/atom (get-in modal-content [:data :row]))]
    (fn []
      (let [{:keys [state-path p on-change on-delete]} (get modal-content :data)]
        [:> Modal {:visible  (= (:type modal-content) :patch-editor)
                   :onCancel #(re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal nil])
                   :header   nil
                   :footer   nil}
         [:div {:class "p-8"}
          [:label {:className "text-xs"} "Operation"]
          [code-select
           {:hide-label? true
            :placeholder "Choose an operation"
            :value-set    "http://genfhi.com/jsonpatch-ops"
            :value       (:op @row)
            :on-change (fn [code]
                         (reset! row {:op code}))}]
          (match (:op @row)
            (:or
             "add"
             "replace"
             "test")     [:<>
                          [:label {:className "text-xs"} "Path"]
                          [dynamic-expression {:p p
                                               :height "40px"
                                               :language "application/x-fhir-query"
                                               :state-path state-path
                                               :value (let [expression (first
                                                                        (fp/evaluate
                                                                         "$this.path.extension.where(url=%extUrl).value"
                                                                         @row
                                                                         {:options/variables
                                                                          {:extUrl expression-value-extension-url}}))]
                                                        expression)
                                               :on-change
                                               (fn [text _view-update]
                                                 (swap!
                                                  row
                                                  #(assoc-in
                                                    %
                                                    [:_path :extension]
                                                    [{:url             expression-value-extension-url
                                                      :valueExpression {:language "application/x-fhir-query"
                                                                        :extension  [(editors/->post-processing-extension "string")]
                                                                        :expression text}}])))}]
                          [:label {:className "text-xs"} "Value"]
                          [dynamic-expression {:p p
                                               :height "40px"
                                               :language "application/x-fhir-query"
                                               :state-path state-path
                                               :value (let [expression (first
                                                                        (fp/evaluate
                                                                         "$this.value.extension.where(url=%extUrl).value"
                                                                         @row
                                                                         {:options/variables
                                                                          {:extUrl expression-value-extension-url}}))]
                                                        expression)
                                               :on-change
                                               (fn [text _view-update]
                                                 (swap!
                                                  row
                                                  #(assoc-in
                                                    %
                                                    [:_value :extension]
                                                    [{:url             expression-value-extension-url
                                                      :valueExpression {:language "application/x-fhir-query"
                                                                        :extension  [(editors/->post-processing-extension "json")]
                                                                        :expression text}}])))}]]
            "remove"     [:<>
                          [:label {:className "text-xs"} "Path"]
                          [dynamic-expression {:p p
                                               :language "application/x-fhir-query"
                                               :state-path state-path
                                               :value (let [expression (first
                                                                        (fp/evaluate
                                                                         "$this.path.extension.where(url=%extUrl).value"
                                                                         @row
                                                                         {:options/variables
                                                                          {:extUrl expression-value-extension-url}}))]
                                                        expression)
                                               :on-change
                                               (fn [text _view-update]
                                                 (swap!
                                                  row
                                                  #(assoc-in
                                                    %
                                                    [:_path :extension]
                                                    [{:url             expression-value-extension-url
                                                      :valueExpression {:language "application/x-fhir-query"
                                                                        :extension  [(editors/->post-processing-extension "string")]
                                                                        :expression text}}])))}]]
            (:or
             "copy"
             "move")     [:<>
                          [:label {:className "text-xs"} "From"]
                          [dynamic-expression {:p p
                                               :height "40px"
                                               :language "application/x-fhir-query"
                                               :state-path state-path
                                               :value (let [expression (first
                                                                        (fp/evaluate
                                                                         "$this.from.extension.where(url=%extUrl).value"
                                                                         @row
                                                                         {:options/variables
                                                                          {:extUrl expression-value-extension-url}}))]
                                                        expression)
                                               :on-change
                                               (fn [text _view-update]
                                                 (swap!
                                                  row
                                                  #(assoc-in
                                                    %
                                                    [:_from :extension]
                                                    [{:url             expression-value-extension-url
                                                      :valueExpression {:language "application/x-fhir-query"
                                                                        :extension  [(editors/->post-processing-extension "json")]
                                                                        :expression text}}])))}]
                          [:label {:className "text-xs"} "Path"]
                          [dynamic-expression {:p p
                                               :height "40px"
                                               :language "application/x-fhir-query"
                                               :state-path state-path
                                               :value (let [expression (first
                                                                        (fp/evaluate
                                                                         "$this.path.extension.where(url=%extUrl).value"
                                                                         @row
                                                                         {:options/variables
                                                                          {:extUrl expression-value-extension-url}}))]
                                                        expression)
                                               :on-change
                                               (fn [text _view-update]
                                                 (swap!
                                                  row
                                                  #(assoc-in
                                                    %
                                                    [:_path :extension]
                                                    [{:url             expression-value-extension-url
                                                      :valueExpression {:language "application/x-fhir-query"
                                                                        :extension  [(editors/->post-processing-extension "string")]
                                                                        :expression text}}])))}]]

            :else [:<>])
          [:div {:className "mt-2 flex justify-end"}
           (when on-delete
             [button {:className "mr-2"
                      :type :danger
                      :on-click (fn [_e]
                                  (on-delete)
                                  (re-frame/dispatch
                                   [:gen-fhi.db.events.ui/set-active-modal
                                    nil]))}
              "Remove"])
           [button {:on-click (fn [_e]
                                (on-change @row)
                                (re-frame/dispatch
                                 [:gen-fhi.db.events.ui/set-active-modal
                                  nil]))}
            "Done"]]]]))))

