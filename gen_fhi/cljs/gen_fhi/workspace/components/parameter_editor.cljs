(ns gen-fhi.workspace.components.parameter-editor
  (:require [oops.core :as oops]
            [clojure.string :as string]
            [reagent.core :as r]

            [antd :refer [AutoComplete Dropdown]]
            ["@ant-design/icons" :refer [DownOutlined]]

            [gen-fhi.workspace.constants.extensions :refer [expression-value-extension-url]]
            [gen-fhi.workspace.components.editors.core :as editors :refer [dynamic-expression]]
            [gen-fhi.workspace.components.terminology :refer [open-code-select]]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.patch-building.path :as path]))

(defn- change-parameter-value [parameter fhir-type]
  (let [filtered-parameter (into {}
                                 (filter
                                  (fn [[k _v]] (and (not (string/starts-with? (name k) "_value"))
                                                    (not (string/starts-with? (name k) "value"))))
                                  parameter))
        return             (if fhir-type
                             (assoc
                              filtered-parameter
                              (keyword (str "value" (string/capitalize fhir-type)))
                              nil)
                             filtered-parameter)]
    return))

(defn parameter-editor [{:keys [ctx-path state-path depth use readonly? parameter-definitions path on-change parameters]}]
  (let [value    (get-in parameters path)
        get-definition (fn [name]
                         (first (filter #(=  (:name %) name) parameter-definitions)))
        value-definition (get-definition (get value :name))
        children (get value :part)
        valueKey (or (first (filter #(string/starts-with? (name %) "value")
                                    (keys value)))
                     :valueString)]
    [:div {:class "ml-2 mt-1"}
     [:div {:class "flex"}
      [:div {:class "flex flex-1 mr-1"}
       [:> AutoComplete {:showSearch true
                         :className  "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded "
                         :value      (get value :name)
                         :options    (map
                                      (fn [p] {:value (:name p)})
                                      parameter-definitions)
                         :placeholder "Parameter name"
                         :filterOption (fn [input option]
                                         (string/includes?
                                          (.toLowerCase (oops/oget option "value"))
                                          (.toLowerCase input)))
                         :on-change (fn [name]
                                      (let [name-definition (get-definition name)]
                                        (on-change
                                         (assoc-in
                                          parameters
                                          path
                                          (cond-> (assoc value :name name)
                                            (some? (get name-definition :type))
                                            (change-parameter-value (get name-definition :type)))))))}]]
      (when (and (not readonly?)
                 (empty? children)
                 (empty? (get value-definition :part)))
        [:div {:class "flex flex-1 mr-1"}
         [open-code-select {:select-props {:disabled (or readonly?
                                                         (some? (get value-definition :type)))}
                            :value-set  "http://hl7.org/fhir/ValueSet/all-types"
                            :value    (string/replace (name valueKey) "value" "")
                            :on-change (fn [type]
                                         (on-change
                                          (assoc-in
                                           parameters
                                           path
                                           (change-parameter-value value type))))}]])
      (when (and (empty? children)
                 (empty? (get value-definition :part)))
        [:div {:class "h-10 w-32 flex flex-1 mr-1 "}
         [dynamic-expression {:p         ctx-path
                              :height    "40px"
                              :language "application/x-fhir-query"
                              :state-path state-path
                              :value (let [expression (first
                                                       (fp/evaluate
                                                        "$this.value.extension.where(url=%extUrl).value"
                                                        value
                                                        {:options/variables
                                                         {:extUrl expression-value-extension-url}}))]
                                       expression)
                              :on-change
                              (fn [expression _view-update]
                                ;; (let [json-form (try
                                ;;                   (js->clj (.parse js/JSON expression)
                                ;;                            :keywordize-keys true)
                                ;;                   (catch :default _e
                                ;;                     nil))]
                                (on-change
                                 (cond-> (assoc-in parameters
                                                   (concat path [(keyword (str "_" (name valueKey))) :extension])
                                                   [{:url             expression-value-extension-url
                                                     :valueExpression {:language "application/x-fhir-query"
                                                                       :extension  [(editors/->post-processing-extension "json")]
                                                                       :expression expression}}]))))}]])

                                     ;; (some? json-form) (assoc-in (concat path [valueKey])
                                     ;;                             json-form)))))}]])
      (when (not readonly?)
        [:div
         [:> Dropdown {:placement "bottomRight"
                       :trigger   "click"
                       :overlay   (r/as-element
                                   [:div {:class "rounded shadow-lg border p-1 bg-white border-slate-100"}
                                    [:div {:class " p-1 cursor-pointer hover:bg-blue-100"
                                           :on-click
                                           (fn [_e]
                                             (let [index       (last path)
                                                   parent-path (subvec path 0 (-  (count path) 1))]
                                               (on-change
                                                (update-in
                                                 parameters
                                                 parent-path
                                                 (fn [params]
                                                   (let [pre   (subvec params 0 index)
                                                         post  (subvec params (+ 1 index) (count params))]
                                                     (into [] (concat pre post))))))))}
                                     [:span {:class "text-red-500"}
                                      "Delete"]]
                                    [:div {:class " p-1 cursor-pointer hover:bg-blue-100"
                                           :on-click (fn [_e]
                                                       (on-change
                                                        (-> (update-in
                                                             parameters
                                                             path
                                                             (fn [v]
                                                               (dissoc v valueKey)))
                                                            (update-in
                                                             (conj path :part)
                                                             (fn [v]
                                                               (into
                                                                []
                                                                (concat
                                                                 v
                                                                 [{}])))))))}
                                     [:span {:class "text-blue-500"}
                                      "Add child"]]])}
          [:button {:className (str "h-full " "border-slate-300 hover:bg-transparent bg-transparent text-gray-600 hover:border-blue-400 hover:text-blue-400 "
                                    "align-middle rounded border inline-block text-center cursor-pointer py-2 px-4 font-semibold disabled:bg-gray-300")
                    :type     :secondary}
           [:> DownOutlined]]]])]
     (when (seq children)
       (map-indexed
        (fn [i _child-param]
          (let [path (into [] (concat path [:part i]))]
            ^{:key (str path)}
            [parameter-editor
             {:depth (inc depth)
              :use use
              :readonly? readonly?
              :parameter-definitions (get value-definition :part)
              :on-change on-change
              :parameters parameters
              :path path}]))
        children))]))

(defn parameters-editor [{:keys [ctx-path state-path use readonly? operation-definition on-change value]
                          :or {ctx-path (path/->path "Parameter" "fake")}}]
  (let [on-change (fn [parameters]
                    (on-change
                     (assoc parameters :resourceType "Parameters")))]
    [:div {:class "-ml-2 flex flex-1 flex-col"}
     (map-indexed
      (fn [i _parameter]
        (let [path [:parameter i]]
          ^{:key (str path)}
          [parameter-editor
           {:depth                 0
            :ctx-path              ctx-path
            :state-path            state-path
            :use                   use
            :readonly?             readonly?
            :parameter-definitions (filter #(= use (:use %))
                                           (get operation-definition :parameter))
            :on-change             on-change
            :parameters            value
            :path                  path}]))
      (get value :parameter))

     (when (not readonly?)
       [:div {:class "pl-2 mt-1 flex flex-1"}
        [button {:className "w-full"
                 :type      :secondary
                 :on-click
                 (fn [_e]
                   (on-change
                    (update value :parameter #(into
                                               []
                                               (concat % [{}])))))}

         "Add Parameter"]])]))
