(ns gen-fhi.workspace.components.popup-menu
  (:require [reagent.core :as r]
            [oops.core :as oops]))

(defn popup-menu [_props _el]
  (let [trigger-ref             (r/atom nil)
        show?                   (r/atom false)
        ;; Used when clicking outside to remove hte popup.
        remove-on-click-outside (fn [_e]
                                  (reset! show? false))]
    (r/create-class
     {:component-did-mount
      (fn []
        (.addEventListener js/window "click" remove-on-click-outside))

      :component-will-unmount
      (fn []
        (.removeEventListener js/window "click" remove-on-click-outside))

      :reagent-render
      (fn [{:keys [class style trigger position menu]} element-trigger]
        (let [bounds (when @trigger-ref (oops/ocall @trigger-ref "getBoundingClientRect"))]
          [:div {:style style
                 :on-click (fn [e]
                             ;; Stop propegation so doesn't hide itself on-click (see remove-on-click-outside).
                             (.stopPropagation e)
                             (when (= trigger :click)
                               (swap! show? (fn [show]
                                              (not show)))))

                 :on-hover (fn [_e]
                             (when (= trigger :hover)
                               (reset! show? true)))

                 :on-blur (fn [_e]
                            (when (= trigger :hover)
                              (reset! show? false)))
                 :class class}
           [:div {:ref (fn [el]
                         (when (nil? @trigger-ref)
                           (reset! trigger-ref el)))}
            element-trigger
            (when @show?
              [:div {:class "absolute bg-white"
                     :style {:z-index "10"
                             :left "4px"
                             :top (- (oops/oget bounds "height")
                                     2)}}
               menu])]]))})))

