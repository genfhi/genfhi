(ns gen-fhi.workspace.components.query
  (:require [re-frame.core :as re-frame]))

(defn- ->request [resource-type parameters]
  (if (some? resource-type)
    {:fhir/level        :fhir/type
     :fhir/request-type :fhir/search
     :fhir/type         resource-type
     :fhir/parameters   parameters}
    {:fhir/level        :fhir/system
     :fhir/request-type :fhir/search
     :fhir/parameters   parameters}))

(defn query [{:keys [resource-type parameters trigger-on-mount?]} child-renderer]
  (let [query           (fn []
                          (re-frame/dispatch
                           [:gen-fhi.db.events.operation/trigger-cached-query
                            (->request resource-type parameters)]))]
    
    (when trigger-on-mount? (query))
    (fn [{:keys [resource-type parameters]}]
      (let [request  (->request resource-type parameters)
            response @(re-frame/subscribe [:gen-fhi.db.subs.operation/get-cached-query request])]
        [child-renderer {:re-query   query
                         :result     (:fhir/response response)
                         :is-loading (not (some? response))}]))))
