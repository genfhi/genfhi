(ns gen-fhi.workspace.components.radio
  (:require [antd :refer [Spin]]))

(defn pill [{:keys [options on-change value loading?]}]
  [:div {:className "flex border border-y-0 border-r-0 border-gray-300"}
   (if loading?
     [:div {:class "border border-l-0 flex-1 flex justify-center items-center bg-gray-100 text-gray-900"}
      [:> Spin {:wrapperClassName "flex justify-center items-center"
                :size "small"}]]
     (map
      (fn [option]
        ^{:key (:display option)}
        [:button {:className (str
                              "flex flex-1 justify-center text-xs p-1 border-gray-300 border border-l-0 hover:bg-blue-500 hover:text-white "
                              (if (= value (:value option))
                                "bg-blue-500 text-white"
                                "bg-gray-100 text-gray-900"))
                  :on-click (partial on-change (:value option))}
         (:display option)])
      options))])
