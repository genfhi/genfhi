(ns gen-fhi.workspace.components.reference
  (:require [clojure.string :as string]
            [re-frame.core :as re-frame]
            [cljs.core.async :refer [take!]]
            [reagent.core :as r]

            [antd :refer [Select]]

            [gen-fhi.patch-building.path :as path]
            [gen-fhi.fhir-client.protocols :refer [search]]))

(defn reference->path [reference]
  (when-let [reference-string (get reference :reference)]
    (let [[resource-type id] (string/split reference-string #"/")]
      (path/->path resource-type id))))

(defn reference-select [{:keys [resource-type reference search-key]}]
  (let [loading?     (r/atom false)
        query-result (r/atom [])
        c            @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        on-search    (fn [value]
                       (reset! loading? true)
                       (take!
                        (search
                         c
                         resource-type
                         (assoc {} search-key value))
                        (fn [{:keys [:fhir/response]}]
                          (reset!
                           loading?
                           false)
                          (reset!
                           query-result
                           response))))]
    (on-search "")

    (fn [{:keys [select-props]}]
      (let [selected-resource (when (some? (:reference reference))
                                @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (reference->path reference)]))]
        [:> Select (merge {:defaultValue (or (get selected-resource search-key) (get reference :reference nil))
                           :on-search    on-search
                           :filterOption (fn [_input _option] true) ;; Always show filter comes from api query
                           :style {:min-width "200px"}
                           :showSearch   true
                           :loading      @loading?
                           :className   "flex items-center pl-1 pr-1 w-full mr-1 border border-slate-300 hover:border-blue-400 rounded"
                           :placeholder  "Search for reference"}
                          select-props)

         (when selected-resource
           [:> (.-Option Select) {:key (:id selected-resource) :value (:reference reference)}
            (or (get selected-resource search-key) (:id selected-resource))])

         (->> @query-result
              (filter (fn [resource] (not= (get resource :id) (get selected-resource :id))))
              (map
               (fn [resource]
                 [:> (.-Option Select)
                  {:key (:id resource)
                   :value (str (:resourceType resource) "/" (:id resource))}
                  (or (get resource search-key) (:id resource))])))]))))


