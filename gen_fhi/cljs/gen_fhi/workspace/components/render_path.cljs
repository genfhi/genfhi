(ns gen-fhi.workspace.components.render-path
  (:require [re-frame.core :as re-frame]
            [gen-fhi.workspace.utilities.mutations :as mutations]))

(defn editor-for-path [{:keys [path]}
                       editor]
  (let [value      @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value path])
        on-change  (fn [value]
                     (mutations/replace
                      path
                      value))
        on-remove  (fn []
                     (mutations/remove path))]
    (editor value {:on-change on-change
                   :on-remove on-remove})))
