(ns gen-fhi.workspace.components.resource-header
  (:require [cljs.core.async :as a]
            [clojure.core.match :refer [match]]
            [reagent.core :as r]
            [re-frame.core :as re-frame]

            [antd :refer [Popconfirm notification]]
            ["@ant-design/icons" :refer [ExclamationCircleOutlined
                                         CheckCircleOutlined
                                         QuestionCircleOutlined]]

            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.fhir-client.protocols :as fhir-client]))

(defn resource-header [{:keys [new? resource on-delete on-update]}]
  (let [client           @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])]
    
    
    [:div {:class "flex flex-1 justify-end"}
     (when (not new?)
       [:> Popconfirm
        {:placement "bottomRight"
         :icon      (r/as-element [:> QuestionCircleOutlined {:style {:color "red"}}])
         :title     "Are you sure you want to delete this resource?"
         :onConfirm (fn [_]
                      (a/take!
                       (fhir-client/delete
                        client
                        (get resource :resourceType)
                        (get resource :id))
                       (fn [_res]
                         (when on-delete
                           (on-delete)))))

         :okText "Yes"
         :okButtonProps {:className "bg-blue-500 hover:bg-blue-600 border-0"}
         :cancelText "No"}
        [:button {:className "mr-2 bg-transparent hover:bg-transparent text-red-400 hover:text-red-500 border-red-400 hover:border-red-500  align-middle rounded border inline-block text-center cursor-pointer py-2 px-4 font-semibold "}
         "Delete"]])
     [button {:style {:width "100px"}
              :on-click (fn [_e]
                          (println client)
                          (a/take!
                           (if new?
                             (fhir-client/create
                              client
                              resource)
                             (fhir-client/update
                              client
                              (get resource :resourceType)
                              (get resource :id)
                              resource))
                           (fn [res]
                             (when on-update
                               (on-update res))
                             (.open
                              notification
                              (clj->js
                               (match (:fhir/response-type res)
                                 :fhir/error
                                 (reduce
                                  (fn [notification issue]
                                    (assoc
                                     notification
                                     :description
                                     (str (:description notification) " " (:diagnostics issue))))
                                  {:icon (r/as-element [:> ExclamationCircleOutlined {:style {:color "red"}}])
                                   :message "Edit rejected"
                                   :description ""}
                                  (:fhir/issue res))
                                 :else {:icon (r/as-element [:> CheckCircleOutlined {:style {:color "green"}}])
                                        :message (str (:resourceType resource) " Saved")}))))))}

      (if new? "Create" "Save")]]))
