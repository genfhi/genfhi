(ns gen-fhi.workspace.components.resource-table
  (:require [clojure.core.match :refer [match]]
            [reagent.core :as r]
            [re-frame.core :as re-frame]
            [accountant.core :as accountant]
            [cljs.core.async :refer [take!]]
            [oops.core :as oops]
            [goog.object :as gobj]

            ["dayjs" :as dayjs]
            ["dayjs/plugin/relativeTime" :as relative-time]
            ["@ant-design/icons" :refer [QuestionCircleOutlined]]
            [antd :refer [Table Popconfirm]]

            [gen-fhi.workspace.utilities.resources :as utilities]
            [gen-fhi.workspace.router :as router]
            [gen-fhi.fhir-client.protocols :refer [delete]]
            [gen-fhi.workspace.utilities.naming :as naming]))

(set! *warn-on-infer* true)

(.extend ^js/dayjs dayjs relative-time)

(def date-format "YYYY-MM-DDThh:mm:ss.SSSZ")

(defn- ->resource-columns [types]
  (match (set types)
    #{"OperationDefinition"}   [{:title "Name" :dataIndex "name" :ellipsis true :width "30%"}
                                {:title "Description" :dataIndex "description" :ellipsis true :width "40%"}]
    #{"Endpoint"}              [{:title "Name" :dataIndex "name" :ellipsis true}
                                {:title "Address" :dataIndex "address" :ellipsis true :width "40%"}
                                {:title "method"  :render (fn [_ record]
                                                            [:render]
                                                            (utilities/endpoint->auth-type
                                                             (js->clj
                                                              record
                                                              :keywordize-keys true)))}]
    #{"ValueSet"}              [{:title "Name" :dataIndex "name" :ellipsis true}
                                {:title "Url" :dataIndex "url" :ellipsis true :width "40%"}]
    #{"CodeSystem"}            [{:title "Name" :dataIndex "name" :ellipsis true}
                                {:title "Url" :dataIndex "url" :ellipsis true :width "40%"}]
    #{"ValueSet" "CodeSystem"} [{:title "Name" :dataIndex "name" :ellipsis true}
                                {:title "Url" :dataIndex "url" :ellipsis true :width "40%"}
                                {:title "Type" :dataIndex "resourceType" :render (fn [_ record]
                                                                                   (r/as-element
                                                                                    [:span
                                                                                     (naming/resource-type->display-name
                                                                                      (oops/oget record "resourceType"))]))}]
    #{"ClientApplication"}     [{:title "Name" :dataIndex "name" :ellipsis true :width "30%"}
                                {:title "Description" :dataIndex "description" :ellipsis true :width "40%"}]
    
    #{"Questionnaire"}         [{:title "Title" :dataIndex "title" :ellipsis true :width "30%"}
                                {:title "Description" :dataIndex "description" :ellipsis true :width "40%"}]
    
    #{"Organization"}          [{:title "Name" :dataIndex "name" :ellipsis true}]
    :else                      [{:title "Name" :render (fn [_ record]
                                                         (or (gobj/get record "name")
                                                             (gobj/get record "title")))
                                 :ellipsis true
                                 :width "40%"}
                                {:title "Type" :dataIndex "resourceType" :render (fn [_ record]
                                                                                   (r/as-element
                                                                                    [:span
                                                                                     (naming/resource-type->display-name
                                                                                      (oops/oget record "resourceType"))]))}]))

(defn resource-table [{:keys [types result on-delete is-loading on-row]}]
  (let [params @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])
        client @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])]
    [:div {:class "border"}
     [:> Table {:size  "small"
                :onRow (fn [record _rowIndex]
                         #js{:onClick
                             (fn []
                               (if on-row
                                 (on-row (js->clj record :keywordize-keys true))
                                 (accountant/navigate!
                                  (router/path-for
                                   :instance
                                   (merge
                                    params
                                    {:modifiers ""
                                     :resource-type (oops/oget record "resourceType")
                                     :id (oops/oget record "id")})))))})
                :loading    is-loading
                :rowClassName "resource-table-row"
                :dataSource (sequence
                             (comp
                              (map #(assoc % :key (:id %)))
                              (map #(update-in % [:meta :lastUpdated] (fn [last-updated]
                                                                        (let [^js/dayjs dayjs-date (dayjs last-updated date-format)]
                                                                          (.fromNow dayjs-date))))))

                             result)
                :columns    (concat
                             (->resource-columns types)
                             [{:title "Last Updated" :dataIndex ["meta" "lastUpdated"]}
                              {:title "" :align "center"
                               :key   "action"
                               :render (fn [_ record]
                                         (r/as-element
                                          [:div {:on-click (fn [^js/Event e] (.stopPropagation e))}
                                           [:> Popconfirm
                                            {:placement "bottomRight"
                                             :icon      (r/as-element [:> QuestionCircleOutlined {:style {:color "red"}}])
                                             :title     (str "Are you sure you want to delete this " (oops/oget record "resourceType") "?")

                                             :onConfirm (fn [_]
                                                          (take!
                                                           (delete
                                                            client
                                                            (oops/oget record "resourceType")
                                                            (oops/oget record "id"))
                                                           (fn [_]
                                                             (on-delete))))

                                             :okText "Yes"
                                             :okButtonProps {:className "bg-blue-500 hover:bg-blue-600 border-0"}
                                             :cancelText "No"}
                                            [:a
                                             {:className "hover:text-red-500 text-red-400"}
                                             "Delete"]]]))}])}]]))
