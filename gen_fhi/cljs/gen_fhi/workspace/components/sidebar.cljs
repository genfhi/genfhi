(ns gen-fhi.workspace.components.sidebar)

(defn sidebar [{:keys [value items on-click]}]
  [:ul
   (map
    (fn [item]
      ^{:key (get item :display)}
      [:li {:class (str "text-slate-600 border-r-0 border-t-0 border-b-0 border p-2 cursor-pointer "
                        (when (= value (:value item))
                          "border-blue-500 text-blue-600"))
            :on-click (partial on-click item)}
       (:display item)])
    items)])
