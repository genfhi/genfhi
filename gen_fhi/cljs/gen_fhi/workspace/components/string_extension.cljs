(ns gen-fhi.workspace.components.string-extension
  (:require
   [clojure.string :as string]
   [gen-fhi.fhirpath.core :as fp]
   [gen-fhi.workspace.components.editors.core :refer [raw-text-editor]]))

(defn- modify-extension [idx value new-extension]
  (let [ext-url (get new-extension :url)
        ext-loc (first
                 (fp/locations "$this.extension.where(url=%extUrl)[%idx]"
                               value
                               {:options/variables {:idx idx
                                                    :extUrl ext-url}}))]
    (if (some? ext-loc)
      (assoc-in value ext-loc new-extension)
      (update value :extension (fn [extensions]
                                 (if (some? extensions)
                                   (conj extensions new-extension)
                                   [new-extension]))))))

(defn- is-encrypted? [expression root options]
  (let [value           (first (fp/evaluate expression root options))
        expression      (str expression ".extension.where(url=%encUrl).value")
        extension-value (first (fp/evaluate
                                expression
                                root
                                (assoc-in options [:options/variables :encUrl]
                                          "https://genfhi.com/Extension/encrypt-value")))]
    (and (some? value)
         (= value extension-value))))

(defn string-extension-editor [{:keys [value
                                       on-change
                                       type
                                       nth
                                       extension-url
                                       encrypted?
                                       label
                                       placeholder]
                                :or {nth 0}}]
  (let [value-key (keyword (str "value" (string/capitalize type)))]
    [raw-text-editor
     {:label label
      :placeholder placeholder
      :value   (if (is-encrypted?
                    "$this.extension.where(url=%extUrl)[%idx].value"
                    value
                    {:options/variables {:idx nth
                                         :extUrl extension-url}})
                 "--encrypted--"
                 (first
                  (fp/evaluate
                   "$this.extension.where(url=%extUrl)[%idx].value"
                   value
                   {:options/variables {:idx nth
                                        :extUrl extension-url}})))
      :on-change (fn [e]
                   (on-change
                    (modify-extension
                     nth
                     value
                     (let [extension (assoc {:url extension-url} value-key (.. e -target -value))]
                       (if encrypted?
                         ;; Associate encryption extension.
                         (assoc extension
                                (keyword (str "_" (name value-key)))
                                {:extension
                                 [{:url "https://genfhi.com/Extension/encrypt-value"}]})
                         extension)))))}]))
