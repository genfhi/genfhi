(ns gen-fhi.workspace.components.table-editor
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]
            [oops.core :as oops]

            ["react-beautiful-dnd" :refer [DragDropContext Droppable Draggable]]

            [gen-fhi.workspace.components.popup-menu :refer [popup-menu]]
            [gen-fhi.workspace.icons.core :as icons]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.utilities.mutations :as mutations]))

(def td-col-class "border border-t-0 border-l-0 border-r-0 focus:border-blue-400 border-transparent bg-transparent px-2 py-1 w-full")

(defn footer-item
  ([children]
   [footer-item {} children])
  ([{:keys [class on-click helper-text]} children]
   [:span {:title helper-text
           :on-click on-click
           :class (str "font-semibold text-gray-600 cursor-pointer " class)}
    children]))

(defn table-editor [{:keys [footer active-row set-active-row columns p row-menu]}]
  (let [values         @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        blur-focused-el (fn [] (when-let [focused-el (.querySelector js/document ":focus")]
                                 (.blur focused-el)))]

    [:div {:class "w-full"}
     [:table {:class "w-full border-separate"}
      [:thead {:class "bg-gray-100 p-1 text-gray-800"}
       [:tr
        (doall
         (map-indexed
          (fn [i column]
            ^{:key (get column :name)}
            [:th {:class (str
                          "border border-slate-300 p-1"
                          (if (= i (- (count columns) 1))
                            " text-center"
                            " text-left"))}
             (get column :name)])
          columns))]]

      [:> DragDropContext
       {:on-drag-end (fn [result]
                       (when-let [destination (.-destination result)]
                         (let [source (.-source result)
                               path       (path/descend p (.-index destination))
                               from       (path/descend p (.-index source))]
                           (re-frame/dispatch-sync
                            [:gen-fhi.db.events.mutate/mutate
                             {:type :move
                              :from from
                              :path path}])
                           (blur-focused-el)
                           (set-active-row path))))}
       [:> Droppable {:droppableId "droppable"}
        (fn [provided _snapshot]
          (r/as-element
           [:tbody (js->clj
                    (.assign js/Object
                             (oops/oget provided "droppableProps")
                             #js {:ref (oops/oget provided "innerRef")}))
            (map-indexed
             (fn [row-idx val]
               (let [row-path (path/descend p row-idx)]
                 [:> Draggable {:index       row-idx
                                :draggableId (str (path/descend p row-idx))
                                :key         (str (path/descend p row-idx))}
                  (fn [provided snapshot]
                    (r/as-element
                     ^{:key row-idx}
                     [:tr (merge {:class (str "group "
                                              (when (= row-path active-row) "outline outline-2 outline-blue-300 ")
                                              (cond
                                                (oops/oget snapshot "isDragging") "bg-blue-100 table"
                                                :else "bg-white"))

                                  :on-click (partial set-active-row row-path)}
                                 (js->clj (oops/oget provided "draggableProps"))
                                 {:ref (oops/oget provided "innerRef")})
                      (doall
                       (map-indexed
                        (fn [col-idx column]

                          ^{:key (get column :name)}
                          [:td {:style {:width
                                        (str (get column :width (/ 100 (count columns)))  "%")}
                                :class (str "relative border border-slate-300")}
                           [:<>
                            (when (= col-idx 0)
                              [popup-menu {:class (str
                                                   (if (= active-row row-path) "block " "hidden ")
                                                   "absolute group-hover:block group-active:block")
                                           :style {:top "calc(50% - 10px)"
                                                   :left "-9px"
                                                   :z-index 10}
                                           :trigger :click
                                           :menu    [:div {:class "rounded border border-slate-300 bg-white overflow-hidden w-32"}
                                                     [:div {:on-click (fn [_e]
                                                                        (mutations/remove
                                                                         row-path))

                                                            :class " cursor-pointer text-slate-600 p-2 border-bottom-1 border-slate-300 hover:bg-gray-100"}
                                                      [:span {:class "text-red-400 mr-2"} "Delete"]
                                                      [:span {:style {:font-size "8px"} :class "text-gray-300"} "Ctrl + Delete"]]]}
                               [:div (merge
                                      (js->clj (oops/oget provided "dragHandleProps"))
                                      {:on-click (fn [_e]
                                                   (blur-focused-el)
                                                   (set-active-row row-path))
                                       :class "flex items-center justify-center border border-slate-300 active:bg-gray-100 hover:bg-gray-100 bg-white p-1 "
                                       :style {:z-index "10"
                                               :width "18px"
                                               :height "18px"}})

                                [icons/drag-handle]]])
                            [:div
                             ((get column :render)
                              {:data val
                               :p (path/descend p row-idx)})]]])
                        columns))]))]))
             values)
            (oops/oget provided "placeholder")]))]]]
     (when footer
       [:div {:class "mt-2"}
        footer])]))
