(ns gen-fhi.workspace.components.tabs
  (:require [reagent.core :as r]
            [oops.core :as oops]

            ["react" :refer [useContext createContext]]))

(def TabContext (createContext nil))

(defn tab-pane [{:keys [key]} pane]
  (let [active-tab (useContext TabContext)
        is-active? (= key active-tab)]
    (when is-active?
      (if (fn? pane)
        [pane]
        pane))))

(defn tab-comp->props [tab-comp]
  (nth tab-comp 2))

(defn tabbar
  ([tabs]
   [:f> tabbar {} tabs])
  ([_tabbar-props tabs]
   (fn [{:keys [tab-right-content tab-left-content tab-direction on-change]}]
     (let [active-tab (useContext TabContext)
           direction-class (cond
                             (= :center tab-direction) "justify-center"
                             (= :left   tab-direction) ""
                             (= :right  tab-direction) "justify-end"
                             :else                   "justify-center")
           tab-props (map
                      tab-comp->props
                      tabs)]
       [:div {:class "bg-white/95 flex sticky top-0 z-10 border border-t-0 border-l-0 border-r-0 border-gray-200 "}
        (when (some? tab-left-content)
          (if (fn? tab-left-content)
            [tab-left-content]
            tab-left-content))
        [:div {:className (str
                           "pr-2 pl-2 flex flex-1 "
                           direction-class
                           " items-center ")}
         (map
          (fn [tab-prop]
            ^{:key (:key tab-prop)}
            [:div {:on-click (fn [_] (on-change (:key tab-prop)))
                   :className (str "hover:text-gray-700 p-1 cursor-pointer font-semibold mr-2 border-b-2 "
                                   (if (= active-tab (:key tab-prop))
                                     "text-gray-700 border-gray-700  "
                                     "text-gray-500 border-transparent"))}
             [:span
              (get tab-prop :name "")]])
          tab-props)]
        (when (some? tab-right-content)
          (if (fn? tab-right-content)
            [tab-right-content]
            tab-right-content))]))))

(defn tabs
  ([children]
   [tabs {} children])
  ([{:keys [active ]} & _tabs]
   (let [local-tab (r/atom active)]
     (fn [{:keys [tab-right-content tab-left-content active on-change tab-direction]} & tabs]
       [:> (oops/oget TabContext "Provider") {:value (if (nil? on-change)
                                                       @local-tab
                                                       active)}
        [:f> tabbar {:tab-right-content tab-right-content
                     :tab-left-content  tab-left-content
                     :tab-direction     (or tab-direction :center)
                     :on-change         (or on-change (fn [key] (reset! local-tab key)))}
         tabs]
        tabs]))))



