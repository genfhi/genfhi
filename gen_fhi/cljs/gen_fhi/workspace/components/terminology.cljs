(ns gen-fhi.workspace.components.terminology
  (:require
   [cljs.core.async :refer [take!] :as a]
   [clojure.string :as string]
   [oops.core :as oops]
   [re-frame.core :as re-frame]
   [reagent.core :as r]
   [cljs.core.async.interop :refer-macros [<p!]]

   [antd :refer [AutoComplete Select Radio Space]]

   [gen-fhi.operations.core :as workspace-ops]
   [gen-fhi.workspace.utilities.debounce :refer [debounce]]
   [gen-fhi.workspace.components.completion :refer [completion]]
   [gen-fhi.fhir-client.protocols :refer [invoke search]]))

(defn- ->descendents-of [parent-code top-level-codes]
  (apply
   concat
   (for [code top-level-codes]
     (if (= (:code code) parent-code)
       (:contains code)
       (if (contains? code :contains)
         (->descendents-of parent-code (code :contains))
         [])))))

(defn- flatten-codes [expansion-entry]
  (let [expansion-entry-contains (mapcat flatten-codes (get expansion-entry :contains []))]
    (conj expansion-entry-contains expansion-entry)))

(defn- ValueSet->list
  ([value-set]
   (ValueSet->list value-set {}))
  ([value-set {:keys [descendent-of]}]
   (let [codes          (get-in value-set [:expansion :contains] [])
         filtered-codes (if (some? descendent-of)
                          (->descendents-of descendent-of codes)
                          codes)]
     (filter
      #(not (:abstract %))
      (mapcat flatten-codes filtered-codes)))))

(defn- code->valueset-item [code-list value]
  (first (filter #(= (:code %) value) code-list)))

(defn- value-set-loader [{:keys [expand]} _children]
  (let [loading?            (r/atom true)
        passed-in-value-set (r/atom nil)
        value-set-atom      (r/atom nil)
        c                   @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        query
        (fn [value-set]
          (if (map? value-set)
            (do
              (reset! loading? false)
              (reset! value-set-atom value-set))
            (when (string? value-set)
              (reset! loading? true)
              (if expand
                (take!
                 (a/go
                   (<p! (expand value-set)))
                 (fn [expanded-valueset]
                   (reset! loading? false)
                   (reset! value-set-atom (js->clj expanded-valueset :keywordize-keys true))))
                (take!
                 (invoke
                  c
                  workspace-ops/expand
                  {:url value-set})
                 (fn [{:keys [:fhir/response]}]
                   (reset!
                    loading?
                    false)
                   (reset!
                    value-set-atom
                    (get response :return))))))))]
    (fn [{:keys [value-set]} children]
      (when (not= value-set @passed-in-value-set)
        (reset! passed-in-value-set value-set)
        (query @passed-in-value-set))
      (children {:loading?  @loading?
                 :value-set @value-set-atom}
                query))))

(defn code-select [{:keys [select-props expand value display value-set on-change placeholder raw-code? descendent-of filter-concepts]}]
  [value-set-loader {:expand expand :value-set value-set}
   (fn [{:keys [value-set loading?]}]
     (let [code-list     (filter
                          (or filter-concepts identity)
                          (ValueSet->list value-set {:descendent-of descendent-of}))
           valueset-item (code->valueset-item code-list value)]
      ;; Users wanted ability to get code display so doing this hack
      ;; raw-code just means include the valueset-item entirely for on-change
      ;; which is currently primarily used in code.cljs widget.
       (when (and raw-code? (not= display valueset-item))
         (on-change valueset-item))
       [:> Select (merge {:loading      loading?
                          :showSearch   true
                          :value        (str value)
                          :on-change    (fn [value]
                                          (let [value (if raw-code?
                                                        (code->valueset-item code-list value)
                                                        value)]
                                            (on-change value)))
                          :className   "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded overflow-hidden"
                          :filterOption (fn [input option]
                                          (string/includes?
                                           (.toLowerCase (.-children option))
                                           (.toLowerCase input)))

                          :placeholder (or placeholder "Search for code")}
                         select-props)
        (map
         (fn [code]
           [:> (.-Option Select) {:key (:code code) :value (:code code)}
            (str (or (:display code) (:code code)))])
         code-list)]))])

(defn open-code-select [{:keys [expand select-props value className value-set on-change placeholder raw-code? descendent-of filter-concepts]}]
  [value-set-loader {:expand expand :value-set value-set}
   (fn [{:keys [value-set loading?]}]
     (let [code-list     (filter (or filter-concepts identity) (ValueSet->list value-set {:descendent-of descendent-of}))]
       [:> AutoComplete
        (merge {:loading      loading?
                :showSearch   true

                :className    (str "flex items-center w-full mr-1 border border-slate-300 hover:border-blue-400 rounded"
                                   className)
                :value        (str value)
                :on-change    (fn [value]

                                (let [value (if raw-code?
                                              ;; Because open code select could include new value default to
                                              ;; :code value when not found.
                                              (or (code->valueset-item code-list value) {:code value})
                                              value)]
                                  (on-change value)))
                :options      (map
                               (fn [code]
                                 {:value (or (:display code) (:code code))})
                               (ValueSet->list value-set {:descendent-of descendent-of}))
                :filterOption (fn [input option]
                                (string/includes?
                                 (.toLowerCase (oops/oget option "value"))
                                 (.toLowerCase input)))

                :placeholder  (or placeholder "Search or type in code.")}
               select-props)]))])

(defn code-radio [{:keys [expand raw-code? value value-set on-change descendent-of filter-concepts]}]
  [value-set-loader {:expand expand :value-set value-set}
   (fn [{:keys [value-set loading?]}]
     (let [code-list     (filter (or filter-concepts identity) (ValueSet->list value-set {:descendent-of descendent-of}))
           on-change     (fn [value]
                           (let [value (if raw-code?
                                         ;; Because open code select could include new value default to
                                         ;; :code value when not found.
                                         (or (code->valueset-item code-list value) {:code value})
                                         value)]
                             (on-change value)))]
       [:> (.-Group Radio) {:className "flex flex-1"
                            :value value
                            :on-change (fn [e]
                                         (on-change (.. e -target -value)))}
        
        [:> Space {:className "border p-1 flex flex-1" :direction "vertical"}
         (map
          (fn [code]
            [:> Radio {:value (:code code)}
             [:span {:id (str "radio-choice-" code)}
              (or (:display code) (:code code))]])
          (ValueSet->list value-set {:descendent-of descendent-of}))]]))])

(defn- ->parameters [url]
  {:url:text url
   :_all "true"})

(defn codesystem-search [{:keys [url]}]
  (let [parameters (r/atom (->parameters url))
        is-loading (r/atom true)
        result     (r/atom [])
        c          @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        query      (debounce
                    (fn [parameters]
                      (reset! is-loading true)
                      (take!
                       (search c "CodeSystem" parameters)
                       (fn [{:keys [:fhir/response]}]
                         (reset!
                          is-loading
                          false)
                         (reset!
                          result
                          response))))
                    200)]
    (query @parameters)
    (fn [{:keys [id url on-change class]}]
      (when (not= (->parameters url) @parameters)
        (reset! parameters (->parameters url))
        (query @parameters))
      [completion {:id       id
                   :on-change on-change
                   :value url
                   :input-class class
                   :options (map (fn [codesystem] {:display (get codesystem :url)
                                                   :value codesystem})
                                 (take 20 @result))}])))

(defn valueset-search [{:keys [url]}]
  (let [parameters (r/atom (->parameters url))
        is-loading (r/atom true)
        result     (r/atom [])
        c          @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        query      (debounce (fn [parameters]
                               (reset! is-loading true)
                               (take!
                                (search c "ValueSet" parameters)
                                (fn [{:keys [:fhir/response]}]
                                  (reset!
                                   is-loading
                                   false)
                                  (reset!
                                   result
                                   response))))
                             200)]
    (query @parameters)
    (fn [{:keys [url on-change class]}]
      (when (not= (->parameters url) @parameters)
        (reset! parameters (->parameters url))
        (query @parameters))
      [completion {:on-change on-change
                   :value url
                   :input-class class
                   :options (map (fn [valueset]
                                   {:secondary (get valueset :url)
                                    :display   (or (get valueset :name) (get valueset :id))
                                    :value     valueset})
                                 (take 20 @result))}])))
