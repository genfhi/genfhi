(ns gen-fhi.workspace.components.tree
  (:require [reagent.core :as r]))

(defn tree-node
  ([{:keys [content leaf?]} & children]
   [:div
    (when (not leaf?)
      "^")
    content
    [:div
     children]]))

(defn tree [props]
  (let [tree-state (r/atom {})]
    (fn [props])))
