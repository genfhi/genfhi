(ns gen-fhi.workspace.components.ui-state
  (:require [re-frame.core :as re-frame]))

(defn ui-state [{:keys [ui-key default-value]} children]
  (let [ui-value @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-component-ui-property ui-key])]
    (children (or ui-value default-value) (fn [value]
                                            (re-frame/dispatch
                                             [:gen-fhi.db.events.ui/ui-change-component-value ui-key value])))))
