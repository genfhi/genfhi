(ns gen-fhi.workspace.constants.core)

(def ?csrf-token
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

(def menu-height 70)
(def bottom-height 30)
(def view-height (str "calc(100vh - " (+ menu-height) ;; bottom-height
                                      "px)"))

(def movement-increment 10)
