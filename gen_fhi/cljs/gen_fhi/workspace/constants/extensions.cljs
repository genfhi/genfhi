(ns gen-fhi.workspace.constants.extensions)

;; JSON string used for the editors.
(def json-value-extension-url "https://genfhi.com/Parameter/extension/json-value")
(def expression-value-extension-url "https://genfhi.com/Expression/derived")
