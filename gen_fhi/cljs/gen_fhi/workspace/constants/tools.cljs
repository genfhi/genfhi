(ns gen-fhi.workspace.constants.tools
  (:require [reagent.core :as r]
            [accountant.core :as accountant]

            ["@ant-design/icons" :refer [AppstoreAddOutlined CodeOutlined DatabaseOutlined TranslationOutlined
                                         FormOutlined]]

            [gen-fhi.workspace.router :as router]
            [gen-fhi.workspace.env :refer [env]]))

(def tools
  (concat
   [{:name "Client Applications"
     :resources ["ClientApplication"]
     :description "Create FHIR based applications."
     :icon (r/as-element [:> AppstoreAddOutlined])
     :on-click (fn [workspace]
                 (accountant/navigate!
                  (router/path-for
                   :type
                   {:resource-type "ClientApplication"
                    :workspace workspace})))}
    {:name "Endpoints"
     :resources ["Endpoint"]
     :description "Create endpoint resources used for queries."
     :icon (r/as-element [:> DatabaseOutlined])
     :on-click (fn [workspace]
                 (accountant/navigate!
                  (router/path-for
                   :type
                   {:resource-type "Endpoint"
                    :workspace workspace})))}]
   (when (= true (:client/enable-terminology env))
     [{:name "Terminologies"
       :resources ["ValueSet" "CodeSystem"]
       :description "Create CodeSystem or ValueSets for your application."
       :icon (r/as-element [:> TranslationOutlined])
       :on-click (fn [workspace]
                   (accountant/navigate!
                    (router/path-for
                     :type
                     {:resource-type "Terminologies"
                      :workspace workspace})))}])

   (when (= true (:client/enable-questionnaires env))
     [{:name "Questionnaires"
       :resources ["Questionnaire"]
       :description "Create questionnaires for form capture"
       :icon (r/as-element [:> FormOutlined])
       :on-click (fn [workspace]
                   (accountant/navigate!
                    (router/path-for
                     :type
                     {:resource-type "Questionnaire"
                      :workspace workspace})))}])

   (when (= true (:client/enable-operation-outcomes env))
     [{:name "Operation Definitions"
       :resources ["OperationDefinition"]
       :description "Create operation definitions that execute custom code."
       :icon (r/as-element [:> CodeOutlined])
       :on-click (fn [workspace]
                   (accountant/navigate!
                    (router/path-for
                     :type
                     {:resource-type "OperationDefinition"
                      :workspace workspace})))}])))
