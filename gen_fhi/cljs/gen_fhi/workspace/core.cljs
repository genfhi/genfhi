(ns ^:figwheel-hooks gen-fhi.workspace.core
  (:require [reagent.core :as r]
            [reagent.dom.client :as rclient]
            [clerk.core :as clerk]
            [accountant.core :as accountant]
            [re-frame.core :as re-frame]
            [clojure.string :as string]
            [oops.core :as oops]
            [goog.object :as gobj]

            ["@auth0/auth0-react" :refer [Auth0Provider useAuth0]]
            [antd :refer [Spin]]

            [gen-fhi.workspace.menu :refer [menu]]
            [gen-fhi.db.log-db]
            [gen-fhi.workspace.sentry :as sentry]
            [gen-fhi.workspace.components.modal.core :refer [modal]]
            [gen-fhi.workspace.constants.core :as constants]
            [gen-fhi.workspace.views.core :as views]
            [gen-fhi.workspace.router :as router]
            [gen-fhi.db.core]
            [gen-fhi.workspace.env :refer [env]]))

(sentry/init!)

(set! (.. js/window -jstoclj) #(js->clj % :keywordize-keys true))

(defn workspace-check
  "Confirms that a user has a workspace and redirects it there if not present in the url."
  [{:keys [workspace]} children]
  (let [auth0Info        (useAuth0)
        user             (oops/oget auth0Info "user")]
    (when (nil? workspace)
      (re-frame/dispatch [:gen-fhi.db.events.route/redirect-workspace user]))
    children))

(defn- identify-analytics [workspace auth0-user]
  (when-let [heap (gobj/getValueByKeys js/window "heap")]
    (let [heap-identify          (gobj/getValueByKeys heap "identify")
          heap-add-user-property (gobj/getValueByKeys heap "addUserProperties")
          user-identifier        (str workspace "/" (oops/oget auth0-user "sub"))]
      (heap-identify user-identifier)
      (heap-add-user-property (clj->js
                               {:name (oops/oget auth0-user "name")
                                :email (oops/oget auth0-user "email")})))))

(defn workspace-services-setup
  "Sets up channel for a workspace and the interceptors for client."
  [{:keys [workspace]} _children]
  (let [auth0Info        (useAuth0)
        get-access-token (oops/oget auth0Info "getAccessTokenSilently")]
    (re-frame/dispatch [:gen-fhi.db.events.client/set-fhir-interceptor workspace get-access-token])
    (re-frame/dispatch [:gen-fhi.db.events.sync/register-channel workspace get-access-token])
    (re-frame/dispatch [:gen-fhi.db.events.sync/create-sync-loop])
    (identify-analytics workspace (oops/oget auth0Info "user"))
    (fn [_props children]
      [:<>
       children])))

(defn client-check
  "Not sure on best approach here but for now not going to allow renders until client subd."
  [children]
  (let [c @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])]
    (when c
      [:<>
       children])))

(defn login-wrapper []
  (let [auth0Info             (useAuth0)
        current-page          @(re-frame/subscribe [:gen-fhi.db.subs.route/get-page])
        route-id              @(re-frame/subscribe [:gen-fhi.db.subs.route/get-route-id])
        cur-workspace         @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :workspace])
        is-syncing            @(re-frame/subscribe [:gen-fhi.db.subs.patches/is-syncing])
        any-temp-edits?       @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/any-temp-edits?])]
    (if (or is-syncing any-temp-edits?)
      (set! (.-onbeforeunload js/window)
            (fn [] "Changes are still syncing are you sure you want to leave?"))
      (set! (.-onbeforeunload js/window)
            nil))

    (when (and (not (oops/oget auth0Info "isLoading"))
               (not (oops/oget auth0Info "isAuthenticated")))
      ((oops/oget auth0Info "loginWithRedirect")
       #js {:appState #js{:returnTo (.. js/window -location -pathname)}}))

    [:div {:class "min-w-[1000px] h-full"}
     (if (oops/oget auth0Info "isLoading")
       [:div {:style {:display "flex"
                      :flex-direction "column"
                      :align-items "center"
                      :justify-content "center"
                      :height constants/view-height}}
        [:div
         [:> Spin {:tip "Logging in"}]]]
       [:f> workspace-check {:workspace cur-workspace}
        (if (nil? cur-workspace)
          (when (some? current-page)
            [current-page])
          [:f> workspace-services-setup {:workspace cur-workspace}
           [client-check
            [:<>
             (when (not= :preview route-id)
               [:f> menu])
             [modal]
             [:div {:style {:height (if (= :preview route-id)
                                      "100%"
                                      (str "calc(100% - " constants/menu-height "px)"))}}
              (if (some? current-page)
                [current-page]
                [:div "Unknown location"])]]]])])]))

(defn- ->redirect-callback [app-state]
  (let [redirection-url (or (oops/oget app-state "returnTo")  (.. js/window -location -pathname))]
    (accountant/navigate! redirection-url)))

(defn app []
  [:> Auth0Provider {:domain             (:client/auth0-domain env)
                     :cacheLocation      "localstorage"
                     :clientId           (:client/auth0-client-id env)
                     :onRedirectCallback ->redirect-callback
                     :audience           (:client/audience env)
                     :redirectUri        (.. js/window -location -origin)
                     ;; Hack to avoid code being pushed on the smart preview.
                     :skipRedirectCallback (or
                                            (string/includes?
                                             (.. js/window -location -pathname)
                                             "/smart-login/")
                                            (string/includes?
                                             (.. js/window -location -pathname)
                                             "/ClientApplication/")
                                            (string/includes?
                                             (.. js/window -location -pathname)
                                             "/preview/"))}

   [:f> login-wrapper]])

(defonce react-root (rclient/create-root (js/document.getElementById "app")))

(defn mount []

  (.render react-root (r/as-element [app])))

(defn page-for [route]
  (case route
    :setting-type            #'views/settings
    :setting-instance        #'views/settings
    :preview                 #'views/preview-view
    :root                    #(constantly [:<>])
    :error                   #'views/error-view
    :workspace               #'views/workspace-view
    :empty-workspace         #'views/empty-workspace
    :type                    #'views/resource-type-view
    :instance                #'views/instance-view
    :smart-login             #'views/smart-login))

(defn init! []
  (clerk/initialize!)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [route-match (router/match-by-path path)
            current-page (:name (:data  route-match))
            route-params (:path-params route-match)]
        (r/after-render clerk/after-render!)
        (re-frame/dispatch [:gen-fhi.db.events.ui/clear-ui])
        (re-frame/dispatch
         [:gen-fhi.db.events.route/set-route
          {:page          (page-for current-page)
           :id            current-page
           :params        route-params
           :query-params  (:query-params route-match)}])
        (clerk/navigate-page! path)))
    :path-exists?
    (fn [path]
      (boolean (router/match-by-path path)))})
  (accountant/dispatch-current!)
  ;; Because different handling between app and workspace for smart.
  ;; Set on init the fn to deal with this.
  (re-frame/dispatch-sync [:gen-fhi.db.events.smart/set-smart-handler-workspace])
  (mount))

(defn ^:after-load re-render []
  (init!))

(defonce start-up (do (init!) true))

