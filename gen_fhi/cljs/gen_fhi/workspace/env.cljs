(ns gen-fhi.workspace.env)

(def env (js->clj (.-env js/window) :keywordize-keys true))

(set! (.-env js/window) (fn [] (clj->js env)))
