(ns gen-fhi.workspace.icons.core)

(defn sidebar-left [{:keys [on-click width height className style]}]
  [:svg {:className className :on-click on-click :style style :xmlns "http://www.w3.org/2000/svg" :width width :height height :viewBox "0 0 150 150" :fill "none"}
   [:rect {:x "4.51087" :y "4.50034" :width "140.989" :height "140.989"  :stroke-width "9"}]
   [:rect {:x "61.5" :y "145.5" :width "57" :height "141" :transform "rotate(-180 61.5 145.5)"  :stroke-width "9"}]
   [:path {:d "M17 93L33 93"  :stroke-width "9"}]
   [:path {:d "M17 31L49 31"  :stroke-width "9"}]
   [:line {:x1 "17" :y1 "61.5" :x2 "41" :y2 "61.5"  :stroke-width "9"}]])

(defn sidebar-right [{:keys [on-click width height className style]}]
  [:svg {:className className :on-click on-click :style style :xmlns "http://www.w3.org/2000/svg" :width width :height height :viewBox "0 0 150 150" :fill "none"}
   [:rect {:x "145.489" :y "145.5" :width "140.989" :height "140.989" :transform "rotate(-179.996 145.489 145.5)" :stroke-width "9"}]
   [:rect {:x "88.5001" :y "4.50006" :width "57" :height "141"  :stroke-width "9"}]
   [:path {:d "M101 31L133 31"  :stroke-width "9"}]
   [:path {:d "M101 93L117 93"  :stroke-width "9"}]
   [:line {:x1 "101" :y1 "61.5" :x2 "125" :y2 "61.5"  :stroke-width "9"}]])

(defn sidebar-bottom [{:keys [on-click width height className style]}]
  [:svg {:className className :on-click on-click :style style :xmlns "http://www.w3.org/2000/svg" :width width :height height :viewBox "0 0 150 150" :fill "none"}
   [:rect {:x "4.50034" :y "145.489" :width "140.989" :height "140.989" :transform "rotate(-89.9957 4.50034 145.489)"  :stroke-width "9"}]
   [:rect {:x "145.5" :y "88.5001" :width "57" :height "141" :transform "rotate(90 145.5 88.5001)"  :stroke-width "9"}]
   [:path {:d "M36 106L111 105.865" :stroke-width "9"}]
   [:path {:d "M54 127.067L91.5 127" :stroke-width "9"}]])

(defn drag-handle [{:keys [on-click width height className style]}]
  [:svg {:className className
         :on-click  on-click
         :style style
         :width  width
         :height height
         :viewBox "0 0 45 45"
         :fill "none"
         :xmlns "http://www.w3.org/2000/svg"}
   [:circle {:cx "10" :cy "10" :r "8" :fill "#C4C4C4"}]
   [:circle {:cx "10" :cy "35" :r "8" :fill "#C4C4C4"}]
   [:circle {:cx "35" :cy "10" :r "8" :fill "#C4C4C4"}]
   [:circle {:cx "35" :cy "35" :r "8" :fill "#C4C4C4"}]])

(defn js-logo [{:keys [on-click width height className style]}]
  [:svg {:className className
         :on-click  on-click
         :style style
         :width  width
         :height height
         :xmlns "http://www.w3.org/2000/svg"
         :viewBox "0 0 630 630"}
   [:rect {:width "630"
           :height "630"
           :fill "#f7df1e"}]
   [:path {:d "m423.2 492.19c12.69 20.72 29.2 35.95 58.4 35.95 24.53 0 40.2-12.26 40.2-29.2 0-20.3-16.1-27.49-43.1-39.3l-14.8-6.35c-42.72-18.2-71.1-41-71.1-89.2 0-44.4 33.83-78.2 86.7-78.2 37.64 0 64.7 13.1 84.2 47.4l-46.1 29.6c-10.15-18.2-21.1-25.37-38.1-25.37-17.34 0-28.33 11-28.33 25.37 0 17.76 11 24.95 36.4 35.95l14.8 6.34c50.3 21.57 78.7 43.56 78.7 93 0 53.3-41.87 82.5-98.1 82.5-54.98 0-90.5-26.2-107.88-60.54zm-209.13 5.13c9.3 16.5 17.76 30.45 38.1 30.45 19.45 0 31.72-7.61 31.72-37.2v-201.3h59.2v202.1c0 61.3-35.94 89.2-88.4 89.2-47.4 0-74.85-24.53-88.81-54.075z"}]])
