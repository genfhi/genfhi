(ns gen-fhi.workspace.menu
  (:require [re-frame.core :as re-frame]
            [clojure.core.match :refer [match]]
            [oops.core :as oops]
            [accountant.core :as accountant]
            [reagent.core :as r]
            [cljs.core.async :as a]
            [clojure.string :as string]

            [antd :refer [Avatar Breadcrumb Dropdown Select]]
            ["@ant-design/icons" :refer [DownloadOutlined UserOutlined SettingOutlined SearchOutlined QuestionCircleOutlined DownOutlined]]
            ["@auth0/auth0-react" :refer [useAuth0]]
            ["js-file-download" :as file-download]

            [gen-fhi.workspace.components.query :refer [query]]
            [gen-fhi.fhir-client.protocols :as client]
            [gen-fhi.workspace.icons.core :refer [sidebar-right sidebar-bottom sidebar-left]]
            [gen-fhi.workspace.constants.tools :refer [tools]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.constants.core :as constants]
            [gen-fhi.workspace.router :as router]
            [gen-fhi.workspace.env :refer [env]]
            [gen-fhi.workspace.utilities.naming :as naming]))

(defn- operation-invoke [{:keys [id]}]
  (let [path                   (path/->path "OperationDefinition" id)
        operation-definition   @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value path])]
    [:button {:className (str "rounded-full h-6 flex items-center justify-center-xs p-2 hover:text-white text-white border "
                              "border-blue-500 hover:border-blue-600 bg-blue-500 hover:bg-blue-600 align-middle "
                              "rounded border inline-block text-center cursor-pointer font-semibold disabled:bg-gray-300")
              :on-click (fn [_e]
                          (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                              {:type :operation-invoke
                                               :data {:operation operation-definition}}]))}

     [:span
      "Invoke"]]))

(defn- search []
  (let [c       @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        search-v (r/atom "")
        active   (r/atom false)
        results  (r/atom [])
        query    (fn [term]
                   (if (empty? term)
                     (reset! results [])
                     (a/take!
                      (a/map vector
                             [(client/search
                               c
                               {:_type "Questionnaire"
                                :title:text term})
                              (client/search
                               c
                               {:_type "ValueSet,CodeSystem,ClientApplication,Endpoint"
                                :name:text term})])
                      (fn [[res1 res2]]
                        (reset! results (concat (res1 :fhir/response) (res2 :fhir/response)))))))]

    (fn []
      (let [params        @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])]
        [:div {:class "relative"
               :on-blur (fn [e]
                          (when (not (oops/ocall e "currentTarget.contains" (oops/oget e "relatedTarget")))
                            (reset! active false)))}
         [:div {:class "border bg-white p-1 border-slate-300 text-slate-600 rounded-lg flex items-center w-72"}
          [:div {:class "flex items-center justify-center mr-1 "}
           [:> SearchOutlined]]
          [:input
           {:on-focus (fn [_e]
                        (reset! active true))
            :value     @search-v
            :placeholder "Search..."
            :class "h-full w-full"
            :on-change (fn [e]
                         (reset! search-v (.. e -target -value))
                         (query
                          (.. e -target -value)))}]]
         (when (and @active (not-empty @results))
           (let [resource-types (set (map #(:resourceType %) @results))]
             [:div {:tabindex "0"
                    :class "z-50 drop-shadow-lg overflow-hidden absolute bg-white border border-slate-200 rounded-lg mt-1 w-72"}
              (map
               (fn [resource-type]
                 ^{:key resource-type}
                 [:div
                  [:div {:class "px-2 p-1 bg-gray-50 text-slate-600 font-semibold"}
                   (naming/resource-type->display-name resource-type true)]
                  (let [resources (filter #(= (:resourceType %) resource-type) @results)]
                    (map
                     (fn [resource]
                       [:div {:key (get resource :id)
                              :on-click (fn [_e]
                                          (reset! search-v "")
                                          (reset! results [])
                                          (accountant/navigate!
                                           (router/path-for
                                            :instance
                                            (merge
                                             params
                                             {:modifiers     ""
                                              :resource-type (get resource :resourceType)
                                              :id            (get resource :id)}))))
                              :class "p-1 px-2 bg-white border border-t-o border-l-0 border-r-0 hover:bg-blue-100 cursor-pointer"}
                        (if (= "Questionnaire" (:resourceType resource))
                          (:title resource)
                          (:name resource))])
                     resources))])
               (sort #(< % %2) resource-types))]))]))))

(defn- environments [{:keys [workspace]}]
  (re-frame/dispatch [:gen-fhi.db.events.environment/load-endpoint-environment workspace])
  (fn []
    [query {:resource-type "Organization"}
     (fn [{:keys [result _re-query is-loading]}]
       (let [environment @(re-frame/subscribe [:gen-fhi.db.subs.environment/environment])]
         [:> Select {:loading is-loading
                     :showSearch true
                     :value environment
                     :on-change (fn [value]
                                  (re-frame/dispatch-sync
                                   [:gen-fhi.db.events.environment/set-endpoint-environment
                                    workspace
                                    value])
                                  (.reload js/location))
                     :style {:height "24px"
                             :width "100px"}
                     :className "text-xs flex rounded items-center border border-slate-300 hover:border-blue-400 rounded overflow-hidden mr-1"
                     :filterOption (fn [input option]
                                     (string/includes?
                                      (.toLowerCase (.-children option))
                                      (.toLowerCase input)))}
          [:> (.-Option Select) {:key "dev"
                                 :value nil}
           "DEV"]
          (map
           (fn [organization]
             [:> (.-Option Select) {:key (:id organization)
                                    :value (str "Organization/" (:id organization))}
              (str (or (:name organization) (:id organization)))])
           result)]))]))

(defn- top-menu []
  (let [route-id               @(re-frame/subscribe [:gen-fhi.db.subs.route/get-route-id])
        auth0Info              (useAuth0)
        user-workspaces        @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-workspaces (oops/oget auth0Info "user")])
        cur-workspace          @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :workspace])
        active-resource-type   @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :resource-type])
        active-id              @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :id])
        resource               (if (and active-resource-type active-id)
                                 @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path active-resource-type active-id)])
                                 nil)]
    [:div {:class "flex flex-1 items-center bg-gray-50 border-t-0 border-l-0 border-r-0 border border-slate-200 p-1"}
     [:div {:class "flex flex-col flex-1 sm:flex-2 ml-2 min-w-[280px] whitespace-nowrap overflow-hidden"}
      [:div {:class "flex"}
       [:img {:src "/assets/icon.svg" :width 28 :height 28 :style {:margin-right "8px"}}]
       [:> Breadcrumb
        [:> (.-Item Breadcrumb)
         [:a {:href (router/path-for :workspace {:workspace cur-workspace})}
          [:span {:className "text-lg text-blue-500 font-semibold"
                  :style {:top "2px"
                          :position "relative"}}
           "GenFHI"]]]
        (when active-resource-type
          [:> (.-Item Breadcrumb)
           [:a {:className "text-xs"
                :href (router/path-for :type {:workspace cur-workspace
                                              :resource-type active-resource-type})}
            (naming/resource-type->display-name active-resource-type true)]])
        (when resource
          (let [name (match (:resourceType resource)
                       "ClientApplication"           (get resource :name active-id)
                       "OperationDefinition"         (get resource :name active-id)
                       "Endpoint"                    (get resource :name active-id)
                       "CodeSystem"                  (get resource :name active-id)
                       "ValueSet"                    (get resource :name active-id)
                       :else                         active-id)]
            [:> (.-Item Breadcrumb)
             [:a {:title name
                  :class "inline-flex justify-center items-center"
                  :href (router/path-for :instance {:workspace cur-workspace
                                                    :modifiers ""
                                                    :resource-type active-resource-type
                                                    :id active-id})}
              [:span {:className "inline-block text-xs text-ellipsis whitespace-nowrap overflow-hidden max-w-[110px]"}
               name]]]))]]]
     [search]
     [:div {:class "flex flex-1 justify-end items-center text-slate-600 "}
      [environments {:workspace cur-workspace}]
      [:> Dropdown {:placement "bottomRight"
                    :trigger   "click"
                    :overlay
                    (r/as-element
                     [:div {:class "p-2 px-4 shadow-md bg-white overflow-hidden rounded border border-slate-300 text-slate-600" :style {:width "200px"}}
                      [:div {:class "mb-1"}
                       [:span {:class "font-semibold text-slate-600 "}
                        "Resources"]]
                      (map
                       (fn [tool]
                         ^{:key (get tool :name)}
                         [:<>
                          (map
                           (fn [resource-type]
                             ^{:key resource-type}
                             [:div {:on-click (fn [_e]
                                                (re-frame/dispatch
                                                 [:gen-fhi.db.events.ui/set-active-modal
                                                  {:type :create-modal
                                                   :data {:type resource-type}}]))
                                                ;;(create-resource! c resource-type params))
                                    :class "cursor-pointer flex items-center text-slate-600 hover:bg-blue-100 hover:text-blue-600"}
                              [:div {:class "flex mr-1 items-center justify-center"}
                               (get tool :icon)]
                              (naming/resource-type->display-name resource-type)])

                           (get tool :resources))])
                       tools)])}
       [:div {:class (str "border flex items-center justify-center cursor-pointer text-xs rounded-full p-1 px-2 border-blue-400 text-blue-600 hover:bg-blue-600 hover:text-white")}
        [:span "Create"]
        [:> DownOutlined {:class "ml-1"}]]]
      [:> Dropdown {:placement "bottomRight"
                    :trigger   "click"
                    :overlay
                    (r/as-element
                     [:div {:class " p-2 px-4 shadow-md bg-white overflow-hidden rounded border border-slate-300 text-slate-600" :style {:width "200px"}}
                      [:div {}
                       [:div {:class "text-slate-600 font-semibold"}
                        [:span "Resources"]]
                       [:div {:class "cursor-pointer"}
                        [:a {:class "hover:text-blue-500"
                             :href "https://docs.genfhi.app/"
                             :target "_blank"}
                         "Documentation"]]
                       [:div {:class "mt-2 text-slate-600 font-semibold"}
                        [:span "Contact"]]
                       [:div {:class "cursor-pointer"}
                        [:a {:class "hover:text-blue-500"
                             :href "mailto: dev@genfhi.app"}
                         "Support"]]]])}
       [:div {:class "cursor-pointer flex justify-center items-center ml-4 mr-4 hover:text-blue-600"}
        [:div {:class "flex justify-center items-center mr-1"}
         [:> QuestionCircleOutlined]]
        [:span
         "Help"]]]
      [:div {:class "mr-4 flex"}
       [:> SettingOutlined {:on-click (fn []
                                        (accountant/navigate!
                                         (router/path-for
                                          :setting-type
                                          {:setting "user"
                                           :workspace cur-workspace})))
                            :class (str "cursor-pointer hover:text-blue-600"
                                        (when (#{:setting-type :setting-instance} route-id)
                                          " text-blue-600"))
                            :style {:font-size "18px"}}]]
      [:> Dropdown {:placement "bottomRight"
                    :trigger "click"
                    :overlay
                    (r/as-element
                     [:div {:class "shadow-md bg-white overflow-hidden rounded border border-slate-300" :style {:width "200px"}}
                      [:div {:class "p-2 flex border border-t-0 border-r-0 border-l-0 border-slate-300"}
                       [:div {:class "flex flex-1 flex-col mr-1 "}
                        [:div {:class "text-slate-600 text-base font-bold overflow-hidden text-ellipsis whitespace-nowrap w-[140px]"}
                         [:span (oops/oget auth0Info "user.name")]]
                        [:div {:class "text-slate-500 text-xs overflow-hidden text-ellipsis whitespace-nowrap w-[140px]"}
                         [:span (oops/oget auth0Info "user.email")]]]
                       [:div {:class "flex items-center"}
                        [:> Avatar {:style {:display "flex"
                                            :justify-content "center"
                                            :background-color "white"
                                            :border (str "1px solid "
                                                         (if (some? (oops/oget auth0Info "user.picture"))
                                                           "rgba(0,0,0,0)"
                                                           "rgba(0, 0, 0, 0.85)"))
                                            :color "rgba(0, 0, 0, 0.85)"
                                            :align-items "center"}
                                    :icon (r/as-element [:> UserOutlined])

                                    :src (oops/oget auth0Info "user.picture")}]]]
                      [:div {:class ""}
                       [:div {:class "mt-1"}
                        [:div {:class "p-2 text-sm text-slate-600 font-semibold"}
                         "Workspaces"]]
                       [:table {:class "border-collapse bg-gray-50 w-full"}
                        (map
                         (fn [workspace]
                           [:tr {:key  workspace
                                 :on-click (fn [_e]
                                             (when (not= workspace cur-workspace)
                                               (set!
                                                (.. js/window -location -href)
                                                (router/path-for :workspace {:workspace workspace}))))
                                 :class "border border-r-0 border-l-0 cursor-pointer"}
                            [:td {:class (str "hover:bg-blue-200 text-xs hover:text-blue-500 p-2 "
                                              (if (= cur-workspace workspace)
                                                "bg-blue-100 text-blue-500"
                                                "text-slate-500"))}
                             workspace]])
                         user-workspaces)]]
                      [:div {:class "cursor-pointer p-2 text-blue-600 hover:text-blue-700 hover:bg-blue-100 mt-1"
                             :on-click (partial
                                        (oops/oget auth0Info "logout")
                                        #js{:returnTo (:client/auth0-logout-returnto env)})}
                       [:span "Sign out"]]
                      [:div {:class "text-xs text-slate-500 flex mb-1"
                             :style {:font-size "8px"}}
                       [:span {:class "ml-2"}
                        (str "version:"
                             (subs (oops/oget js/window "version")
                                   0
                                   (min (count (oops/oget js/window "version"))
                                        8)))]]])}
       [:div {:class "mr-2 cursor-pointer"}
        [:> Avatar {:style {:background-color "rgba(0,0,0,0)"
                            :color "rgba(0,0,0,0.85)"
                            :display "flex"
                            :justify-content "center"
                            :align-items "center"
                            :height "18px"
                            :width "18px"
                            :font-weight "800"}
                    :icon (r/as-element [:> UserOutlined
                                         {:class "cursor-pointer hover:text-blue-600"}])}]]]]]))

(defn- menu-pillbox [{:keys [active? on-click]} children]
  [:div {:on-click on-click
         :class    (str "flex items-center justify-center mx-1 cursor-pointer text-xs rounded-full p-1 px-2 "
                        (if active?
                          "bg-blue-500 text-white"
                          "hover:bg-gray-100"))}

   children])

(defn menu []
  (let [route-id               @(re-frame/subscribe [:gen-fhi.db.subs.route/get-route-id])
        cur-workspace          @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :workspace])
        active-resource-type   @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :resource-type])
        active-id              @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :id])

        left-sidebar-showing   @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :left])
        right-sidebar-showing  @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :right])
        bottom-sidebar-showing @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-display-bar :bottom])]
    [:div {:class "flex flex-col border border-x-0 border-t-0 border-gray-300"
           :style {:height constants/menu-height}}
     [:div {:class "flex h-1/2"}
      [:f> top-menu]]
     [:div {:class "px-4 flex grow justify-content items-center"}
      [:div {:class "flex flex-1 min-w-[540px]"}
       [menu-pillbox {:on-click (fn [_e]
                                  (accountant/navigate!
                                   (router/path-for
                                    :workspace
                                    {:workspace cur-workspace})))
                      ;; Terminology a bit wonky could be Terminology or the terms resource-types
                      :active? (= route-id :workspace)}
        [:span "Home"]]
       (map
        (fn [tool]
          ^{:key (get tool :name)}
          [menu-pillbox {:on-click (partial (:on-click tool) cur-workspace)
                         ;; Terminology a bit wonky could be Terminology or the terms resource-types
                         :active? (or (contains? (set (:resources tool)) active-resource-type)
                                      (= (:name tool) active-resource-type))}
           [:span (:name tool)]])
        tools)]

      [:div {:class "flex items-center justify-center"}
       (when (and (= :instance route-id)
                  (some? (#{"ValueSet" "ClientApplication" "OperationDefinition" "CodeSystem" "Questionnaire"}
                          active-resource-type)))
         [:<>
          [sidebar-left {:on-click #(re-frame/dispatch [:gen-fhi.db.events.ui/flip-show-bar :left])
                         :width 20
                         :height 20
                         :style {:margin-right "4px"}
                         :className (str "cursor-pointer " (if left-sidebar-showing "stroke-blue-500" "stroke-gray-400"))}]

          [sidebar-bottom {:on-click  #(re-frame/dispatch [:gen-fhi.db.events.ui/flip-show-bar :bottom])
                           :width     20
                           :height    20
                           :style     {:margin-right "4px"}
                           :className (if (nil? (#{"ClientApplication" "OperationDefinition"} active-resource-type))
                                        "stroke-gray-200 cursor-not-allowed "
                                        (str "cursor-pointer " (if bottom-sidebar-showing "stroke-blue-500" "stroke-gray-400")))}]

          [sidebar-right {:on-click #(re-frame/dispatch [:gen-fhi.db.events.ui/flip-show-bar :right])
                          :width 20
                          :height 20
                          :style {:margin-right "4px"}
                          :className (str "cursor-pointer "
                                          (if right-sidebar-showing "stroke-blue-500" "stroke-gray-400"))}]])]

      [:div {:class "flex flex-1 items-center justify-end"}
       (cond
         (and (= :instance route-id) (= "ClientApplication" active-resource-type))
         [:<>
          [:button {:className (str "rounded-full h-6 mr-2 flex items-center justify-center-xs p-2 border border-blue-500 "
                                    "hover:border-blue-600 text-blue-500 hover:text-blue-600 align-middle rounded "
                                    "border inline-block text-center cursor-pointer font-semibold disabled:bg-gray-300")
                    :on-click (fn [_e]
                                (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                                    {:type :deployment
                                                     :data {:id        active-id
                                                            :workspace cur-workspace}}]))}

           [:span
            "Deploy"]]
          [:a {:tabIndex "-1"
               :target "_blank"
               :href   (router/path-for
                        :preview
                        {:workspace cur-workspace
                         :id        active-id})
               :className (str "rounded-full h-6 flex items-center justify-center-xs p-2 hover:text-white text-white border "
                               "border-blue-500 hover:border-blue-600 bg-blue-500 hover:bg-blue-600 align-middle "
                               "rounded border inline-block text-center cursor-pointer font-semibold disabled:bg-gray-300")}
           [:span {:className "text-center"}
            "Preview"]]]
         (and (= :instance route-id) (= "OperationDefinition" active-resource-type))
         [operation-invoke {:id active-id}]

         (and (= :instance route-id) (= "Questionnaire" active-resource-type))
         (let [questionnaire @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value
                                                   (path/->path active-resource-type active-id)])]
           [:div {:on-click (fn [_e] (file-download
                                      (.stringify js/JSON (clj->js questionnaire) nil 2)
                                      (str (get questionnaire :title (get questionnaire :id))
                                           "-" (get-in questionnaire [:meta :versionId])
                                           ".json")))
                  :class "text-slate-400 font-md hover:text-blue-400 cursor-pointer flex items-center "}
            [:span {:class "mr-2"} "Download"]
            [:> DownloadOutlined]])

         :else [:<>])]]]))

