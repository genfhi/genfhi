(ns gen-fhi.workspace.message-handler
  (:require [clojure.core.match :refer [match]]
            [oops.core :as oops]

            [re-frame.core :as re-frame]))

(defonce messanger-setup!? (atom false))

(defn- listener [event]
  (let [data (js->clj (oops/oget event "data") :keywordize-keys true)]
    (match (get data :type)
      "modify-variable"           (let [{:keys [item-path state-path variable key value]} data]
                                    (re-frame/dispatch
                                     [:gen-fhi.db.events.mutate/modify-variable item-path state-path variable key value]))

      "set-selected-item-path" (do (re-frame/dispatch [:gen-fhi.db.events.ui/set-selected-item-path
                                                       (get data :item-path)])
                                   (re-frame/dispatch [:gen-fhi.db.events.ui/show-item-properties]))
      "trigger-action"          (let [{:keys [variable item-path state-path]} data]
                                  (re-frame/dispatch [:gen-fhi.db.events.action/trigger-action
                                                      {:id variable
                                                       :p item-path
                                                       :state-path state-path}]))
      :else :default)))

(defn setup-listener []
  (when (not @messanger-setup!?)
    (println "listener setup!")
    (reset! messanger-setup!? true)
    (.addEventListener
     js/window
     "message"
     listener)))

