(ns gen-fhi.workspace.router
  (:require [reitit.frontend :as reitit]))

(def router
  (reitit/router
   [["/"             :root]
    ["/registration" :empty-workspace]
    ["/error"        :error]
    ["/w/:workspace"
     [""              :workspace]
     ["/settings"
      ["/:setting"     :setting-type]
      ["/:setting/:id" :setting-instance]]
     ["/preview/:id" :preview]
     ["/smart-login/:resource-type/:id/"   :smart-login]
     ["/editor/:resource-type"
      [""            :type]
      ["/:id/{*modifiers}"        :instance]]]]
   {:conflicts nil}))

(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

(def match-by-path (partial reitit/match-by-path router))

