(ns gen-fhi.workspace.sentry
  (:require ["@sentry/react" :as sentry]
            ["@sentry/tracing" :refer [BrowserTracing]]

            [gen-fhi.workspace.env :refer [env]]))

(defn init! []
  (when-let [sentry-key (:client/sentry-key env)]
    (sentry/init
     (clj->js
      {:dsn sentry-key
       :integrations [(BrowserTracing.)]
       ;; Set tracesSampleRate to 1.0 to capture 100%
       ;; of transactions for performance monitoring.
       ;; We recommend adjusting this value in production
       :tracesSampleRate 1.0}))))
