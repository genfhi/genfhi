(ns gen-fhi.workspace.smart-client
  (:require [clojure.string :as string]
            [oops.core :as oops]
            [cljs.core.async :refer [go]]
            [cljs.core.async.interop :refer-macros [<p!]]

            [gen-fhi.fhir-client.utils
             :refer [fhir-req->http-req http-res->fhir-res oo->fhir-res]]
            [gen-fhi.interceptors.core :as it]))

(defn- headers->fetch-headers [headers]
  (reduce
   (fn [header-map header]
     (let [[key val] (string/split header #":")]
       (assoc header-map key val)))
   {}
   headers))

(defn ->interceptor
  ([smart-client]
   (->interceptor smart-client {}))
  ([smart-client request-options]
   (it/->interceptor
    [{:error (fn [v] (.log js/console (:interceptors/exception v)))
      :enter (it/alt-value
              (fn [fhir-req]
                (let [http-req (fhir-req->http-req "" fhir-req)]
                  (go
                    (<p!
                     (-> (oops/ocall smart-client "request"
                                     (let [parsed-headers (update
                                                           request-options
                                                           :headers
                                                           (fn [headers]
                                                             (merge {:Content-Type "application/json"}
                                                                    (headers->fetch-headers headers))))
                                           request         (clj->js
                                                            (merge-with
                                                             into
                                                             parsed-headers
                                                             {:includeResponse true
                                                              :useRefreshToken true}
                                                             http-req))]
                                       ;; (.log js/console (clj->js parsed-headers))
                                       ;; (.log js/console (clj->js http-req))
                                       ;; (.log js/console "end-request:" request)
                                       request))
                         (.then (fn [res]
                                  (let [res (js->clj res :keywordize-keys true)]
                                    (http-res->fhir-res fhir-req {:status (oops/oget (get res :response) "status")
                                                                  :body    (get res :body)}))))

                         (.catch (fn [err]
                                   (oo->fhir-res {:resourceType "OperationOutcome"
                                                  :issue [{:serverity "error" :code "http-error"
                                                           :diagnostics (oops/oget err "statusText")}]}
                                                 (oops/oget err "statusCode"))))))))))}])))



