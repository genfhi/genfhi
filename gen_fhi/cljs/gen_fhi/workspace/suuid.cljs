(ns gen-fhi.workspace.suuid
  (:require [oops.core :as oops]
            
            ["short-uuid" :as short-uuid]))
             

(defn uuid->suuid [uuid]
  (let [translator (short-uuid)]
    ((oops/oget translator "fromUUID") uuid)))

(defn suuid->uuid [suuid]
  (let [translator (short-uuid)]
    ((oops/oget translator "toUUID") suuid)))
