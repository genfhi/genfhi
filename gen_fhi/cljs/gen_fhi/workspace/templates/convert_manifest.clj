(ns gen-fhi.workspace.templates.convert-manifest
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]

            [gen-fhi.fhirpath.core :as fp]))

(defn- entry->reference [entry]
  (str (get-in entry [:resource :resourceType])
       "/"
       (get-in entry [:resource :id])))

(defn- create-ref-map [bundle]
  (reduce
   (fn [references entry]
     (assoc
      references
      (entry->reference entry)
      (get entry :fullUrl)))
   {}
   (get  bundle :entry)))

(defn- resolve-local-deps [bundle]
  (let [local-deps (create-ref-map bundle)
        edits-to-apply (apply
                        concat
                        (map
                         (fn [entry]
                           (let [reference-id (entry->reference entry)]
                             (fp/locations
                              "$this.descendants().where($this=%ref)"
                              bundle
                              {:options/variables {:ref reference-id}})))
                         (get bundle :entry)))]
    ;;(println local-deps edits-to-apply)
    (reduce
     (fn [bundle ref-loc]
       (update-in
        bundle
        ref-loc
        (fn [ref]
          (get local-deps ref))))

     bundle
     edits-to-apply)))

(defn- assoc-random-url [entry]
  (assoc
   entry
   :request {:method "POST", :url (get-in entry [:resource :resourceType])}
   :fullUrl (str "urn:uuid:" (java.util.UUID/randomUUID))))

(defn bundle->template [types-allowed bundle]
  (let [bundle (update
                bundle
                :entry
                (fn [entries]
                  (into []
                        (->> entries
                             (filter
                              (fn [entry]
                                ((set types-allowed)
                                 (get-in entry [:resource :resourceType]))))
                             (map assoc-random-url)))))]
    {:type "transaction"
     :resourceType "Bundle"
     :entry (:entry (resolve-local-deps bundle))}))

(defmacro ->template [resource-name meta-data types-allowed]
  (let [bundle (json/read-str (slurp (io/resource resource-name)) :key-fn keyword)]
    `(do
       ~(merge meta-data {:data (bundle->template types-allowed bundle)}))))

(defmacro ->app-template [resource-name metadata]
  `(->template
    ~resource-name
    ~metadata
    #{"ClientApplication" "Endpoint"}))
