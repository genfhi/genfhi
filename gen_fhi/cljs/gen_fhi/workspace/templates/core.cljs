(ns gen-fhi.workspace.templates.core
  (:require-macros [gen-fhi.workspace.templates.convert-manifest :refer [->app-template]]
                   [gen-fhi.workspace.utilities.inline-load :refer [load-resource]])
  (:require [cljs.core.async :as a]
            [accountant.core :as accountant]
            [re-frame.core :as re-frame]

            [gen-fhi.workspace.router :as router]
            [gen-fhi.workspace.utilities.naming :as naming]
            [gen-fhi.workspace.utilities.alerts :as alerts]
            [gen-fhi.fhir-client.protocols :refer [transaction]]
            [gen-fhi.fhirpath.core :as fp]))

(defn create [client template params]
  (a/take!
   (transaction client (get template :data))
   (fn [res]
     (let [bundle (:fhir/response res)]
       (if (some? (:fhir/issue res))
         (alerts/issue-message
          (str
           "Faied to create from template "
           (get template :name))
          (alerts/fhir-response->issue-string
           res))
         (do
           (alerts/success-message
            (str
             "Creating a new "
             (naming/resource-type->display-name (get template :type))))
           (let [id (first
                     (fp/evaluate
                      "$this.entry.resource.where(resourceType=%resourceType).id"
                      bundle
                      {:options/variables {:resourceType (get template :type)}}))]

             (accountant/navigate!
              (router/path-for
               :instance
               (merge
                params
                {:modifiers ""
                 :resource-type (get template :type)
                 :id             id})))
             (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :left    true])
             (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :bottom  true])
             (re-frame/dispatch [:gen-fhi.db.events.ui/show-component-adder]))))))))

(defn template->resource-count
  "returns hashmap of resources within the template data"
  [template]
  (reduce
   (fn [counts resourceType]
     (if (nil? (get counts resourceType))
       (assoc counts resourceType 1)
       (update counts resourceType #(+ 1 %))))
   {}
   (fp/evaluate
    "$this.entry.resource.resourceType"
    (get template :data))))

(def templates
  {:Questionnaire     [{:name "McGill Pain"
                        :type "Questionnaire"
                        :description "Evaluate the severity of a persons pain."
                        :data {:type         "transaction"
                               :resourceType "Bundle"
                               :entry        [{:request {:method "POST" :url "Questionnaire"}
                                               :resource (load-resource "gen_fhi/workspace/data/questionnaires/mcgill-pain.json")}]}}
                       {:name "PHQ-9"
                        :type "Questionnaire"
                        :description "Mental Health evaluation Questionnaire."
                        :data (load-resource "gen_fhi/workspace/data/questionnaires/phq-9.json")}
                       {:name "BMI Calculator"
                        :type "Questionnaire"
                        :description "Calculates patient BMI using inputted height and weight."
                        :data {:type         "transaction"
                               :resourceType "Bundle"
                               :entry        [{:request {:method "POST" :url "Questionnaire"}
                                               :resource (load-resource "gen_fhi/workspace/data/questionnaires/bmi.json")}]}}]
   :ClientApplication [(->app-template
                        "gen_fhi/workspace/data/template-manifests/patient_observations.json"
                        {:name "Patients and Observations"
                         :type "ClientApplication"
                         :description "SMART on FHIR app that displays a logged in practitioners assigned patients. And graphs their observations over time."})
                       (->app-template
                        "gen_fhi/workspace/data/template-manifests/search_parameter.json"
                        {:name "Search Parameter Editor"
                         :type "ClientApplication"
                         :description "Search and edit search parameter resources"})]})
