(ns gen-fhi.workspace.utilities.alerts
  (:require [oops.core :as oops]
            ["antd" :refer [message]]))

(defn fhir-response->issue-string [fhir-response]
  (str
   "'"
   (reduce (fn [message issue]
             (let [issue-message (or (get-in issue [:details :text])
                                     (get issue :diagnostics))]
               (str message " " issue-message)))
           ""
           (get fhir-response :fhir/issue (get fhir-response :issue)))
   "'"))

(defn issue-message [id message-string]
  (oops/ocall
   message
   "error"
   (str
    (name id)
    " failed "
    message-string)))

(defn success-message [id]
  (oops/ocall message "success" (str (name id) " succeeded")))
