(ns gen-fhi.workspace.utilities.events
  (:require [re-frame.core :as re-frame]))

(defn dispatch-handlers [event-type {:keys [item item-path state-path]}]
  (let [on-click-handlers
        (filter #(= (:eventType %) event-type) (get item :handler []))]
     (when (not-empty [on-click-handlers (:on-click item)])
       (doall
        (for [handler on-click-handlers]
          (re-frame/dispatch
           [:gen-fhi.db.events.item/trigger-event-handler
            handler
            item-path
            state-path]))))))
