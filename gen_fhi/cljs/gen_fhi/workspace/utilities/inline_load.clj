(ns gen-fhi.workspace.utilities.inline-load
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]))

(defmacro load-resource
  "Inline loads a json resource."
  [resource-name]
  (json/read-str
   (slurp
    (io/resource resource-name))
   :key-fn keyword))
