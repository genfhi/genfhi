(ns gen-fhi.workspace.utilities.mutations
  (:refer-clojure :exclude [replace remove])
  (:require [re-frame.core :as re-frame]
            [gen-fhi.db.events.mutate :refer [should-remove?]]))

(defn- local->mutate-event [local?]
  (if local?
    :gen-fhi.db.events.mutate/mutate-local
    :gen-fhi.db.events.mutate/mutate))

(defn replace
  ([path value]
   (replace path value {:remove-empty? true
                        :sync?         false
                        :local?        false}))
  ([path value {:keys [sync? remove-empty? local?]}]
   (let [dispatcher (if sync? re-frame/dispatch-sync re-frame/dispatch)
         mutate-event (local->mutate-event local?)]
     (if (should-remove? value remove-empty?)
       (dispatcher [mutate-event {:path path :type :remove}])
       (dispatcher [mutate-event
                    {:path path
                     :type :replace
                     :value value}])))))

(defn add
  ([path value]
   (add path value {:sync? false}))
  ([path value {:keys [sync? local?]}]
   (let [dispatcher (if sync? re-frame/dispatch-sync re-frame/dispatch)
         mutate-event (local->mutate-event local?)]
     (dispatcher [mutate-event
                  {:path path
                   :type :add
                   :value value}]))))

(defn remove
  ([path]
   (remove path {:sync? false}))
  ([path {:keys [sync? local?]}]
   (let [dispatcher (if sync? re-frame/dispatch-sync re-frame/dispatch)
         mutate-event (local->mutate-event local?)]
     (dispatcher [mutate-event
                  {:path path
                   :type :remove}]))))

(defn move
  ([path from]
   (move path from {:sync? false}))
  ([path from {:keys [sync? local?]}]
   (let [dispatcher (if sync? re-frame/dispatch-sync re-frame/dispatch)
         mutate-event (local->mutate-event local?)]
     (dispatcher
      [mutate-event
       {:type :move
        :from from
        :path path}]))))
