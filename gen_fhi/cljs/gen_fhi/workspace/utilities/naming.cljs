(ns gen-fhi.workspace.utilities.naming
  (:require ["pluralize" :as pluralize]
            [clojure.string :as string]))

(defn resource-type->display-name
  ([resource-type]
   (resource-type->display-name resource-type false))
  ([resource-type plural?]
   (when resource-type
     (let [output (string/join " " (re-seq #"[A-Z][a-z]*" resource-type))]
       (if plural?
         (pluralize output)
         output)))))

