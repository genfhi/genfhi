(ns gen-fhi.workspace.utilities.resources
  (:require [gen-fhi.fhirpath.core :as fp]))

(defn endpoint->auth-type [endpoint]
  (or (first (fp/evaluate
              "$this.extension.where(url=%extUrl).value"
              endpoint
              {:options/variables {:extUrl "https://genfhi.com/Extension/auth-type"}}))
      "none"))
