(ns gen-fhi.workspace.utilities.routes
  (:require [gen-fhi.workspace.router :as router]))

(defn ->workspace-smart-route [resource]
  (let [workspace        (get-in (router/match-by-path (.. js/window -location -pathname)) [:parameters :path :workspace])
        smart-auth-route (router/path-for :smart-login
                                          {:workspace     workspace
                                           :resource-type (:resourceType resource)
                                           :id            (:id resource)})]
    (str (.. js/window -location -origin) smart-auth-route)))
