(ns gen-fhi.workspace.utilities.throttle
  (:import [goog.async Throttle Debouncer]))

(defn throttle [f interval]
  (let [dbnc (Throttle. f interval)]
    ;; We use apply here to support functions of various arities
    (fn [& args] (.apply (.-fire dbnc) dbnc (to-array args)))))



(defn debounce [f interval]
  (let [dbnc (Debouncer. f interval)]
    (fn [& args] (.apply (.-fire dbnc) dbnc (to-array args)))))
