(ns gen-fhi.workspace.views.core
  (:require
   [gen-fhi.workspace.views.empty-workspace :as empty-workspace]
   [gen-fhi.workspace.views.error :as error]
   [gen-fhi.workspace.views.instance.core :as instance]
   [gen-fhi.workspace.views.preview :as preview]
   [gen-fhi.workspace.views.resource-type :as resource-type]
   [gen-fhi.workspace.views.workspace :as workspace]
   [gen-fhi.workspace.views.smart-login :as smart-login]
   [gen-fhi.workspace.views.settings :as settings]))

(def instance-view      instance/view)
(def workspace-view     workspace/view)
(def resource-type-view resource-type/view)
(def error-view         error/view)
(def preview-view       preview/view)
(def empty-workspace    empty-workspace/view)
(def smart-login smart-login/view)
(def settings settings/view)
