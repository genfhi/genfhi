(ns gen-fhi.workspace.views.empty-workspace
  (:require [reagent.core :as r]
            [oops.core :as oops]

            ["@auth0/auth0-react" :refer [useAuth0]]
            ["antd" :refer [Card]]

            [gen-fhi.workspace.env :refer [env]]
            [gen-fhi.components.base.button :refer [button]]))

(defn -view []
  (let [auth0-info  (useAuth0)]
    [:div {:class "w-full flex justify-center"}
     [:> Card {:icon "/assets/icon.svg"
               :title (r/as-element
                       [:<>
                        [:img {:src "/assets/icon.svg" :width 28 :height 28 :style {:margin-right "8px"}}]
                        [:span "Thank you for registering"]])

               :style {:margin-top "80px"}
               :width "600px"}
      [:div
       [:div [:span "Thank you for signing up with GenFHI! We will email you once your workspace is ready."]]
       [:div [:span
              [:span "Feel free to  "]
              [:a {:class "text-blue-400 hover:underline hover:text-blue-500 underline" :href "mailto:dev@genfhi.app"} "email us"]
              [:span " with any inquiries, or for more information, visit our website "]
              [:a {:class "text-blue-400 hover:text-blue-500 hover:underline underline" :href "https://genfhi.com"} "here"]
              [:span "."]]
        [:div [:span "We look forward to working with you!"]]]

       [:div {:class "mt-4"}
        [button {:type :secondary
                 :on-click (partial
                            (oops/oget auth0-info "logout")
                            #js{:returnTo (:client/auth0-logout-returnto env)})}
         "Logout"]]]]]))
       

(defn view []
  [:f> -view])
