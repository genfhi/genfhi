(ns gen-fhi.workspace.views.instance.application-editor.action-editor.core
  (:require [clojure.string :as string]
            [reagent.core :as r]
            [re-frame.core :as re-frame]
            [clojure.core.match :refer [match]]

            [antd :refer [Menu Dropdown]]
            ["@ant-design/icons" :refer [EllipsisOutlined CloseOutlined]]
            ["@codemirror/lang-javascript" :refer [javascript javascriptLanguage]]
            ["@codemirror/basic-setup" :refer [basicSetup]]
            ["fp-lang-codemirror" :refer [applicationXFhirQuery]]

            [gen-fhi.workspace.components.dnd :refer [drag-drop-context droppable draggable]]
            [gen-fhi.workspace.constants.extensions :refer [expression-value-extension-url]]
            [gen-fhi.workspace.components.parameter-editor :refer [parameters-editor]]
            [gen-fhi.workspace.icons.core :as logos]
            [gen-fhi.x-fhir-query.core :as x-fhir-query]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.components.capability-completions :refer [resource-autocomplete
                                                                         interactions
                                                                         operation-autocomplete
                                                                         search-parameter-autocomplete]]
            [gen-fhi.components.base.codemirror :refer [codemirror]]
            [gen-fhi.workspace.components.tabs :refer [tabs tab-pane]]
            [gen-fhi.workspace.constants.smart :refer [smart-key]]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.workspace.components.endpoint-editor :refer [endpoint-select]]
            [gen-fhi.workspace.components.editors.core :as editors :refer [text-form-blur dynamic-expression raw-text-editor]]
            [gen-fhi.workspace.components.list :refer [list-item]]
            [gen-fhi.workspace.views.instance.application-editor.constants :as c]
            [gen-fhi.workspace.components.terminology :refer [code-select open-code-select]]
            [oops.core :as oops]
            [gen-fhi.fhirpath.core :as fp]))

(def read-types #{"search-system" "search-type" "read" "vread" "history-instance" "history-type"})
(def selector-height "40.78px")

(defn- handler-list-item [{:keys [handlers idx p on-change]}]
  (let [handler (nth handlers idx)]
    [list-item {:style {:cursor "pointer"}
                :primary-text (:eventType handler)
                :secondary-text (str (:type handler) " " (:action handler))
                :on-click (fn [_e] (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                                       {:type :handler-editor
                                                        :data {:handler-type :action
                                                               :on-change    (fn [handler]
                                                                               (on-change
                                                                                [:handler]
                                                                                (assoc handlers idx handler)))
                                                               :on-delete    (fn []
                                                                               (on-change
                                                                                [:handler]
                                                                                (into (subvec handlers 0 idx) (subvec handlers (inc idx)))))

                                                               :handler      handler
                                                               :handler-path p}}]))}]))

(defn- handler-editor [{:keys [action on-change p]}]
  (let [handlers (get action :handler [])
        add-handler! (fn [] (re-frame/dispatch
                             [:gen-fhi.db.events.ui/set-active-modal
                              {:type :handler-add
                               :data {:handler-path p
                                      :handler-type :action
                                      :on-change (fn [handler]
                                                   (on-change
                                                    [:handler]
                                                    (conj handlers handler)))
                                      :handler {}}}]))]
    [:div {:className "flex mb-2"}
     [:div {:className "md:w-20 flex ml-2 mr-2 pt-2 justify-end"}
      [:label "Handlers"]]
     [:div {:className "flex flex-col items-center flex flex-1 mr-1"}
      [:div {:className "w-full"}
       (map-indexed
        (fn [i handler]
          [handler-list-item {:idx i
                              :on-change on-change
                              :handlers handlers
                              :p (path/descend p i)}])
        handlers)
       [button {:className "w-full" :type :secondary :on-click add-handler!}
        "+ Create Handler"]]]]))

(defn- header-editor [{:keys [action on-change]}]
  (let [headers (get action :header [])]
    [:div {:className "flex mb-2"}
     [:div {:className "md:w-20 flex ml-2 mr-2 pt-2 justify-end"}
      [:label "Headers"]]
     [:div {:className "items-center flex flex-1 flex-col mr-1"}
      (map-indexed
       (fn [idx header]
         (let [[header value] (string/split header #":")]
           ^{:key idx}
           [:div {:className "flex w-full mb-1"}
            [open-code-select {:value-set    "http://genfhi.com/ValueSet/http-methods-valueset"
                               :value       header
                               :on-change   (fn [header]
                                              (on-change
                                               [:header]
                                               (assoc
                                                headers
                                                idx
                                                (str header ":" value))))
                               :select-props
                               {:placeholder "Key"
                                :style {:height "39px"}}}]

            [raw-text-editor {:hide-label? true
                              :value value
                              :placeholder "Value"
                              :on-change (fn [e]
                                           (on-change
                                            [:header]
                                            (assoc
                                             headers
                                             idx
                                             (str header ":" (.. e -target -value)))))}]
            [button {:type :secondary
                     :on-click (fn [_e]
                                 (on-change [:header]
                                            (into (subvec headers 0 idx) (subvec headers (inc idx)))))
                     :className "ml-1"
                     :style    {:height "39px"}
                     :icon     (r/as-element [:> CloseOutlined])}
             "X"]]))
       headers)

      [button {:className "w-full"
               :type :secondary
               :on-click (fn [_e]
                           (on-change [:header] (into headers [":"])))}
       "+ Add Header"]]]))

(defn- ->smart-iss-path [client-path]
  (let [{:keys [resource-type id]} (path/path-meta client-path)]
    (path/->path resource-type id "smart" "iss")))

(defn- query-editor [{:keys [action on-change p state-path]}]
  (let []
    (fn [{:keys [action]}]
      (let [action-type   (get action :type)
            expression    (get action :location)
            parsed-query  (x-fhir-query/parse-query (get expression :expression ""))
            level         (get action :level)
            action-smart? (= smart-key (get-in action [:endpoint :reference]))
            smart-iss     @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (->smart-iss-path p)])
            endpoint      (if action-smart? {:resourceType "Endpoint"
                                             :address smart-iss
                                             :connectionType {:code "hl7-fhir-rest"}
                                             :name "SMART"
                                             :payloadType [{:coding [{:code "urn:ihe:pcc:cm:2008"}]}]
                                             :header ["Content-type:application/json"]
                                             :status "active"}
                              (get action :endpoint))]
        [:div
         (when (= "resource" level)
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
             [:label "Resource"]]
            [:div {:className "flex flex-1 items-center mr-1"}
             [resource-autocomplete {:endpoint    endpoint
                                     :placeholder "Select a resource type."
                                     :value       (get-in parsed-query [:path 0] "")
                                     :on-change   (fn [resource]
                                                    (let [exp (assoc-in
                                                               parsed-query
                                                               [:path 0]
                                                               resource)
                                                          query (x-fhir-query/parse-query->string exp)]
                                                      (on-change [:location]
                                                                 {:language "application/x-fhir-query"
                                                                  :expression query})))}]]])
         (when (or
                (= action-type "patch")
                (= action-type "delete")
                (= action-type "update")
                (= action-type "read"))
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
             [:label "ID"]]
            [:div {:className "flex flex-1 items-center mr-1"}
             [dynamic-expression   {:value     {:expression (get-in parsed-query [:path 1] "")}
                                    :height    "40px"
                                    :on-change (fn [text]
                                                 (let [exp (assoc-in
                                                            parsed-query
                                                            [:path 1]
                                                            text)
                                                       query (x-fhir-query/parse-query->string exp)]
                                                   (on-change [:location]
                                                              {:language "application/x-fhir-query"
                                                               :expression query})))
                                    :state-path state-path
                                    :p p
                                    :language "application/x-fhir-query"}]]])

         [:div {:className "flex mb-2"}
          [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
           [:label "Location"]]
          [:div {:className "flex flex-1 items-center"}
           [:div {:className "w-32 mr-1"}
            [interactions {:style         {:height selector-height}
                           :endpoint      endpoint
                           :level         level
                           :resource-type (get-in parsed-query [:path 0])
                           :placeholder   "Select an interaction"
                           :value         (get action :type)
                           :on-change     (fn [value]
                                            (on-change
                                             [:trigger]
                                             (if (some? (read-types value))
                                               "automatic"
                                               "manual"))
                                            ;; Setting the content-type too json-patch+json
                                            ;; which is required for json patch
                                            (when (= "patch" value)
                                              (on-change [:header]
                                                         ["Content-type:application/json-patch+json"]))
                                            (on-change
                                             [:type]
                                             value))}]]
           [:div {:className "flex flex-1 items-center mr-1"}
            [dynamic-expression   {:value      expression
                                   :height     "40px"
                                   :theme     {".cm-scroller" {"overflow" "hidden"}}
                                   :on-change  (fn [text]
                                                 (on-change [:location]
                                                            {:language "application/x-fhir-query"
                                                             :expression text}))
                                   :state-path state-path
                                   :p          p
                                   :language   "application/x-fhir-query"}]]]]
         (when (= (get action :type) "operation")
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
             [:label "Operations"]]
            [:div {:className "flex flex-1 mr-1"}
             [operation-autocomplete {:resource-type (get-in parsed-query [:path 0])
                                      :level         level
                                      :endpoint      endpoint
                                      :value         (get-in action [:operation :canonical])
                                      :on-change     (fn [op-info]
                                                       (on-change [:operation]
                                                                  {:name      (:name op-info)
                                                                   :canonical (:definition op-info)})
                                                       (:definition op-info))}]]])

         (when (or (= "search-system" action-type)
                   (= "search-type" action-type))
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex ml-2 mr-2 pt-2 justify-end"}
             [:label "Parameters"]]
            [:div {:className "flex flex-1 flex-col items-center mr-1"}
             (doall
              (map-indexed
               (fn [i query]
                 [:div {:key i :className "mb-2 flex w-full"}
                  [:div {:className "w-5/12 mr-1"}
                   [search-parameter-autocomplete
                    {:style         {:height selector-height}
                     :endpoint      endpoint
                     :level         level
                     :resource-type (get-in parsed-query [:path 0])
                     :value         (:key query)
                     :on-change     (fn [text]
                                      (let [exp (assoc-in
                                                 parsed-query
                                                 [:queries i :key]
                                                 text)
                                            query (x-fhir-query/parse-query->string
                                                   exp)]
                                        (on-change [:location]
                                                   {:language "application/x-fhir-query"
                                                    :expression query})))}]]

                  [:div {:className "flex w-6/12"}
                   [dynamic-expression {:p p
                                        :height    "40px"
                                        :language "application/x-fhir-query"
                                        :state-path state-path
                                        :value {:expression (:val query)}
                                        :on-change
                                        (fn [text _view-update]
                                          (let [exp (assoc-in
                                                     parsed-query
                                                     [:queries i :val]
                                                     text)
                                                query (x-fhir-query/parse-query->string
                                                       exp)]
                                            (on-change [:location]
                                                       {:language "application/x-fhir-query"
                                                        :expression query})))}]]
                  [:div {:className "flex w-1/12 justify-end"}
                   [button {:className "ml-1 w-full"
                            :type :secondary
                            :on-click (fn [_e]
                                        (let [exp (update
                                                   parsed-query
                                                   :queries
                                                   (fn [queries]
                                                     (vec
                                                      (concat
                                                       (subvec queries 0 i)
                                                       (subvec queries (+ 1 i) (count queries))))))]

                                          (on-change [:location]
                                                     {:language "application/x-fhir-query"
                                                      :expression (x-fhir-query/parse-query->string exp)})))
                            :style    {:height selector-height}}
                    "X"]]])
               (get parsed-query :queries [])))
             [button {:className "w-full"
                      :type :secondary
                      :on-click (fn [_e]
                                  (on-change [:location]
                                             {:language "application/x-fhir-query"
                                              :expression (x-fhir-query/parse-query->string
                                                           (update-in parsed-query [:queries]
                                                                      (fn [q] (conj q {:key "" :val ""}))))}))}

              "+ Add Parameter"]]])]))))

(defn- patch-row [{:keys [state-path p on-change on-delete row]}]
  (let [path-expression (first (fp/evaluate "path.extension.where(url=%extUrl).value.expression" row
                                            {:options/variables
                                             {:extUrl expression-value-extension-url}}))
        value-expression (first (fp/evaluate "value.extension.where(url=%extUrl).value.expression" row
                                             {:options/variables
                                              {:extUrl expression-value-extension-url}}))]
    [list-item {:style {:cursor "pointer"}
                :primary-text (:op row)
                :secondary-text (match (:op row)
                                  "add"     (str "at: " path-expression " with: " value-expression)
                                  "replace" (str "at: " path-expression " with: " value-expression)
                                  :else     "")

                :on-click (fn [_e]
                            (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                                {:type :patch-editor
                                                 :data {:state-path   state-path
                                                        :p            p
                                                        :row          row
                                                        :on-change    on-change
                                                        :on-delete    on-delete}}]))}]))

(defn- patch-editor [{:keys [on-change state-path  p]}]
  [:div {:className "flex mb-2"}
   [:div {:className "md:w-20 flex ml-2 mr-2 pt-2 justify-end"}
    [:label "Patches"]]
   (let [patch-body (try
                      (js->clj (.parse js/JSON @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit (path/extend-path p :body :expression)]))
                               :keywordize-keys true)
                      (catch js/Error _e
                        []))]
     [:div {:className "flex flex-1 items-center mr-1"}
      [:div {:className "flex flex-1 flex-col"}
       (map-indexed
        (fn [idx row]
          [patch-row {:p p
                      :state-path state-path
                      :row row
                      :on-delete
                      (fn []
                        (on-change
                         [:body]
                         {:language   "application/x-fhir-query"
                          :extension  [(editors/->post-processing-extension "json")]
                          :expression
                          (.stringify js/JSON (clj->js
                                               (concat
                                                (subvec
                                                 patch-body
                                                 0 idx)
                                                (subvec
                                                 patch-body
                                                 (min
                                                  (count patch-body)
                                                  (+ 1 idx))
                                                 (count patch-body)))))}))
                      :on-change
                      (fn [new-row]
                        (on-change
                         [:body]
                         {:extension  [(editors/->post-processing-extension
                                        "json-derive-expressions")]
                          :language   "genfhi/post-json-x-fhir-query-process"
                          :expression  (.stringify
                                        js/JSON
                                        (clj->js (assoc patch-body idx new-row)))}))}])

        patch-body)
       [:div {:className "flex"}
        [button
         {:type :secondary
          :className "w-full"
          :on-click (fn [_e]
                      (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                          {:type :patch-editor
                                           :data {:state-path   state-path
                                                  :p            p
                                                  :row          {:op "add"}
                                                  :on-change
                                                  (fn [new-row]
                                                    (on-change
                                                     [:body]
                                                     {:language   "genfhi/post-json-x-fhir-query-process"
                                                      :extension  [(editors/->post-processing-extension
                                                                    "json-derive-expressions")]
                                                      :expression (.stringify
                                                                   js/JSON
                                                                   (clj->js
                                                                    (conj
                                                                     patch-body
                                                                     new-row)))}))}}]))}

         "+ Create operation"]]]])])

(defn- body-editor [{:keys [p state-path action on-change]}]
  (match (:type action)
    "patch" [:<>
             [patch-editor
              {:on-change on-change
               :p p
               :state-path state-path}]]

    "operation"
    (let [evaled-body          (js->clj (.parse js/JSON @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit (path/extend-path p :body :expression)]))
                                        :keywordize-keys true)
          canonical-resolve-op @(re-frame/subscribe
                                 [:gen-fhi.db.subs.operation/get-operation-response
                                  "resolve-canonical"
                                  {:resourceType "Parameters"
                                   :parameter
                                   [{:name          "endpoint"
                                     :valueReference (get action :endpoint)}
                                    {:name "url"
                                     :valueString (get-in action [:operation :canonical])}]}])]
      [:div {:className "flex mb-2"}
       [:div {:className "md:w-20 flex ml-2 mr-2 pt-2 justify-end"}
        [:label "Parameters"]]
       [:div {:className "flex flex-1 items-center mr-1 w-full"}
        [parameters-editor
         {:use       "in"
          :ctx-path p
          :state-path state-path
          :operation-definition (:return canonical-resolve-op)
          :value     evaled-body
          :on-change (fn [parameters]
                       (on-change
                        [:body]
                        {:extension  [(editors/->post-processing-extension "json-derive-expressions")]
                         :language   "genfhi/post-json-x-fhir-query-process"
                         :expression (.stringify js/JSON (clj->js parameters))}))}]]])

    :else [:div {:className "flex mb-2"}
           [:div {:className "md:w-20 flex items-center justify-end ml-2 mr-2"}
            [:label "Body"]]
           [:div {:className "flex flex-1 items-center mr-1"}
            [dynamic-expression
             {:value     (get action :body)
              :height     "150px"
              :extensions [(applicationXFhirQuery #js{:baseLanguage javascriptLanguage})]
              :on-change (fn [text]
                           (on-change
                            [:body]
                            {:language "application/x-fhir-query"
                             :expression text
                             :extension [(editors/->post-processing-extension "json")]}))
              :state-path state-path
              :p p
              :language "application/x-fhir-query"}]]]))

(defn- action-tab-header [{:keys [p on-delete]}]
  (let [is-temp-edits? @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/temp-edits? p])]
    [:div {:className "flex w-full items-center mr-2 ml-2"}
     [text-form-blur {:hide-label? true
                      :className   (str c/id-text-classname
                                        " text-center")
                      :p           (path/descend p "id")}]
     [:div {:className "flex flex-1 justify-end pr-1"}
      [:div {:class "p-1"}
       [button {:type :primary
                :on-click (fn [_e]
                            (re-frame/dispatch
                             [:gen-fhi.db.events.temp-edits/apply-temp-mutation p]))
                :disabled (not is-temp-edits?)}
        "Save"]]
      [:div {:class "flex justify-center items-center"}
       [:> Dropdown {:placement "bottomRight"
                     :overlay
                     (r/as-element
                      [:> Menu {:mode "horizontal" :style {:border "0px"}}
                       [:> (.-Item Menu) {:key "delete" :on-click (fn []
                                                                    (on-delete))}
                        "Delete"]])}
        [:a {:className "ant-dropdown-link" :on-click (fn [e] (.preventDefault e))}
         [:> EllipsisOutlined {:style {:font-size "18px"}}]]]]]]))

(defn- fhir-rest-editor [{:keys [p state-path on-delete]}]
  [:div {:class "flex flex-1 flex-col w-full overflow-auto"}
   [tabs {:active "general"
          :tab-direction :left
          :tab-right-content [action-tab-header {:p p :on-delete on-delete}]}
    [:f> tab-pane {:key "general" :name "General"}
     (let [action      @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit p])]
       ^{:key (get action :id)}
       [:div {:className "flex flex-col flex-1 overflow-auto"}
        [:div {:className "mt-2 w-full"}
         [:div {:className "flex flex-col"}
          [:div {:className "border-r-0 border-l-0 border-t-0 border"}
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex items-center justify-end ml-2 mr-2"}
             [:label "Endpoint"]]
            [:div {:className "flex flex-1 items-center mr-1"}
             [endpoint-select {:reference (get action :endpoint)
                               :client-path p
                               :select-props
                               {:placeholder "Select an endpoint"
                                :style {:width "100%"}
                                :on-change (fn [reference]
                                             (re-frame/dispatch
                                              [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                               {:path (path/descend p "endpoint")
                                                :type :replace
                                                :value {:reference reference}}]))}}]]]
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex items-center justify-end ml-2 mr-2"}
             [:label "Trigger"]]
            [:div {:className "flex flex-1 items-center mr-1"}
             [code-select
              {:value-set  "http://genfhi.com/ValueSet/trigger-types"
               :value     (get action :trigger)
               :on-change (fn [code]
                            (re-frame/dispatch
                             [:gen-fhi.db.events.temp-edits/set-temp-mutation
                              {:path (path/descend p "trigger")
                               :type :replace
                               :value code}]))}]]]
           [:div {:className "flex mb-2"}
            [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
             [:label "Level"]]
            [:div {:className "flex flex-1 items-center mr-1"}
             [code-select
              {:value-set  "http://genfhi.com/application-action-level"
               :value     (get action :level)
               :on-change (fn [code]
                            (re-frame/dispatch
                             [:gen-fhi.db.events.temp-edits/set-temp-mutation
                              {:path p
                               :type :replace
                               :value {:id       (get action :id)
                                       :endpoint (get action :endpoint)
                                       :trigger  (get action :trigger)
                                       :level code}}]))}]]]]

          [:div {:className "mt-4"}
           [query-editor {:p          (path/descend p "location")
                          :action     action
                          :on-change  (fn [keys value]
                                        (re-frame/dispatch-sync
                                         [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                          {:path (apply path/extend-path p keys)
                                           :type :replace
                                           :value value}]))
                          :state-path state-path}]

           [header-editor {:action      action
                           :on-change (fn [keys value]
                                        (re-frame/dispatch
                                         [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                          {:path (apply path/extend-path p keys)
                                           :type :replace
                                           :value value}]))
                           :action-type (get action :type)}]

           (when (nil? (read-types (get action :type)))
             [body-editor {:action      action
                           :on-change (fn [keys value]
                                        (re-frame/dispatch
                                         [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                          {:path (apply path/extend-path p keys)
                                           :type :replace
                                           :value value}]))
                           :state-path state-path
                           :p          p}])]]]])]

    [:f> tab-pane {:key "settings" :name "Settings"}
     (let [action      @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit p])]
       [:div {:class "p-2 h-full w-full overflow-auto"}
        [:div {:className "flex mb-2"}
         [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
          [:label "Is Disabled"]]
         [:div {:className "flex flex-1 items-center mr-1"}
          [dynamic-expression
           {:value     (get action :isDisabled)
            :extensions [(applicationXFhirQuery #js{:baseLanguage javascriptLanguage})]
            :on-change (fn [text]
                         (re-frame/dispatch-sync
                          [:gen-fhi.db.events.temp-edits/set-temp-mutation
                           {:path (path/extend-path p "isDisabled")
                            :type :replace
                            :value {:expression text
                                    :language   "application/x-fhir-query"
                                    :extension  [(editors/->post-processing-extension "boolean")]}}]))
            :state-path state-path
            :p p
            :language "application/x-fhir-query"}]]]
        [handler-editor {:action    action
                         :on-change (fn [keys value]
                                      (re-frame/dispatch
                                       [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                        {:path (apply path/extend-path p keys)
                                         :type :replace
                                         :value value}]))
                         :p         (path/descend p  "handler")}]])]
    [:f> tab-pane {:key "response" :name "Response"}
     (let [cur-id   @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/descend p "id")])
           response @(re-frame/subscribe [:gen-fhi.db.subs.actions/get-cached-action p (keyword cur-id)])]
       [:div {:className "json-editor h-full w-full bg-white p-2 overflow-auto"}
        [:f> codemirror
         {:extensions [basicSetup (javascript #js{})]
          :value      (when (some? response)
                        (try
                          (.stringify js/JSON (clj->js (get response :response)) nil 2)
                          (catch js/Error _e
                            "")))
          :disabled?  true
          :height     "100%"
          :width      "100%"}]])]]])

(defn- custom-code-editor [{:keys [p state-path on-delete]}]
  [:div {:class "flex flex-1 flex-col w-full overflow-auto"}
   [tabs {:active "general"
          :tab-direction :left
          :tab-right-content [action-tab-header {:p p :on-delete on-delete}]}
    [:f> tab-pane {:key "general" :name "General"}
     (let [action @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit p])]
       [:div {:className "flex flex-col flex-1 overflow-auto"}
        [:div {:className "flex mb-2 mt-2"}
         [:div {:className "md:w-20 flex items-center justify-end ml-2 mr-2"}
          [:label "Trigger"]]
         [:div {:className "flex flex-1 items-center mr-1"}
          [code-select
           {:value-set  "http://genfhi.com/ValueSet/trigger-types"
            :value     (get action :trigger)
            :on-change (fn [code]
                         (re-frame/dispatch
                          [:gen-fhi.db.events.temp-edits/set-temp-mutation
                           {:path (path/descend p "trigger")
                            :type :replace
                            :value code}]))}]]]
        [:div {:className "flex flex-1 mb-4"}
         [:div {:className "md:w-20 flex ml-2 mr-2 pt-2 justify-end"}
          [:label "JavaScript"]]
         [:div {:className "flex flex-1 items-center mr-1"}
          [:div {:className "json-editor h-full w-full bg-white"}
           [dynamic-expression {:allow-modal true
                                :hide-label? true
                                :extensions [(applicationXFhirQuery #js{:baseLanguage javascriptLanguage})]
                                :p (path/descend p "body")
                                :state-path state-path
                                :value      (get action :body)
                                :height     "180px"
                                :width      "100%"
                                :on-change (fn [text]
                                             (re-frame/dispatch-sync
                                              [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                               {:path (path/extend-path p "body")
                                                :type :replace
                                                :value {:expression text
                                                        :language "application/x-fhir-query"
                                                        :extension [(editors/->post-processing-extension "javascript")]}}]))
                                :language "application/x-fhir-query"}]]]]])]
    [:f> tab-pane {:key "settings" :name "Settings"}
     (let [action      @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit p])]
       [:div {:class "p-2 h-full w-full overflow-auto"}
        [:div {:className "flex mb-2"}
         [:div {:className "md:w-20 flex items-center justify-end justify-end ml-2 mr-2"}
          [:label "Is Disabled"]]
         [:div {:className "flex flex-1 items-center mr-1"}
          [dynamic-expression
           {:value     (get action :isDisabled)
            :extensions [(applicationXFhirQuery #js{:baseLanguage javascriptLanguage})]
            :on-change (fn [text]
                         (re-frame/dispatch-sync
                          [:gen-fhi.db.events.temp-edits/set-temp-mutation
                           {:path (path/extend-path p "isDisabled")
                            :type :replace
                            :value {:expression text
                                    :language   "application/x-fhir-query"
                                    :extension  [(editors/->post-processing-extension "boolean")]}}]))
            :state-path state-path
            :p p
            :language "application/x-fhir-query"}]]]
        [handler-editor {:action    action
                         :on-change (fn [keys value]
                                      (re-frame/dispatch
                                       [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                        {:path (apply path/extend-path p keys)
                                         :type :replace
                                         :value value}]))
                         :p         (path/descend p  "handler")}]])]
    [:f> tab-pane {:key "response" :name "Response"}
     (let [cur-id   @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/descend p "id")])
           response @(re-frame/subscribe [:gen-fhi.db.subs.actions/get-cached-action p (keyword cur-id)])]
       [:div {:className "json-editor h-full w-full bg-white p-2 overflow-auto"}
        [:f> codemirror
         {:extensions [basicSetup (javascript #js{})]
          :value      (when (some? response)
                        (try
                          (.stringify js/JSON (clj->js (get response :response)) nil 2)
                          (catch js/Error _e
                            "")))
          :disabled?  true
          :height     "100%"
          :width      "100%"}]])]]])

(defn- single-action-editor [{:keys [p state-path on-delete]}]
  (let [action      @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit p])
        app-actions @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])]
    (if (empty? app-actions)
      [:div {:className "flex items-center justify-center w-full"}
       [:div {:className "flex items-center justify-center text-gray-600 font-semibold"}
        [:span "Select an action to view its properties."]]]
      (when (some? action)
        (match (get action :actionType "fhir-rest")
          "fhir-rest"   [fhir-rest-editor   {:p p :state-path state-path :on-delete on-delete}]
          "custom-code" [custom-code-editor {:p p :state-path state-path :on-delete on-delete}])))))

(defn pulse []
  [:span {:class "flex h-full w-full"}
   [:span {:class "animate-pulse inline-flex h-full w-full rounded-full bg-orange-300 opacity-75"}]])

(defn- action-icon [{:keys [action-type]}]
  (match action-type
    "fhir-rest"   [:div {:class "h-3 w-8 text-red-400 text-xs"} "FHIR"]
    "custom-code" [:span {:class "text-gray-400 text-xs"} [:div {:class "w-8"}
                                                           [logos/js-logo {:className "w-5 h-5"}]]]))

(defn- action-list-item [{:keys [p item-props index action] :or {item-props {}}}]
  (let [is-temp-edits? @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/temp-edits? (path/descend p index)])]
    [:li item-props
     [:div {:style {:display "flex" :flex 1 :align-items "center"}}
      [:div {:class "flex flex-1 items-center"}
       [:div {:class "mr-1"}
        [action-icon {:action-type (get action :actionType "fhir-rest")}]]
       (str (or (:name action) (:id action)))]
      [:span {:style {:font-size "8px" :font-weight "300"}}
       (when (:type action)
         (str "<" (:type action) "> "))]
      [:div {:class "ml-1 h-3 w-3"}
       (when is-temp-edits?
         [pulse])]]]))

(defn action-editor [_props]
  (let [selected-action-index (r/atom 0)]
    (fn [{:keys [state-path p]}]
      (let [app-actions    @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
            create-action (fn [action]
                            (re-frame/dispatch
                             [:gen-fhi.db.events.mutate/mutate
                              {:path (path/extend-path p (count app-actions))
                               :type :add
                               :value (merge
                                       {:id   (str "action" (.round js/Math (* (.random js/Math) 100000)))}
                                       action)}])
                            (reset! selected-action-index (count app-actions)))]
        [:div {:className "flex flex-1 h-full" :style {:min-width "1000px"}}

         [:div {:className "border border-y-0 border-l-0 border-gray-300 overflow-auto" :style {:min-width "250px"
                                                                                                :max-width "250px"}}
          [:div  {:class "flex items-center p-1 border border-t-0 border-r-0 border-l-0 border-grey-300"}
           [:div {:class "flex flex-1"}
            [:span {:className "text-base "}

             "Actions"]]
           [:> Dropdown {:placement "bottomRight"
                         :overlay
                         (r/as-element
                          [:> Menu {:mode "horizontal" :style {:border "0px"}}
                           [:> (.-Item Menu) {:key "fhir-rest"
                                              :on-click
                                              (fn []
                                                (create-action {:type "search-type"
                                                                :actionType "fhir-rest"
                                                                :trigger "automatic"
                                                                :level   "resource"
                                                                :description "New action description"}))}
                            [:div {:class "items-center flex text-slate-600"}
                             [:div {:class "mr-1"}
                              [action-icon {:action-type "fhir-rest"}]]
                             "FHIR query"]]

                           [:> (.-Item Menu) {:key "custom-code"
                                              :on-click
                                              (fn []
                                                (create-action {:actionType "custom-code"
                                                                :trigger "automatic"
                                                                :level   "resource"
                                                                :description "New action description"}))}
                            [:div {:class " items-center flex text-slate-600"}
                             [:div {:class "mr-1"}
                              [action-icon {:action-type "custom-code"}]]
                             "Custom code"]]])}

            [:button {:class "text-white border-0 bg-blue-500 hover:bg-blue-600 align-middle rounded border inline-block text-center cursor-pointer py-2 px-4 font-semibold disabled:bg-gray-300"}
             "New"]]]
          (when (empty? app-actions)
            [:div {:className "flex items-center justify-center w-full mt-2 p-1"}
             [:div {:className "flex items-center justify-center text-gray-600 font-semibold"}
              [:span "There are no actions."]]])
          [drag-drop-context {:on-drag-end (fn [result]
                                             (when-let [destination (.-destination result)]
                                               (let [source (.-source result)
                                                     path (path/descend p (.-index destination))
                                                     from (path/descend p (.-index source))]

                                                 (reset! selected-action-index (.-index destination))
                                                 ;; [TODO] Saving temp states because invalidated by index should instead just alter those temps to be in sync
                                                 ;; with move as opposed to saving?
                                                 (re-frame/dispatch-sync
                                                  [:gen-fhi.db.events.action/move-action from path]))))}
                                                  ;; (re-frame/dispatch-sync
                                                  ;;  [:gen-fhi.db.events.action/move-action
                                                  ;;   {:type :move
                                                  ;;    :from from
                                                  ;;    :path path}]))))}

           [:ul {:style {:list-style-type "none"}}
            [droppable {:droppableId "droppable"}
             [:<>
              (map-indexed
               (fn [index action]
                 ^{:key (:id action)}
                 [draggable {:index index :draggableId (:id action) :key (:id action)}
                  (fn [_provided snapshot]
                    [action-list-item
                     {:item-props
                      {:on-click #(reset! selected-action-index index)
                       :className (str "cursor-pointer p-1 border border-x-0 border-t-0 border-gray-100 hover:bg-blue-100 hover:text-blue-500 hover:border-blue-200"
                                       (when (or (oops/oget snapshot "isDragging")
                                                 (= index @selected-action-index))  " bg-blue-100 text-blue-500 border-blue-200"))}
                      :p p
                      :index index
                      :action action}])])

               app-actions)]]]]]
         [single-action-editor {:p (path/descend p @selected-action-index)
                                :state-path state-path
                                :on-delete (fn []
                                             (re-frame/dispatch
                                              [:gen-fhi.db.events.temp-edits/remove-temp-mutation
                                               (path/extend-path p @selected-action-index)])
                                             (reset! selected-action-index 0))}]]))))
