(ns gen-fhi.workspace.views.instance.application-editor.constants
  (:require [clojure.core.match :refer [match]]
            ["@ant-design/icons" :refer [AlignLeftOutlined
                                         PlusSquareOutlined
                                         FileImageOutlined
                                         EditOutlined
                                         NumberOutlined
                                         TranslationOutlined
                                         CodeOutlined
                                         TableOutlined
                                         Html5Outlined
                                         ClockCircleOutlined
                                         CalendarOutlined
                                         FormOutlined]]))

(def id-text-classname (str "rounded font-semibold border !border-transparent p-1 w-full "
                            "hover:!bg-gray-100 hover:!border-transparent "
                            "focus:!bg-white focus:!border-blue-400"))

(defn react-icon [{:keys [height width]}]
  [:svg {:height height :width width :xmlns "http://www.w3.org/2000/svg" :viewBox "-11.5 -10.23174 23 20.46348"}
   [:title "React Logo"]
   [:circle {:cx "0" :cy "0" :r "2.05" :fill "currentColor"}]
   [:g {:stroke "currentColor" :stroke-width "1" :fill "none"}
    [:ellipse {:rx "11" :ry "4.2"}]
    [:ellipse {:rx "11" :ry "4.2" :transform "rotate(60)"}]
    [:ellipse {:rx "11" :ry "4.2" :transform "rotate(120)"}]]])

(defn item-icon [{:keys [type]}]
  (match type
    "questionnaire" [:> FormOutlined]
    "list"     [:> TableOutlined]
    "text"     [:> EditOutlined]
    "label"    [:> AlignLeftOutlined]
    "image"    [:> FileImageOutlined]
    "integer"  [:> NumberOutlined]
    "decimal"  [:> NumberOutlined]
    "html"     [:> Html5Outlined]
    "code"     [:> TranslationOutlined]
    "json"     [:> CodeOutlined]
    "button"   [:> PlusSquareOutlined]
    "date"     [:> CalendarOutlined]
    "datetime" [:> CalendarOutlined]
    "time"     [:> ClockCircleOutlined]
    "react"    [react-icon {:height "16px" :width "16px"}]
    :else      [:<>]))

