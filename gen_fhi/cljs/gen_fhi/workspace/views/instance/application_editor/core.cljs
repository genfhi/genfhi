(ns gen-fhi.workspace.views.instance.application-editor.core
  (:require [clojure.string :as string]
            [reagent.core :as r]
            [re-frame.core :as re-frame]
            [clojure.core.match :refer [match]]

            ["antd" :refer [Alert]]

            [gen-fhi.workspace.utilities.routes :as smart-util]
            [gen-fhi.application.core :refer [application]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.components.tabs :refer [tabs tab-pane]]
            [gen-fhi.workspace.components.ui-state :refer [ui-state]]
            [gen-fhi.workspace.components.editors.core :refer [text-form textarea-form]]
            [gen-fhi.workspace.components.layout :refer [layout left-sidebar right-sidebar bottom-bar]]

            [gen-fhi.workspace.views.instance.application-editor.templates.core :refer [templates]]
            [gen-fhi.workspace.views.instance.application-editor.constants :refer [item-icon]]
            [gen-fhi.workspace.views.instance.application-editor.item-properties :refer [item-properties]]
            [gen-fhi.workspace.views.instance.application-editor.action-editor.core :refer [action-editor]]))

(defn palette-item [{:keys [item]}]
  [:li {:key (:type item)
        :className "h-8 hover:text-blue-500 hover:bg-blue-100 border border-x-0 border-t-0 border-gray-100 flex items-center"
        :style {:user-select "none"
                :cursor "grab"
                :padding-left "8px"}
        :on-mouse-down
        #(re-frame/dispatch [:gen-fhi.db.events.ui/set-dragging-item {:type :drag
                                                                      :item item}])}
   (r/as-element [:div {:className "flex items-center"}
                  [:div {:className "flex items-center mr-4"}
                   [item-icon {:type (:type item)}]]
                  [:div {:className "text-sm"} (string/capitalize (:type item))]])])
(defn palette []
  [:ul {:style {:padding "0px"
                :font-size "16px"
                :list-style-type "none"}}
   (doall
    (map
     (fn [item-type]
       (palette-item {:item (templates item-type)}))
     (sort compare (keys templates))))])

(defn application-properties [{:keys [p]}]
  [:div {:class "p-2"}
   [:div {:className "mb-4"}
    [text-form  {:p (path/descend p "name")}]]
   [:div {:className "mb-4"}
    [textarea-form {:p (path/descend p "description")}]]])

(defn smart-properties [{:keys [p]}]
  (let [app       @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        client-id @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/extend-path p "smart" "clientId")])]
    [:div {:className "p-2"}
     [text-form {:className "mb-2"
                 :p         (path/extend-path p "smart" "clientId")}]
     [text-form {:className "mb-2"
                 :p         (path/extend-path p "smart" "scope")}]
     [text-form {:className "mb-2"
                 :p         (path/extend-path p "smart" "iss")}]
     (when (some? client-id)
       [:> Alert {:type    "warning"
                  :message (str "To tie SMART auth to editor use the following redirect url '" (smart-util/->workspace-smart-route app) "'.")}])]))

(defn application-editor [_props]
  ;; On mount display the right and bottom sidebars initially.
  (fn [{:keys [resource-type id]}]
    (let [app-path         (path/->path resource-type id)
          state-path       (path/->path "ApplicationState" "editor" "states")
          app-val          @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value app-path])]
      [:div 
       {:class "h-full"
        :on-key-down   (fn [e]
                         (match [(.-ctrlKey e) (.-keyCode e)]
                                                            ;; Ctrl + backspace remove
                           [true 189] (re-frame/dispatch [:gen-fhi.db.events.ui/remove-selected-item])
                                                            ;; Escape
                           [false 27] (do
                                        (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :right false])
                                        (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :left false])
                                        (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :bottom false])
                                        (re-frame/dispatch [:gen-fhi.db.events.ui/deselect-item-path]))
                           :else      nil))}
       [layout
        {:left-sidebar
         [:<>
          (if (get app-val :smart)
            [left-sidebar {}
             [tabs {:active "app"}
              [:f> tab-pane {:key "app" :name "Application"}
               [:div {:className "p-1"}
                [application-properties {:p app-path}]]]
              [:f> tab-pane {:key "smart" :name "SMART"}
               [:div {:className "p-1"}
                [smart-properties {:p app-path}]]]]]
            [left-sidebar {}
             [tabs {:active "app"}
              [:f> tab-pane {:key "app" :name "Application"}
               [:div {:className "p-1"}
                [application-properties {:p app-path}]]]]])]

         :right-sidebar
         [ui-state {:ui-key :item-sidebar :default-value "item-properties"}
          (fn [tab tab-on-change]
            [right-sidebar {}
             [tabs {:active tab :on-change tab-on-change}
              [:f> tab-pane {:key "item-properties" :name "Item"}
               [:div {:className "p-1"}
                [item-properties {:state-path state-path}]]]
              [:f> tab-pane {:key "components" :name "Components"}
               [:div {:className "p-1"}
                [palette]]]]])]
         :bottom-bar
         [bottom-bar {}
          [action-editor {:state-path state-path :p (path/descend app-path "action")}]]}

        [:div {:style {:display "flex" :flex 1}}
         [application {:is-editable? true
                       :path         app-path
                       :state-path   state-path}]]]])))
