(ns gen-fhi.workspace.views.instance.application-editor.item-properties
  (:require
   [clojure.string :as string]
   [clojure.core.match :refer [match]]
   [reagent.core :as r]
   [re-frame.core :as re-frame]
   [oops.core :as oops]

   ["fp-lang-codemirror" :refer [applicationXFhirQuery]]
   ["@codemirror/lang-javascript" :refer [javascriptLanguage]]
   ["antd" :refer [Dropdown Menu Popconfirm Switch]]
   ["@ant-design/icons" :refer [EllipsisOutlined MacCommandOutlined EditOutlined EyeOutlined EyeInvisibleOutlined]]
   ["uuid" :refer [v4]]

   [gen-fhi.application.widgets.core :as widgets]
   [gen-fhi.workspace.components.dnd :refer [drag-drop-context droppable draggable]]
   [gen-fhi.workspace.views.instance.application-editor.upgrades.widgets :as upgrades]
   [gen-fhi.workspace.router :as router]
   [gen-fhi.patch-building.path :as path]
   [gen-fhi.workspace.components.editors.core :refer [-label
                                                      reference-select-form
                                                      code-expression-form
                                                      valueset-select-expression
                                                      text-form-blur
                                                      expression-form]]
   [gen-fhi.workspace.views.instance.application-editor.constants :as c]
   [gen-fhi.workspace.components.list :refer [list-item]]
   [gen-fhi.workspace.utilities.mutations :as mutations]))

(defn- handler-list-item [{:keys [item handler p]}]
  [list-item {:style {:cursor "pointer"}
              :primary-text (:eventType handler)
              :secondary-text (str (:type handler) " " (:action handler))
              :on-click (fn [_e] (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                                     {:type :handler-editor
                                                      :data {:item-type (get item :type)
                                                             :handler-type :widget
                                                             :on-change    (fn [handler]
                                                                             (mutations/replace p handler))
                                                             :on-delete    (fn []
                                                                             (mutations/remove p))
                                                             :handler      handler
                                                             :handler-path p}}]))}])

(defn- column-list-item [{:keys [hidden? state-path ctx column p is-active?]}]
  [list-item {:left-icon           (if (get column :generated)
                                     [:> MacCommandOutlined {:style {:font-size "18px"}}]
                                     [:> EditOutlined       {:style {:font-size "18px"}}])
              :right-icon     (if hidden?
                                [:> EyeInvisibleOutlined
                                 {:on-click (fn [e]
                                              (.stopPropagation e)
                                              (re-frame/dispatch [:gen-fhi.db.events.item/show-column
                                                                  p
                                                                  (get column :key)]))
                                  :class "cursor-pointer"
                                  :style {:font-size "18px"}}]
                                [:> EyeOutlined
                                 {:on-click (fn [e]
                                              (.stopPropagation e)
                                              (re-frame/dispatch [:gen-fhi.db.events.item/hide-column
                                                                  p
                                                                  (get column :key)]))
                                  :class "cursor-pointer"
                                  :style {:font-size "18px"}}])
              :is-active?     is-active?
              :primary-text   (:title column)
              :secondary-text (get-in column [:dataIndex :expression])
              :on-click       (fn [_e]
                                (when (not (get column :generated))
                                  (re-frame/dispatch
                                   [:gen-fhi.db.events.ui/set-active-modal
                                    {:type :column-editor

                                     :data {:state-path  state-path
                                            :ctx         ctx
                                            :column      column
                                            :column-path p}}])))}])

(defn- columns-editor [{:keys [p item style state-path]}]
  (let [{:keys [parent]} (path/ascend p)
        evaled-item      @(re-frame/subscribe [:gen-fhi.db.subs.item/get-item parent state-path])
        columns          (get item :column [])
        hidden-columns   (set (get item :hiddenColumns []))
        add-column!      (fn [] (re-frame/dispatch
                                 [:gen-fhi.db.events.ui/set-active-modal
                                  {:type :column-add
                                   :data {:state-path  state-path
                                          :column-path (path/descend p  (count columns))
                                          :ctx         (get evaled-item :value [])
                                          :column      {:key  (v4)
                                                        :type "automatic"}}}]))]
    [:div {:style (or style {})}
     [:div {:className "flex items-center"}
      [-label "Columns"]
      [:div {:className "flex flex-1 justify-end"}
       [:a {:className "text-xs hover:text-blue-500"
            :on-click add-column!} "+ Add Column"]]]
     [drag-drop-context {:on-drag-end (fn [result]
                                        (when-let [destination (.-destination result)]
                                          (let [source (.-source result)
                                                path (path/descend p (.-index destination))
                                                from (path/descend p (.-index source))]
                                            (re-frame/dispatch-sync
                                             [:gen-fhi.db.events.mutate/mutate
                                              {:type :move
                                               :from from
                                               :path path}]))))}
      [droppable {:droppableId "droppable"}
       [:<>
        (map-indexed
         (fn [i column]
           ^{:key (:key column)}
           [draggable {:index i :draggableId (:key column) :key (:key column)}
            (fn [provided snapshot]
              [column-list-item {:hidden?    (contains? hidden-columns (get column :key))
                                 :state-path state-path
                                 :ctx        (get evaled-item :value [])
                                 :column     column
                                 :is-active? (oops/oget snapshot "isDragging")
                                 :p          (path/descend p i)}])])
         columns)]]]]))

(defn- handler-handler-editor [{:keys [p item style]}]
  (let [handlers     (get item :handler)
        add-handler! (fn [] (re-frame/dispatch
                             [:gen-fhi.db.events.ui/set-active-modal
                              {:type :handler-add
                               :data {:item-type (get item :type)
                                      :handler-type :widget
                                      :handler-path (path/descend p  (count handlers))
                                      :on-change (fn [handler]
                                                   (mutations/add
                                                    (path/descend p  (count handlers))
                                                    handler))
                                      :handler {}}}]))]
    [:div {:style (or style {})}
     [:div {:className "flex text-sm items-center"}
      [-label "Handlers"]
      [:div {:className "hover:text-blue-500 text-xs flex flex-1 justify-end"}
       [:a {:on-click add-handler!} "+ Add Handler"]]]
     [:div
      (map-indexed
       (fn [i handler]
         ^{:key (path/descend p i)}
         [handler-list-item {:item    item
                             :handler handler
                             :p       (path/descend p i)}])
       handlers)]]))

(defn- item-properties-header [{:keys [selected-item-path]}]
  (let [selected-item      (when selected-item-path @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value selected-item-path]))]
    [:div {:className "mt-1 flex items-center justify-end border-l-0 border-r-0 border-t-0 border border-gray-300 pl-1 pb-1 pr-1 mb-2"}
     [:div {:className "flex justify-center items-center mr-1" :title (:type selected-item)}
      [c/item-icon  {:type (:type selected-item)}]]
     [text-form-blur     {:className c/id-text-classname
                          :hide-label? true
                          :p (path/descend selected-item-path "id")}]
     [:div
      [:> Dropdown {:placement "bottomRight"
                    :overlay
                    (r/as-element
                     [:div {:style {:border "1px solid #e0e0e0"}}
                      [:div {:style {:background-color "#f7f7f7"
                                     :font-size "10px"
                                     :color "#9e9e9e"
                                     :display "flex"
                                     :flex-direction "column"
                                     :justify-content "center"
                                     :border-bottom "1px solid #e0e0e0"}}
                       [:span {:style {:display "block" :margin-left "8px" :margin-right "8px" :margin-top "2px" :margin-bottom "2px" :font-size "14px" :font-weight "500"}}
                        (string/capitalize (or (:type selected-item) ""))]]
                      [:> Menu {:mode "vertical" :style {:border "0px"}}
                       [:> (.-Item Menu) {:key "delete-selected"
                                          :on-click (fn []
                                                      (re-frame/dispatch
                                                       [:gen-fhi.db.events.ui/remove-selected-item]))}
                        [:div {:style {:display "flex" :align-items "center" :justify-content "center"}}
                         [:span {:style {:font-size "12px"}} "Delete"]
                         [:span {:style {:margin-left "10px" :font-size "8px" :color "#c9c9c9"}}
                          "Ctrl + -"]]]]])}
       [:a {:className "ant-dropdown-link" :on-click (fn [e] (.preventDefault e))}
        [:> EllipsisOutlined {:style {:font-size "18px"}}]]]]]))

(defn- extend-to-property [path name type value-key]
  (let [fields  (clojure.string/split name #"\.")
        parents (->> (pop fields)
                     (mapcat (fn [field] [{:name field :type "nested"} "property"]))
                     (concat ["property"]))]

    (apply path/extend-path path (concat parents [{:name (last fields) :type type} value-key]))))

(defn- upgrade-header [{:keys [selected-item-path]}]
  (let [selected-item @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value selected-item-path])]
    [:> Popconfirm
     {:placement "bottomRight"
      :title     (str "Confirm you would like to upgrade the component to latest version?")
      :onConfirm (fn [_]
                   (mutations/replace
                    selected-item-path
                    (upgrades/upgrade-to-latest selected-item)))

      :okText "Yes"
      :okButtonProps {:className "bg-blue-500 hover:bg-blue-600 border-0"}
      :cancelText "No"}
     [:div {:class "flex justify-center hover:bg-amber-100 bg-amber-50 p-1 cursor-pointer"}
      [:span {:class "font-semibold text-amber-500"} "Upgrade component?"]]]))

(defn- pagination [{:keys [p item state-path]}]
  (let [pagination-props (get item :pagination)
        is-checked?      (some? pagination-props)]
    [:div
     [:div {:class "mb-2 flex items-center"}
      [:label {:class "flex flex-1"}
       "Enable Pagination"]
      [:> Switch {:className (if is-checked?
                               "bg-blue-500"
                               "bg-gray-200")
                  :on-change (fn [checked? _event]
                               (if checked?
                                 (mutations/add
                                  (path/descend p "pagination")
                                  {:type "offset-based"})
                                 (mutations/remove
                                  (path/descend p "pagination"))))
                  :checked (some? pagination-props)}]]
     (when is-checked?
       [:<>
        [:div {:class "mb-2"}
         [expression-form {:label           "Total Number of Rows"
                           :state-path      state-path
                           :height          "40px"
                           :language        "application/x-fhir-query"
                           :post-processing "integer"
                           :p               (path/extend-path p  "pagination" "total")}]]
        [:div {:class "mb-2"}
         [expression-form {:label           "Rows per Page"
                           :state-path      state-path
                           :height          "40px"
                           :language        "application/x-fhir-query"
                           :post-processing "integer"
                           :p               (path/extend-path p  "pagination" "countPerPage")}]]])]))

(defn- generate-property-pane [{:keys [p state-path selected-item]}]
  [:<>
   (->> (get-in widgets/widgets [(:type selected-item) :properties])
        (filter #(not (:hide? %)))
        (map
         (fn [render-property]
           (let [init-props {:p               (extend-to-property p (get render-property :name) (get render-property :type) :valueExpression)
                             :state-path      state-path
                             :post-processing (get render-property :type)
                             :language        "application/x-fhir-query"}

                 props (merge init-props (get render-property :editor-props {}))]
             ^{:key (get render-property :name)}
             [:div {:className "mb-2"}
              (match (get render-property :type)
                "code" [code-expression-form props]
                :else  [expression-form props])]))))])

(defn item-properties [{:keys [state-path]}]
  (let [selected-item-path @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-selected-item-path])
        selected-item      (when selected-item-path @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value selected-item-path]))
        has-upgrade?       (upgrades/has-upgrade? selected-item)
        cur-workspace      @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :workspace])]
    [:div {:key selected-item-path}
     (if (nil? selected-item-path)
       [:div {:className "mt-2 font-semibold text-gray-600 flex items-center justify-center"}
        [:span "Select an item to see its properties."]]

       [:<>
        (when has-upgrade?
          [upgrade-header {:selected-item-path selected-item-path}])
        [item-properties-header {:selected-item-path selected-item-path}]

        [:div {:className "pl-2 pr-2"}
         [generate-property-pane {:selected-item selected-item
                                  :p selected-item-path
                                  :state-path state-path}]
         (match (:type selected-item)
           "questionnaire" [:<>
                            [reference-select-form
                             {:label "Local Questionnaire"
                              :p (extend-to-property
                                  selected-item-path
                                  "questionnaireLocal"
                                  "reference"
                                  "valueReference")
                              
                              :resource-type "Questionnaire"
                              :search-key :title
                              :value-fn identity}]]
           "code"     [:<>
                       [:div {:className "mb-2"}
                        [:div {:class "flex items-center "}
                         [:div {:class "flex flex-1"}
                          [:label "ValueSet"]]
                         [:a {:tabIndex "-1"
                              :target "_blank"
                              :href (router/path-for
                                     :type
                                     {:resource-type "Terminology"
                                      :workspace cur-workspace})

                              :class "cursor-pointer text-xs hover:text-blue-500 text-blue-400"}
                          "+ Create"]]

                        [:div {:class "mb-2 mt-1 flex items-center"}
                         [:label {:class "flex flex-1"}
                          "Use a value set resource?"]
                         [:> Switch {:className (if (some? (get-in selected-item [:bindingV2 :valueset]))
                                                  "bg-blue-500"
                                                  "bg-gray-200")
                                     :on-change (fn [checked? _event]
                                                  (if checked?
                                                    (mutations/replace
                                                     (path/descend selected-item-path "bindingV2")
                                                     {:valueset {:language "application/x-fhir-query"}})
                                                    (mutations/replace
                                                     (path/descend selected-item-path "bindingV2")
                                                     {:url {:language "application/x-fhir-query"}})))
                                     :checked (some? (get-in selected-item [:bindingV2 :valueset]))}]]
                        (if (some? (get-in selected-item [:bindingV2 :valueset]))
                          [expression-form   {:language        "application/x-fhir-query"
                                              :extensions      [(applicationXFhirQuery #js{:baseLanguage javascriptLanguage})]
                                              :state-path      state-path
                                              :max-height      "250px"
                                              :hide-label?     true
                                              :post-processing "json"
                                              :p               (path/extend-path selected-item-path  "bindingV2" "valueset")}]
                          [valueset-select-expression   {:state-path      state-path
                                                         :hide-label?     true
                                                         :p               (if (= "v0" (get selected-item :version "v0"))
                                                                            (path/extend-path selected-item-path  "valueSet")
                                                                            (path/extend-path selected-item-path  "bindingV2" "url"))}])]]
           "list"     [:<>
                       [:div {:className "mt-8 mb-4"}
                        [columns-editor {:item             selected-item
                                         :state-path       state-path
                                         :p                (path/extend-path selected-item-path  "column")}]]
                       [:div {:className "mt-8 mb-4"}
                        [pagination     {:item       selected-item
                                         :state-path state-path
                                         :p          selected-item-path}]]
                       [:div {:className "mt-8 "}
                        [handler-handler-editor {:item selected-item
                                                 :p (path/extend-path selected-item-path "handler")}]]]
           "button"   [:<>
                       [:div {:className "mt-4"}
                        [handler-handler-editor {:item selected-item
                                                 :p (path/extend-path selected-item-path "handler")}]]]

           :else [:<>])]])]))
