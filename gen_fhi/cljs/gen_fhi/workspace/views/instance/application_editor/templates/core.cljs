(ns gen-fhi.workspace.views.instance.application-editor.templates.core
  (:require
   [gen-fhi.workspace.components.editors.core
    :refer [assoc-post-processing]]
   [gen-fhi.workspace.views.instance.application-editor.templates.react
    :refer
    [react-item]]))

(def templates
  {:questionnaire {:type    "questionnaire"
                   :version "v0"
                   :height  200
                   :width   100}

   :list     {:type "list"
              :version "v0"
              :width 200
              :height 100
              :property [{:name "label" :type "string" :valueExpression {:expression "List" :language "application/x-fhir-query"}}]}
   :button   {:type "button"
              :version "v0"
              :width 100
              :height 40
              :property [{:name "label" :type "string" :valueExpression {:expression "Button" :language "application/x-fhir-query"}}]}
   :text     {:type "text"
              :version "v0"
              :width 200
              :height 60
              :property [{:name "label" :type "string" :valueExpression {:expression "Text" :language "application/x-fhir-query"}}]}
   :date     {:type "date"
              :version "v0"
              :width 200
              :height 60
              :property [{:name "label" :type "string" :valueExpression {:expression "Date" :language "application/x-fhir-query"}}]}
   :datetime {:type "datetime"
              :version "v0"
              :width 200
              :height 60
              :property [{:name "label" :type "string" :valueExpression {:expression "Date Time" :language "application/x-fhir-query"}}]}
   :time     {:type "time"
              :version "v0"
              :width 200
              :height 60
              :property [{:name "label" :type "string" :valueExpression {:expression "Time" :language "application/x-fhir-query"}}]}
   :label    {:type "label"
              :version "v0"
              :width 200
              :height 40}
   :integer  {:type "integer"
              :version "v0"
              :width 200
              :height 60
              :property [{:name "label" :type "string" :valueExpression {:expression "Integer" :language "application/x-fhir-query"}}]}
   :image    {:type "image"
              :version "v0"
              :width 200
              :height 200}
   :html     {:type "html"
              :version "v0"
              :width 200
              :height 100}
   :code     {:type "code"
              :version "v1"
              :width 200
              :height 60
              :bindingV2 {:url (assoc-post-processing
                                "json"
                                {:expression "\"http://hl7.org/fhir/ValueSet/event-timing\""

                                 :language "application/x-fhir-query"})}
              :property [{:name "label" :type "string" :valueExpression {:expression "Code" :language "application/x-fhir-query"}}]}
   :json     {:type "json"
              :version "v0"
              :width 200
              :height 200
              :property [{:name "label" :type "string" :valueExpression {:expression "JSON" :language "application/x-fhir-query"}}]}

   :react    react-item

   :decimal  {:type "decimal"
              :version "v0"
              :width 200
              :height 60
              :property [{:name "label" :type "string" :valueExpression {:expression "Decimal" :language "application/x-fhir-query"}}]}})
