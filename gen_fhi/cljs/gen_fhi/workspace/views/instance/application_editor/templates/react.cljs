(ns gen-fhi.workspace.views.instance.application-editor.templates.react)

(def react-item
  {:type "react"
   :version "v0"
   :width 650
   :height 150
   :property [{:name "value" :type "string" :valueExpression {:language "application/x-fhir-query"
                                                              :extension []
                                                              :expression "<div id=\"react\"></div>\n<script src=\"https://www.unpkg.com/evergreen-ui@6.10.3/umd/evergreen.min.js\"></script>\n<script type=\"text/babel\">\n  const { Button, Autocomplete, TextInput, Pane } = window[\"EvergreenUI\"];\n  const MyCustomComponent = ({ props }) => {\n   return (\n    <Pane padding={16} background=\"tint2\" borderRadius={3}>\n      <Pane marginBottom={8}>\n        <TextInput value={props.text} onChange={e=>modifyProps({...props, text: e.target.value})}/>\n      </Pane>\n      <Pane marginBottom={8}>\n        <Autocomplete\n          title=\"Selected\"\n          onChange={(changedItem) => console.log(changedItem)}\n          items={props.options ? props.options : []}\n        >\n          {(props) => {\n            const { getInputProps, getRef, inputValue } = props;\n            return (\n              <TextInput\n                placeholder=\"Select Option\"\n                value={inputValue}\n                ref={getRef}\n                {...getInputProps()}\n              />\n            );\n          }}\n        </Autocomplete>\n      </Pane>\n      <Button\n        onClick={() => {\n          modifyProps({ ...props, text: \"HELLO\" + Math.random() });\n        }}\n      >\n        Update Model\n      </Button>\n    </Pane>\n  )};\n\n  const ConnectedComponent = Connect(MyCustomComponent);\n  const root = ReactDOM.createRoot(document.getElementById(\"react\"));\n  root.render(<ConnectedComponent />);\n</script>\n"}}
              {:name "props" :type "json" :valueExpression {:language "application/x-fhir-query"
                                                            :extension [{:url "https://genfhi.com/Extension/post-processing"
                                                                         :valueCode "json"}]
                                                            :expression "{\n  \"text\": \"TEXT STATE\", \n \"options\": [\"option1\", \"option2\"]\n}"}}]})
