(ns gen-fhi.workspace.views.instance.application-editor.upgrades.resource
  (:require
   [clojure.core.match :refer [match]]
   [clojure.set :as set]
   [gen-fhi.fhirpath.core :as fp]
   [gen-fhi.patch-building.path :as path]
   [gen-fhi.workspace.components.editors.core
    :refer [assoc-post-processing]]
   [re-frame.core :as re-frame]))

(defn- fp-exp->x-fhir-query-exp
  "Used to convert an text/fhirpath expression into an application/x-fhir-query."
  ([expression]
   (fp-exp->x-fhir-query-exp expression nil))
  ([expression post-process]
   (if (= "text/fhirpath" (get expression :language))
     (let [return (-> expression
                      (assoc :language "application/x-fhir-query")
                      (update :expression (fn [exp] (str "{{" exp "}}"))))]
       (if post-process
         (assoc-post-processing post-process return)
         return))
     expression)))

(defn- alter-shared-properties [item]
  (cond->
   item
    (some? (get item :label))      (update :label #(fp-exp->x-fhir-query-exp %))
    (some? (get item :src))        (update :src   #(fp-exp->x-fhir-query-exp %))
    (some? (get item :isDisabled)) (update :isDisabled #(fp-exp->x-fhir-query-exp % "boolean"))))

(defn- alter-pagination-property [item]
  (if (nil? (get item :pagination))
    item
    (update
     item
     :pagination
     (fn [pagination]
       (-> pagination
           (update
            :total
            #(fp-exp->x-fhir-query-exp % "integer"))
           (update
            :countPerPage
            #(fp-exp->x-fhir-query-exp % "integer")))))))

(defn- alter-value-property [item]
  (if (nil? (get item :value))
    item
    (update
     item
     :value
     (fn [value]
       (match (get item :type)
         "list"     (fp-exp->x-fhir-query-exp value "json")
         "code"     (fp-exp->x-fhir-query-exp value "string")
         "button"   (fp-exp->x-fhir-query-exp value "string")
         "text"     (fp-exp->x-fhir-query-exp value "string")
         "label"    (fp-exp->x-fhir-query-exp value "string")
         "image"    (fp-exp->x-fhir-query-exp value "string")
         "choice"   (fp-exp->x-fhir-query-exp value "string")
         "integer"  (fp-exp->x-fhir-query-exp value "integer")
         "html"     (fp-exp->x-fhir-query-exp value "json")
         "react"    (fp-exp->x-fhir-query-exp value "json")
         "json"     (fp-exp->x-fhir-query-exp value "string")
         "date"     (fp-exp->x-fhir-query-exp value "string")
         "time"     (fp-exp->x-fhir-query-exp value "string")
         "datetime" (fp-exp->x-fhir-query-exp value "string")
         :else      value)))))

(defn- expression->post-process-type [expression]
  (let [post-processing-ext-type (first (fp/evaluate "$this.extension.where(url='https://genfhi.com/Extension/post-processing').value" expression))]
    post-processing-ext-type))

(def properties-to-migrate #{:style ;string
                             :value ;depends need to use what's on the expression post processing.
                             :props ;json
                             :label ;string
                             :labelPositioning ;string
                             :src ;string
                             :isDisabled}) ;boolean

(defn- item-migrate-properties [item]
  (let [properties-to-migrate (into
                               #{}
                               (filter
                                (fn [key]
                                  (some? (properties-to-migrate key)))
                                (keys item)))
        new-properties (->> (select-keys item properties-to-migrate)
                            (map
                             (fn [[key value]]
                               (let [value (if (string? value)
                                             {:language   "application/x-fhir-query"
                                              :expression value}
                                             value)]
                                 {:name (name key)
                                  :value value
                                  :type  (or
                                          (expression->post-process-type value)
                                          "string")}))))]

    ;; Dissoc all properties I'm migrating by using the set difference of properties-to-migrate.
    (-> (select-keys item (set/difference (into #{} (keys item)) properties-to-migrate))
        (update
         :property
         (fn [existing-properties]
           (into []
                 (concat
                  (filter
                   #(nil? (properties-to-migrate (keyword (:name %))))
                   existing-properties)
                  new-properties)))))))

(defn- migrate-properties [properties]
  (map
   (fn [property]
     (let [expression (get property :value)]
       (-> property
           (dissoc :value)
           (assoc :valueExpression expression)
           (update :property migrate-properties))))
   properties))

(defn- move-properties-value->valueExpression [item]
  (update item :property migrate-properties))

(def resource-upgrades
  {:ClientApplication
   {:v1 (fn [resource]
          (-> resource
              (assoc :version "v2")
              (update
               :action
               (fn [actions]
                 (vec
                  (map
                   (fn [action]
                     (if-let [reference (get-in action [:location :reference])]
                       (if (some? (get action :endpoint))
                         action
                         (-> action
                             (assoc :endpoint {:reference reference})
                             (update-in [:location] dissoc :reference)))
                       action))
                   actions))))))
    :v2 (fn [resource]
          (-> resource
              (assoc :version "v3")
              (update
               :manifest
               (fn [manifest]
                 (let [return (map
                               (fn [item]
                                 (alter-shared-properties item))
                               manifest)]
                   (into
                    []
                    return))))))
    ;; Migration to application/x-fhir-query for text/fhirpath
    :v3 (fn [resource]
          (-> resource
              (assoc :version "v4")
              (update
               :manifest
               (fn [manifest]
                 (let [return (map
                               (fn [item]
                                 (->> item
                                      (alter-value-property)
                                      (alter-pagination-property)))
                               manifest)]
                   (into
                    []
                    return))))))
    ;; Migration for binding to binding object for url
    :v4 (fn [resource]
          (-> resource
              (assoc :version "v5")
              (update
               :manifest
               (fn [manifest]
                 (map
                  (fn [item]
                    (if-let [url-expression (or
                                             (get item :valueset)
                                             (get item :binding))]
                      (assoc item :bindingV2 {:url url-expression})
                      item))
                  manifest)))))

    :v5 (fn [resource]
          (let [new-resource (-> resource
                                 (assoc :version "v6")
                                 (update
                                  :manifest
                                  (fn [manifest]
                                    (map
                                     item-migrate-properties
                                     manifest))))]
            new-resource))
    :v6 (fn [resource]
          (let [new-resource (-> resource
                                 (assoc :version "v7")
                                 (update
                                  :manifest
                                  (fn [manifest]
                                    (map
                                     move-properties-value->valueExpression
                                     manifest))))]
            (println "upgrade:" new-resource)
            new-resource))}})

(defn- get-upgrade [resource]
  (let [resource-type (get resource :resourceType)
        version (get resource :version "v1")
        upgrade (get-in resource-upgrades [(keyword resource-type) (keyword version)])]
    upgrade))

(defn- apply-upgrade [resource]
  (let [upgrade-fn (get-upgrade resource)]
    (upgrade-fn resource)))

(defn has-upgrade? [resource]
  (some? (get-upgrade resource)))

(defn upgrade-to-latest [resource]
  (let [upgraded-resource
        (loop  [resource resource]
          (if (has-upgrade? resource)
            (recur (apply-upgrade resource))
            resource))]
    (when (not= resource upgraded-resource)
      (re-frame/dispatch-sync
       [:gen-fhi.db.events.mutate/mutate
        {:path (path/->path (get resource :resourceType)
                            (get resource :id))
         :type :replace
         :value upgraded-resource}]))
    upgraded-resource))
