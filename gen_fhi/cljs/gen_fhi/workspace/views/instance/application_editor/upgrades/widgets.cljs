(ns gen-fhi.workspace.views.instance.application-editor.upgrades.widgets)

(def widget-upgrades
  {:code {:v0 (fn [code]
                (let [valueSet (get code :valueSet)]
                  (-> (dissoc code :valueSet)
                      (assoc :version "v1")
                      (assoc-in [:binding]  valueSet))))}})

(defn- get-upgrade [widget]
  (let [type (get widget :type)
        version (get widget :version "v0")]
    (get-in
     widget-upgrades
     [(keyword type)
      (keyword version)])))

(defn- apply-upgrade [widget]
  (let [upgrade-fn (get-upgrade widget)]
    (upgrade-fn widget)))

(defn has-upgrade? [widget]
  (some? (get-upgrade widget)))

(defn upgrade-to-latest [widget]
  (loop  [widget widget]
    (if (has-upgrade? widget)
      (recur (apply-upgrade widget))
      widget)))

