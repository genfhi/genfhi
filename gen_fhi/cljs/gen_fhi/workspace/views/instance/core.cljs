(ns gen-fhi.workspace.views.instance.core
  (:require [clojure.core.match :refer [match]]
            [re-frame.core :as re-frame]
            [accountant.core :as accountant]

            [gen-fhi.workspace.views.instance.application-editor.core   :refer [application-editor]]
            [gen-fhi.workspace.views.instance.questionnaire-editor.core :refer [questionnaire-editor]]
            [gen-fhi.workspace.views.instance.endpoint-editor           :refer [endpoint-editor]]
            [gen-fhi.workspace.views.instance.operation-editor          :refer [operation-definition-editor]]
            [gen-fhi.workspace.views.instance.terminology-editor.core   :refer [terminology-editor]]
            [gen-fhi.workspace.router :as router]))

(defn- environment-redirect [_props]
  (let [params @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])
        path (router/path-for :setting-instance (merge
                                                 params
                                                 {:setting "environment"}))]
    (accountant/navigate!
     path)

    [:div]))

(defn view []
  (let [resource-type @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :resource-type])
        id            @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :id])
        modifiers     @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :modifiers])

        props {:resource-type resource-type
               :id id
               :modifiers modifiers}]

    (match resource-type
      "ClientApplication"   [application-editor          props]
      "OperationDefinition" [operation-definition-editor props]
      "Endpoint"            [endpoint-editor             props]
      "CodeSystem"          [terminology-editor          props]
      "ValueSet"            [terminology-editor          props]
      "Organization"        [environment-redirect        props]
      "ImplementationGuide" [:<>]
      "Questionnaire"       [questionnaire-editor        props]
      :else                 [:div (str "Resource not supported " resource-type)])))
