(ns gen-fhi.workspace.views.instance.endpoint-editor
  (:require [clojure.string :as string]
            [re-frame.core :as re-frame]
            [reagent.core :as r]
            [accountant.core :as accountant]
            [clojure.core.match :refer [match]]
            [clojure.core.async :as a]

            ["@ant-design/icons" :refer [CloseOutlined]]
            [antd :refer [Alert Spin]]

            [gen-fhi.workspace.components.resource-header :refer [resource-header]]
            [gen-fhi.workspace.components.query :refer [query]]
            [gen-fhi.workspace.components.resource-table :refer [resource-table]]
            [gen-fhi.workspace.components.sidebar :refer [sidebar]]
            [gen-fhi.workspace.components.string-extension :refer [string-extension-editor]]
            [gen-fhi.workspace.utilities.resources :refer [endpoint->auth-type]]
            [gen-fhi.fhir-client.protocols :as fhir-client]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.workspace.components.terminology :refer [open-code-select code-select]]
            [gen-fhi.workspace.router :as router]
            [gen-fhi.workspace.components.editors.core :refer [raw-text-editor]]
            [gen-fhi.workspace.components.layout :refer [layout]]
            [gen-fhi.auth.constants :as auth-constants]
            [gen-fhi.fhirpath.core :as fp]))

(defn endpoint-applications [{:keys [endpoint]}]
  [query {:parameters {:_type ["ClientApplication"]
                       :endpoint (:id endpoint)}}
   (fn [{:keys [result re-query is-loading]}]
     [resource-table {:types ["ClientApplication"]
                      :on-delete re-query
                      :result result
                      :is-loading is-loading}])])

(defn- ->smart-route [resource]
  (let [workspace        (get-in (router/match-by-path (.. js/window -location -pathname)) [:parameters :path :workspace])
        smart-auth-route (router/path-for :smart-login
                                          {:workspace     workspace
                                           :resource-type (:resourceType resource)
                                           :id            (:id resource)})]
    (str (.. js/window -location -origin) smart-auth-route)))

(defn- endpoint-auth-message [endpoint]
  ;; Using address for the issuer.
  (when (or (= "smart-ehr"        (endpoint->auth-type endpoint))
            (= "smart-standalone" (endpoint->auth-type endpoint)))
    (let [smart-route (->smart-route endpoint)]
      [:> Alert {:type    "warning"
                 :message (str "To use endpoint in editor register '" smart-route "' as redirect url.")}])))

(defn- auth-section [{:keys [endpoint on-change]}]
  (let [auth-type (endpoint->auth-type endpoint)]
    [:<>
     [endpoint-auth-message endpoint]
     [:div {:class "mb-2"}
      [:label "Method"]
      [code-select {:value-set     "http://genfhi.com/supported-endpoint-auths"
                    :value        auth-type
                    :on-change    (fn [auth-type]
                                    (let [filtered-extensions
                                          (vec (fp/evaluate "$this.extension.where($this.url.startsWith(%authExts)!=true)"
                                                            endpoint
                                                            {:options/variables {:authExts auth-constants/auth-prefix}}))]
                                      (on-change (assoc endpoint :extension
                                                        (conj
                                                         filtered-extensions
                                                         {:url auth-constants/auth-type-url
                                                          :valueCode auth-type})))))
                    :select-props {:placeholder "Select an authentication method"}}]]
     (match auth-type
       "none"
       [:<>]
       (:or "smart-standalone" "smart-ehr")
       [:<>
        [:div {:className "mb-2"}
         (when (= auth-type "smart-ehr")
           [:> Alert
            {:type    "warning"
             :message (str "ISS will only be used in development once app is in production it will instead use the iss parameter passed in from the EHR.")}])
         [raw-text-editor {:label "Issuer"
                           :placeholder "Enter an issuer"
                           :value (get endpoint :address nil)
                           :on-change (fn [e]
                                        (on-change (assoc endpoint :address (.. e -target -value))))}]]
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Client ID"
                                   :placeholder    "Enter your smart apps client id"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-smart-client-id-url}]]
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Scopes"
                                   :placeholder    "Enter scopes for this endpoint"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-smart-scopes-url}]]]
       "oauth2-confidential"
       [:<>
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Token URL"
                                   :placeholder    "Enter the token url endpoint"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-oauth2-token-url}]]
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Audience"
                                   :placeholder    "Enter the  target audience which access is being requested"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-oauth2-audience}]]
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Resource"
                                   :placeholder    "Enter the  target resource to which access is being requested"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-oauth2-resource}]]
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Client ID"
                                   :placeholder    "Enter the OAuth2 client id"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-oauth2-client-id}]]

        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Client Secret"
                                   :placeholder    "Enter the OAuth2 client secret"
                                   :type           "string"
                                   :encrypted?     true
                                   :extension-url  auth-constants/auth-oauth2-client-secret}]]]

       "basic"
       [:<>
        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Username"
                                   :placeholder    "Enter a username"
                                   :type           "string"
                                   :extension-url  auth-constants/auth-basic-username-url}]]

        [:div {:className "mb-2"}
         [string-extension-editor {:value          endpoint
                                   :on-change      #(on-change %)
                                   :label          "Password"
                                   :placeholder    "Enter a password"
                                   :type           "string"
                                   :encrypted?     true
                                   :extension-url  auth-constants/auth-basic-password-url}]]])]))

(defn- environments [_props]
  (let [client   @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        loading? (r/atom nil)
        result   (r/atom nil)
        query    (fn []
                   (reset! loading? true)
                   (a/take!
                    (fhir-client/search client "Organization" {})
                    (fn [res]
                      (reset! loading? false)
                      (reset! result
                              (:fhir/response res)))))]
    (query)
    (fn [{:keys [on-click value]}]
      [:div
       [sidebar {:value    value
                 :on-click (fn [selected-item] (on-click selected-item))
                 :items    (concat [{:display "Base" :value nil}]
                                   (map (fn [organization]
                                          {:display (:name organization)
                                           :value (:id organization)})
                                        @result))}]
       [:div {:class "mt-2 flex items-center"}
        (when @loading?
          [:> Spin])]])))

(defn -endpoint-editor [{:keys [endpoint on-change environment]}]
  [:div {:class "flex flex-1 flex-col"}
   (when (:value environment)
     [:div {:class "mb-4"}
      [:div {:class "text-base font-semibold"}
       [:span (str (:display environment) " environment overrides")]]
      [:span "If using this environment the configuration provided will be merged into the base."]])
   [:span {:class "font-semibold text-base mb-1"}
    "Properties"]
   [:div {:class "ml-4 mb-4"}
    (when (nil? (:value environment))
      [:div {:className "mb-2"}
       [raw-text-editor {:label "Name"
                         :placeholder "Enter a name"
                         :value (get endpoint :name nil)
                         :on-change (fn [e]
                                      (on-change (assoc endpoint :name (.. e -target -value))))}]])

               ;; Moving this to authorization as issuer.
    (when (and (not= "smart-ehr"        (endpoint->auth-type endpoint))
               (not= "smart-standalone" (endpoint->auth-type endpoint)))
      [:div {:className "mb-2"}
       [raw-text-editor {:label "Address"
                         :placeholder "Enter an address"
                         :value (get endpoint :address nil)
                         :on-change (fn [e]
                                      (on-change (assoc endpoint :address (.. e -target -value))))}]])

    [:div {:className "mb-2"}
     [:label "Headers"]
     (map-indexed
      (fn [i header]
        (let [[header value] (string/split header #":")]
          [:div {:class "flex mb-1"}
           [:div {:class "flex flex-1"}
            [open-code-select {:value-set    "http://genfhi.com/ValueSet/http-methods-valueset"
                               :value       header
                               :on-change   (fn [header]
                                              (on-change (assoc-in endpoint [:header i] (str header ":" value))))
                               :select-props
                               {:placeholder "Key"}}]

            [raw-text-editor {:hide-label? true
                              :value value
                              :placeholder "Value"
                              :on-change (fn [e] (on-change (assoc-in endpoint [:header i] (str header ":" (.. e -target -value)))))}]]
           [button {:type :secondary
                    :on-click (fn [_e]
                                (on-change (update endpoint
                                                   :header (fn [headers] (vec (concat
                                                                               (subvec headers 0 i)
                                                                               (subvec headers (+ 1 i) (count headers))))))))
                    :className "ml-1"
                    :style    {:height "39px"}
                    :icon     (r/as-element [:> CloseOutlined])}
            "X"]]))

      (get endpoint :header []))
     [button {:type :secondary
              :className "w-full"
              :on-click (fn [] (on-change (update endpoint
                                                  :header
                                                  (fn [headers] (conj headers "")))))}

      "Add Header"]]]
   [:header {:class "text-base mb-1 font-semibold"}
    "Authorization"]
   [:div {:class "ml-4"}
    [:div {:className "mb-2"}
     [auth-section {:endpoint endpoint
                    :on-change on-change}]]]])

(defn- find-endpoint-for-organization [organization-id endpoint]
  (first
   (filter
    #(= (str "Organization/" organization-id)
        (get-in % [:managingOrganization :reference]))
    (get endpoint :contained))))

(defn endpoint-editor [_props]
  (let [endpoint-state   (r/atom nil)
        environment-selected (r/atom nil)]
    (fn [{:keys [resource-type id]}]
      (let [params           @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])
            path             (path/->path resource-type id)
            base-endpoint  @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value path])]
        (when (not= (:id base-endpoint) (:id @endpoint-state))
          (reset! endpoint-state base-endpoint))
        [layout
         {}
         [:div {:class "flex justify-center text-slate-600"}
          [:div {:class "container "}
           [:div
            [:div {:class "flex mt-8 mb-4"}
             [:h2  {:class "text-3xl align-middle"}
              (:name @endpoint-state)]
             [resource-header {:resource @endpoint-state
                               :on-delete (fn []
                                            (accountant/navigate!
                                             (router/path-for
                                              :type
                                              (merge
                                               params
                                               {:resourceType "Endpoint"}))))}]]]
           [:div {:class "flex"}
            [:div {:class "mr-8 w-[175px]"}
             [:div {:class "text-base font-semibold mb-1"}
              [:span "Environment"]]
             [environments {:on-click (fn [environment]
                                        (reset! environment-selected environment))
                            :value (:value @environment-selected)}]]

            [-endpoint-editor {:on-change   (fn [new-endpoint]
                                              (if (some? (:value @environment-selected))
                                                (swap!
                                                 endpoint-state
                                                 (fn [endpoint-state]
                                                   (update
                                                    endpoint-state
                                                    :contained
                                                    (fn [contained]
                                                      (concat
                                                       [new-endpoint]
                                                       (filter
                                                        (fn [endpoint]
                                                          (not=
                                                           (get-in endpoint [:managingOrganization :reference])
                                                           (str "Organization/" (:value @environment-selected))))
                                                        contained))))))

                                                (reset! endpoint-state new-endpoint)))
                               :base        @endpoint-state
                               :environment @environment-selected
                               :endpoint    (if (some? (:value @environment-selected))
                                              (or (find-endpoint-for-organization
                                                   (:value @environment-selected)
                                                   @endpoint-state)
                                                  {:resourceType "Endpoint"
                                                   :managingOrganization {:reference (str "Organization/" (:value @environment-selected))}
                                                   :header ["Content-Type:application/json"]})
                                              ;; Means using the base resource IE the root.
                                              @endpoint-state)}]]]]]))))
