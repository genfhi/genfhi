(ns gen-fhi.workspace.views.instance.operation-editor
  (:require [re-frame.core :as re-frame]
            [oops.core :as oops]
            [reagent.core :as r]

            ["react" :refer [useEffect]]
            [antd :refer [Button  Tree]]
            ["@ant-design/icons" :refer [DownOutlined   FormOutlined]]
            ["@codemirror/commands" :refer [indentWithTab]]
            ["@codemirror/view" :refer [keymap]]
            ["@codemirror/lang-javascript" :refer [javascript]]
            ["@codemirror/basic-setup" :refer [basicSetup]]

            [gen-fhi.operations.constants :as op-constant]
            [gen-fhi.workspace.components.string-extension :refer [string-extension-editor]]
            [gen-fhi.components.base.button :refer [button]]
            [gen-fhi.workspace.components.tabs :refer [tabs tab-pane]]
            [gen-fhi.components.base.codemirror :refer [codemirror]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.components.editors.core :as editors]
            [gen-fhi.workspace.components.layout :refer [bottom-bar layout left-sidebar right-sidebar]]
            [gen-fhi.workspace.utilities.mutations :as mutations]
            [gen-fhi.fhirpath.core :as fp]))

(defn operation-editproperties [{:keys [p]}]
  (let [margin-between-items "4px"]
    [:div {:class "p-2"}
     [:div {:className "mb-4"}
      [editors/text-form {:container-style {:margin-bottom margin-between-items}
                          :p (path/descend p "name")}]]
     [:div {:className "mb-4"}
      [editors/textarea-form {:p (path/descend p "description")}]]
     [:div {:className "mb-4"}
      [editors/text-form {:container-style {:margin-bottom margin-between-items}
                          :p (path/descend p "code")}]]
     [editors/code-select-form {:p            (path/descend p "kind")
                                :select-props {:style
                                               {:width "100%"
                                                :margin-bottom margin-between-items}}
                                :valueset     "http://hl7.org/fhir/ValueSet/operation-kind"}]]))

(defn- render-parameter [_props]
  (let [is-focused? (r/atom false)
        component-ref (r/atom nil)]
    (fn [{:keys [p parameter]}]
      ;; Hacky implementation to detect outside click to remove editing form for parameter.
      ;; Detects using event listener when clicking outside of component after focused/.
      (useEffect
       (fn []
         (let [handle-click (fn [event]
                              (let [component-ref @component-ref]
                                (when (and (some? component-ref)
                                           is-focused?
                                           (not (oops/ocall component-ref "contains" (oops/oget event "target"))))
                                  ;; Putting a set timeout because user could click on item beneath current.
                                  ;; Puttinga delay allows click to go through as height will change from editor.
                                  (js/setTimeout
                                   (fn []
                                     (reset! is-focused? false))
                                   100))))]

           (.addEventListener js/document "mousedown" handle-click)
           (fn []
             (.removeEventListener js/document "mousedown" handle-click))))
       [@component-ref])
      (if @is-focused?
        [:div {:ref #(reset! component-ref %)
               :class "flex flex-col"}
         [:div
          [editors/text-form {:className "p-1 "
                              :p (path/descend p "name")}]]
         (when (empty? (get parameter :part))
           [editors/code-select-form {:select-props {:style        {:width "100%"}}
                                      :valueset     "http://hl7.org/fhir/ValueSet/all-types"
                                      :p            (path/extend-path p "type")}])
         [:div {:class "flex flex-row"}
          [:div {:class "mr-1"}
           [editors/number-form {:className "p-1"
                                 :p (path/descend p "min")}]]
          [:div
           [editors/text-form {:className "p-1"
                               :p (path/descend p "max")}]]]
         [:div {:class "flex flex-row items-center justify-center mt-2"}
          [:div {:class "flex flex-1"}
           [:span {:on-click (fn []
                               (mutations/remove
                                (path/extend-path p "type"))
                               (mutations/add
                                (path/extend-path p "part" (count (get parameter :part [])))
                                {:name (str "param" (.round js/Math (* (.random js/Math) 1000)))
                                 :use  (get parameter :use)
                                 :min  0
                                 :max  "1"}))
                   :class "text-xs cursor-pointer font-semibold text-blue-500 hover:text-blue-600 "}
            "Add child parameter"]]
          [:div
           [:span {:on-click (fn [] (mutations/remove p))
                   :class "text-xs cursor-pointer font-semibold text-red-500 hover:text-red-600 "}
            "Delete"]]]]

        [:div {:class "text-sm text-slate-800"
               :on-click #(reset! is-focused? true)}
         [:div
          [:span {:class "font-semibold"} (get parameter :name)]]
         [:div {:class "text-xs"}
          [:span
           (get parameter :type)]]
         [:div {:class "text-xs"}
          [:span "Min"]
          [:span {:class "mx-1 font-semibold"} (get parameter :min)]
          [:span "Max"]
          [:span {:class "mx-1 font-semibold"} (get parameter :max)]]]))))

(defn- parameters->tree-data [direction p parameters]
  (->> parameters
       (map-indexed
        (fn [i param]
          (let [path (path/descend p i)]
            {:isLeaf (empty? (get param :part []))
             :title (get param :name)
             :icon  (fn [_selected]
                      (r/as-element [:> FormOutlined]))
             :key    path
             :data   {:p path :parameter param}
             :children (parameters->tree-data
                        direction
                        (path/descend path "part")
                        (get param :part []))})))
       (filter (fn [tree-node]
                 (= direction (get-in tree-node [:data :parameter :use]))))))

(defn parameter-editor [{:keys [direction p]}]
  (let [parameter @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/descend p "parameter")])]
    [:div
     [:> Tree {:switcherIcon (r/as-element [:> DownOutlined])
               :treeData (parameters->tree-data
                          direction
                          (path/descend p "parameter")
                          parameter)
               :titleRender (fn [node]
                              (r/as-element
                               [:f> render-parameter (js->clj
                                                      (oops/oget node "data")
                                                      :keywordize-keys true)]))}]
     [:> Button
      {:className "mt-4"
       :on-click (fn []
                   (mutations/add
                    (path/extend-path p "parameter" (count parameter))
                    {:name (str "param" (.round js/Math (* (.random js/Math) 1000)))
                     :use  direction
                     :min  0
                     :max  "1"}))}
      "Add Parameter"]]))

(defn- environment-editor [{:keys [p]}]
  (let [operation-definition @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/get-applied-edit p])
        extensions-locs (fp/locations
                         "$this.extension.where(url=%extUrl)"
                         operation-definition
                         {:options/variables {:extUrl op-constant/operation-environment-variable-url}})]
    [:div {:className "flex mb-2"}
     [:div {:className "md:w-20 flex mr-2 pt-2 justify-end"}
      [:label "Variables"]]
     [:div {:className "flex flex-1 flex-col items-center mr-1"}
      (map-indexed
       (fn [i ext-environment-loc]
         ^{:key i}
         [:div {:class "w-full mb-2 flex items-center justify-center"}

          [:div {:class "flex flex-1 mr-1"}
           [string-extension-editor
            {:value          operation-definition
             :nth            i
             :extension-url  op-constant/operation-environment-variable-url
             :on-change      (fn [operation-definition]
                               (re-frame/dispatch
                                [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                 {:path  (apply path/extend-path p ext-environment-loc)
                                  :type  :replace
                                  :value (get-in operation-definition ext-environment-loc)}]))
             :placeholder    "Enter the environment variables name."
             :type           "string"}]]
          [:div {:class "flex flex-1 mr-1"}
           [string-extension-editor
            {:value        (get-in operation-definition ext-environment-loc)
             :encrypted?   true
             :extension-url op-constant/operation-environment-variable-value-url
             :on-change    (fn [extension]
                             (re-frame/dispatch
                              [:gen-fhi.db.events.temp-edits/set-temp-mutation
                               {:path (apply path/extend-path p ext-environment-loc)
                                :type :replace
                                :value extension}]))
             :placeholder "Enter the environment variables value."
             :type        "string"}]]
          [button {:type :secondary
                   :on-click (fn [_e]
                               (re-frame/dispatch
                                [:gen-fhi.db.events.temp-edits/set-temp-mutation
                                 {:path (apply path/extend-path p ext-environment-loc)
                                  :type :remove}]))}
           "X"]])
       extensions-locs)
      [button {:className "w-full"
               :type      :secondary
               :on-click  (fn [_e]
                            (re-frame/dispatch
                             [:gen-fhi.db.events.temp-edits/set-temp-mutation
                              {:path (apply path/extend-path p ["extension" (count (get operation-definition :extension []))])
                               :type :add
                               :value {:url op-constant/operation-environment-variable-url}}]))}

       "Add"]]]))

(defn- operation-definition-tab-header [{:keys [p]}]
  (let [is-temp-edits? @(re-frame/subscribe [:gen-fhi.db.subs.temp-edits/temp-edits? p])]
    [:div {:className "flex items-center mr-2 ml-2"}
     [:div {:className "flex flex-1 justify-end pr-1"}
      [:div {:class "p-1"}
       [button {:type :primary
                :on-click (fn [_e]
                            (re-frame/dispatch
                             [:gen-fhi.db.events.temp-edits/apply-temp-mutation p]))
                :disabled (not is-temp-edits?)}
        "Save"]]]]))

(defn operation-definition-editor []
  (fn [{:keys [resource-type id]}]
    (let [path                   (path/->path resource-type id)
          op-code-extension-path @(re-frame/subscribe [:gen-fhi.db.subs.fhir/get-extension-path-by-url
                                                       (path/descend path "extension")
                                                       "https://genfhi.com/OperationDefinition/op-code"])
          code-ext               @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value op-code-extension-path])]
      [layout
       {:left-sidebar
        [left-sidebar {}
         [operation-editproperties {:p path}]]
        :bottom-bar
        [bottom-bar {}
         [tabs {:active "environment" :tab-direction :left
                :tab-right-content [operation-definition-tab-header {:p path}]}
          [:f> tab-pane {:key "environment" :name "Environment"}
           [:div {:class "p-2"}
            [environment-editor {:p path}]]]]]

        :right-sidebar
        [right-sidebar {}
         [:div {:class "p-1"}
          [:div {:class "p-2"}
           [:h3 {:class "text-lg font-bold mb-2"} "Parameters"]
           [:div {:class "mb-2"}
            [:h4 {:class "text-base font-semibold"} "Input"]
            [parameter-editor {:direction "in"
                               :p path}]]
           [:div {:class "mb-2"}
            [:h4 {:class "text-base font-semibold"} "Output"]
            [parameter-editor {:direction "out"
                               :p path}]]]]]}
       [:f> codemirror
        {:key        "editor"
         :height     "100%"
         :width      "100%"
         :theme      {"&.cm-editor" {:border "0px"
                                     :padding "0px"}}

         :extensions [basicSetup (javascript #js{})
                      [(oops/ocall keymap "of"
                                   #js[indentWithTab])]]
         :value (get-in code-ext [:valueExpression :expression] "")
         :on-change (fn [value _view-update]
                      (re-frame/dispatch
                       [:gen-fhi.db.events.mutate/mutate
                        {:path (path/extend-path op-code-extension-path "valueExpression" "expression")
                         :type :replace
                         :value value}]))}]])))



