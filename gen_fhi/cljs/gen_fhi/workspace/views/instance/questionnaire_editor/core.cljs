(ns gen-fhi.workspace.views.instance.questionnaire-editor.core
  (:require [re-frame.core :as re-frame]
            [clojure.string :as string]
            [reagent.core :as r]
            [oops.core :as oops]

            ["react-dnd-html5-backend" :refer [HTML5Backend]]
            ["react-dnd" :as dnd]
            ["antd" :refer [Dropdown Menu Switch Collapse Select]]
            ["@ant-design/icons" :refer [EllipsisOutlined]]

            [gen-fhi.workspace.components.helper :refer [helper-text]]
            [gen-fhi.db.questionnaire.constants :as constants]
            [gen-fhi.workspace.views.instance.application-editor.constants :as c]
            [gen-fhi.workspace.views.instance.questionnaire-editor.templates :as templates]
            [gen-fhi.workspace.components.layout :refer [layout left-sidebar right-sidebar]]
            [gen-fhi.workspace.components.tabs :refer [tabs tab-pane]]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.questionnaire.core :refer [Questionnaire questionnaire-control]]
            [gen-fhi.workspace.components.editors.core :as editors]
            [gen-fhi.workspace.components.editors.questionnaire :as q-editors]
            [gen-fhi.workspace.utilities.mutations :as mutations]
            [gen-fhi.db.questionnaire.utilities :as q-utils]))

(def Panel (oops/oget Collapse "Panel"))

(defn- tooltip [{:keys [link text]}]
  (r/as-element
   [:div
    [:div [:span (str text)]]
    [:div
     [:span "For more information click "]
     [:a {:class "text-blue-400 underline hover:underline hover:text-blue-500"
          :target "_blank"
          :href link}
      "here"]]]))

(defn- item-properties-header [{:keys [p]}]
  (let [selected-item      (when p @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p]))]
    [:div {:className "flex items-center justify-end border-l-0 border-r-0 border-t-0 border border-gray-300 p-1 "}
     [:div {:className "flex justify-center items-center mr-1" :title (:type selected-item)}
      (get-in templates/items [(:type selected-item) :icon])]
     [editors/text-form-blur     {:className c/id-text-classname
                                  :hide-label? true
                                  :p (path/descend p "linkId")}]
     [:div
      [:> Dropdown {:placement "bottomRight"
                    :overlay
                    (r/as-element
                     [:div {:style {:border "1px solid #e0e0e0"}}
                      [:div {:style {:background-color "#f7f7f7"
                                     :font-size "10px"
                                     :color "#9e9e9e"
                                     :display "flex"
                                     :flex-direction "column"
                                     :justify-content "center"
                                     :border-bottom "1px solid #e0e0e0"}}
                       [:span {:style {:display "block" :margin-left "8px" :margin-right "8px" :margin-top "2px" :margin-bottom "2px" :font-size "14px" :font-weight "500"}}
                        (string/capitalize (or (:type selected-item) ""))]]
                      [:> Menu {:mode "vertical" :style {:border "0px"}}
                       [:> (.-Item Menu) {:key "delete-selected"
                                          :on-click (fn []
                                                      (re-frame/dispatch
                                                       [:gen-fhi.db.events.ui/remove-selected-item]))}
                        [:div {:style {:display "flex" :align-items "center" :justify-content "center"}}
                         [:span {:style {:font-size "12px"}} "Delete"]
                         [:span {:style {:margin-left "10px" :font-size "8px" :color "#c9c9c9"}}
                          "Ctrl + -"]]]]])}
       [:a {:className "ant-dropdown-link" :on-click (fn [e] (oops/ocall e "preventDefault"))}
        [:> EllipsisOutlined {:style {:font-size "18px"}}]]]]]))

(defn- table-header [children]
  [:th {:class "px-1 text-sm font-medium bg-gray-100 font-slate-700 text-left"}
   children])

(defn- structured-enable-when [{:keys [p]}]
  (let [enable-when-p (path/descend p :enableWhen)
        enable-whens @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value enable-when-p])
        linkid-map   @(re-frame/subscribe [:gen-fhi.db.questionnaire.subs/questionnaire-linkid-map p])]
    ;; Setting height here
    [:div
     [:div {:class "mb-1"}
      [editors/code-select-form
       {:p (path/extend-path p :enableBehavior)
        :label "Enable Behavior"
        :placeholder "All"
        :tooltip (r/as-element
                  [tooltip {:text "Controls how multiple enableWhen values are interpreted - whether all or any must be true."
                            :link "http://hl7.org/fhir/R4/questionnaire-definitions.html#Questionnaire.item.enableBehavior"}])
        :valueset    "http://hl7.org/fhir/ValueSet/questionnaire-enable-behavior"}]]

     [:div {:class "max-h-48 overflow-x-auto overflow-y-auto"}
      [editors/-label
       "Conditions"]
      [:table {:class "w-full" :style {:height "fit-content"}}
       [:thead
        [:tr
         [table-header "Question"]
         [table-header "Operator"]
         [table-header "Value"]
         [:th]]]
       [:tbody
        (map-indexed
         (fn [i enable-when]
           (let [_q-item (get linkid-map (get enable-when :question))]
             [:tr
              [:td {:class "max-w-[120px]"}
               [:> Select
                {:className    "flex items-center w-full border border-slate-300 hover:border-blue-400 rounded overflow-hidden"
                 :id           (path/extend-path enable-when-p i :question)
                 :value        (get enable-when :question)
                 :showSearch   true
                 :filterOption (fn [input option]
                                 (string/includes?
                                  (.toLowerCase (.-children option))
                                  (.toLowerCase input)))

                 :on-change    (fn [link-id]
                                 (mutations/replace
                                  (path/extend-path enable-when-p i :question)
                                  link-id))}
                (map
                 (fn [linkid]
                   [:> (.-Option Select) {:key linkid :value linkid}
                    (str linkid)])
                 (keys linkid-map))]]
              [:td
               [:div {:class "h-full w-full"}
                [editors/code-select-form
                 {:p (path/extend-path enable-when-p i :operator)
                  :hide-label? true
                  :container-style {:height "100%"}
                  :placeholder "Enter an operator"
                  :valueset "http://hl7.org/fhir/ValueSet/questionnaire-enable-operator"}]]]
              [:td {:class "min-w-[100px]"}
               (when-let [item-path (get linkid-map (:question enable-when))]
                 (let [enable-q-item @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value item-path])
                       answer-key    (keyword (str "answer" (string/capitalize
                                                             (q-utils/q-item-type->fhir-primitive
                                                              (:type enable-q-item)))))]
                   ;; Exists is always a bool.
                   (if (= "exists" (:operator enable-when))
                     [questionnaire-control {:item {:type "boolean"}
                                             :value (get enable-when :answerBoolean)
                                             :on-change (fn [value]
                                                          (mutations/replace
                                                           (path/extend-path enable-when-p i :answerBoolean)
                                                           value))}]

                     [questionnaire-control {:item      enable-q-item
                                             :value     (get enable-when answer-key)
                                             :on-change (fn [value]
                                                          (mutations/replace
                                                           (path/extend-path enable-when-p i answer-key)
                                                           value))}])))]
              [:td
               (when (seq enable-whens)
                 [:div {:class "ml-1 font-bold hover:text-red-400 cursor-pointer flex items-center justify-center"
                        :on-click (fn [_e]
                                    (mutations/remove
                                     (path/extend-path enable-when-p i)))}
                  [:span "X"]])]]))
         enable-whens)]]]
     [:div
      [:span {:class "text-sm cursor-pointer hover:text-blue-600 mt-1"
              :on-click (fn [_e]
                          (mutations/add
                           (path/descend enable-when-p :-)
                           {:question (first (keys linkid-map))
                            :operator "="}))}
       "+ Add Condition"]]]))

(defn- enable-when [{:keys [p qr-path]}]
  (let [enable-when-expression-path (path/extend-path p :extension {:url constants/enablewhen-expression-url})
        enable-when-expression      @(re-frame/subscribe
                                      [:gen-fhi.db.subs.value/get-value
                                       enable-when-expression-path])]
    [:div
     [:div {:class "mb-2"}
      [:label {:class "flex flex-1"}
       [:span {:class "mr-1"} "λ Use Expression"]
       [helper-text {:content  (r/as-element
                                [tooltip {:text "An expression that returns a boolean value for whether to enable the item."
                                          :link "https://build.fhir.org/ig/HL7/sdc/StructureDefinition-sdc-questionnaire-enableWhenExpression.html"}])}]]
      [:> Switch {:className (if (some? enable-when-expression)
                               "bg-blue-500"
                               "bg-gray-200")
                  :on-change (fn [checked? _event]
                               (if checked?
                                 (do (mutations/add
                                      (path/extend-path p :extension :-)
                                      {:url constants/enablewhen-expression-url})
                                     (mutations/remove
                                      (path/extend-path p :enableWhen)))
                                 (do (mutations/remove enable-when-expression-path))))
                  :checked (some? enable-when-expression)}]]
     (if enable-when-expression
       [q-editors/expression-form {:tooltip (r/as-element
                                             [tooltip {:text "An expression that returns a boolean value for whether to enable the item."
                                                       :link "https://build.fhir.org/ig/HL7/sdc/StructureDefinition-sdc-questionnaire-enableWhenExpression.html"}])
                                   :p          (path/extend-path p :extension {:url constants/enablewhen-expression-url} :valueExpression)
                                   :label      "Is Enabled Expression"
                                   :state-path qr-path
                                   :language   "text/fhirpath"}]
       [structured-enable-when {:p p}])]))

(defn- initial-value [{:keys [p qr-path]}]
  (let [item                    @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        initial-expression-path (path/extend-path p :extension {:url constants/initial-expression-url})
        initial-expression?     (some? (first (filter
                                               (fn [ext] (= constants/initial-expression-url (:url ext)))
                                               (get item :extension []))))]

    [:<>
     [:div {:class "mb-1 flex justify-center items-center"}
      [:label "Initial Value"]
      [:div {:class "flex flex-1 justify-end items-center"}
       [helper-text {:content  (r/as-element
                                [tooltip {:text "Use an expression to specify initial value."
                                          :link "http://build.fhir.org/ig/HL7/sdc/StructureDefinition-sdc-questionnaire-initialExpression.html"}])}]
       [:span {:class "mx-1"}
        "λ"]
       [:> Switch {:className (if initial-expression?
                                "bg-blue-500"
                                "bg-gray-200")
                   :on-change (fn [checked? _event]
                                (if checked?
                                  (do (mutations/add
                                       (path/extend-path p :extension :-)
                                       {:url constants/initial-expression-url})
                                      (mutations/remove
                                       (path/extend-path p :initial)))
                                  (do (mutations/remove initial-expression-path))))
                   :checked   initial-expression?}]]]
     [:div {:class "mb-2"}
      (if initial-expression?
        [q-editors/expression-form {:p           (path/extend-path p :extension {:url constants/initial-expression-url} :valueExpression)
                                    :hide-label? true
                                    :tooltip     (r/as-element
                                                  [tooltip {:text "Initial value for a question answer as determined by the evaluated expression."
                                                            :link "http://build.fhir.org/ig/HL7/sdc/StructureDefinition-sdc-questionnaire-initialExpression.html"}])
                                    :state-path  qr-path
                                    :language    "text/fhirpath"}]
        [:div
         (let [value-key (keyword (str "value" (string/capitalize
                                                (q-utils/q-item-type->fhir-primitive
                                                 (:type item)))))]
           [questionnaire-control {:item      item
                                   :value     (value-key (first (get item :initial)))
                                   :on-change (fn [value]
                                                (mutations/replace
                                                 (path/extend-path p :initial 0 value-key)
                                                 value))}])])]]))

(defn- variable [{:keys [p qr-path]}]
  [:<>
   [:div {:class "mb-2"}
    [editors/text-form {:p (path/extend-path p :valueExpression :name)}]]
   [:div {:class "mb-2"}
    [q-editors/expression-form {:remove-expression-on-empty? false
                                :label "Value"
                                :p (path/extend-path p :valueExpression)
                                :state-path qr-path}]
    [:div {:class "mt-1 flex justify-end items-center"}
     [:span {:on-click (fn [_e]
                         (mutations/remove p))
             :class "cursor-pointer hover:text-red-500 text-xs text-red-400"} "Remove Variable"]]]])

(defn- variables [{:keys [p qr-path]}]
  (let [item                     @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
        variable-extension-paths (->> (get item :extension [])
                                      (map-indexed (fn [i v] [i v]))
                                      (filter (fn [[i ext]]
                                                (= constants/variable-expression-url (:url ext))))
                                      (map (fn [[i _v]]
                                             (path/extend-path p :extension i))))]
    [:div
     (doall
      (map
       (fn [variable-path]
         ^{:key variable-path}
         [variable {:p variable-path
                    :qr-path qr-path}])
       variable-extension-paths))
     [:span {:on-click (fn [_e]
                         (mutations/add
                          (path/extend-path p :extension :-)
                          {:url constants/variable-expression-url
                           :valueExpression
                           {:language "text/fhirpath"}}))
             :class "text-sm cursor-pointer hover:text-blue-600 mt-1"}
      "+ Add Variable"]]))

(defn- item-properties [{:keys [qr-path]}]
  (let [p          @(re-frame/subscribe [:gen-fhi.db.subs.ui/get-selected-item-path])
        item-type  (when p @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/descend p :type)]))
        item       (when p @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p]))]
    ;; Item could be nil if moved to seperate loc in editor.
    ;; But still selected.
    (if (or (nil? item)
            (nil? p))
      [:div {:className "mt-2 font-semibold text-gray-600 flex items-center justify-center"}
       [:span "Select an item to see its properties."]]

      [:div {}
       [item-properties-header {:p p}]
       [:div {}
        [:> Collapse {:ghost true :defaultActiveKey ["properties" "variables" "enablewhen"]}
         [:> Panel {:key "properties"
                    :header (r/as-element [:h4 {:class "font-semibold text-slate-800"}
                                           "Properties"])}
          [:<>
           [:div {:class "mb-2"}
            [editors/code-select-form {:p (path/descend p "type")
                                       :placeholder "Select question type"
                                       :valueset "http://hl7.org/fhir/ValueSet/item-type"}]]

           [:div {:class "mb-2"}
            [editors/text-form     {:p (path/descend p "text")
                                    :placeholder "Enter questions text."
                                    :label "Text"}]]

           [:div {:class "mb-2"}
            [editors/text-form {:p (path/descend p "definition")
                                :placeholder "Enter a definition."
                                :label "Definition"
                                :tooltip (r/as-element
                                          [tooltip {:text "URI that refers to an element definition (used in structure definition extraction)."
                                                    :link "http://hl7.org/fhir/R4/questionnaire-definitions.html#Questionnaire.item.definition"}])}]]

           [:div {:class "mb-2"}
            [editors/text-form     {:p (path/descend p "prefix")
                                    :placeholder "Enter prefix text."}]]

           [:div {:class "mb-2"}
            [editors/boolean-form {:p (path/descend p "required")}]]

           [:div {:class "mb-2"}
            [editors/boolean-form {:tooltip (r/as-element
                                             [tooltip {:text "An indication, if true, that the item may occur multiple times in the response."
                                                       :link "http://hl7.org/fhir/R4/questionnaire-definitions.html#Questionnaire.item.repeats"}])
                                   :p (path/descend p "repeats")}]]

           [:div {:class "mb-2"}
            [editors/boolean-form {:p (path/descend p "readOnly")}]]

           [initial-value {:p p :qr-path qr-path}]

           [:div {:class "mb-2"}
            [q-editors/expression-form {:p  (path/extend-path p :extension {:url constants/calculated-expression-url} :valueExpression)
                                        :tooltip (r/as-element
                                                  [tooltip {:text "Calculated value for a question answer as determined by the evaluated expression."
                                                            :link "http://build.fhir.org/ig/HL7/sdc/StructureDefinition-sdc-questionnaire-calculatedExpression.html"}])
                                        :label "Calculated Expression"
                                        :state-path qr-path
                                        :language "text/fhirpath"}]]

           (when (#{"choice" "open-choice"} (:type item))
             [:div {:class "mb-2"}
              [editors/code-select-form {:tooltip (r/as-element
                                                   [tooltip {:text "The type of data entry control or structure that should be used to render the item."
                                                             :link "http://hl7.org/fhir/R4/extension-questionnaire-itemcontrol-definitions.html#extension.itemControl"}])
                                         :label           "Item Control"
                                         :descendent-of   "question"
                                         :filter-concepts (fn [concept]
                                                            (some? (#{"radio-button" "drop-down"} (:code concept))))
                                         :p               (path/extend-path
                                                           p
                                                           :extension
                                                           {:url "http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl"}
                                                           :valueCodeableConcept
                                                           :coding
                                                           0
                                                           :code)
                                         :placeholder     "Select a questionnaire item control"
                                         :valueset        "http://hl7.org/fhir/ValueSet/questionnaire-item-control"}]])]]

         [:> Panel {:className "!border-b-0 !border-x-0 !border"
                    :key "enablewhen"
                    :header (r/as-element [:h4 {:class "font-semibold text-slate-800"}
                                           "Enabled?"])}
          [:div {:class "mb-2"}
           [enable-when {:p       p
                         :qr-path qr-path}]]]

         [:> Panel {:className "!border-b-0 !border-x-0 !border"
                    :key "variables"
                    :header (r/as-element [:h4 {:class "font-semibold text-slate-800"}
                                           "Variables"])}
          [:<>
           [variables {:p p
                       :qr-path qr-path}]]]

         (when (#{"choice" "open-choice"} item-type)
           [:> Panel {:className "!border-b-0 !border-x-0 !border"
                      :key "answer-options"
                      :header (r/as-element [:h4 {:class "font-semibold text-slate-800"}
                                             "Allowed Answers"])}
            [:<>
             [:div {:class "mb-2"}
              [editors/-label {:tooltip (r/as-element
                                         [tooltip {:text "Can set use a valueset to set options for answer or inline options."}])}
               "Inline Answers?"]
              [:> Switch {:checked (some? (get item :answerOption))
                          :className (if (some? (get item :answerOption))
                                       "bg-blue-400"
                                       "bg-gray-200")
                          :onChange (fn [checked? _e]
                                      (if checked?
                                        (do
                                          (mutations/remove
                                           (path/descend p :answerValueSet))
                                          (mutations/replace
                                           (path/descend p :answerOption)
                                           []))
                                        (mutations/remove
                                         (path/descend p :answerOption))))}]]

             (if (some? (get item :answerOption))
               [:div {:class "overflow-x-auto"}
                (let [paths (map-indexed
                             (fn [i _v]
                               (path/extend-path p :answerOption i :valueCoding))
                             (if (empty? (get item :answerOption []))
                               [{}]
                               (get item :answerOption)))]
                  [:table {:class "table-fixed" :style {:height "fit-content"}}
                   [:thead
                    [:tr
                     [table-header
                      "Code"]
                     [table-header
                      "Display"]
                     [table-header
                      "Ordinal Value"]
                     [:th]]]
                   [:tbody
                    (doall
                     (map
                      (fn [answer-option-value-p]
                        ^{:key answer-option-value-p}
                        [:tr
                         ;; If it's a virtual don't show deletion
                         ;; UI consideration
                         ;;[editors/text-form    {:p (path/descend answer-p :system)}]
                         [:td {:class "w-1/3"}
                          [editors/text-form    {:className "!p-1"
                                                 :hide-label? true
                                                 :p (path/descend answer-option-value-p :code)}]]
                         [:td {:class "w-1/3"}
                          [editors/text-form    {:className "!p-1"
                                                 :hide-label? true
                                                 :p (path/descend answer-option-value-p :display)}]]
                         [:td {:class "w-1/3"}
                          [editors/number-form  {:className "!p-1"
                                                 :hide-label? true
                                                 :p     (path/extend-path
                                                         answer-option-value-p
                                                         :extension
                                                         {:url constants/ordinal-expression-url}
                                                         :valueDecimal)}]]
                         [:td
                          (when (seq (get item :answerOption))
                            [:div {:class "ml-1 font-bold hover:text-red-400 cursor-pointer flex items-center justify-center"
                                   :on-click (fn [_e]
                                               (mutations/remove
                                                (:parent (path/ascend answer-option-value-p))))}
                             [:span "X"]])]])
                      paths))]])
                [:div {:class "text-sm cursor-pointer hover:text-blue-600 mt-1"
                       :on-click (fn [_e]
                                   (if (empty? (get item :answerOption []))
                                     (do
                                       (mutations/add
                                        (path/extend-path p :answerOption :-)
                                        {})
                                       (mutations/add
                                        (path/extend-path p :answerOption :-)
                                        {}))
                                     (mutations/add
                                      (path/extend-path p :answerOption :-)
                                      {})))}
                 [:span "+ Add Inlined Answer"]]]

               [:div {:class "mb-2"}
                [editors/valueset-select-url {:tooltip (r/as-element
                                                        [tooltip {:text "A reference to a value set containing a list of codes representing permitted answers."
                                                                  :link "http://hl7.org/fhir/R4/questionnaire-definitions.html#Questionnaire.item.answerValueSet"}])
                                              :p (path/descend p :answerValueSet)
                                              :label "ValueSet"}]])]])]]])))

(defn- questionnaire-properties [{:keys [p qr-p]}]
  [:div {:class "p-4"}
   [:div {:class "mb-2"}
    [editors/text-form {:p (path/descend p "title")
                        :placeholder "Enter questionnaire title."}]]
   [:div {:class "mb-2"}
    [editors/text-form {:p (path/descend p "description")
                        :placeholder "Enter questionnaire description."}]]
   [:div {:class "mb-2"}
    [editors/text-form {:p (path/descend p "url")
                        :placeholder "Enter questionnaire url."}]]

   [:div {:class "mb-2"}
    [editors/code-select-form {:p (path/descend p "status")
                               :placeholder "Enter questionnaire status."
                               :valueset "http://hl7.org/fhir/ValueSet/publication-status"}]]])

(defn palette-item [{:keys [template]}]
  (let [[collected drag preview] (dnd/useDrag #js {:type "questionnaire-move-item"
                                                   :collect (fn [monitor]
                                                              {:isDragging (oops/ocall monitor "isDragging")})
                                                   :item {:type    :add
                                                          :q-item  (assoc (get template :data)
                                                                          :linkId (str (get-in template [:data :type]) "-"
                                                                                       (js/parseInt (* (.random js/Math) 100000000))))}})]
    [:<>
     ;;[:> dnd/DragPreviewImage {:connect preview :src "https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"}]
     [:li {:ref drag
           :key (get-in template [:data :type])
           :className "h-8 hover:text-blue-500 hover:bg-blue-100 border border-x-0 border-t-0 border-gray-100 flex items-center"
           :style {:user-select "none"
                   :cursor "grab"
                   :padding-left "8px"}}
      [:div {:className "flex items-center"}
       [:div {:className "flex items-center mr-4"}
        (:icon template)]
       [:div {:className "text-sm"} (string/capitalize (get-in template [:data :type]))]]]]))
(defn palette []
  [:ul {:style {:padding "0px"
                :font-size "16px"
                :list-style-type "none"}}
   (doall
    (map
     (fn [template-type]
       ^{:key template-type}
       [:f> palette-item {:template (templates/items template-type)}])
     (sort compare (keys templates/items))))])

(defn questionnaire-editor []
  (let [operation-outcome! (r/atom nil)]
    (fn [{:keys [resource-type id]}]
      [:> dnd/DndProvider {:backend HTML5Backend}
       [layout
        {:left-sidebar [left-sidebar {}
                        [questionnaire-properties {:p    (path/->path resource-type id)
                                                   :qr-p (path/->path "QuestionnaireResponse" id)}]]
         :right-sidebar [right-sidebar {}
                         [tabs {:active "item-properties"}
                          [:f> tab-pane {:key "item-properties" :name "Item"}
                           [item-properties {:qr-path (path/->path "QuestionnaireResponse" id)}]]
                          [:f> tab-pane {:key "components" :name "Components"}
                           [:div
                            [palette]]]]]}
        [:div {:class "p-2 pb-40"}
         (let [questionnaire @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path resource-type id)])]
           (when questionnaire
             [:f> Questionnaire {:key                   id
                                 :editable?             true
                                 :questionnaire         questionnaire
                                 :operation-outcome     @operation-outcome!
                                 :on-submit             (fn [_questionnaire-response operation-outcome]
                                                          (reset! operation-outcome! operation-outcome))
                                 :on-item-click         (fn [p _item]
                                                          (re-frame/dispatch
                                                           [:gen-fhi.db.events.ui/set-selected-item-path p]))}]))]]])))

