(ns gen-fhi.workspace.views.instance.questionnaire-editor.templates
  (:require
   ["@ant-design/icons" :refer [EditOutlined
                                NumberOutlined
                                TranslationOutlined
                                TableOutlined
                                ClockCircleOutlined
                                CalendarOutlined
                                CheckCircleOutlined
                                FileOutlined
                                GlobalOutlined
                                LinkOutlined
                                SlidersOutlined
                                AlignLeftOutlined]]))

(def items
  {"group"       {:icon [:> TableOutlined]
                  :data {:type "group"}}

   "display"     {:icon [:> AlignLeftOutlined]
                  :data {:type "display"}}

   "date"        {:icon [:> CalendarOutlined]
                  :data {:type "date"}}

   "dateTime"    {:icon [:> CalendarOutlined]
                  :data {:type "dateTime"}}

   "time"        {:icon [:> ClockCircleOutlined]
                  :data {:type "time"}}

   "boolean"     {:icon [:> CheckCircleOutlined]
                  :data {:type "boolean"}}

   "string"      {:icon [:> EditOutlined]
                  :data {:type "string"}}

   "text"        {:icon [:> EditOutlined]
                  :data {:type "text"}}

   "url"         {:icon [:> GlobalOutlined]
                  :data {:type "url"}}

   "integer"     {:icon [:> NumberOutlined]
                  :data {:type "integer"}}

   "decimal"     {:icon [:> NumberOutlined]
                  :data {:type "decimal"}}

   "choice"      {:icon [:> TranslationOutlined]
                  :data {:type "choice"
                         :extension [{:url "http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl"
                                      :valueCodeableConcept {:coding [{:display "Drop down" :code "drop-down"}]}}]}}

   "open-choice" {:icon [:> TranslationOutlined]
                  :data {:type "choice"}}

   "attachment"  {:icon [:> FileOutlined]
                  :data {:type "attachment"}}

   "reference"   {:icon [:> LinkOutlined]
                  :data {:type "reference"}}

   "quantity"    {:icon [:> SlidersOutlined]
                  :data {:type "quantity"}}})


