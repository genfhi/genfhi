(ns gen-fhi.workspace.views.instance.terminology-editor.codesystem
  (:require [gen-fhi.patch-building.path :as path]
            [re-frame.core :as re-frame]
            [reagent.core :as r]
            [clojure.string :as string]
            [clojure.core.match :refer [match]]
            [oops.core :as oops]
            [accountant.core :as accountant]

            ["js-file-download" :as file-download]
            ["antd" :refer [Breadcrumb Dropdown Menu]]
            ["@ant-design/icons" :refer [RightOutlined EllipsisOutlined DownloadOutlined]]

            [gen-fhi.db.questionnaire.constants :as questionnaire-constants]
            [gen-fhi.workspace.components.table-editor :refer [table-editor footer-item td-col-class]]
            [gen-fhi.workspace.utilities.mutations :as mutations]
            [gen-fhi.workspace.router :as router]
            [gen-fhi.workspace.components.editors.core :as editors]
            [gen-fhi.workspace.views.instance.application-editor.constants :as c]
            [gen-fhi.workspace.components.layout :refer [layout left-sidebar right-sidebar]]
            [gen-fhi.fhirpath.core :as fp]))

(defn- path->parent-code-paths [path]
  (loop [paths []
         cur-path path]

    (let [res (when cur-path (path/ascend cur-path))]
      (if (and (some? res) res (= :concept (get res :field)))
        (let [{:keys [parent]} (path/ascend (get res :parent))]
          (recur (conj paths (get res :parent)) parent))
        (reverse paths)))))

(defn codesystem-breadcrumbs [{:keys [p on-click]}]
  (let [parent-paths (path->parent-code-paths p)
        vals (map (fn [p] @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p]))
                  parent-paths)]
    [:> Breadcrumb
     (doall
      (map-indexed (fn [i v]
                     ^{:key i}
                     [:> (oops/oget Breadcrumb "Item")

                      {:key i
                       :on-click (partial on-click (nth parent-paths i))}
                      [:div {:class "text-slate-700 hover:text-blue-400 cursor-pointer inline-block"}
                       (if (= 0 i)
                         [:span {:class "flex items-center justify-center"}
                          [:span {:class "inline-block font-semibold select-all truncate "}
                           (get v :url)]]
                         (get v :code))]])
                   vals))]))

(defn concept-properties [{:keys [p]}]
  (if (nil? p)
    [:div {:className "mt-2 font-semibold text-gray-600 flex items-center justify-center"}
     [:span "Click on a concept to select it."]]
    (let [concept @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])]
      [:div
       [:div {:className "mb-2 mt-1 flex items-center justify-end border-l-0 border-r-0 border-t-0 border border-gray-300 pl-1 pb-1 pr-1 mb-2"}
        [:div {:className "flex-1 flex justify-center items-center mr-1"}
         [editors/text-form {:className c/id-text-classname
                             :hide-label? true
                             :p (path/descend p "code")}]]
        [:div
         [:> Dropdown {:placement "bottomRight"
                       :overlay
                       (r/as-element
                        [:div {:style {:border "1px solid #e0e0e0"}}
                         [:div {:style {:background-color "#f7f7f7"
                                        :font-size "10px"
                                        :color "#9e9e9e"
                                        :display "flex"
                                        :flex-direction "column"
                                        :justify-content "center"
                                        :border-bottom "1px solid #e0e0e0"}}
                          [:span {:style {:display "block" :margin-left "8px" :margin-right "8px" :margin-top "2px" :margin-bottom "2px" :font-size "14px" :font-weight "500"}}
                           (string/capitalize (or (get concept :code) ""))]]
                         [:> Menu {:mode "vertical" :style {:border "0px"}}
                          [:> (.-Item Menu) {:key "delete-selected"
                                             :on-click (fn []
                                                         (mutations/remove p))}

                           [:div {:style {:display "flex" :align-items "center" :justify-content "center"}}
                            [:span {:style {:font-size "12px"}} "Delete"]
                            [:span {:style {:margin-left "10px" :font-size "8px" :color "#c9c9c9"}}
                             "Ctrl+-"]]]]])}
          [:a {:className "ant-dropdown-link" :on-click (fn [e] (.preventDefault e))}
           [:> EllipsisOutlined {:style {:font-size "18px"}}]]]]]
       [:div {:class "px-2"}
        [:div {:class "mb-2"}
         [editors/number-form {:label "Ordinal Value"
                               :p (path/extend-path
                                   p
                                   :extension
                                   {:url questionnaire-constants/ordinal-expression-url}
                                   :valueDecimal)}]]

        (when (seq (get concept :property))
          [:div
           [:label "Concept Properties"]
           [:table {:class "w-full"}
            [:thead
             [:tr
              [:th "Code"]
              [:th "Value"]]]
            (map
             (fn [property]
               [:tr
                [:td (get property :code)]
                [:td (first (fp/evaluate "$this.value" concept))]])

             (get concept :property))]])]])))

(defn codesystem-editor [{:keys [resource-type id]}]
  (let [p          (path/->path resource-type id)
        params     @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])
        active-row (r/atom nil)
        set-active-row (fn [p] (reset! active-row p))]
    (fn [_props]
      (let [codesystem       @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
            query-parameters @(re-frame/subscribe [:gen-fhi.db.subs.route/get-query-parameters])
            cur-concept-path (get query-parameters :current-path (str (path/descend p "concept")))
            concepts         @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value cur-concept-path])
            add-concept      (fn []
                               (mutations/add
                                (path/descend cur-concept-path (count concepts))
                                {:code (str "code"
                                            (.round
                                             js/Math
                                             (* 10000
                                                (.random js/Math))))}))]

        [layout
         {:right-sidebar
          [right-sidebar
           [concept-properties {:p @active-row}]]

          :left-sidebar
          [left-sidebar
           [:div  {:class "p-1"}
            [:div {:class "mb-2 mt-2"}
             [editors/text-form {:p (path/descend p "name")}]]
            [:div {:class "mb-2"}
             [editors/text-form {:p (path/descend p "url")}]]
            [:div {:class "mb-2"}
             [editors/code-select-form {:p        (path/descend p "status")
                                        :valueset "http://hl7.org/fhir/ValueSet/publication-status"}]]]]}

         [:div {:class "h-full w-full flex justify-center mt-8"
                :on-key-up   (fn [e]
                               (match [(.-ctrlKey e) (.-keyCode e)]
                                 [true 13]  (add-concept)
                                 ;; Ctrl + - remove
                                 [true 189] (when (some? @active-row)
                                              (mutations/remove @active-row))
                                 ;; Escape
                                 [false 27] (do
                                              (oops/ocall js/document "activeElement.blur")
                                              (set-active-row nil)
                                              (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :right false])
                                              (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :left false]))

                                 :else      (do)))}
          [:div {:class "container p-8"}
           [:div {:class "mb-2 flex items-center"}
            [:div {:class "flex flex-1"}
             [codesystem-breadcrumbs {:p cur-concept-path
                                      :on-click (fn [p]
                                                  (accountant/navigate!
                                                   (router/path-for :instance params)
                                                   {:current-path (str (path/descend p "concept"))}))}]]
            [:div {:on-click (fn [_e] (file-download
                                       (.stringify js/JSON (clj->js codesystem) nil 2)
                                       (str (get codesystem :name (get codesystem :id))
                                            "-" (get-in codesystem [:meta :versionId])
                                            ".json")))
                   :class "text-slate-400 font-md hover:text-blue-400 cursor-pointer flex items-center "}
             [:span {:class "mr-2"} "Download"]
             [:> DownloadOutlined]]]
           [table-editor {:p              cur-concept-path
                          :set-active-row set-active-row
                          :active-row     @active-row
                          :footer [footer-item {:helper-text "Ctrl + Enter"
                                                :on-click (fn [_e]
                                                            (add-concept))}

                                   "+ Add Concept"]
                          :columns [{:name "Code"
                                     :width 30
                                     :render (fn [{:keys [data p]}]
                                               [:input {:class td-col-class
                                                        :value (get data :code)
                                                        :on-focus (fn [_] (set-active-row p))
                                                        :on-change (fn [e]
                                                                     (mutations/replace
                                                                      (path/descend p "code")
                                                                      (oops/oget e "target.value")
                                                                      {:remove-empty? false}))}])}
                                    {:name "Display"
                                     :width 30
                                     :render (fn [{:keys [data p]}]
                                               [:input {:class td-col-class
                                                        :value (get data :display)
                                                        :on-focus (fn [] (set-active-row p))
                                                        :on-change (fn [e]
                                                                     (mutations/replace
                                                                      (path/descend p "display")
                                                                      (oops/oget e "target.value")))}])}
                                    {:name "Definition"
                                     :width 30
                                     :render (fn [{:keys [data p]}]
                                               [:input {:class td-col-class
                                                        :value     (get data :definition)
                                                        :on-focus (fn [] (set-active-row p))
                                                        :on-change (fn [e]
                                                                     (mutations/replace
                                                                      (path/descend p "definition")
                                                                      (oops/oget e "target.value")))}])}
                                    {:name "Children"
                                     :width 12
                                     :render (fn [{:keys [data p]}]
                                               [:div {:on-click (fn [_e]
                                                                  (accountant/navigate!
                                                                   (router/path-for :instance params)
                                                                   {:current-path (str (path/descend p "concept"))}))
                                                      :class "flex items-center justify-center cursor-pointer hover:text-blue-600 text-blue-400"}
                                                [:div
                                                 [:span {:class "font-semibold mr-2"}
                                                  (count (get data :concept []))]]
                                                [:> RightOutlined]])}]}]]]]))))
