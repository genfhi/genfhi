(ns gen-fhi.workspace.views.instance.terminology-editor.core
  (:require [clojure.core.match :refer [match]]
            [gen-fhi.workspace.views.instance.terminology-editor.valueset :refer [valueset-editor]]
            [gen-fhi.workspace.views.instance.terminology-editor.codesystem :refer [codesystem-editor]]))

(defn terminology-editor [{:keys [resource-type id]}]
  (match resource-type
    "CodeSystem" [codesystem-editor {:resource-type resource-type :id id}]
    "ValueSet"   [valueset-editor {:resource-type resource-type :id id}]))



