(ns gen-fhi.workspace.views.instance.terminology-editor.valueset
  (:require [re-frame.core :as re-frame]
            [oops.core :as oops]
            [reagent.core :as r]
            [cljs.core.async :refer [take!]]
            [clojure.core.match :refer [match]]

            ["js-file-download" :as file-download]
            ["antd" :refer [Dropdown Menu]]
            ["@ant-design/icons" :refer [DownloadOutlined EllipsisOutlined]]

            [gen-fhi.workspace.utilities.debounce :refer [debounce]]
            [gen-fhi.fhirpath.core :as fp]
            [gen-fhi.workspace.components.completion :refer [completion]]
            [gen-fhi.fhir-client.protocols :as fhir-client]
            [gen-fhi.workspace.components.terminology :refer [codesystem-search]]
            [gen-fhi.workspace.views.instance.application-editor.constants :as c]
            [gen-fhi.patch-building.path :as path]
            [gen-fhi.workspace.components.layout :refer [layout left-sidebar right-sidebar]]
            [gen-fhi.workspace.components.editors.core :as editors]
            [gen-fhi.workspace.components.table-editor :refer [table-editor footer-item td-col-class]]
            [gen-fhi.workspace.utilities.mutations :as mutations]))

(defn- codesystem->flatten-codes [v]
  (mapcat
   (fn [concept]
     (concat
      [concept]
      (if (some? (get v :concept))
        (codesystem->flatten-codes concept)
        [])))
   (get v :concept)))

(defn- valueset-compose-properties [{:keys [p]}]
  (let [->parameters (fn [url] {:url url :_all "true"})
        params       (r/atom nil)
        is-loading?   (r/atom true)
        codesystem   (r/atom nil)
        c            @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        query        (debounce
                      (fn [parameters]
                        (reset! params parameters)
                        (reset! is-loading? true)
                        (take!
                         (fhir-client/search c "CodeSystem" parameters)
                         (fn [{:keys [:fhir/response]}]
                           (reset! is-loading? false)
                           (reset!
                            codesystem
                            (first
                             (filter
                              (fn [codesystem] (= (:url codesystem) (get parameters :url)))
                              (or response [])))))))
                      200)]
    (fn [{:keys [p]}]
      (if (nil? p)
        [:div {:className "mt-2 font-semibold text-gray-600 flex items-center justify-center"}
         [:span "Click on a compose row to see its properties."]]

        (let [compose-element @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])]
          (when (not= @params (->parameters (get compose-element :system)))
            (query (->parameters (get compose-element :system))))
          [:div
           [:div {:className "mt-1 flex items-center justify-end border-l-0 border-r-0 border-t-0 border border-gray-300 pl-1 pb-1 pr-1 mb-2"}
            [:div {:className "flex-1 flex justify-center items-center mr-1"}
             [editors/text-form {:className c/id-text-classname
                                 :hide-label? true
                                 :placeholder "Compose System"
                                 :p (path/descend p "system")}]]
            [:div
             [:> Dropdown {:placement "bottomRight"
                           :overlay
                           (r/as-element
                            [:div {:style {:border "1px solid #e0e0e0"}}
                             [:div {:style {:background-color "#f7f7f7"
                                            :font-size "10px"
                                            :color "#9e9e9e"
                                            :display "flex"
                                            :flex-direction "column"
                                            :justify-content "center"
                                            :border-bottom "1px solid #e0e0e0"}}
                              [:span {:style {:display "block" :margin-left "8px" :margin-right "8px" :margin-top "2px" :margin-bottom "2px" :font-size "14px" :font-weight "500"}}
                               "Options"]]
                             [:> Menu {:mode "vertical" :style {:border "0px"}}
                              [:> (.-Item Menu) {:key "delete-selected"
                                                 :on-click (fn []
                                                             (mutations/remove p))}

                               [:div {:style {:display "flex" :align-items "center" :justify-content "center"}}
                                [:span {:style {:font-size "12px"}} "Delete"]
                                [:span {:style {:margin-left "10px" :font-size "8px" :color "#c9c9c9"}}
                                 "Ctrl + -"]]]]])}
              [:a {:className "ant-dropdown-link" :on-click (fn [e] (.preventDefault e))}
               [:> EllipsisOutlined {:style {:font-size "18px"}}]]]]]
           [:div {:class "mt-4 p-2"}
            [:label {:class "font-semibold"} "Include only following codes:"]
            ^{:key (get @codesystem :url)}
            [table-editor {:p (path/extend-path p "concept")
                           :columns [{:name "Code"    :render (fn [{:keys [data p]}]
                                                                [completion {:input-class td-col-class
                                                                             :value (get data :code)
                                                                             :on-change (fn [concept]
                                                                                          (mutations/replace
                                                                                           (path/descend p "code")
                                                                                           (get concept :code))

                                                                                          (mutations/replace
                                                                                           (path/descend p "display")
                                                                                           (get concept :display)))
                                                                             :loading?  @is-loading?
                                                                             :options (map (fn [concept]
                                                                                             {:value concept :display (get concept :code)})
                                                                                           (codesystem->flatten-codes @codesystem))}])}

                                     {:name "Display" :render (fn [{:keys [data p]}]
                                                                [:input {:class td-col-class
                                                                         :value (get data :display)
                                                                         :on-change (fn [e]
                                                                                      (mutations/replace
                                                                                       (path/descend p "display")
                                                                                       (oops/oget e "target.value")))}])}]
                           :footer [footer-item {:on-click (fn []
                                                             (mutations/add
                                                              (path/extend-path
                                                               p
                                                               "concept"
                                                               (count (get compose-element :concept [])))
                                                              {:code (or (first (fp/evaluate "$this.concept.code" @codesystem)) "new-code")
                                                               :display (or (first (fp/evaluate "$this.concept.display" @codesystem)) "")}))}
                                    "+ Add Code"]}]]])))))

(defn valueset-editor [{:keys [resource-type id]}]
  (let [active-row     (r/atom nil)
        set-active-row (fn [p] (reset! active-row p))]
    ;; SPAM SYNC TESTING
    ;; (.setInterval js/window
    ;;               (fn []
    ;;                 (when @active-row
    ;;                   (let [p           (path/->path resource-type id "compose" "include" 0)
    ;;                         random-num  (.floor js/Math (* 10 (.random js/Math)))
    ;;                         random-num2 (.floor js/Math (* 10 (.random js/Math)))]
    ;;                     ;; (if (< random-num2 5)
    ;;                     ;;   (.click (.getElementById js/document p))
    ;;                     ;;   (.click (.getElementById js/document (path/->path resource-type id "compose" "include" 1))))
    ;;                     (if (< random-num 5)
    ;;                       (do
    ;;                         (mutations/replace
    ;;                          (path/descend p "system")
    ;;                          "https://local-url.com")
    ;;                         (mutations/replace
    ;;                          (path/descend p "version")
    ;;                          "*"))
    ;;                       (do
    ;;                         (mutations/replace
    ;;                          (path/descend p "system")
    ;;                          "")
    ;;                         (mutations/replace
    ;;                          (path/descend p "version")
    ;;                          ""))))))

    ;;               50)

    (fn [{:keys [resource-type id]}]
      (let [p              (path/->path resource-type id)
            valueset       @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
            add-include    (fn []
                             (let [path (path/extend-path
                                         p
                                         "compose"
                                         "include"
                                         (count (get-in valueset [:compose :include] [])))]
                               (mutations/add path {})))]
        [layout {:left-sidebar
                 [left-sidebar
                  [:div  {:class "p-1"}
                   [:div {:class "mb-2 mt-2"}
                    [editors/text-form {:p (path/descend p "name")}]]
                   [:div {:class "mb-2"}
                    [editors/text-form {:p (path/descend p "url")}]]]]
                 :right-sidebar
                 [right-sidebar
                  [:div {:class "p-1"}
                   [valueset-compose-properties {:p @active-row}]]]}
         [:div {:class "mt-8 flex items-center justify-center"}
          [:div {:on-key-up   (fn [e]
                                (match [(.-ctrlKey e) (.-keyCode e)]
                                  ;; Ctrl + enter add include
                                  [true 13]  (add-include)
                                  ;; Ctrl + - remove
                                  [true 189] (when (some? @active-row)
                                               (mutations/remove @active-row))
                                  ;; Escape
                                  [false 27] (do
                                               (oops/ocall js/document "activeElement.blur")
                                               (set-active-row nil)
                                               (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :right false])
                                               (re-frame/dispatch [:gen-fhi.db.events.ui/set-show-bar :left false]))

                                  :else      (do)))
                 :class "container p-8"}
           [:div {:class "mb-2 flex items-center"}
            [:div {:class "flex flex-1"}
             [:span {:class "font-semibold text-slate-700 select-all"} (get valueset :url (get valueset :id))]]
            [:div {:on-click (fn [_e] (file-download
                                       (.stringify js/JSON (clj->js valueset) nil 2)
                                       (str (get valueset :name (get valueset :id))
                                            "-" (get-in valueset [:meta :versionId])
                                            ".json")))
                   :class "text-slate-400 font-md hover:text-blue-400 cursor-pointer flex items-center "}
             [:span {:class "mr-2"} "Download"]
             [:> DownloadOutlined]]]
           [table-editor {:active-row     @active-row
                          :set-active-row set-active-row
                          :p (path/extend-path p "compose" "include")
                          :columns [{:name "System"
                                     :render (fn [{:keys [data p]}]
                                               [codesystem-search
                                                {:id   (path/descend p "system")
                                                 :class td-col-class
                                                 :url   (get data :system)
                                                 :on-change (fn [codesystem-or-text]
                                                              (let [url (if (string? codesystem-or-text)
                                                                          codesystem-or-text
                                                                          (get codesystem-or-text :url))]
                                                                (mutations/replace
                                                                 (path/descend p "system")
                                                                 url)
                                                                (when (= "CodeSystem" (get codesystem-or-text :resourceType))
                                                                  (mutations/replace
                                                                   (path/descend p "version")
                                                                   (get codesystem-or-text :version "*")))))}])}
                                    {:name "Version"
                                     :render (fn [{:keys [data p]}]
                                               [:input {:class (str td-col-class " read-only:bg-gray-100")
                                                        :read-only true
                                                        :value (get data :version "")}])}]

                          :footer [footer-item {:helper-text "Ctrl + Enter"
                                                :on-click (fn []
                                                            (add-include))}
                                   "+ Add CodeSystem"]}]]]]))))

