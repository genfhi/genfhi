(ns gen-fhi.workspace.views.preview
  (:require [gen-fhi.application.core :refer [application]]
            [gen-fhi.patch-building.path :as path]
            [re-frame.core :as re-frame]))

(defn view []
  (let [app-id @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :id])
        app-path (path/->path "ClientApplication" app-id)]
    [:f> application {:is-editable? false :path app-path}]))
