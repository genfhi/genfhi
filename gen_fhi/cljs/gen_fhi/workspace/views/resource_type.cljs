(ns gen-fhi.workspace.views.resource-type
  (:require
   [clojure.core.match :refer [match]]

   [gen-fhi.workspace.components.sidebar :refer [sidebar]]
   [gen-fhi.workspace.utilities.naming :as naming]
   [gen-fhi.workspace.components.create-resource
    :refer [create-resource-btn]]
   [gen-fhi.workspace.components.query :refer [query]]
   [gen-fhi.workspace.components.resource-table :refer [resource-table]]
   [re-frame.core :as re-frame]
   [reagent.core :as r]))

(defn- param->resource-types [type]
  (match type
    "Terminologies" ["ValueSet" "CodeSystem"]
    :else [type]))

(defn- param->parameters [type]
  (match type
    "Endpoint" {:organization:missing true}
    :else {}))

(defn view []
  (let [sidebar-selected (r/atom nil)]
    (fn []
      (let [type           @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :resource-type])
            resource-types (param->resource-types type)]
        [:div {:className "container mx-auto pt-8 h-full"}
         [:div {:className "h-full mx-auto"}
          [:div {:class "mb-4 flex items-center"}
           [:h3 {:className "text-base font-semibold text-slate-700 mb-2"}
            (naming/resource-type->display-name type true)]
           [:div {:class "flex flex-1 justify-end items-center"}
            [create-resource-btn {:resource-types resource-types}]]]
          [:div {:class "flex"}
           (when (< 1 (count resource-types))
             [:div {:class "mr-8 w-[175px]"}
              [sidebar {:value  (:value @sidebar-selected)
                        :on-click (fn [selected-item]
                                    (reset! sidebar-selected selected-item))
                        :items (concat
                                [{:display "All" :value nil}]
                                (map
                                 (fn [resource-type]
                                   {:display (naming/resource-type->display-name resource-type)
                                    :value resource-type})
                                 resource-types))}]])
           [:div {:class "w-full"}
            ^{:key (str resource-types)}
            [query {:trigger-on-mount? true
                    :parameters (merge
                                 (param->parameters
                                  type)
                                 {:_type (or (:value @sidebar-selected) resource-types)})}
             (fn [{:keys [result re-query is-loading]}]
               [resource-table {:types resource-types
                                :on-delete re-query
                                :result result
                                :is-loading is-loading}])]]]]]))))

