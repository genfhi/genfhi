(ns gen-fhi.workspace.views.settings
  (:require [cljs.core.async :refer [take!]]
            [reagent.core :as r]
            [re-frame.core :as re-frame]
            [accountant.core :as accountant]

            ["antd" :refer [Table Avatar]]

            [gen-fhi.workspace.components.resource-header :refer [resource-header]]
            [gen-fhi.workspace.components.editors.core    :refer [raw-text-editor]]
            [gen-fhi.workspace.components.resource-table  :refer [resource-table]]
            [gen-fhi.workspace.components.query           :refer [query]]
            [gen-fhi.workspace.components.sidebar         :refer [sidebar]]
            [gen-fhi.components.base.button               :refer [button]]
            [gen-fhi.fhir-client.protocols                :as fhir-client]
            [gen-fhi.operations.core                      :as workspace-ops]
            [gen-fhi.workspace.router                     :as router]
            [oops.core                                    :as oops]
            [gen-fhi.patch-building.path                  :as path]))

(defn user-view []
  (let [client @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        users (r/atom nil)
        loading? (r/atom false)
        query  (fn []
                 (reset! loading? true)
                 (take!
                  (fhir-client/invoke
                   client
                   workspace-ops/users
                   {})
                  (fn [{:keys [:fhir/response]}]
                    (reset! loading? false)
                    (reset!
                     users
                     response))))]
    (query)
    (fn []
      [:div {:class "w-full"}
       [:div {:class "flex justify-content items-center mb-1"}
        [:div {:class "text-base font-semibold  text-slate-600"}
         [:span "Workspace users"]]
        [:div {:class "flex flex-1 justify-end items-center"}
         [button {:type :primary
                  :on-click (fn [_e]
                              (re-frame/dispatch [:gen-fhi.db.events.ui/set-active-modal
                                                  {:type :invite-user}]))}
          "Invite"]]]

       [:div {:class "border"}
        [:> Table {:loading @loading?
                   :size  "small"
                   :dataSource (if (sequential? (:return @users))
                                 (:return @users)
                                 (if (some? (:return @users))
                                   [(:return @users)]
                                   []))
                   :columns [{:title     ""
                              :dataIndex ["photo" "url"]
                              :width     "52px"
                              :render    (fn [picture-url _record _index]
                                           (r/as-element
                                            [:> Avatar {:src picture-url}]))}

                             {:title "Name"
                              :key "name"
                              :render (fn [_text record _index]
                                        (let [person (js->clj record :keywordize-keys true)]
                                          (r/as-element
                                           [:span
                                            (or
                                             (get-in person [:name 0 :text])
                                             (str (get-in person [:name 0 :given 0] "")
                                                  (get-in person [:name 0 :family] "")))])))}
                             {:title "Email"
                              :key "email"
                              :render (fn [_text record _index]
                                        (let [person (js->clj record :keywordize-keys true)
                                              emails (filter #(= "email" (:system %)) (get person :telecom))]
                                          (r/as-element
                                           [:span
                                            (get (first emails) :value)])))}]}]]])))

(defn- published-apps []
  (let [loading? (r/atom false)
        client @(re-frame/subscribe [:gen-fhi.db.subs.client/get-fhir-client])
        published-apps (r/atom nil)
        query (fn []
                (reset! loading? true)
                (take!
                 (fhir-client/invoke
                  client
                  workspace-ops/cloud-list
                  {})
                 (fn [{:keys [:fhir/response]}]
                   (reset! loading? false)
                   (reset! published-apps response))))]
    (query)
    (fn []
      (let [cur-workspace          @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :workspace])]
        [:div {:class "w-full"}
         [:div {:class "flex justify-content items-center mb-1"}
          [:div {:class "text-base font-semibold  text-slate-600"}
           [:span "Published Applications"]]
          [:div {:class "flex flex-1 justify-end items-center"}]]
         [:div {:class "border"}
          [:> Table {:loading @loading?
                     :size  "small"
                     :dataSource (if (sequential? (:return @published-apps))
                                   (:return @published-apps)
                                   (if (some? (:return @published-apps))
                                     [(:return @published-apps)]
                                     []))
                     :columns [{:title "App"
                                :render (fn [_text record _index]
                                          (let [record (js->clj record :keywordize-keys true)]
                                            (r/as-element
                                             [:div {:on-click (fn [_e]
                                                                (accountant/navigate!
                                                                 (router/path-for
                                                                  :instance
                                                                  {:workspace cur-workspace
                                                                   :resource-type "ClientApplication"
                                                                   :id (get record :id)
                                                                   :modifiers ""})))

                                                    :class "cursor-pointer hover:text-blue-500"}
                                              (if (get record :resource)
                                                [:div
                                                 [:div {:class "font-semibold"}
                                                  (get-in record [:resource :name])]
                                                 [:div
                                                  (get-in record [:resource :description])]]
                                                [:span (get record :id)])])))}
                               {:title "Action"
                                :key "actions"
                                :align "center"
                                :render (fn [_text record _index]
                                          (let [client-app-id (oops/oget record "id")]
                                            (r/as-element
                                             [:div {:class "flex justify-center items-center"}
                                              [button {:class "w-full"
                                                       :on-click (fn [_e]
                                                                   (re-frame/dispatch
                                                                    [:gen-fhi.db.events.ui/set-active-modal
                                                                     {:type :deployment
                                                                      :on-close (fn []
                                                                                  (query))
                                                                      :data {:id        client-app-id
                                                                             :workspace cur-workspace}}]))
                                                       :type :secondary}
                                               "Manage"]])))}]}]]]))))

(defn- environment-view []
  (let [params      @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])]
    [:div {:class "w-full"}
     [:div {:class "flex justify-content items-center mb-1"}
      [:div {:class "text-base font-semibold  text-slate-600"}
       [:span "Environment"]]
      [:div {:class "flex flex-1 justify-end items-center"}
       [button {:type :primary
                :on-click (fn [_e]
                            (accountant/navigate!
                             (router/path-for
                              :setting-instance
                              (merge
                               params
                               {:setting "environment"
                                :id      "create"}))))}
        "Create"]]]
     [query {:resource-type "Organization"}
      (fn [{:keys [result re-query is-loading]}]
        [resource-table {:types ["Organization"]
                         :is-loading is-loading
                         :on-delete (fn [] (re-query))
                         :on-row (fn [organization]
                                   (accountant/navigate!
                                    (router/path-for
                                     :setting-instance
                                     (merge
                                      params
                                      {:setting "environment"
                                       :id      (get organization :id)}))))
                         :result result}])]]))

(defn- environment-update []
  (let [state        (r/atom nil)]
    (fn []
      (let [params       @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])
            id           (:id params)
            new?         (= id "create")
            organization (if new?
                           {:id "new"
                            :resourceType "Organization"
                            :type [{:coding [{:code "environment"}]}]}
                           @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value (path/->path "Organization" id)]))]

        (when (not= (:id @state) (get organization :id))
          (reset! state organization))
        [query {:resource-type "Organization"}
         (fn [{:keys [re-query]}]
           [:div {:class "w-full"}
            [:div {:class "flex justify-content items-center mb-1"}
             [:div {:class "text-base font-semibold  text-slate-600"}
              [:span "Environment"]]
             [resource-header {:resource @state
                               :new? new?
                               :on-update (fn [_res]
                                            (re-query)
                                            (accountant/navigate!
                                             (router/path-for
                                              :setting-type
                                              (merge
                                               params
                                               {:setting "environment"}))))

                               :on-delete (fn []
                                            (re-query)
                                            (accountant/navigate!
                                             (router/path-for
                                              :setting-type
                                              (merge
                                               params
                                               {:setting "environment"}))))}]]

            [raw-text-editor {:label "Name"
                              :on-change (fn [e]
                                           (swap!
                                            state
                                            (fn [org]
                                              (assoc
                                               org
                                               :name
                                               (oops/oget e "target" "value")))))
                              :value (get @state :name)}]])]))))

(def setting-views {:setting-instance
                    {"environment"    {:name "Environments"
                                       :view environment-update}}
                    :setting-type
                    {"user"           {:name "Users"
                                       :view user-view}
                     "published-apps" {:name "Published Apps"
                                       :view published-apps}

                     "environment"    {:name "Environments"
                                       :view environment-view}}})

(defn view []
  (let [route-id    @(re-frame/subscribe [:gen-fhi.db.subs.route/get-route-id])
        params      @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameters])
        active-view (get-in setting-views [route-id (get params :setting) :view])]
    [:div {:class "h-full flex justify-center pt-8"}
     [:div {:class "flex container"}
      [:div {:class "w-48"}
       [:div {:class "flex mb-1 mt-1 text-base text-slate-600 font-semibold "}
        [:span "Settings"]]
       [:div {:class " p-1"}
        [sidebar
         {:on-click (fn [selected-item]
                      (accountant/navigate!
                       (router/path-for
                        :setting-type
                        (merge
                         params
                         {:setting (:value selected-item)}))))

          :value (get params :setting)
          :items (map
                  (fn [key]
                    {:display (get-in setting-views [:setting-type key :name])
                     :value key})
                  (keys (get setting-views :setting-type)))}]]]

      [:div {:class "flex flex-1 p-2"}
       (when active-view
         [active-view])]]]))


