(ns gen-fhi.workspace.views.smart-login
  (:require [re-frame.core :as re-frame]
            [oops.core :as oops]

            ["antd" :refer [Spin]]
            ["fhirclient" :as fhir-client]
            ["@ant-design/icons" :refer [ExclamationCircleOutlined CheckCircleOutlined]]

            [gen-fhi.db.events.smart :refer [resource->smart-config]]
            [gen-fhi.patch-building.path :as path]
            [reagent.core :as r]))

(defn view []
  (let [response          (r/atom {:response nil :error nil})
        loading?       (r/atom false)
        has-queried    (r/atom false)
        endpoint-id    @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :id])
        resource-type  @(re-frame/subscribe [:gen-fhi.db.subs.route/get-parameter :resource-type])
        p              (path/->path resource-type endpoint-id)
        query (fn [smart-config]
                (when (and (not @has-queried) smart-config)
                  (reset! has-queried true)
                  (reset! loading? true)
                  (-> (oops/ocall
                       fhir-client
                       "oauth2.init"
                       (clj->js
                        (cond-> {:clientId    (get smart-config :clientId)
                                 :scope       (get smart-config :scope)}
                          (some? (get smart-config :iss)) (assoc :iss (get smart-config :iss))))
                       p)
                      (.then (fn [_client]
                               (let [key (.parse js/JSON (.getItem js/sessionStorage p))]
                                 ;; (.log js/console "key:" key)
                                 ;; (.log js/console "state:" (.getItem js/sessionStorage key))
                                 (reset! response {:success true})
                                 (.postMessage
                                  js/opener
                                  #js{:type "auth-completed"
                                      :path   (str p)
                                      :key    (.getItem js/sessionStorage p)
                                      :config (clj->js smart-config)
                                      :auth   (.parse js/JSON (.getItem js/sessionStorage key))})
                                 (.setTimeout
                                  js/window
                                  (fn []
                                    (.close js/window))
                                  1000))))
                      (.catch (fn [error]
                                (.error js/console error)
                                (reset! response {:error error})))
                      (.finally (fn [_e]
                                  (reset! loading? false))))))]
                                ;; (.close js/window)))))))]
    (fn []
      (let [endpoint     @(re-frame/subscribe [:gen-fhi.db.subs.value/get-value p])
            smart-config (resource->smart-config endpoint true)]
        (query smart-config)
        [:div {:class "w-full h-full flex items-center justify-center"}
         (if @loading?
           [:div
            [:> Spin {:tip "Smart Loading"}]]
           (cond
             (get @response :error)
             [:div {:class "flex flex-col items-center justify-center text-red-600 text-lg"}
              [:div {:class "text-2xl"}
               [:> ExclamationCircleOutlined]]
              [:div {:class "text-center"}
               [:div {:class "font-semibold"}
                "Auth Failed"]
               [:div
                (str (get @response :error))]]]
             (get @response :success)
             [:div {:class "flex flex-col items-center justify-center text-green-600 text-lg"}
              [:div {:class "text-2xl"}
               [:> CheckCircleOutlined]]
              [:div {:class "font-semibold"}
               "Auth Succeeded"]]))]))))

