(ns gen-fhi.workspace.views.workspace
  (:require [reagent.core :as r]

            [gen-fhi.workspace.components.sidebar :refer [sidebar]]
            [gen-fhi.workspace.components.query :refer [query]]
            [gen-fhi.workspace.components.resource-table :refer [resource-table]]))

(def sidebar-items
  [{:display "All"                 :value {:_type ["ClientApplication"
                                                   "ValueSet"
                                                   "CodeSystem"
                                                   "Endpoint"
                                                   "Questionnaire"]}}
   {:display "Client Applications" :value {:_type ["ClientApplication"]}}
   {:display "Questionnaires"      :value {:_type ["Questionnaire"]}}
   {:display "Value Sets"          :value {:_type ["ValueSet"]}}
   {:display "Code Systems"        :value {:_type ["CodeSystem"]}}
   {:display "Endpoints"           :value {:_type ["Endpoint"]}}])

(defn view []
  (let [sidebar-selected (r/atom (first sidebar-items))]
    (fn []
      [:div {:className "container mx-auto pt-8 h-full"}
       [:div {:className "h-full mx-auto"}
        [:div {:class "mb-4"}
         [:h3 {:className "text-base font-semibold text-slate-700 mb-2"}
          "Recent"]]
        [:div {:class "flex"}
         [:div {:class "mr-8 w-[175px]"}
          [sidebar {:value  (:value @sidebar-selected)
                    :on-click (fn [selected-item]
                                (reset! sidebar-selected selected-item))
                    :items sidebar-items}]]

         [:div {:class "w-full"}
          [query {:trigger-on-mount? true
                  :parameters (:value @sidebar-selected)}
           (fn [{:keys [result re-query is-loading]}]
             [resource-table {:types (get-in @sidebar-selected [:value :_type])
                              :on-delete re-query
                              :result result
                              :is-loading is-loading}])]]]]])))
