(ns gen-fhi.workspace.ws
  (:require
   [cljs.core.async :as a]
   [cljs.core.async.interop :refer-macros [<p!]]
   [gen-fhi.workspace.constants.core :refer [?csrf-token]]
   [taoensso.sente :as sente]))

(defn create-workspace-channel
  "Creates ws returning object with ch-recv send-fn state keys"

  [workspace get-access-token]
  (a/go
    (let [access-token (<p! (get-access-token))
          {:keys [chsk ch-recv send-fn state]}
          (sente/make-channel-socket-client!
           (str "/chsk/" workspace)
           ?csrf-token
           {:type   :auto
            :params {:jwt access-token}})]

      {:chsk    chsk
       :ch-recv ch-recv
       :send-fn send-fn
       :state   state})))
