(ns user
  (:use (clojure.repl))
  (:require [integrant.core :as ig]
            [clojure.tools.build.api :as b]
            [clojure.core.async :refer [thread]]
            [config.core :refer [env]]
            ;;[gen-fhi.fhir-ops-executor.aws-lambda.core :as lambda]
            [clojure.tools.namespace.repl :as repl]
            [clojure.java.io :as io]
            [integrant.repl :refer [suspend clear go halt prep init]]))

;; (def lambda-client
;;   (lambda/->lambda-client
;;    {:region            (:server/aws-region env)
;;     :access-key-id     (:server/aws-lambda-access-key-id env)
;;     :secret-access-key (:server/aws-lambda-secret-access-key env)}))

;; (def tagging-client
;;   (lambda/->tagging-client
;;    {:region            (:server/aws-region env)
;;     :access-key-id     (:server/aws-lambda-access-key-id env)
;;     :secret-access-key (:server/aws-lambda-secret-access-key env)}))

;; (def lambda-executor
;;   (lambda/->op-executor
;;    {:role              (:server/aws-lambda-role env)
;;     :region            (:server/aws-region env)
;;     :access-key-id     (:server/aws-lambda-access-key-id env)
;;     :secret-access-key (:server/aws-lambda-secret-access-key env)}))

(def config (ig/read-string (slurp (io/resource "gen_fhi/services.edn"))))
(ig/load-namespaces config)

(repl/set-refresh-dirs "clj" "cljc")

(integrant.repl/set-prep! (constantly config))

;;(def tailwindprocess (atom nil))
(defn tailwind-watch [_config]
  (println "[CSS] Recompiling tailwind css files")
  (b/process {:command-args ["npx" "tailwindcss"
                             "-i"  "./css/custom.css"
                             "-o"  "./target/public/main.dev.css"]}))

(defn start-server []
  (go [:workspace-backend/server
       :workspace-migrate/migrate
       :workspace-backend/channel-server
       :app-backend/server]))

(defn reset []
  (halt)
  (def config (ig/read-string (slurp (io/resource "gen_fhi/services.edn"))))
  (ig/load-namespaces config)
  (repl/refresh :after 'user/start-server))
