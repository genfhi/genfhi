import type { FC } from "react";
import type {
  ValueSet,
  Questionnaire,
  QuestionnaireResponse,
} from "@iguhealth/fhir-types";

export declare var QuestionnaireReact: FC<{
  id: string;
  questionnaire: Questionnaire;
  questionnaireResponse?: QuestionnaireResponse;
  onChange?: (qr: QuestionnaireResponse) => void;
  expand?: (valueSetUri: string) => Promise<ValueSet>;
}>;
