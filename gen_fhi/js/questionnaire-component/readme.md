# Questionnaire Component
## Installation
Install via `npm i @iguhealth/questionnaire-component`
## Usage
```js
import "@iguhealth/questionnaire-component/css/antd.min.css";
import "@iguhealth/questionnaire-component/css/custom.css";
import { QuestionnaireReact } from "@iguhealth/questionnaire-component";

<QuestionnaireReact
    id="unique-identifier"
    expand={async (valueset) => {
            // Use whatever method to derive expanded valueset
            // whether fetch or inline here.
          }}
    onChange={(qr) => console.log(qr)}
    questionnaire={prepare} />
```
## Properties
| Property      | Description                                                                                                                                          | required? |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| id            | Unique identifier for the Questionnaire (note this is distinct from Questionnaire.id as you could have multiple instances of the same Questionnaire. | yes       |
| expand        | Asynchronous function used during valueset expansion (choice, open-choice items).                                                                    | depends   |
| onChange      | On change handler used as a callback that returns QuestionnaireResponse resource when any changes occur.                                             | no        |
| questionnaire | Questionnaire to be rendered.                                                                                                                        | yes       |
|               |                                                                                                                                                      |           |
