(ns gen-fhi.migrate
  (:require [migratus.core :as migratus]
            [taoensso.timbre :as timbre]
            [integrant.core :as ig]
            [next.jdbc :as jdbc]
            [gen-fhi.workspace.environment :as env]))

(defn- ->migration-config []
  (let [server-env (env/->server-env)]
    {:store :database
     :db    {:datasource (jdbc/get-datasource
                          {:dbtype   "postgres"
                           :host     (:server/workspace-db-host server-env)
                           :port     (:server/workspace-db-port server-env)
                           :dbname   (:server/workspace-db-name server-env)
                           :user     (:server/workspace-db-username server-env)
                           :password (:server/workspace-db-password server-env)})}}))

(defmethod ig/init-key :gen-fhi.migrate/init [_ {:keys []}]
  (timbre/info "Initialize db")
  (migratus/init (->migration-config)))

(defmethod ig/init-key :gen-fhi.migrate/migrate [_ {:keys []}]
  (timbre/info "Migrating db")
  (println (migratus/migrate (->migration-config))))

(defmethod ig/init-key :gen-fhi.migrate/rollback [_ {:keys []}]
  (timbre/info "Rollingback db")
  (migratus/rollback (->migration-config)))
