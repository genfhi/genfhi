# GENFHI

Low-code tooling for FHIR.

## Environment Variables

Can also load as edn file config.edn. For example auth0 environment variables translate as:

```edn
{:client/api-url         "GENFHI FHIR server endpoint string"
 :client/auth0-domain    "auth0 domain string"
 :client/audience        "auth0 audience string"
 :client/auth0-client-id "auth0 clientid-string"}
```

Note that not all environment variables are required. It depends on features+service choice.

### Configuration

```bash
dev=true                            # Sets server to dev environment
```

### Redis

```bash
server__redis_host=string
server__redis_port=number
```

### Auth0

```bash
client__api_url=string                 # Server FHIR API EX: http://localhost:8080/api/v1/fhir
client__auth0_domain=string            # Auth0 Domain passed to client for login.
client__audience=string                # Auth0 audience (need custom api endpoint for jwts).
client__auth0_client_id=string         # Auth0 application client id.
client__auth0_logout_returnto=string   # Auth0 logout return to url.
server__auth0_management_client_id     # Used for pulling user information
server__auth0_management_client_secret # Used for pulling user information
```

### Database

Note only database type supported at this time is postgresql.

```bash
server__workspace_db_name=string
server__workspace_db_host=string
server__workspace_db_port=string
server__workspace_db_username=string
server__workspace_db_password=string
```

### AWS

```bash
server__aws_lambda_access_key_id=string
server__aws_lambda_secret_access_key=string
server__aws_lambda_role=string
server__aws_region=string

server__aws_s3_access_key_id=string
server__aws_s3_secret_access_key=string
server__aws_s3_bucket_name=string
```

### Encryption
```
server__encryption_type=aws-kms,local
```
#### AWS KMS
```
server__aws_kms_access_key_id=string
server__aws_kms_access_key_secret=string
```
#### Local
```
server__encryption_local_secret_key
```


### Sentry
```bash
client__sentry_key=string # sentry dsn key
```

### Analytics
```bash
client__heap_id=string # heap analytics app id
```


### Flags
```bash
client__enable_operation_outcomes=true Enables operationoutcome editor.
```

### Running

#### Server 
```bash
clj -A:dev
```

#### Frontend
```bash
clj -A:dev -m figwheel.main -b workspace-frontend-dev
```
