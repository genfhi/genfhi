ALTER TABLE resources
DROP CONSTRAINT prev_version_id_constraint;
--;;
ALTER TABLE resources
DROP COLUMN prev_version_id;
