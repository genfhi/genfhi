ALTER TABLE resources
ADD COLUMN prev_version_id BIGINT;
--;;
ALTER TABLE resources
ADD CONSTRAINT prev_version_id_constraint FOREIGN KEY (prev_version_id) REFERENCES resources(version_id) MATCH FULL;
