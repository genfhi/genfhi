CREATE OR REPLACE FUNCTION generate_fhir_instant_string (tstamp TIMESTAMPTZ) RETURNS TEXT AS $$
     declare utc_time TIMESTAMPTZ;
     BEGIN
          utc_time := tstamp AT TIME ZONE 'UTC';
	  RETURN to_char(utc_time, 'YYYY-MM-DD') ||
	         'T' ||
         	 to_char(utc_time, 'HH:MI:SS.MS+00:00');
	  
     END;
$$  LANGUAGE plpgsql;
