# !/bin/bash

clj -m gen-fhi.workspace.core workspace > server.log 2>&1 &

# keep waiting until the server is started 
while ! grep -q "workspace server initiated" server.log
do
  sleep .1
done
echo -e "server has started\n"
exit 0
