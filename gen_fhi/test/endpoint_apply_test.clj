(ns endpoint-apply-test
  (:require [clojure.test :as t]
            [clojure.java.io :as io]
            [clojure.data.json :as json]

            [gen-fhi.workspace.endpoint-environment :as ee]))

(defn json-read [uri]
  (clojure.data.json/read-str
   (slurp (io/resource uri))
   :key-fn keyword))

(def endpoint-muli-env (json-read "json/endpoint/multi-env.json"))

(t/deftest unfound-environment-test
  (t/is
   (=
    {:connectionType {:code "hl7-fhir-rest"},
     :address "https://test.com",
     :meta {:versionId "15338", :lastUpdated "2023-03-09T17:32:35.402+00:00"},
     :name "TESTING",
     :resourceType "Endpoint",
     :payloadType [{:coding [{:code "urn:ihe:pcc:cm:2008"}]}],
     :header ["Content-Type:application/json"],
     :status "active",
     :id "c9201755-3cf5-4f5c-81bf-77afbfec835c"}
    (ee/apply-endpoint-environment
     {:reference "Organization/123"}
     endpoint-muli-env))))

(t/deftest environment-test-oauth2-override
  (t/is
   (=
    {:connectionType {:code "hl7-fhir-rest"},
     :address "https://environment3.com",
     :meta {:versionId "15338", :lastUpdated "2023-03-09T17:32:35.402+00:00"},
     :managingOrganization {:reference "Organization/65b3c9cd-b97b-452a-aa31-aa7c92c074e0"},
     :name "TESTING",
     :resourceType "Endpoint",
     :payloadType [{:coding [{:code "urn:ihe:pcc:cm:2008"}]}],
     :header ["Content-Type:application/fhir+json" "Age:123"],
     :extension [{:url "https://genfhi.com/Extension/auth-type", :valueCode "oauth2-confidential"}
                 {:url "https://genfhi.com/Extension/auth-oauth2-token-url", :valueString "token.com"}
                 {:url "https://genfhi.com/Extension/auth-oauth2-resource", :valueString "resource"}
                 {:url "https://genfhi.com/Extension/auth-oauth2-client-id", :valueString "client-id"}],
     :status "active",
     :id "c9201755-3cf5-4f5c-81bf-77afbfec835c"}
    (ee/apply-endpoint-environment
     {:reference "Organization/65b3c9cd-b97b-452a-aa31-aa7c92c074e0"}
     endpoint-muli-env))))
