(ns parameter-parsing-test
  (:require [clojure.test :as t]
            [clojure.java.io :as io]
            [clojure.data.json :as json]

            [gen-fhi.operations.parameter-parsing.core :as p]))

(defn json-read [uri]
  (clojure.data.json/read-str
   (slurp (io/resource uri))
   :key-fn keyword))

(def op-valueset-expand (json-read "json/op/valueset-expand.json"))
(def op-custom-part (json-read "json/op/custom-part.json"))

(t/deftest validation-test
  (t/is
   (=
    {:resourceType "Parameters"
     :parameter
     '({:name "system-version", :valueCanonical \5})}
    (p/map->parameters op-valueset-expand {:system-version "5"} "in"))))

(t/deftest validation-part
  (t/is
   (=
    {:resourceType "Parameters"
     :parameter
     [{:name "nested"
       :part
       [{:name "nestedInteger" :valueInteger 5}
        {:name "nestedCode"    :valueCode "code"}
        {:name "nestedString"  :valueString "string"}]}]}
    (p/map->parameters
     op-custom-part
     {:nested
      {:nestedInteger 5
       :nestedCode "code"
       :nestedString "string"}}
     "in"))))

(t/deftest round-trip
  (t/is
   (=
    {:nested
     {:nestedInteger 5
      :nestedCode "code"
      :nestedString "string"}}
    (p/parameters->map
     (p/map->parameters
      op-custom-part
      {:nested
       {:nestedInteger 5
        :nestedCode "code"
        :nestedString "string"}}
      "in")))))
