(ns parameter-validation-test
  (:require [clojure.test :as t]
            [clojure.java.io :as io]
            [clojure.data.json :as json]

            [gen-fhi.fhir-ops-executor.validation.core :as v]))

;; (def operation
;;   (clojure.data.json/read-str
;;    (slurp (io/resource "json/op/valueset-expand.json"))
;;    :key-fn keyword))

;; (t/deftest validation-test
;;   (t/is
;;    (thrown?
;;     Exception
;;     (v/validate-parameters operation {:z "5"} "in")))
;;   (t/is
;;    (thrown?
;;     Exception
;;     (v/validate-parameters operation {:system-version 5} "in")))

;;   (t/is
;;    (v/validate-parameters operation {:system-version "5"} "in"))

;;   (t/is
;;    (v/validate-parameters operation {} "in")))


