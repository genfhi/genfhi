(ns shortened-uuid-conversion-test
  (:import [java.util UUID])
  (:require  [clojure.test :as t]
             [gen-fhi.shortened-uuid.core :as uuidshort]))

(t/deftest confirm_conversions
  (dorun
   (for [_i (range 0 1000)]
     (let [random-uuid    (UUID/randomUUID)
           suuid          (uuidshort/uuid->suuid random-uuid)
           roundtrip-uuid (uuidshort/suuid->uuid suuid)]
       (when (< 25 (.length suuid))
         (throw (ex-info (str "'" suuid "' greater than 30 length")
                         {:suuid suuid})))

       (when (not= roundtrip-uuid random-uuid)
         (throw (ex-info (str  "'" random-uuid  "'!='" roundtrip-uuid "'")
                         {:uuid random-uuid
                          :roundtrip-uuid roundtrip-uuid})))))))
