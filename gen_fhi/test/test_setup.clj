(ns test-setup
  (:require [gen-fhi.interceptors.core :as it]
            [gen-fhi.fhir-client.utils
             :refer [fhir-req->http-req http-res->fhir-res]]
            [taoensso.timbre :as timbre]
            [org.httpkit.client :as http]
            [clojure.data.json :as json])
  (:import (com.auth0.json.auth TokenHolder)
           (com.auth0.client.auth AuthAPI)))

(defn ->auth [client-id client-secret]
  (AuthAPI. "genfhi.us.auth0.com" client-id client-secret))

(defn ->access-token [auth username password]
  (let [login-request (doto (.login auth username password)
                        (.setScope "openid"))
        token-holder (cast TokenHolder (.execute login-request))]
    (.getIdToken token-holder)))

(defn- ->interceptor [api-url access-token]
  (it/->interceptor
   [{:error (fn [v] (timbre/error (:interceptors/exception v)))
     :enter
     (it/alt-value
      (fn [fhir-req]
        [fhir-req (fhir-req->http-req api-url fhir-req)]))}
    {:enter (it/alt-value
             (fn [[fhir-req http-req]]
               (let [http-req (assoc http-req
                                     :headers {"Authorization" (str "Bearer " access-token)
                                               "Content-Type" "application/json"})
                     http-res @(http/request http-req)
                     http-res (update http-res :body (fn [json-str]
                                                       (json/read-str json-str :key-fn keyword)))]

                 (http-res->fhir-res fhir-req http-res))))}]))

(defn ->fhir-interceptor [{:keys [api-url client-id client-secret username password]}]
  (let [auth (->auth client-id client-secret)
        access-token (->access-token auth username password)]
    (->interceptor api-url access-token)))
