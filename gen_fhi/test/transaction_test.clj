(ns transaction-test
  (:require [config.core :refer [load-env]]
            [clojure.data.json :as json]
            [clojure.test :as t]
            [test-setup :as ts]))

(def env (load-env))
(def client (atom nil))

(defn setup-client-fixture [f]
  (let [{:keys [:test/client-id :test/client-secret :test/username :test/password]} env]
    (assert (some? client-id)     "client-id not present")
    (assert (some? client-secret) "client-secret not present")
    (assert (some? username)      "username not present")
    (assert (some? password)      "password not present")
    (reset! client (ts/->fhir-interceptor
                    {:api-url       "http://localhost:8080/api/v1/fa5ac337-361d-4e76-aab5-792f4f43f34b/fhir/"
                     :client-id     client-id
                     :client-secret client-secret
                     :username      username
                     :password      password})))
  (f))

(t/use-fixtures :once setup-client-fixture)

;; [TODO] NEED to deal with antiforgery token
;; (t/deftest transaction_test
;;   (let [client @client
;;         response (client {:fhir/request-type :fhir/transaction
;;                           :fhir/level        :fhir/system
;;                           :fhir/data         (json/read-str
;;                                               (slurp "test/json/transaction/transaction-example.json")
;;                                               :key-fn keyword)})]
;;     (t/is (= :fhir/transaction
;;              (:fhir/response response)))))
