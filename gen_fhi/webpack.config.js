// in webpack.config.js (or similar)
const path = require("path");
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const { GitRevisionPlugin } = require("git-revision-webpack-plugin");

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: "pre",
        use: ["source-map-loader"],
      },
    ],
  },
  output: {
    path: path.join(__dirname, "target/public/cljs-out/js/"),
    filename: "[name].[git-revision-hash].js",
  },
  plugins: [/*new BundleAnalyzerPlugin(),*/ new GitRevisionPlugin()],
};
