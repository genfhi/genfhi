# GENFHI

## Organization

### clj

Clojure libraries

### cljc

Clojure/Clojurescript Libraries

### cljs

Clojurescript libraries

### genfhi

GENFHI low-code project. Modules organized as above with clj/cljc/cljs modules organized under corresponding directories.
