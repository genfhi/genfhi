FROM clojure:tools-deps as clojure
RUN apt-get update
RUN apt-get --assume-yes install curl
ENV NODE_VERSION=16.7.0
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
SHELL ["/bin/bash", "-c"]

WORKDIR /gen_fhi/gen_fhi
COPY . /gen_fhi

ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN ["clj", "-T:build", "workspace-uber"]

FROM openjdk:22-slim-bullseye
EXPOSE 8080
COPY --from=clojure /gen_fhi/gen_fhi/target/workspace-standalone.jar ./workspace-standalone.jar
CMD java -jar workspace-standalone.jar workspace